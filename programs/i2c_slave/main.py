from programs.i2c_slave.i2c_slave import i2c_slave

def main():
    global robot  
    s_i2c = i2c_slave(0,sda=16,scl=17,slaveAddress=0x41)
    counter =1
    try:
        while True:
            if s_i2c.any():
                print(s_i2c.get())
            if s_i2c.anyRead():
                counter = counter + 1
                s_i2c.put(counter & 0xff)
        
    except KeyboardInterrupt:
        pass
    
main()