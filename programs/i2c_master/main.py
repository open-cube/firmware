from machine import I2C, Pin

def main():
    global robot
    i2c = I2C(id=0,scl=Pin(17), sda=Pin(16), freq=100000)
    
    while True:
        #i2c.writeto(0x41, b'123')   
        print(int.from_bytes(i2c.readfrom(0x41, 1), 'big'))
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            break
main()