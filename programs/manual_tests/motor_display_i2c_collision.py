import utime

import motors_ll
from lib.cube.sh1106 import SH1106_I2C
from lib.robot_consts import Port


def main():
    """
    Verify that the code handles concurrent accesses
    to the internal I2C bus gracefully.

    Success: The brick will try to regulate the motor around
             the zero position. The regulator might oscillate as
             its effective sampling rate is reduced.
    Fail: The brick crashes or shuts down.
    """
    global robot
    display: SH1106_I2C = robot.display

    display.fill(0)
    display.text("I2C CRASHTEST", 0, 0, 1)
    display.text("starting in 1 sec", 0, 8, 1)
    display.show()
    utime.sleep_ms(1000)

    robot.init_motor(Port.M2)
    motor: motors_ll.Motor = robot.motors[Port.M2]
    motor.init_regulator()
    motor.set_regulator_position(0)

    iteration = 0
    while True:
        display.fill(0)
        display.text(str(iteration), 0, 0, 1)
        display.text(f"pos: {motor.get_position()} deg", 0, 8, 1)
        display.text(f"spd: {motor.get_speed()} d/s", 0, 16, 1)
        display.show()
        iteration += 1


main()
