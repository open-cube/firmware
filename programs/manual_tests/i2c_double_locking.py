import brick_ll


def main():
    """
    Verify that nested I2C locking is reported properly.
    """
    brick_ll.lock_i2c()
    try:
        brick_ll.lock_i2c()
        print("fail!")
    except brick_ll.InternalI2CBusyError:
        print("pass!")
    finally:
        # do not break the menu
        brick_ll.unlock_i2c()


main()
