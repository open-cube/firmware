"""
Perform a hardware recalibration of the EV3 Color sensor plugged into the port S1.

BEWARE. Do not run this program this unless you know what you are doing.

The calibration is intended to give the sensor an idea what a
"100% white" is. Once the sensor receives the calibration
command from this program, it will perform one RGB measurement.
This measurement will then be saved into the sensor's EEPROM
(and any previous factory calibration will be overwritten!).

The hardware calibration affects only the following sensor modes:
 - COL-REFLECT (handled by the sensor.reflection() method)
 - COL-COLOR (handled by the sensor.color() method)
These are the sensor modes that perform scaling within the sensor itself.

After the calibration, the COL-REFLECT mode should return 100 %
if you keep the sensor in the same position as during calibration.

For a repeatable calibration process, it should be possible to
build a simple calibration jig. It seems that the factory LEGO
calibration makes the sensor return 100 % in COL-REFLECT when
the sensor is pointed at a 3x3 surface made of white LEGO 32523
parts from a distance of ~one Technic brick.
"""
import time

from lib.cube import SH1106_I2C
from lib.ev3 import ColorSensor
from lib.ev3.color import COLOR_SENSOR_ID
from lib.robot_consts import Port, Sensor, Button


def main():
    global robot
    display: SH1106_I2C = robot.display

    robot.init_sensor(sensor_type=Sensor.EV3_COLOR, port=Port.S1)
    cs: ColorSensor = robot.sensors.light[Port.S1]

    debounce = False
    this_calibrated = False
    while True:
        status_text, unplugged, ready = check_readiness(cs, this_calibrated)
        if unplugged:
            this_calibrated = False

        display.fill(0)
        display.text("EV3 CS cal tool", 0, 0, 1)
        display.hline(0, 8, 128, 1)
        display.text(status_text, 0, 10, 1)

        if ready:
            try:
                reading = cs.reflection()
            except:
                reading = -1
            pressed = robot.buttons.pressed()

            if not pressed[Button.OK] or debounce:
                display.text(f"R: {reading} %", 0, 18, 1)
                display.text("Press OK to cal", 0, 26, 1)
                display.text("The scanned sur-", 0, 40, 1)
                display.text("face shall later", 0, 48, 1)
                display.text("return R: 100 %", 0, 56, 1)
                display.show()
                if not pressed[Button.OK]:
                    debounce = False
            else:
                debounce = True
                display.text("Calibrating...", 0, 26, 1)
                display.show()
                recalibrate(cs)
                this_calibrated = True
        else:
            if unplugged:
                display.text("- use port S1", 0, 18, 1)
            display.show()

        time.sleep(0.1)


def check_readiness(cs: ColorSensor, this_calibrated):
    is_connected = cs.is_connected()
    is_ready = cs.is_ready()  # implies is_connected
    is_cs = cs.sensor_id() == COLOR_SENSOR_ID  # implies is_ready

    if is_cs:
        if this_calibrated:
            status = "S: done"
        else:
            status = "S: ready"
    elif is_ready:
        status = "S: incorr. type"
    elif is_connected:
        status = "S: handshaking"
    else:
        status = "S: unplugged"

    return status, not is_connected, is_cs


def recalibrate(sensor: ColorSensor):
    # this will prepare the sensor; it will respond with one DATA message with all zeros
    sensor.read_raw_mode(ColorSensor.MODE_COL_CAL)

    # this will allow the calibration to start
    sensor.write_command(b'LEGO-FAC-CAL-1')

    # loop until the sensor sends a new DATA message - the calibration is then complete
    # the new message should contain nonzero data
    items = sensor.read_raw_mode(ColorSensor.MODE_COL_CAL)
    while items[0] == 0:
        items = sensor.read_raw_mode(ColorSensor.MODE_COL_CAL)

    # restart the sensor (after the calibration it will be stuck in the COL-CAL mode)
    sensor.start_reset()

    # wait for reconnect
    sensor.reflection()

main()
