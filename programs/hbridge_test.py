import utime

from lib.ev3 import Motor
from lib.hw_defs.ports import *
from lib.cube.sh1106 import SH1106_I2C
from lib.robot_consts import  Button


class Hbridge:
    def __init__(self, port: int):
        self.port = port
        self.motor = Motor(port)

        self.is_enabled = False
        self.power_pct = 0

    def set_enabled(self, is_on: bool):
        self.is_enabled = is_on
        self._push_to_c()

    def set_power_pct(self, pct: int):
        if pct > 100:
            pct = 100
        if pct < -100:
            pct = -100
        self.power_pct = pct
        self._push_to_c()

    def _push_to_c(self):
        if self.is_enabled:
            self.motor.dc(self.power_pct)
        else:
            self.motor.brake()

    def get_position(self) -> int:
        return self.motor.angle()

    def get_speed(self) -> int:
        return self.motor.speed()


def main():
    global robot
    display: SH1106_I2C = robot.display
    ports = [
        Hbridge(i)
        for i in range(len(MOTOR_PORT_PINS))
    ]

    current_port_idx = 0
    current_port = ports[current_port_idx]
    last_btns = robot.buttons.pressed()
    while True:
        new_btns = robot.buttons.pressed()
        just_pressed = [not was and is_now for was, is_now in zip(last_btns, new_btns)]
        last_btns = new_btns

        if just_pressed[Button.OK]:
            current_port.set_enabled(not current_port.is_enabled)
        elif just_pressed[Button.UP]:
            current_port.set_power_pct(current_port.power_pct + 10)
        elif just_pressed[Button.DOWN]:
            current_port.set_power_pct(current_port.power_pct - 10)
        elif just_pressed[Button.LEFT]:
            current_port_idx = (current_port_idx - 1 + 4) % 4
            current_port = ports[current_port_idx]
        elif just_pressed[Button.RIGHT]:
            current_port_idx = (current_port_idx + 1) % 4
            current_port = ports[current_port_idx]

        vbatt = robot.battery.voltage()
        vbatt_str = f"{vbatt:3.2f}V"

        display.fill(0)
        display.text(f"Motor test", 0, 0)
        display.text(vbatt_str, 128 - 8 * len(vbatt_str), 0)
        display.hline(0, 8, 128, 1)

        TOP_SPACER = 10
        display.text(f"Port {current_port_idx + 1}:", 0, 0 + TOP_SPACER)
        display.text(f" hbridge {'ON' if current_port.is_enabled else 'OFF'}", 0, 8 + TOP_SPACER)
        display.text(f" pwm: {current_port.power_pct:+5d} %", 0, 16 + TOP_SPACER)
        display.text(f" pos: {current_port.get_position():+5d} d", 0, 24 + TOP_SPACER)
        display.text(f" spd: {current_port.get_speed():+5d} d/s", 0, 32 + TOP_SPACER)
        display.show()
        utime.sleep_ms(100)


main()
