'''
Example program for the NXT light and touch sensors
Light sensors are on ports S1 and S2, both are in continuous mode S2 is also in switching mode
Touch sensors are on ports S3 and S4
'''
from time import sleep

from lib.robot_consts import Button, Port, Sensor


# Continuous mode parameters
CONT_PERIOD = 1100
CONT_SWITCH_WAIT_TIME = 10000

def main():
    global robot

    # Initialize touch sensors and light sensors in continuous mode 
    # with port S2 being in switching mode
    robot.init_sensor(Sensor.NXT_LIGHT, Port.S1)
    robot.init_sensor(Sensor.NXT_LIGHT, Port.S2)
    robot.init_sensor(Sensor.NXT_TOUCH, Port.S3)
    robot.init_sensor(Sensor.NXT_TOUCH, Port.S4)

    robot.sensors.light[Port.S2].set_switching()
    robot.sensors.light[Port.S2].set_continuous(CONT_PERIOD, CONT_SWITCH_WAIT_TIME)

    counter = 0
    while True:
        counter += 1 

        # Get measured light intensity and touch states
        value_on1 = robot.sensors.light[Port.S2].get_value(1)
        value_off1 = robot.sensors.light[Port.S2].get_value(0)
        value_on2 = robot.sensors.light[Port.S2].get_value(1)
        value_off2 = robot.sensors.light[Port.S2].get_value(0)
        value_on3 = robot.sensors.touch[Port.S3].pressed()
        value_on4 = robot.sensors.touch[Port.S4].pressed()
        # Print measured light intensity and touch sensor states
        print("Light 1 on:",value_on1,"1 off:",value_off1,
                    "2 on:",value_on2,"2 off:",value_off2,
              "Touch 3:",   value_on3, 
                    "4:",   value_on4)
        
        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            break
        sleep(0.1)

    robot.sensors.light[Port.S2].stop_continuous()
    robot.deinit_all()

main()