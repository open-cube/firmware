'''
Example program for the NXT light sensors on all ports
The program measures the light intensity with the LED on and off
'''
import sys
from time import sleep

from lib.robot_consts import Button, Port, Sensor


def main():
    global robot

    robot.init_sensor(Sensor.NXT_LIGHT, Port.S1)
    robot.init_sensor(Sensor.NXT_LIGHT, Port.S2)
    robot.init_sensor(Sensor.NXT_LIGHT, Port.S3)
    robot.init_sensor(Sensor.NXT_LIGHT, Port.S4)

    counter = 0
    while True:
        counter += 1 

        # Change light sensor LED state
        if counter % 2:
            state = 'on'
            robot.sensors.light[Port.S1].on()
            robot.sensors.light[Port.S2].on()
            robot.sensors.light[Port.S3].on()
            robot.sensors.light[Port.S4].on()
        else:
            state = 'off'
            robot.sensors.light[Port.S1].off()
            robot.sensors.light[Port.S2].off()
            robot.sensors.light[Port.S3].off()
            robot.sensors.light[Port.S4].off()
        sleep(1)

        # Measure ligh intensity and print it
        value1 = robot.sensors.light[Port.S1].get_value(0)
        value2 = robot.sensors.light[Port.S2].get_value(1)
        value3 = robot.sensors.light[Port.S3].get_value(1)
        value4 = robot.sensors.light[Port.S4].get_value(0)
        print("LED state", state, "Light intensity:",value1,value2,value3,value4)
        
        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            robot.deinit_all()
            break

main()