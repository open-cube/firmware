'''
Example program for NXT or EV3 motors on all ports with regulators 
with optional ESP BT communication in binary mode
Motors 1 and 
'''
from time import sleep
import struct
import utime

from lib.robot_consts import Button, Port


# ESP settings
BINARY_HEADER = (203, 98, 116, 140)
NUM_RECEIVED_BYTES = 36

def main():
    global robot
    # Set initial regulator target position and speed
    ref_position = 500
    ref_speed = 200
     # Initialize motors with motors with regulators
    robot.init_motor(Port.M1)
    robot.motors[Port.M1].init_regulator()
    robot.motors[Port.M1].set_regulator_position(ref_position)
    robot.init_motor(Port.M2)
    robot.motors[Port.M2].init_regulator()
    robot.motors[Port.M2].set_regulator_position(ref_position)
    robot.init_motor(Port.M3)
    robot.motors[Port.M3].init_regulator()
    robot.motors[Port.M3].set_regulator_speed(ref_speed)
    robot.init_motor(Port.M4)
    robot.motors[Port.M4].init_regulator()
    robot.motors[Port.M4].set_regulator_speed(ref_speed)
    robot.init_esp_uart()
    robot.esp_uart.set_binary(BINARY_HEADER, NUM_RECEIVED_BYTES)
    start_time = utime.ticks_us()
    counter = 0
    while(True):
        counter+=1
        # Get motors positions, speeds, regulator errors and powers and print them
        pos1 = robot.motors[Port.M1].position()
        pos2 = robot.motors[Port.M2].position()
        pos3 = robot.motors[Port.M3].position()
        pos4 = robot.motors[Port.M4].position()
        speed1 = robot.motors[Port.M1].speed()
        speed2 = robot.motors[Port.M2].speed()
        speed3 = robot.motors[Port.M3].speed()
        speed4 = robot.motors[Port.M4].speed()
        error1 = robot.motors[Port.M1].regulator_error()
        error2 = robot.motors[Port.M2].regulator_error()
        error3 = robot.motors[Port.M3].regulator_error()
        error4 = robot.motors[Port.M4].regulator_error()
        pwr1 = robot.motors[Port.M1].regulator_power()
        pwr2 = robot.motors[Port.M2].regulator_power()
        pwr3 = robot.motors[Port.M3].regulator_power()
        pwr4 = robot.motors[Port.M4].regulator_power()
        if counter % 10 == 0:
            print("Enc pos:", pos1, pos2, pos3, pos4, 
                    "Speed:", speed1, speed2, speed3, speed4)
            print("Error:", error1, error2, error3, error4, 
                  "Power:", pwr1, pwr2, pwr3, pwr4)

        # Read regulator parameters from ESP 
        line = robot.esp_uart.read()
        # Set new regulator parameters and target position and speed if received from ESP
        if line:
            read_values = struct.unpack('<fffffffff', line)
            (ref_position,ref_speed, KpP, KiP, KdP, KpS, KiS, KdS, KfdS) = read_values
            robot.motors[Port.M1].regulator_pos_set_consts(KpP, KiP, KpS)
            robot.motors[Port.M2].regulator_pos_set_consts(KpP, KiP, KpS)
            robot.motors[Port.M3].regulator_speed_set_consts(KpS, KiS, KdS)
            robot.motors[Port.M4].regulator_speed_set_consts(KpS, KiS, KdS)
            robot.motors[Port.M1].set_regulator_position(ref_position)
            robot.motors[Port.M2].set_regulator_position(ref_position)
            robot.motors[Port.M3].set_regulator_speed(ref_speed)
            robot.motors[Port.M4].set_regulator_speed(ref_speed)
        # Send current motors states to ESP
        robot.esp_uart.write(struct.pack('<ffffffff',
                                         pos1,speed1,pos2,speed2,
                                         pos3,speed3,pos4,speed4))

        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            break
        sleep(0.1)

    robot.deinit_all()

main()
  
