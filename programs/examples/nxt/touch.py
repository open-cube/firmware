'''
Example program for the NXT touch sensors on all ports
The program measures touch sensors states
'''
from time import sleep

from lib.robot_consts import Button, Port, Sensor


def main():
    global robot

    # Initialize sound sensors on all ports
    robot.init_sensor(Sensor.NXT_TOUCH, Port.S1)
    robot.init_sensor(Sensor.NXT_TOUCH, Port.S2)
    robot.init_sensor(Sensor.NXT_TOUCH, Port.S3)
    robot.init_sensor(Sensor.NXT_TOUCH, Port.S4)
    while True:
        # Get touch sensors states and print them
        print("Port 1: {}, 2: {}, 3: {}, 4: {}".format(
              robot.sensors.touch[Port.S1].pressed()
             ,robot.sensors.touch[Port.S2].pressed()
             ,robot.sensors.touch[Port.S3].pressed()
             ,robot.sensors.touch[Port.S4].pressed()))

        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            break
        sleep(1)

    robot.deinit_all()

main()