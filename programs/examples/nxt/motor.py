'''
Example program for NXT or EV3 motors on all ports with encoders
'''
from time import sleep
import struct
import utime

from lib.robot_consts import Button, Port

def main():
    global robot
    
    # Initialize motors with encoders
    robot.init_motor(Port.M1)
    robot.init_motor(Port.M2)
    robot.init_motor(Port.M3)
    robot.init_motor(Port.M4)
    robot.motors[Port.M1].init_encoder()
    robot.motors[Port.M2].init_encoder()
    robot.motors[Port.M3].init_encoder()
    robot.motors[Port.M4].init_encoder()
    pwr1, pwr2, pwr3, pwr4 = 0, 0, 0, 0
    
    counter = 0
    while(True):
        counter+=1
        # Get encoder positions and speeds and print them
        pos1 = robot.motors[Port.M1].position()
        pos2 = robot.motors[Port.M2].position()
        pos3 = robot.motors[Port.M3].position()
        pos4 = robot.motors[Port.M4].position()
        speed1 = robot.motors[Port.M1].speed()
        speed2 = robot.motors[Port.M2].speed()
        speed3 = robot.motors[Port.M3].speed()
        speed4 = robot.motors[Port.M4].speed()
        print("Enc pos:", pos1, pos2, pos3, pos4,
                "Speed:", speed1, speed2, speed3, speed4)
        
        # Every second set new motor power 
        # with motors 1 and 2 rotating clockwise 
        #  and motors 3 and 4 rotating counterclockwise
        if counter % 10 == 0:
            pwr1 += 10
            pwr1 %= 80
            pwr2 += 10
            pwr2 %= 40
            pwr3 = -pwr2
            pwr4 = -pwr1
            robot.motors[Port.M1].set_power(pwr1)
            robot.motors[Port.M2].set_power(pwr2)
            robot.motors[Port.M3].set_power(pwr3)
            robot.motors[Port.M4].set_power(pwr4)

        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            break
        sleep(0.1)

    robot.deinit_all()
main()
  
