from time import sleep
import struct
import utime

from lib.robot_consts import Button, Port

BINARY_HEADER = (203, 98, 116, 140)
DATA_SIZE = 4
DATA_LENGTH = 8

def main():
    global robot
    ref_position = 0
    ref_speed = 0
    
    robot.init_motor(Port.M2)
    robot.motors[Port.M2].init_encoder()
    robot.motors[Port.M2].init_regulator()
    robot.motors[Port.M2].set_regulator_position(ref_position)
    robot.init_motor(Port.M3)
    robot.motors[Port.M3].init_encoder()
    robot.motors[Port.M3].init_regulator()
    robot.motors[Port.M3].set_regulator_position(ref_position)
    robot.init_esp_uart()
    robot.esp_uart.set_binary(BINARY_HEADER, DATA_SIZE*DATA_LENGTH)

    counter = 0
    while(True):
        counter+=1
        pos2 = robot.motors[Port.M2].position()
        pos3 = robot.motors[Port.M3].position()
        speed2 = robot.motors[Port.M2].speed()
        speed3 = robot.motors[Port.M3].speed()
        error2 = robot.motors[Port.M2].regulator_error()
        error3 = robot.motors[Port.M3].regulator_error()
        pwr2 = robot.motors[Port.M2].regulator_power()
        pwr3 = robot.motors[Port.M3].regulator_power()
        if counter % 10 == 0:
            print("Enc pos:", pos2, pos3, "Speed:", speed2, speed3)
            print("Error:", error2, error3, "Power:", pwr2, pwr3)

        line = robot.esp_uart.read()
        if line:
            read_values = struct.unpack('<ffffffff', line)
            (ref_position,ref_speed, KpP, KiP, KdP, KpS, KiS, KdS) = read_values
            robot.motors[Port.M2].set_regulator_position(ref_position)
            robot.motors[Port.M3].set_regulator_position(ref_position)
            robot.motors[Port.M2].regulator_pos_set_consts(KpP, KiP, KdP)
            robot.motors[Port.M3].regulator_pos_set_consts(KpP, KiP, KdP)
  
        robot.esp_uart.write(struct.pack('<ffffffff',ref_position, ref_speed, pos2, pos3, speed2, speed3, pwr2, pwr3))

        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            robot.deinit_all()
            break
        sleep(0.005)

main()
  
