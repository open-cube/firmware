'''
Example program for testing gyroscope and accelerometer values from the ICM20608-G sensor
'''
from time import sleep
from machine import Pin

from lib.robot_consts import Button, Sensor


def main():
    global robot
    
    robot.init_sensor(sensor_type=Sensor.GYRO_ACC)

    while True:
        # Read and print gyroscope and accelerometer values
        gyro_acc_meas = robot.sensors.gyro_acc.read_value()
        print("Acc:", gyro_acc_meas[0], gyro_acc_meas[1], gyro_acc_meas[2],
              "Gyro:",gyro_acc_meas[3], gyro_acc_meas[4], gyro_acc_meas[5])
        
        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            robot.deinit_all()
            break
        sleep(0.1)

main()