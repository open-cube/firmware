'''
Example program for the NXT light sensor in continuous mode on all ports
Sensors on ports S2 and S3 are in switching mode
Sensors on ports S1 and S4 are in non-switching mode
'''
from time import sleep

from lib.robot_consts import Button, Port, Sensor


# Continuous mode parameters
CONT_PERIOD = 1100
CONT_SWITCH_WAIT_TIME = 10000

def main():
    global robot

    # Initialize light sensors in continuous mode on all ports 
    # with ports S2 and S3 being in switching mode
    robot.init_sensor(Sensor.NXT_LIGHT, Port.S1)
    robot.init_sensor(Sensor.NXT_LIGHT, Port.S2)
    robot.init_sensor(Sensor.NXT_LIGHT, Port.S3)
    robot.init_sensor(Sensor.NXT_LIGHT, Port.S4)

    robot.sensors.light[Port.S2].set_switching()
    robot.sensors.light[Port.S3].set_switching()
    robot.sensors.light[Port.S2].set_continuous(CONT_PERIOD, CONT_SWITCH_WAIT_TIME)

    counter = 0
    while True:
        counter += 1 
        # Get measured light intensity with LED on and off
        value_on1  = robot.sensors.light[Port.S2].get_value(1)
        value_off1 = robot.sensors.light[Port.S2].get_value(0)
        value_on2  = robot.sensors.light[Port.S2].get_value(1)
        value_off2 = robot.sensors.light[Port.S2].get_value(0)
        value_on3  = robot.sensors.light[Port.S3].get_value(1)
        value_off3 = robot.sensors.light[Port.S3].get_value(0)
        value_on4  = robot.sensors.light[Port.S4].get_value(1)
        value_off4 = robot.sensors.light[Port.S4].get_value(0)
        # Print measured light intensity
        # value_on1 should be equal to value_off1 and value_on4 should be equal to value_off4
        # because sensors on ports S1 and S4 are in non-switching mode
        print("Light intesnity 1 on:",value_on1,"1 off:",value_off1,
                              "2 on:",value_on2,"2 off:",value_off2,
                              "3 on:",value_on3,"3 off:",value_off3,
                              "4 on:",value_on4,"4 off:",value_off4)
        
        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            break
        sleep(0.1)

    robot.sensors.light[Port.S2].stop_continuous()
    robot.deinit_all()

main()