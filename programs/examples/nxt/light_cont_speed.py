'''
Example program for the NXT light sensor continuous mode speed
The program measures the light intensity on port S2 for 1 second with the sensor 
in continuous and switching mode 
'''
from time import sleep
from machine import Timer

from lib.robot_consts import Button, Port, Sensor

# Initialize global variables
off = 0
on = 0
off_counter = 0
on_counter = 0
running = True
TIMELEN = 1000

# Continuous mode parameters
CONT_PERIOD = 1100
CONT_SWITCH_WAIT_TIME = 10000

# Print light sensor statistics and stop the program
def get_stats(timer):
        global off_counter,on_counter, running, on, off, TIMELEN
        print("On counter:", on_counter/TIMELEN*1000, 
              "Off counter:", off_counter/TIMELEN*1000,
              "Counter:", (on_counter+off_counter)/TIMELEN*1000)
        print("On value:", on/on_counter, "Off value:", off/off_counter)
        running = False

def main():
    global robot, off, on, off_counter, on_counter, running, TIMELEN
    counter = 0
    
    # Initialize light sensor on port S2 in continuous switching mode 
    robot.init_sensor(Sensor.NXT_LIGHT, Port.S2)
    robot.sensors.light[Port.S2].set_switching()
    robot.sensors.light[Port.S2].set_continuous(CONT_PERIOD, CONT_SWITCH_WAIT_TIME)
    
    # Start one shot timer for 1 second
    tim = Timer(mode=Timer.ONE_SHOT, period=TIMELEN, callback=get_stats)

    # Get light sensor data with LED on and off
    while running:
        if robot.sensors.light[Port.S2].new_data(0):
            val = robot.sensors.light[Port.S2].get_value(0)
            off+=val
            off_counter += 1 
        if robot.sensors.light[Port.S2].new_data(1):
            val = robot.sensors.light[Port.S2].get_value(1)
            on += val
            on_counter += 1 

    tim.deinit()
    robot.deinit_all()

main()






