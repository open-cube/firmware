'''
Example program for the NXT sound sensors on all ports
The program measures the sound intensity
'''
from time import sleep

from lib.robot_consts import Button, Port, Sensor


def main():
    global robot

    # Initialize sound sensors on all ports
    robot.init_sensor(Sensor.NXT_SOUND, Port.S1)
    robot.init_sensor(Sensor.NXT_SOUND, Port.S2)
    robot.init_sensor(Sensor.NXT_SOUND, Port.S3)
    robot.init_sensor(Sensor.NXT_SOUND, Port.S4)
    while True:
        # Get sound intensity and print it
        print("Sound level 1: {}, 2: {}, 3: {}, 4: {}".format(
              robot.sensors.sound[Port.S1].intensity()
             ,robot.sensors.sound[Port.S2].intensity()
             ,robot.sensors.sound[Port.S3].intensity()
             ,robot.sensors.sound[Port.S4].intensity()))

        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            break
        sleep(0.1)

    robot.deinit_all()

main()