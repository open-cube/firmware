'''
Example program for the NXT ultrasonic sensor
'''
from time import sleep

from lib.robot_consts import Button, Sensor


def main():
    global robot
    # Initialize the ultrasonic sensor
    robot.init_sensor(sensor_type=Sensor.NXT_ULTRASONIC)

    while True:
        # get distance measurement and print it
        print("Ultra: ", robot.sensors.ultra_nxt.distance())

        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            robot.deinit_all()
            break
        sleep(0.1)

main()