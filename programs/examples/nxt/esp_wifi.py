import utime

from lib.robot_consts import Button
from lib.cube.esp_config import Esp

def main():
    global robot
    run = True
    while run:
        time = utime.ticks_us()
        robot.buttons.set_pin(8, False)
        utime.sleep(0.02)
        wifi = (robot.esp.repeat_cmd(Esp.COM_WIFI, 50) == Esp.ACK)
        print("Wifi: ", wifi)
        utime.sleep(2)
        if wifi:
            print("Name: ", robot.esp.set_name("Open-Cube-Wifi",50))

        
        robot.buttons.set_pin(8, True)
        while run:
            buttons = robot.buttons.pressed()
            if buttons[Button.LEFT]:
                run = False
    

main()