'''
Example program for cube utility testing
The program measures battery voltage, turns on and off buzzer and LED
and displays info on the display
'''
from time import sleep

from lib.robot_consts import Button

def main():
    global robot

    counter = 0
    while True:
        counter += 1
        # Turn on or off buzzer and LED 
        if counter % 2 == 0:
            robot.buzzer.set_freq_duty(1000, 0.02)
            robot.led.on()
        else:
            robot.buzzer.off()
            robot.led.off()

        # Get battery voltage and print it
        bat_voltage = robot.battery.voltage()
        print("Battery voltage: ", bat_voltage)

        # Show info on the display
        robot.display.fill(0)
        robot.display.text('Counter: {}'.format(counter), 0, 30, 1)
        robot.display.text('{} V'.format(bat_voltage), 87, 0, 1)
        robot.display.text('LED state: {}'.format(counter % 2 == 0), 0, 40, 1)
        robot.display.show()

        # Get cube buttons states and print it
        buttons = robot.buttons.pressed_since()
        print("Buttons state:", buttons)
        # Exit program if left cube button is pressed
        if buttons[Button.LEFT]:
            break
        sleep(1)

    robot.deinit_all()

main()