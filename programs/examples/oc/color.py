'''
Example program for the Open-Cube Color sensor.
Measure and show light intensity on display.
Pressing down button changes sensor mode. (Reflection Red, Ambient, Color, 
Red reflection raw, RGB raw, Green reflection raw, Blue Reflection raw)
'''
import utime

from lib.OC import ColorSensor
from lib.robot_consts import Port, Button, Sensor


def main():
    global robot

    robot.display.fill(0)
    robot.display.text("Connecting...", 0, 0, 1)
    robot.display.show()
    robot.init_sensor(sensor_type=Sensor.OC_COLOR, port=Port.S1)

    mode = 0
    debounce = False
    while True:
        robot.display.fill(0)
        if mode == 0:
            ref = robot.sensors.light[Port.S1].reflection()
            robot.display.text("Reflection:", 0, 0, 1)
            robot.display.text("R: {}".format(ref), 0, 8, 1)
        if mode == 1:
            ambient = robot.sensors.light[Port.S1].ambient()
            robot.display.text("Ambient:", 0, 0, 1)
            robot.display.text("A: {}".format(ambient), 0, 8, 1)
        if mode == 2:
            color = robot.sensors.light[Port.S1].color()
            robot.display.text("color:", 0, 0, 1)
            robot.display.text("C: {}".format(color), 0, 8, 1)
        if mode == 3:
            ref, off = robot.sensors.light[Port.S1].reflection_raw()
            robot.display.text("R Reflection raw:", 0, 0, 1)
            robot.display.text("R: {}".format(ref), 0, 8, 1)
            robot.display.text("Off: {}".format(off), 0, 16, 1)
        if mode == 4:
            r, g, b, off = robot.sensors.light[Port.S1].rgb_raw()
            robot.display.text("RGB raw:", 0, 0, 1)
            robot.display.text("R: {}".format(r), 0, 8, 1)
            robot.display.text("G: {}".format(g), 0, 16, 1)
            robot.display.text("B: {}".format(b), 0, 24, 1)
            robot.display.text("Off: {}".format(off), 0, 32, 1)
        if mode == 5:
            ref, off = robot.sensors.light[Port.S1].reflection_raw_green()
            robot.display.text("G Reflection raw:", 0, 0, 1)
            robot.display.text("G: {}".format(ref), 0, 8, 1)
            robot.display.text("Off: {}".format(off), 0, 16, 1)
        if mode == 6:
            ref, off = robot.sensors.light[Port.S1].reflection_raw_blue()
            robot.display.text("B Reflection raw:", 0, 0, 1)
            robot.display.text("B: {}".format(ref), 0, 8, 1)
            robot.display.text("Off: {}".format(off), 0, 16, 1)
        robot.display.show()

        pressed = robot.buttons.pressed()
        # Exit program if left cube button is pressed
        if pressed[Button.LEFT]:
            break
        if pressed[Button.DOWN] and not debounce:
            mode += 1
            mode %= 7
            debounce = True
            print(mode)
        if not pressed[Button.DOWN]:
            debounce = False
        utime.sleep(0.2)
        


main()
