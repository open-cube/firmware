'''
Example program for the Open-Cube Laser sensor o port 1.
Measure and show distance on display.
Pressing down button changes sensor mode. (Normal, Large FOV, Short distance)
'''
import utime

from lib.robot_consts import Port, Button, Sensor


def main():
    global robot

    robot.display.fill(0)
    robot.display.text("Connecting...", 0, 0, 1)
    robot.display.show()

    robot.init_sensor(sensor_type=Sensor.OC_LASER, port=Port.S1)
    # At this distance (and closer) sensor green LED lights up (200 mm)
    robot.sensors.laser[Port.S1].set_led_distance(200)
    mode = 0
    mode_str = "Normal"
    debounce = False
    while True:
        if mode == 0:
            mode_str = "Normal"
            dist = robot.sensors.laser[Port.S1].distance()
        if mode == 1:
            mode_str = "Large FOV"
            dist = robot.sensors.laser[Port.S1].distance_fov()
        if mode == 2:
            mode_str = "Short dist" 
            dist = robot.sensors.laser[Port.S1].distance_short()
        robot.display.fill(0)
        robot.display.text("distance:", 0, 0, 1)
        robot.display.text(mode_str, 0, 8, 1)
        robot.display.text("d: {} mm".format(dist), 0, 16, 1)
        robot.display.show()
        
        pressed = robot.buttons.pressed()
        # Exit program if left cube button is pressed
        if pressed[Button.LEFT]:
            break;
        if not debounce:
            if pressed[Button.DOWN]:
                mode += 1
                mode %= 3
                debounce = True
                print("Mode:", mode)
            
        if not pressed[Button.DOWN] and not pressed[Button.UP]:
            debounce = False
        utime.sleep(0.1)
        

main()