'''
Example program for the Open-Cube Ultrasonic sensor
Measure and show distance on display.
'''
import utime

from lib.robot_consts import Port, Button, Sensor


def main():
    global robot

    robot.display.fill(0)
    robot.display.text("Connecting...", 0, 0, 1)
    robot.display.show()

    robot.init_sensor(sensor_type=Sensor.OC_ULTRASONIC, port=Port.S1)
    # At this distance (and closer) sensor green LED lights up (200 mm)
    robot.sensors.ultrasonic[Port.S1].set_led_distance(200)

    while True:
        dist = robot.sensors.ultrasonic[Port.S1].distance()
        robot.display.fill(0)
        robot.display.text("Distance:", 0, 0, 1)
        robot.display.text("d: {} mm".format(dist), 0, 8, 1)
        robot.display.show()
        
        pressed = robot.buttons.pressed()
        # Exit program if left cube button is pressed
        if pressed[Button.LEFT]:
            break
        utime.sleep(0.1)

main()