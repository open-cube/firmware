'''
Example program for the Open-Cube Gyro sensor on port 1.
The program measures the light intensity with the LED on and off
Pressing down button changes sensor mode. (Euler angles, Quaternions, Raw values, Calibration)
'''
import sys
import utime

from lib.robot_consts import Button, Port, Sensor

mode_str = (("Euler angles"), ("Quaternions"), ("Raw values"), ("Calibration"))

def main():
    global robot
    robot.init_sensor(Sensor.OC_GYRO, Port.S1)

    robot.display.fill(0)
    robot.display.text("Connecting...", 0, 0, 1)
    robot.display.show()

    mode = 0
    debounce = False
    while True:
        robot.display.fill(0)
        if mode == 0:
            euler = robot.sensors.gyro[Port.S1].euler_angles()
            robot.display.text("yaw:   {} deg".format(euler[0]), 0, 16, 1)
            robot.display.text("pitch: {} deg".format(euler[1]), 0, 24, 1)
            robot.display.text("roll:  {} deg".format(euler[2]), 0, 32, 1)
        if mode == 1:
            quat = robot.sensors.gyro[Port.S1].quaternions()
            robot.display.text("{},{}".format(quat[0], quat[1]), 0, 16, 1)
            robot.display.text("{},{}".format(quat[2], quat[3]), 0, 24, 1)
        if mode == 2:
            raw = robot.sensors.gyro[Port.S1].raw()
            robot.display.text("g{},{},{}".format(raw[0][0], raw[0][1], raw[0][2],), 0, 16, 1)
            robot.display.text("a{},{},{}".format(raw[1][0], raw[1][1], raw[1][2],), 0, 24, 1)
            robot.display.text("m{},{},{}".format(raw[2][0], raw[2][1], raw[2][2],), 0, 32, 1)
        if mode == 3:
            cal = robot.sensors.gyro[Port.S1].calibrate()
            robot.display.text("Calibrated: {}".format(cal), 0, 16, 1)
        
        robot.display.text("OC Gyro", 0, 0, 1)
        robot.display.text(mode_str[mode], 0, 8, 1)
        
        robot.display.show()
        
        pressed = robot.buttons.pressed()
        # Exit program if left cube button is pressed
        if pressed[Button.LEFT]:
            break
        if not debounce:
            if pressed[Button.DOWN]:
                mode += 1
                mode %= 4
                debounce = True
                print("Mode:", mode_str[mode])
            
        if not pressed[Button.DOWN] and not pressed[Button.UP]:
            debounce = False
        utime.sleep(0.1)
        

main()
