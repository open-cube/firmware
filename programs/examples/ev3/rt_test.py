import uarray
import utime

from lib.ev3 import GyroSensor, ColorSensor
from lib.robot_consts import Port


def main():
    gs = GyroSensor(Port.S1)
    cs = ColorSensor(Port.S2)

    print("Waiting for sensors...")
    gs.wait_for_connection()
    cs.wait_for_connection()

    print("Setting intensive modes...")
    gs.angle()
    cs.rgb()

    print(":hoho: let's go")
    data = uarray.array('l', [0] * 4096)
    idx = 0
    dlen = len(data)

    tlast = utime.ticks_us()

    while idx < dlen:
        utime.sleep_us(1000)
        tnow = utime.ticks_us()
        data[idx] = utime.ticks_diff(tnow, tlast)
        tlast = tnow
        idx += 1

    print("Measured time deltas:")
    for i in range(dlen):
        print(data[i])


main()
