import sys

import utime

from lib.cube.sh1106 import SH1106_I2C
from lib.ev3 import ColorSensor, Motor
from lib.robot import Robot
from lib.robot_consts import Button, Port

CALIBRATION_RPM = 30
CALIBRATION_TIME = 1000
CALIBRATION_WAIT_UNTIL_DONE = 100
REG_SP = 0.5
REG_KP = 75
REG_KD = 5000
REG_BIAS = 40
REG_OUT_SIGN = +1
OPTIMAL_DT = 10


class LineFollower:
    def __init__(self, robot: Robot):
        self.robot = robot
        self.display = robot.display
        self.cs = ColorSensor(Sensor.S1)
        self.lm = Motor(Port.M1)
        self.rm = Motor(Port.M4)
        self.light_min = None
        self.light_max = None
        self.light_sp = None
        self.loop_times = [0] * 1000
        self.loop_times_idx = 0
        self.loop_count = 0
        self.loop_min = None
        self.loop_max = None
        self.loop_skip = True

    def wait_for_sensors(self):
        self.display.fill(0)
        self.display.text("Waiting for", 0, 0, 1)
        self.display.text("sensors....", 0, 10, 1)
        self.display.show()
        self.cs.wait_for_connection()
        self.display.text("OK!", 0, 20, 1)
        self.display.show()

    def run(self):
        self.wait_for_sensors()
        self.calibrate_light()

        last_t = utime.ticks_ms()
        utime.sleep_ms(1)
        err_last = None

        self.display.fill(0)
        self.display.text("Running...", 0, 0, 1)
        self.display.show()
        while True:
            now_t = utime.ticks_ms()
            delta_t = utime.ticks_diff(now_t, last_t)
            last_t = now_t

            light = self.read_calibrated_light()
            err = REG_SP - light

            if err_last is not None:
                derr = (err - err_last) / delta_t
            else:
                derr = 0
            err_last = err

            u = REG_KP * err + REG_KD * derr

            spd_r = REG_BIAS + u * REG_OUT_SIGN
            spd_l = REG_BIAS - u * REG_OUT_SIGN
            self.rm.dc(round(-spd_r))
            self.lm.dc(round(-spd_l))

            self.poll_buttons()

            elapsed = utime.ticks_diff(utime.ticks_ms(), last_t)
            if elapsed < OPTIMAL_DT:
                utime.sleep_ms(OPTIMAL_DT - elapsed - 1)

    def poll_buttons(self):
        global REG_BIAS
        self.robot.buttons.read_value()
        buttons = self.robot.buttons.pressed()

        pressed = None
        if buttons[Button.UP]:
            REG_BIAS += 5
            pressed = Button.UP
        elif buttons[Button.DOWN]:
            REG_BIAS -= 5
            pressed = Button.DOWN
        if pressed is not None:
            self.display.fill(0)
            self.display.text("> Custom speed  <", 0, 0, 1)
            self.display.text(f"Kp={REG_KP:3d}", 0, 10, 1)
            self.display.text(f"Kd={REG_KD:3d}", 0, 18, 1)
            self.display.text(f"Spd={REG_BIAS:3d}", 0, 26, 1)
            self.display.show()
            while True:
                self.robot.buttons.read_value()
                if self.robot.buttons.pressed()[pressed]:
                    utime.sleep_ms(1)

    def calibrate_light(self):
        self.display.fill(0)
        self.display.text("Calibrating...", 0, 0, 1)
        self.display.show()
        light_min = 1e3
        light_max = 0
        self.lm.dc(+CALIBRATION_RPM)
        self.rm.dc(-CALIBRATION_RPM)

        start_t = utime.ticks_ms()
        loop_cnt = 0
        while utime.ticks_diff(utime.ticks_ms(), start_t) < CALIBRATION_TIME:
            light = self.cs.reflection_raw()
            light_min = min(light_min, light)
            light_max = max(light_max, light)
            loop_cnt += 1

        self.lm.brake()
        self.rm.brake()
        self.light_max = light_max
        self.light_min = light_min
        self.display.text("OK!", 0, 10, 1)
        self.display.text(f"cnt={loop_cnt}", 0, 20, 1)
        self.display.text(f"min={light_min}", 0, 30, 1)
        self.display.text(f"max={light_max}", 0, 40, 1)
        self.display.show()
        utime.sleep_ms(1000)

        self.display.fill(0)
        self.display.text("Aligning...", 0, 0, 1)
        self.display.show()
        self.lm.dc(-CALIBRATION_RPM)
        self.rm.dc(+CALIBRATION_RPM)
        while self.read_calibrated_light() > REG_SP:
            utime.sleep_ms(1)
        self.lm.brake()
        self.rm.brake()

        self.display.fill(0)
        self.display.show()

    def read_calibrated_light(self) -> float:
        raw = self.cs.reflection_raw()
        full_span = self.light_max - self.light_min
        msrd_span = raw - self.light_min
        return msrd_span / full_span

    def shutdown(self):
        self.lm.close()
        self.rm.close()
        self.cs.close()


def display_error(display: SH1106_I2C, text: str):
    display.fill(0)
    display.text("== Exception  ==", 0, 0, 1)
    rowchars = 16
    rowheight = 8
    for i in range(0, 7):
        part = text[(i + 0) * rowchars:(i + 1) * rowchars]
        display.text(part, 0, (i + 1) * rowheight, 1)
    display.show()


if __name__ == '__main__':
    global robot
    follower = None
    try:
        follower = LineFollower(robot)
        follower.run()
        follower.shutdown()
    except Exception as e:
        sys.stderr.write(b"Uncaught exception:\n")
        sys.print_exception(e, sys.stderr)

        if follower is not None:
            follower.shutdown()
            display_error(follower.display, str(e))
            while True:
                utime.sleep_ms(1)
