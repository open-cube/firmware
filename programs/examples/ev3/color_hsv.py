import utime

from lib.ev3 import ColorSensor
from lib.robot_consts import Port


def main():
    global robot

    robot.display.fill(0)
    robot.display.text("Connecting...", 0, 0, 1)
    robot.display.show()
    sensor = ColorSensor(Port.S1)
    sensor.wait_for_connection()

    try:
        while True:
            h, s, v = sensor.hsv()
            robot.display.fill(0)
            robot.display.text("HSV Ref:", 0, 0, 1)
            robot.display.text(f"H: {int(h):3d} deg", 0, 8, 1)
            robot.display.text(f"S: {s * 100:5.1f} %", 0, 16, 1)
            robot.display.text(f"V: {v * 100:5.1f} %", 0, 24, 1)
            robot.display.show()

            utime.sleep(0.01)
    finally:
        sensor.close()


main()
