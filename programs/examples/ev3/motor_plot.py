import uarray
import utime

from lib.ev3 import Motor
from lib.robot_consts import Port


def main():
    m = Motor(Port.M1)
    samples = 2048
    time = uarray.array('L', [0] * samples)
    pos = uarray.array('l', [0] * samples)
    speed = uarray.array('l', [0] * samples)
    power = uarray.array('b', [0] * samples)

    m.max_acceleration = 720
    m.run_angle(360, 360, wait=False)

    tstart = utime.ticks_us()
    tnext = utime.ticks_add(tstart, 1000)
    idx = 0
    while idx < samples:
        time[idx] = utime.ticks_diff(utime.ticks_us(), tstart)
        pos[idx] = m.angle()
        speed[idx] = m.speed()
        power[idx] = m.power()
        idx += 1

        tremain = utime.ticks_diff(tnext, utime.ticks_us())
        tnext = utime.ticks_add(tnext, 1000)
        if tremain > 0:
            utime.sleep_us(tremain)

    print("================")
    print("time,pos,speed,power")
    for i in range(0, idx):
        print(f"{time[i]},{pos[i]},{speed[i]},{power[i]}")
    print("================")

main()
