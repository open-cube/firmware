import time

from lib.ev3 import UltrasonicSensor
from lib.robot_consts import Button, Port


def main():
    global robot

    robot.display.fill(0)
    robot.display.text("Connecting...", 0, 0, 1)
    robot.display.show()
    sensor = UltrasonicSensor(Port.S1)
    sensor.wait_for_connection()

    try:
        reading_count = 0
        while True:
            mm = sensor.distance(silent=True)
            reading_count += 1
            robot.display.fill(0)
            robot.display.text("US-SI-CM:", 0, 0, 1)
            robot.display.text(f"#{reading_count}: {mm:4d} mm", 0, 8, 1)
            robot.display.show()

            while not robot.buttons.pressed()[Button.OK]:
                time.sleep(0.01)

            time.sleep(0.1)

    finally:
        sensor.close()


main()
