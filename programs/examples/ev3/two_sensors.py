import utime

from lib.ev3 import ColorSensor, GyroSensor
from lib.robot_consts import Port


def main():
    global robot

    robot.display.fill(0)
    robot.display.text("Connecting...", 0, 0, 1)
    robot.display.show()
    cs = ColorSensor(Port.S1)
    gs = GyroSensor(Port.S2)
    cs.wait_for_connection()
    gs.wait_for_connection()

    try:
        while True:
            ref = cs.reflection()
            angle = gs.angle()

            robot.display.fill(0)
            robot.display.text("Dual sensors:", 0, 0, 1)
            robot.display.text(f"Color: {ref:3d} %", 0, 8, 1)
            robot.display.text(f"Gyro: {angle:4d} deg", 0, 16, 1)
            robot.display.show()

            utime.sleep(0.01)
    finally:
        cs.close()
        gs.close()


main()
