import utime

from lib.ev3 import EV3UartSensor
from lib.robot_consts import Port


def main():
    global robot

    robot.display.fill(0)
    robot.display.text("Connecting...", 0, 0, 1)
    robot.display.show()
    sensor = EV3UartSensor(Port.S1)
    sensor.wait_for_connection()

    try:
        while True:
            mode, data = sensor.read_raw()
            robot.display.fill(0)
            robot.display.text(f"Sensor ID: {sensor.sensor_id()}", 0, 0, 1)
            for i, val in enumerate(data):
                robot.display.text(f"V{i}: {data[i]}", 0, 8 * i + 16, 1)
            robot.display.show()

            utime.sleep(0.01)
    finally:
        sensor.close()


main()
