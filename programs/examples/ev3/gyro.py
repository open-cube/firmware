import utime

from lib.ev3 import GyroSensor
from lib.robot_consts import Port


def main():
    global robot

    robot.display.fill(0)
    robot.display.text("Connecting...", 0, 0, 1)
    robot.display.show()
    sensor = GyroSensor(Port.S1)
    sensor.wait_for_connection()

    try:
        while True:
            angle, rate = sensor.angle_and_speed()
            robot.display.fill(0)
            robot.display.text("GYRO-G&A:", 0, 0, 1)
            robot.display.text(f"ANGL: {angle} deg", 0, 8, 1)
            robot.display.text(f"RATE: {rate} d/s", 0, 16, 1)
            robot.display.show()

            utime.sleep(0.01)
    finally:
        sensor.close()


main()
