import utime

from lib.ev3 import TouchSensor, GyroSensor, ColorSensor, UltrasonicSensor
from lib.robot_consts import Port, Button


def main():
    global robot
    display = robot.display

    display.fill(0)
    display.text("Connecting...", 0, 0, 1)
    display.show()
    print("CS init")
    cs = ColorSensor(Port.S1)
    print("GS init")
    gs = GyroSensor(Port.S2)
    print("US init")
    us = UltrasonicSensor(Port.S3)
    print("TS init")
    ts = TouchSensor(Port.S4)
    print("CS wait")
    cs.wait_for_connection()
    print("GS wait")
    gs.wait_for_connection()
    print("US wait")
    us.wait_for_connection()

    try:
        while True:
            ref = cs.reflection()
            angle = gs.angle()
            dist = us.distance()
            press = ts.pressed()

            press_str = "PRESSED" if press else "RELEASED"

            vbatt = robot.battery.voltage()
            vbatt_str = f"{vbatt:4.2f}V"

            TOP_SPACER = 12

            display.fill(0)
            display.text("EV3 inputs", 0, 0, 1)
            display.text(vbatt_str, 128 - len(vbatt_str) * 8, 0, 1)
            display.hline(0, 8, 128, 1)
            display.text(f"Color: {ref:4d} %", 0, 0 + TOP_SPACER, 1)
            display.text(f"Gyro:  {angle:4d} deg", 0, 10 + TOP_SPACER, 1)
            display.text(f"Sonic: {dist:4d} mm", 0, 20 + TOP_SPACER, 1)
            display.text(f"Touch: {press_str}", 0, 30 + TOP_SPACER, 1)
            display.show()
            
            buttons = robot.buttons.pressed()
            if buttons[Button.LEFT]:
				break
            utime.sleep(0.01)
    finally:
        cs.close()
        gs.close()
        us.close()


main()
