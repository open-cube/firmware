import utime

from lib.ev3 import UltrasonicSensor
from lib.robot_consts import Port


def main():
    global robot

    robot.display.fill(0)
    robot.display.text("Connecting...", 0, 0, 1)
    robot.display.show()
    sensor = UltrasonicSensor(Port.S1)
    sensor.wait_for_connection()

    try:
        while True:
            present = sensor.presence()
            robot.display.fill(0)
            robot.display.text("US-LISTEN:", 0, 0, 1)
            robot.display.text(f"Found: {present}", 0, 8, 1)
            robot.display.show()
            utime.sleep(0.01)
    finally:
        sensor.close()


main()
