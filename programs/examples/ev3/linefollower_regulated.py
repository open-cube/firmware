import sys

import utime
from micropython import const

from lib.cube.sh1106 import SH1106_I2C
from lib.ev3 import ColorSensor, Motor
from lib.robot import Robot
from lib.robot_consts import Button, Port

CALIBRATION_SPEED = 180
CALIBRATION_MSEC = 1000
CALIBRATION_WAIT_UNTIL_DONE = 100
REG_SP = 0.5
REG_KP = 1200
REG_KD = 20
REG_BIAS = 800
OPTIMAL_DT = 0.001

MODE_KP = const(0)
MODE_KD = const(1)
MODE_SPD = const(2)
NUM_MODES = const(3)


class TunerUI:
    def __init__(self, cube: Robot):
        self.Kp = REG_KP
        self.Kd = REG_KD
        self.bias = REG_BIAS
        self.cube = cube
        self.mode = MODE_KP
        self.last_buttons = cube.buttons.pressed()

    def render_ui(self):
        display = self.cube.display
        display.fill(0)
        display.centered_text("Open-Cube", 0)
        display.centered_text("Linefollower", 8)

        display.centered_text("Live tuning", 24)
        display.text(f"{'*' if self.mode == MODE_KP else ' '} Kp = {self.Kp:.1f}", 0, 32)
        display.text(f"{'*' if self.mode == MODE_KD else ' '} Kd = {self.Kd:.1f}", 0, 40)
        display.text(f"{'*' if self.mode == MODE_SPD else ' '} Pwr = {self.bias:.1f}", 0, 48)
        display.show()

    def update_ui(self, force_render: bool = False):
        new_buttons = self.cube.buttons.pressed()
        just_pressed = [now and not past for now, past in zip(new_buttons, self.last_buttons)]
        self.last_buttons = new_buttons

        if just_pressed[Button.UP]:
            self.mode = (self.mode - 1) % NUM_MODES
        if just_pressed[Button.DOWN]:
            self.mode = (self.mode + 1) % NUM_MODES
        if just_pressed[Button.LEFT]:
            if self.mode == MODE_KP:
                self.Kp -= 50
            elif self.mode == MODE_KD:
                self.Kd -= 5
            elif self.mode == MODE_SPD:
                self.bias -= 20
        if just_pressed[Button.RIGHT]:
            if self.mode == MODE_KP:
                self.Kp += 50
            elif self.mode == MODE_KD:
                self.Kd += 5
            elif self.mode == MODE_SPD:
                self.bias += 20

        if force_render or any(just_pressed):
            self.render_ui()


class LineFollower:
    def __init__(self, robot: Robot):
        self.robot = robot
        self.display = robot.display
        self.cs = ColorSensor(Port.S1)
        self.lm = Motor(Port.M1)
        self.rm = Motor(Port.M2)
        self.lm.max_acceleration = float('+inf')
        self.rm.max_acceleration = float('+inf')
        self.light_min = None
        self.light_max = None
        self.ui = TunerUI(robot)

    def wait_for_sensors(self):
        self.display.fill(0)
        self.display.text("Waiting for", 0, 0, 1)
        self.display.text("sensors....", 0, 10, 1)
        self.display.show()
        self.cs.wait_for_connection()
        self.display.text("OK!", 0, 20, 1)
        self.display.show()

    def run(self):
        self.wait_for_sensors()
        self.calibrate_light()

        err_last = None

        self.ui.render_ui()

        timer = LoopTimer(OPTIMAL_DT).start()
        while True:
            light = self.read_calibrated_light()
            err = REG_SP - light

            if err_last is not None:
                derr = (err - err_last) / OPTIMAL_DT
            else:
                derr = 0
            err_last = err

            u = self.ui.Kp * err + self.ui.Kd * derr

            if u > 0:
                self.lm.run(self.ui.bias)
                self.rm.run(self.ui.bias - u)
            else:
                self.lm.run(self.ui.bias + u)
                self.rm.run(self.ui.bias)

            self.ui.update_ui()

            timer.wait_for_next_iteration()


    def calibrate_light(self):
        self.display.fill(0)
        self.display.text("Calibrating...", 0, 0, 1)
        self.display.show()
        light_min = 1e3
        light_max = 0
        self.lm.run_time(+CALIBRATION_SPEED, CALIBRATION_MSEC, then=Motor.BRAKE, wait=False)
        self.rm.run_time(-CALIBRATION_SPEED, CALIBRATION_MSEC, then=Motor.BRAKE, wait=False)

        start_t = utime.ticks_ms()
        loop_cnt = 0
        while not (self.lm.is_complete() or self.rm.is_complete()):
            light = self.cs.reflection_raw()
            light_min = min(light_min, light)
            light_max = max(light_max, light)
            loop_cnt += 1

        self.light_max = light_max
        self.light_min = light_min
        self.display.text("OK!", 0, 10, 1)
        self.display.text(f"cnt={loop_cnt}", 0, 20, 1)
        self.display.text(f"min={light_min}", 0, 30, 1)
        self.display.text(f"max={light_max}", 0, 40, 1)
        self.display.show()
        utime.sleep_ms(1000)

        self.display.fill(0)
        self.display.text("Aligning...", 0, 0, 1)
        self.display.show()
        self.lm.run(-CALIBRATION_SPEED)
        self.rm.run(-CALIBRATION_SPEED)
        while self.read_calibrated_light() > REG_SP:
            utime.sleep_ms(1)
        self.lm.brake()
        self.rm.brake()

        self.display.fill(0)
        self.display.show()

    def read_calibrated_light(self) -> float:
        raw = self.cs.reflection_raw()
        full_span = self.light_max - self.light_min
        msrd_span = raw - self.light_min
        return msrd_span / full_span

    def shutdown(self):
        self.lm.close()
        self.rm.close()
        self.cs.close()


def display_error(display: SH1106_I2C, text: str):
    display.fill(0)
    display.text("== Exception  ==", 0, 0, 1)
    rowchars = 16
    rowheight = 8
    for i in range(0, 7):
        part = text[(i + 0) * rowchars:(i + 1) * rowchars]
        display.text(part, 0, (i + 1) * rowheight, 1)
    display.show()


class LoopTimer:
    def __init__(self, Ts: float):
        self.Ts = Ts
        self.Ts_usec = round(Ts * 1e6)
        self.Tnext = None

    def start(self) -> 'LoopTimer':
        self.Tnext = utime.ticks_add(utime.ticks_us(), self.Ts_usec)
        return self

    def remaining_usec(self) -> int:
        return utime.ticks_diff(self.Tnext, utime.ticks_us())

    def wait_for_next_iteration(self):
        Tremain = self.remaining_usec()
        if Tremain > 0:
            utime.sleep_us(Tremain)
        else:
            while self.remaining_usec() <= 0:
                self.advance_to_next_iteration()

    def advance_to_next_iteration(self):
        self.Tnext = utime.ticks_add(self.Tnext, self.Ts_usec)


if __name__ == '__main__':
    global robot
    follower = None
    try:
        follower = LineFollower(robot)
        follower.run()
        follower.shutdown()
    except Exception as e:
        sys.stderr.write(b"Uncaught exception:\n")
        sys.print_exception(e, sys.stderr)

        if follower is not None:
            follower.shutdown()
            display_error(follower.display, str(e))
            while True:
                utime.sleep_ms(1)
