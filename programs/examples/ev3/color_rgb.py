import utime

from lib.ev3 import ColorSensor
from lib.robot_consts import Port


def main():
    global robot

    robot.display.fill(0)
    robot.display.text("Connecting...", 0, 0, 1)
    robot.display.show()
    sensor = ColorSensor(Port.S1)
    sensor.wait_for_connection()

    try:
        while True:
            r, g, b = sensor.rgb()
            robot.display.fill(0)
            robot.display.text("RGB Ref:", 0, 0, 1)
            robot.display.text(f"R: {int(r * 100):3d} %", 0, 8, 1)
            robot.display.text(f"G: {int(g * 100):3d} %", 0, 16, 1)
            robot.display.text(f"B: {int(b * 100):3d} %", 0, 24, 1)
            robot.display.show()

            utime.sleep(0.01)
    finally:
        sensor.close()


main()
