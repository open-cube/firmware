import utime
import random

from lib.robot_consts import Button
from lib.cube.esp_config import Esp

def main():
    global robot
    counter = 0
    indicators = [True, False, True, False, False]
    labels = ["1", "test1", "test2", "", "123"]
    btn_labels = ["", "up", "", "left", "ok", "right", "", "down", "" ]
    run = True
    while run:
        time = utime.ticks_us()
        wifi = robot.esp.wifi()
        print("Wifi: ", wifi)
        if wifi:
            break 
        print("Name: ", robot.esp.set_name("Open-Cube-Wifi"))
        print("Password: ", robot.esp.set_password("01234567"))
        
        robot.esp.wifi_set_indicators_labels(("1", "", "test2", "test3", "123"))
        robot.esp.wifi_set_buttons_labels(btn_labels)
        robot.esp.wifi_set_switches_labels(labels)
        robot.esp.wifi_set_numbers_labels(("t:", "s:", "v:", "test1:", "", "test2:"))
        while run:
            utime.sleep(0.1)
            if counter % 10 == 0:
                robot.esp.wifi_set_indicators(indicators)
                robot.esp.wifi_set_numbers((123456789, 0.123456, random.random(), 0.123456, random.random(), 5.2))
                indicators = indicators[-1:]+indicators[:-1]
                
            print("Buttons: ", robot.esp.wifi_get_buttons())
            print("Switches: ", robot.esp.wifi_get_switches())
            
            counter +=1
            buttons = robot.buttons.pressed()
            if buttons[Button.LEFT]:
                run = False
    

main()