import utime
import struct

from lib.robot_consts import Button, Port, Sensor, Light

LIGHT_SETTLING_TIME_US = 4000
ADC_SAMPLE_TIME_US = 1100
REGUL_PERIOD_US = 10000
REGUL_PERIOD_S = REGUL_PERIOD_US / 1000000

BINARY_HEADER = (203, 98, 116, 140)
NUM_RECEIVED_VALS = 6
DATA_SIZE = 4

INITIAL_MEASUREMENTS = 50
LIGHT_NORM_MAX = 100

def measure_mean(robot):
    light_diff = 0
    for i in range(INITIAL_MEASUREMENTS):
        while not robot.sensors.light[Port.S2].new_data(Light.ON):
            pass
        light_on = robot.sensors.light[Port.S2].get_value(Light.ON)
        while not robot.sensors.light[Port.S2].new_data(Light.OFF):
            pass
        light_off = robot.sensors.light[Port.S2].get_value(Light.OFF)
        print("Light on:", light_on, "Light off:", light_off) 
        light_diff += light_off - light_on
    light_diff /= INITIAL_MEASUREMENTS
    return light_diff

def measure_initial_intensity(robot):
    black_mean = measure_mean(robot)
    robot.motors[Port.M2].init_regulator()
    robot.motors[Port.M2].regulator_pos_set_consts(0.3, 0.03, 0)
    robot.motors[Port.M2].set_regulator_position(300)
    while abs(robot.motors[Port.M2].regulator_error()) > 20:
        print(robot.motors[Port.M2].regulator_error())
        utime.sleep_ms(100)
    white_mean = measure_mean(robot)
    robot.motors[Port.M2].set_regulator_position(50)
    while abs(robot.motors[Port.M2].regulator_error()) > 20:
        print(robot.motors[Port.M2].regulator_error())
        utime.sleep_ms(100)
    robot.motors[Port.M2].deinit_regulator()
    print(black_mean, white_mean)
    light_mean = (black_mean + white_mean)/2
    light_spread = abs(black_mean - white_mean)/2
    return light_mean, light_spread

# Normalize light (-100 for white, 100 for black)
def normalize_light(light_mean, light_spread, light_diff):
    light = (light_diff - light_mean)/light_spread*LIGHT_NORM_MAX
    if light > LIGHT_NORM_MAX:
        light = LIGHT_NORM_MAX
    if light < -LIGHT_NORM_MAX:
        light = -LIGHT_NORM_MAX
    return light
        

def main():
    global robot
    # Initialize regulator parameters
    Kp, Ki, Kd = 0.1, 0.0, 0.0
    light_error_sum, light_error_prev = 0, 0
    light_setpoint, motor_base_pwr, motor_max_pwr = 0, 40, 30

    # Init robot motors, sensors and BT communication
    robot.init_motor(Port.M2)
    robot.init_motor(Port.M3)

    robot.init_sensor(port=Port.S2, sensor_type=Sensor.NXT_LIGHT)
    robot.sensors.light[Port.S2].set_switching()
    robot.sensors.light[Port.S2].set_continuous(
        ADC_SAMPLE_TIME_US, LIGHT_SETTLING_TIME_US)

    light_mean, light_spread = measure_initial_intensity(robot)
    print(light_mean, light_spread)
    utime.sleep_ms(500)

    robot.init_esp_uart()
    robot.esp_uart.set_binary(BINARY_HEADER, NUM_RECEIVED_VALS*DATA_SIZE)
    counter = 0
    while True:
        regul_start_time = utime.ticks_us()
        counter +=1
        # Read light sensor on and off values, calculate difference and normalize (-100 for white, 100 for black)
        light_on = robot.sensors.light[Port.S2].get_value(Light.ON)
        light_off = robot.sensors.light[Port.S2].get_value(Light.OFF)
        light_diff = light_off - light_on
        light = normalize_light(light_mean, light_spread, light_diff)
        if counter % 10 == 0:
            print(light_diff, light, counter)
        # PID regulator
        light_error = light_setpoint - light
        P = Kp*light_error
        I = Ki*light_error_sum*REGUL_PERIOD_S
        D = Kd*(light_error - light_error_prev)/REGUL_PERIOD_S
        motor_pwr = P + I + D

        # Save previous error for derivative part
        light_error_prev = light_error
        # Anti-windup for integral part
        if ((light_error > 0 and motor_pwr < motor_max_pwr) 
            or (light_error < 0 and motor_pwr > -motor_max_pwr)):
            light_error_sum += Ki*light_error*REGUL_PERIOD_S 

        # Limit motor power to motor_max_pwr
        if motor_pwr > motor_max_pwr:
            motor_pwr = motor_max_pwr
        elif motor_pwr < -motor_max_pwr:
            motor_pwr = -motor_max_pwr

        # Set motor power
        robot.motors[Port.M2].set_power(motor_base_pwr + motor_pwr)
        robot.motors[Port.M3].set_power(motor_base_pwr - motor_pwr)

        # Receive data from ESP and update regulator parameters
        newdata = robot.esp_uart.read()
        if newdata:
            values = struct.unpack('<ffffff', newdata)
            (Kp, Ki, Kd, 
            light_setpoint, motor_base_pwr, motor_max_pwr ) = values
        
        # Send data to ESP
        send_data = struct.pack('<ffffff',
                    light_setpoint, light, motor_pwr, P, I, D)
        robot.esp_uart.write(send_data)

        # Check if exit button is pressed and exit program if so
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            break

        # Sleep
        regul_new_time = utime.ticks_us()
        regul_time = utime.ticks_diff(regul_new_time, regul_start_time)
        #print("regul:", regul_time/1000, "ms", "Counter: ", counter)
        if REGUL_PERIOD_US-regul_time > 0:
            utime.sleep_us(REGUL_PERIOD_US-regul_time)
        

    robot.deinit_all()


main()