'''
Example program of a simple line follower robot with PID regulator.
'''
import utime

from lib.robot_consts import Button, Port, Sensor, Light

# Main program
def main():
    global robot
    # Regulation period 0.01 s
    REGUL_PERIOD_MS = 10
    # Initialize regulator parameters
    Kp, Ki, Kd = 0.3, 0.0, 0.004
    light_setpoint, motor_base_pwr = 16000, 45
    light_error_sum, light_error_prev = 0, 0
    
    # Initialize motors at ports M2 and M3
    robot.init_motor(Port.M2)
    robot.init_motor(Port.M3)

    # Initialize light sensor at port S2 and turn on sensor LED
    robot.init_sensor(sensor_type=Sensor.NXT_LIGHT, port=Port.S2)
    robot.sensors.light[Port.S2].on()

    # Follower loop
    while True:
        # Read light intensity
        light_intensity = robot.sensors.light[Port.S2].get_value(Light.ON)

        # PID regulator
        light_error = light_setpoint - light_intensity
        P = Kp*light_error
        I = Ki*light_error_sum
        D = Kd*(light_error - light_error_prev)
        motor_pwr = P + I + D

        # Save previous error for derivative part
        light_error_prev = light_error
        # Save sum of errors for integral part
        light_error_sum += light_error

        # Set motor power
        robot.motors[Port.M2].set_power(motor_base_pwr + motor_pwr)
        robot.motors[Port.M3].set_power(motor_base_pwr - motor_pwr)

        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            break

        utime.sleep_ms(REGUL_PERIOD_MS)

# Start the program
main()