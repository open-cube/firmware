'''
Example program of a balancing robot (segway).
Start program with robot standing on the wheels.
Optional ESP communication in binary mode.
'''
import utime
from machine import Pin
from math import radians, atan2, pi
import struct

from lib.robot_consts import Button, Port, Sensor, GyroAcc
from lib.robot import Robot

# Gyro and accelerometer settings
offset_angle = 0.13
angle_compl = offset_angle
Kcomp = 0.001
gyro_start_time = 0

# Regulator settings
regul_start_time = 0
REGUL_PERIOD_US = 10000
REGUL_PERIOD = REGUL_PERIOD_US/1000000

# ESP communication settings
BINARY_HEADER = (203, 98, 116, 140, 15, 42, 254)
NUM_RECEIVED_BYTES = 96

# Length constants
WHEEL_RADIUS = 0.0425
ROTATION_RADIUS = 0.08
ULTRA_MAX_RANGE = 2.55

robot = Robot()
# Average for ultrasonic measurements
class StreamingMovingAverage:
    def __init__(self, window_size):
        self.window_size = window_size
        self.values = []
        self.sum = 0

    def process(self, value):
        self.values.append(value)
        self.sum += value
        if len(self.values) > self.window_size:
            self.sum -= self.values.pop(0)
        return float(self.sum) / len(self.values)
    
# Gyro and accelerometer callback function on new data interrupt
# Calculates angle using comlementary filter
def gyro_callback(p):
    global robot, Kcomp, angle_compl, gyro_start_time
    new_time = utime.ticks_us()
    dt = utime.ticks_diff(new_time, gyro_start_time)/1000000
    gyro_start_time = new_time
    gyro_acc_meas = robot.sensors.gyro_acc.read_value()
    
    gy = -gyro_acc_meas[GyroAcc.GY]
    ax = -gyro_acc_meas[GyroAcc.AX]
    az = gyro_acc_meas[GyroAcc.AZ]
    
    angle_acc = atan2(az, ax)
    angle_compl = (angle_compl +radians(gy)*dt)*(1-Kcomp) + angle_acc*(Kcomp)   

# Main program
def main():
    global robot, gyro_start_time, angle_compl, Kcomp, offset_angle

    # Get robot position, speed and rotation
    def get_position(m1_last, m2_last, rotation):
        m1 = -robot.motors[Port.M2].position()*pi*WHEEL_RADIUS/180
        m2 = -robot.motors[Port.M3].position()*pi*WHEEL_RADIUS/180
        
        position = (m1+m2)/2

        inc1 = m1-m1_last
        inc2 = m2-m2_last
        traj_vect_len = (inc1+inc2)/2
        speed = traj_vect_len/REGUL_PERIOD

        rotation_inc = (inc1-inc2)/2/ROTATION_RADIUS
        rotation += rotation_inc
        
        return position, speed, rotation, m1, m2,

    # Universal PID regulator with anti-windup and filtered derivative part
    def PID_regulator(current, reference, d_e, last_e, sum_e, max_output, max_int, K):
        (Kp, Ki, Kd, Kfd) = K
        e = reference - current
        d_e = (d_e*Kfd + (e - last_e))/(1+Kfd)
        output = Kp*e + Kd*d_e/REGUL_PERIOD + Ki*sum_e*REGUL_PERIOD

        last_e = e
        if e > 0:
            if Ki*sum_e*REGUL_PERIOD < max_int and output < max_output:
                sum_e += e
        else:
            if Ki*sum_e*REGUL_PERIOD > -max_int and output > -max_output:
                sum_e += e

        if output > max_output:
            output = max_output
        elif output <-max_output:
            output = -max_output
            
        return output, d_e, last_e, sum_e
    
    # PID regulator for obstacle avoidance
    def PIDwall(wall_dist, ref_dist, d_eL, last_eL, sum_eL):
        MAX_SPEEED = 0.18
        MAX_SPEEED_INT = 0.05
        DISTANCE_SLOW = 0.1
        eL = wall_dist-ref_dist-DISTANCE_SLOW
        if wall_dist-ref_dist > DISTANCE_SLOW:
            PID_L = 0
            d_eL = 0
            last_eL = 0
            sum_eL = 0
        else:
            d_eL = (d_eL*KfdL + (eL - last_eL))/(1+KfdL)
            PID_L = KpL*eL + KdL*d_eL/REGUL_PERIOD + KiL*sum_eL*REGUL_PERIOD
            last_eL = eL
            if eL > 0:
                if KiL*sum_eL*REGUL_PERIOD < MAX_SPEEED_INT:
                    sum_eL += eL
            else:
                if KiL*sum_eL*REGUL_PERIOD > -MAX_SPEEED_INT:
                    sum_eL += eL
        ref_ultra_speed = PID_L
        return ref_ultra_speed, d_eL, last_eL, sum_eL

    # Show info on display
    robot.display.fill(0)
    robot.display.centered_text("Segway running", 32)
    robot.display.show()

    # Initialize regulator constants
    KpP,KiP,KdP,KfdP,max_P,max_i_P = 0.4,0.01,0.0,0.0,0.22,0.05
    KpS,KiS,KdS,KfdS,max_S,max_i_S = 0.25,0.0,-0.07,25,0.06,0.06
    KpA,KiA,KdA,KfdA,max_A,max_i_A = 2000,0,-500,80,100,0
    KpR,KiR,KdR,KfdR,max_R,max_i_R = 100,1,0,0,40,1
    KpL,KiL,KdL,KfdL,max_L,max_i_L = 0,0,0,0,0.23,0

    ref_position,ref_rotation,ref_speed = 0.0,0.0,0.0
    ref_angle,ref_ultra_speed,ref_dist = 0.0,0.0,0.3
    
    m1, m2, m1_dist, m2_dist = 0.0,0.0,0.0,0.0
    d_eA, last_eA, sum_eA = 0.0,0.0,0.0
    d_eS, last_eS, sum_eS = 0.0,0.0,0.0
    d_eP, last_eP, sum_eP = 0.0,0.0,0.0
    d_eR, last_eR, sum_eR = 0.0,0.0,0.0
    d_eL, last_eL, sum_eL = 0.0,0.0,0.0
    speed,rotation = 0.0, 0.0
    
    wall_dist = 0
    
    # Initialize ultrasonic sensor
    ultra_avg = StreamingMovingAverage(20)
    robot.init_sensor(sensor_type=Sensor.NXT_ULTRASONIC)

    # Initialize ESP communication in binary mode
    robot.esp.bt_set_binary(BINARY_HEADER, NUM_RECEIVED_BYTES)

    # Initialize motors
    robot.init_motor(Port.M2)
    robot.init_motor(Port.M3)
    robot.motors[Port.M2].init_encoder()
    robot.motors[Port.M3].init_encoder()
    
    # Inititialize gyro and accelerometer with interrupt
    robot.init_sensor(sensor_type=Sensor.GYRO_ACC)
    gyro_start_time = utime.ticks_us()
    p0 = Pin(GyroAcc.IRQ_PIN, Pin.IN)
    p0.irq(trigger=Pin.IRQ_FALLING, handler=gyro_callback)
    
    
    counter = 0
    # Regulator loop
    while True:
        regul_start_time = utime.ticks_us()
        counter += 1

        e_angle = angle_compl-offset_angle
        position, speed, rotation, m1, m2 = get_position(m1, m2, rotation)
        if counter % 5 == 0:
            wall_dist = ultra_avg.process(robot.sensors.ultra_nxt.distance()/100)

        # Regulators
        ref_speed, d_eP, last_eP, sum_eP = PID_regulator(position, ref_position, d_eP, last_eP, sum_eP, max_P, max_i_P, (KpP,KiP,KdP,KfdP))
        ref_ultra_speed, d_eL, last_eL, sum_eL = PIDwall(wall_dist, ref_dist, d_eL, last_eL, sum_eL)
        ref_angle, d_eS, last_eS, sum_eS = PID_regulator(speed,ref_speed+ref_ultra_speed, d_eS, last_eS, sum_eS, max_S, max_i_S, (KpS,KiS,KdS,KfdS))
        motor_power, d_eA, last_eA, sum_eA = PID_regulator(e_angle, ref_angle, d_eA, last_eA, sum_eA, max_A, max_i_A, (KpA,KiA,KdA,KfdA))
        rot_power, d_eR, last_eR, sum_eR = PID_regulator(rotation, ref_rotation, d_eR, last_eR, sum_eR, max_R, max_i_R, (KpR,KiR,KdR,KfdR))
        
        # Set motor power to zero if robot has fallen
        if(abs(e_angle) > 0.8):
            motor_power = 0
            rot_power = 0

        # Set motor power
        robot.motors[Port.M2].set_power(motor_power-rot_power)
        robot.motors[Port.M3].set_power(motor_power+rot_power)

        # Receive data from ESP and update regulator parameters
        line = robot.esp.bt_read()
        if line:
            read_values = struct.unpack('<ffffffffffffffffffffffff', line)
            (KpP,KiP,max_P,
            KpS,KiS,KdS,KfdS,max_S,
            KpA,KiA,KdA,KfdA,max_A,
            KpR,KiR,max_R,
            KpL,KiL,max_L,
            ref_position,ref_rotation,ref_dist,offset_angle,Kcomp) = read_values
    
        # Send data to 
        write_message = struct.pack('<fffffffffffff', 
            ref_position, position, ref_speed, speed, ref_angle, e_angle,
            ref_rotation, rotation, ref_dist, wall_dist, 
            motor_power, rot_power, ref_ultra_speed)
        robot.esp.bt_write(write_message)
        
        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            break

        # Sleep for the rest of the regulation period
        regul_new_time = utime.ticks_us()
        regul_time = utime.ticks_diff(regul_new_time, regul_start_time)
        if REGUL_PERIOD_US-regul_time > 0:
            utime.sleep_us(REGUL_PERIOD_US-regul_time)

    robot.deinit_all()

main()