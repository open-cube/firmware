'''
Example program of a balancing robot (segway).
Start program with robot standing on the wheels.
Optional wifi ESP communication.
'''
import utime
from machine import Pin
from math import radians, atan2, pi
import struct

from lib.robot_consts import Button, Port, Sensor, GyroAcc


# Gyro and accelerometer settings
offset_angle = 0.13
angle_compl = offset_angle
Kcomp = 0.001
gyro_start_time = 0

# Regulator settings
regul_start_time = 0
REGUL_PERIOD_US = 10000
REGUL_PERIOD = REGUL_PERIOD_US/1000000

# Length constants
WHEEL_RADIUS = 0.0425
ROTATION_RADIUS = 0.08
ULTRA_MAX_RANGE = 2.55

# Control settings
MAX_SPEED = 0.2
MAX_ROT_SPED = 1.3
SPEED_STEP = MAX_SPEED/150
ROT_SPEED_STEP = MAX_ROT_SPED/50

# Average for ultrasonic measurements
class StreamingMovingAverage:
    def __init__(self, window_size):
        self.window_size = window_size
        self.values = []
        self.sum = 0

    def process(self, value):
        self.values.append(value)
        self.sum += value
        if len(self.values) > self.window_size:
            self.sum -= self.values.pop(0)
        return float(self.sum) / len(self.values)
    
# Gyro and accelerometer callback function on new data interrupt
# Calculates angle using comlementary filter
def gyro_callback(p):
    global robot, Kcomp, angle_compl, gyro_start_time
    new_time = utime.ticks_us()
    dt = utime.ticks_diff(new_time, gyro_start_time)/1000000
    gyro_start_time = new_time
    gyro_acc_meas = robot.sensors.gyro_acc.read_value()
    
    gy = -gyro_acc_meas[GyroAcc.GY]
    ax = -gyro_acc_meas[GyroAcc.AX]
    az = gyro_acc_meas[GyroAcc.AZ]
    
    angle_acc = atan2(az, ax)
    angle_compl = (angle_compl +radians(gy)*dt)*(1-Kcomp) + angle_acc*(Kcomp)   

# Main program
def main():
    global robot, gyro_start_time, angle_compl, Kcomp, offset_angle

    # Get robot position, speed and rotation
    def get_position(m1_last, m2_last, rotation):
        m1 = -robot.motors[Port.M2].position()*pi*WHEEL_RADIUS/180
        m2 = -robot.motors[Port.M3].position()*pi*WHEEL_RADIUS/180
        
        position = (m1+m2)/2

        inc1 = m1-m1_last
        inc2 = m2-m2_last
        traj_vect_len = (inc1+inc2)/2
        speed = traj_vect_len/REGUL_PERIOD

        rotation_inc = (inc1-inc2)/2/ROTATION_RADIUS
        rotation += rotation_inc
        rot_speed = rotation_inc/REGUL_PERIOD

        return position, speed, rotation, m1, m2, rot_speed

    # Universal PID regulator with anti-windup and filtered derivative part
    def PID_regulator(current, reference, d_e, last_e, sum_e, max_output, max_int, K):
        (Kp, Ki, Kd, Kfd) = K
        e = reference - current
        d_e = (d_e*Kfd + (e - last_e))/(1+Kfd)
        output = Kp*e + Kd*d_e/REGUL_PERIOD + Ki*sum_e*REGUL_PERIOD

        last_e = e
        if e > 0:
            if Ki*sum_e*REGUL_PERIOD < max_int and output < max_output:
                sum_e += e
        else:
            if Ki*sum_e*REGUL_PERIOD > -max_int and output > -max_output:
                sum_e += e

        if output > max_output:
            output = max_output
        elif output <-max_output:
            output = -max_output
            
        return output, d_e, last_e, sum_e
    
    # PID regulator for obstacle avoidance
    def PIDwall(wall_dist, ref_dist, d_eL, last_eL, sum_eL):
        MAX_SPEEED = 0.18
        MAX_SPEEED_INT = 0.05
        DISTANCE_SLOW = 0.1
        eL = wall_dist-ref_dist-DISTANCE_SLOW
        if wall_dist-ref_dist > DISTANCE_SLOW:
            PID_L = 0
            d_eL = 0
            last_eL = 0
            sum_eL = 0
        else:
            d_eL = (d_eL*KfdL + (eL - last_eL))/(1+KfdL)
            PID_L = KpL*eL + KdL*d_eL/REGUL_PERIOD + KiL*sum_eL*REGUL_PERIOD
            last_eL = eL
            if eL > 0:
                if KiL*sum_eL*REGUL_PERIOD < MAX_SPEEED_INT:
                    sum_eL += eL
            else:
                if KiL*sum_eL*REGUL_PERIOD > -MAX_SPEEED_INT:
                    sum_eL += eL
        ref_ultra_speed = PID_L
        return ref_ultra_speed, d_eL, last_eL, sum_eL

    # Show info on display
    robot.display.fill(0)
    robot.display.centered_text("Segway running", 32)
    robot.display.show()

    # Initialize regulator constants
    KpS,KiS,KdS,KfdS,max_S,max_i_S = 0.3,0.55,-0.07,25,0.05,0.2
    KpA,KiA,KdA,KfdA,max_A,max_i_A = 1800,0,-500,80,100,0
    KpR,KiR,KdR,KfdR,max_R,max_i_R = 30,50,0,0,40,40
    KpL,KiL,KdL,KfdL,max_L,max_i_L = 0,0,0,0,0.23,0

    ref_position,ref_rotation,ref_speed = 0.0,0.0,0.0
    ref_angle,ref_ultra_speed,ref_dist = 0.0,0.0,0.3
    
    m1, m2, m1_dist, m2_dist = 0.0,0.0,0.0,0.0
    d_eA, last_eA, sum_eA = 0.0,0.0,0.0
    d_eS, last_eS, sum_eS = 0.0,0.0,0.0
    d_eP, last_eP, sum_eP = 0.0,0.0,0.0
    d_eR, last_eR, sum_eR = 0.0,0.0,0.0
    d_eL, last_eL, sum_eL = 0.0,0.0,0.0
    speed,rotation = 0.0, 0.0
    
    wall_dist = 0
    
    # Initialize ultrasonic sensor
    ultra_avg = StreamingMovingAverage(20)
    robot.init_sensor(sensor_type=Sensor.NXT_ULTRASONIC)

    # Initialize ESP communication in binary mode
    wifi = robot.esp.wifi()
    print("Wifi: ", wifi)
    if wifi:
        return 
    print("Name: ", robot.esp.set_name("Open-Cube-Wifi"))
    print("Password: ", robot.esp.set_password("01234567"))
        
    robot.esp.wifi_set_indicators_labels(("", "", "", "", ""))
    robot.esp.wifi_set_buttons_labels(("", "forward", "", "left", "", "right", "", "backward", "" ))
    robot.esp.wifi_set_switches_labels(("", "", "", "", ""))
    robot.esp.wifi_set_numbers_labels(("motorpwr:", "rot pwr:", "speed:", "rotation:", "ref s:", "ref r:"))

    # Initialize motors
    robot.init_motor(Port.M2)
    robot.init_motor(Port.M3)
    robot.motors[Port.M2].init_encoder()
    robot.motors[Port.M3].init_encoder()
    
    # Inititialize gyro and accelerometer with interrupt
    robot.init_sensor(sensor_type=Sensor.GYRO_ACC)
    gyro_start_time = utime.ticks_us()
    p0 = Pin(GyroAcc.IRQ_PIN, Pin.IN)
    p0.irq(trigger=Pin.IRQ_FALLING, handler=gyro_callback)
    
    
    counter = 0
    # Regulator loop
    while True:
        regul_start_time = utime.ticks_us()
        counter += 1

        e_angle = angle_compl-offset_angle
        position, speed, rotation, m1, m2, rot_speed = get_position(m1, m2, rotation)
        #if counter % 5 == 0:
        #    wall_dist = ultra_avg.process(robot.sensors.ultra_nxt.distance()/100)

        # Regulators
        #ref_speed, d_eP, last_eP, sum_eP = PID_regulator(position, ref_position, d_eP, last_eP, sum_eP, max_P, max_i_P, (KpP,KiP,KdP,KfdP))
        ref_ultra_speed, d_eL, last_eL, sum_eL = PIDwall(wall_dist, ref_dist, d_eL, last_eL, sum_eL)
        ref_angle, d_eS, last_eS, sum_eS = PID_regulator(speed,ref_speed+ref_ultra_speed, d_eS, last_eS, sum_eS, max_S, max_i_S, (KpS,KiS,KdS,KfdS))
        motor_power, d_eA, last_eA, sum_eA = PID_regulator(e_angle, ref_angle, d_eA, last_eA, sum_eA, max_A, max_i_A, (KpA,KiA,KdA,KfdA))
        rot_power, d_eR, last_eR, sum_eR = PID_regulator(rot_speed, ref_rotation, d_eR, last_eR, sum_eR, max_R, max_i_R, (KpR,KiR,KdR,KfdR))
        
        # Set motor power to zero if robot has fallen
        if(abs(e_angle) > 0.8):
            motor_power = 0
            rot_power = 0

        # Set motor power
        robot.motors[Port.M2].set_power(motor_power-rot_power)
        robot.motors[Port.M3].set_power(motor_power+rot_power)

        # Receive data from ESP and update regulator parameters
        esp_buttons = robot.esp.wifi_get_buttons()
        esp_switches = robot.esp.wifi_get_switches()
        if esp_switches[2]:
            MAX_SPEED = 0.18
            MAX_ROT_SPED = 2.5
        else:
            MAX_SPEED = 0.15
            MAX_ROT_SPED = 1
        SPEED_STEP = MAX_SPEED/150
        ROT_SPEED_STEP = MAX_ROT_SPED/50
        if esp_buttons[1]:
            if ref_speed < MAX_SPEED:
                ref_speed += SPEED_STEP
        elif esp_buttons[7]:
            if ref_speed > MAX_SPEED*(-1):
                ref_speed -= SPEED_STEP
        else:
            if ref_speed > 0:
                ref_speed -= SPEED_STEP
            elif ref_speed < 0:
                ref_speed += SPEED_STEP
        if esp_buttons[3]:
            if ref_rotation < MAX_ROT_SPED:
                ref_rotation += ROT_SPEED_STEP
        elif esp_buttons[5]:
            if ref_rotation > MAX_ROT_SPED*(-1):
                ref_rotation -= ROT_SPEED_STEP
        else:
            if ref_rotation > 0:
                ref_rotation -= ROT_SPEED_STEP
            elif ref_rotation < 0:
                ref_rotation += ROT_SPEED_STEP
        
        # Send data to ESP
        if counter % 20 == 0:
            robot.esp.wifi_set_numbers((motor_power, rot_power, speed, rot_speed, ref_speed, ref_rotation))
        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed()
        if buttons[Button.LEFT]:
            break

        # Sleep for the rest of the regulation period
        regul_new_time = utime.ticks_us()
        regul_time = utime.ticks_diff(regul_new_time, regul_start_time)
        if REGUL_PERIOD_US-regul_time > 0:
            utime.sleep_us(REGUL_PERIOD_US-regul_time)

    robot.deinit_all()

main()