'''
Simple example program for the Open-Cube BT Serial communication. Pair your device with Open-Cube while in menu first.
'''
# Import libraries
import time

# Acces global variable robot
global robot

send_data = 0.1

while True:
    # Check if any data received
    received_data = robot.esp.bt_read()
    if received_data:
        # Split received data and convert them from string to float format
        a, b, c = map(float, received_data.split(" "))
        # Show received values int Thonny console
        print(a, b, c) 

    # Send data to DataPlotter
    buffer = "$$P-," + str(send_data) + "," + str(-send_data) + ";"
    robot.esp.bt_write(buffer)
    send_data += 0.1

    # Do nothing for 0.1 s
    time.sleep(0.1)

    # Exit program if left cube button is pressed
    pressed = robot.buttons.pressed()
    if pressed[Button.LEFT]:
        break