'''
Simple example program for the Open-Cube RGB sensor on port S1.
Measure light intensity and show it on the display.
'''
# Import libraries
import time
from lib.OC import ColorSensor
from lib.robot_consts import Port, Button, Sensor

# Acces global variable robot before using sensors
global robot

# Initialize Open-Cube RGB sensor on port S1
robot.init_sensor(sensor_type=Sensor.OC_COLOR, port=Port.S1)

# Infinite program loop - get data from sensor and show it on display
while True:
    # Get data from sensor
    light_on, light_off = robot.sensors.light[Port.S1].reflection_raw()

    # Clear display frame buffer = fill it with color (0=black)
    robot.display.fill(0) 
    
    # Write text to the frame buffer
    # parameters are x and y coordinates in pixels (display is 128x64) and color (1=white)
    robot.display.text("Reflection raw:", 0, 0, 1)
    robot.display.text(f"On:  {light_on}", 0, 8, 1)
    robot.display.text(f"Off: {light_off}", 0, 16, 1)

    # Show frame buffer on display = refresh display with new text
    robot.display.show() 

    # Do nothing for 0.2 s
    time.sleep(0.2)

    # Exit program if left cube button is pressed
    pressed = robot.buttons.pressed()
    if pressed[Button.LEFT]:
        break
