'''
Example program of a simple line follower robot with 2 motors, 1 OC RGB sensor using a PID regulator.
Fill TODO parts with your own code.
'''
import time

from lib.robot_consts import Button, Port, Sensor, Light

# TODO Change these constants
REGULATION_PERIOD_MS = 10       # Regulation period 0.01 s <=> 100 Hz
MOTOR_BASE_POWER = 30           # %
LIGHT_SETPOINT = 50             # %
Kp, Ki, Kd = 1, 0, 0            # PID constants

# Acces global variable robot before using motors and sensors
global robot

# Initialize motors on ports M2 and M3
robot.init_motor(Port.M2)
robot.init_motor(Port.M3)

# Initialize Open-Cube RGB sensor on port S1
robot.init_sensor(sensor_type=Sensor.OC_COLOR, port=Port.S1)

# Regulator variables
e_sum = 0
e_prev = 0

# Follower regulation loop
while True:
    # Read light intensity
    light_intensity = robot.sensors.light[Port.S1].reflection() # 0-100%

    # TODO - PID regulator
    # 1. calculate error value e from light setpoint and measured ligth intensity
    e = 0

    # 2. calculate motor_pwr using error variables (e, e_sum, e_prev) and PID constants (Kp, Ki, Kd)
    motor_pwr = 0

    # 3. save previous error for the derivative part
    e_prev = 0

    # 4. Save sum of errors for the integral part
    e_sum = 0

    # Set motor power
    robot.motors[Port.M2].set_power(MOTOR_BASE_POWER + motor_pwr)
    robot.motors[Port.M3].set_power(MOTOR_BASE_POWER - motor_pwr)

    # Do nothing
    time.sleep_ms(REGULATION_PERIOD_MS)

    # Exit program if left cube button is pressed
    buttons = robot.buttons.pressed()
    if buttons[Button.LEFT]:
        break
