# Open-Cube firmware řídicí kostky a senzorů

[Oficiální web Open-Cube](https://open-cube.fel.cvut.cz/)

## Stažení firmware
 - [Open-Cube micropython firmware](https://gitlab.fel.cvut.cz/open-cube/firmware/-/blob/main/micropython/README.md)
 - [Open-Cube senzory firmware](https://gitlab.fel.cvut.cz/open-cube/firmware/-/blob/main/oc_sensors/CI_BUILDS.md)