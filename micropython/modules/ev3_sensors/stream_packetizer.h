/**
 * Helper module for splitting a flat byte stream into individual messages.
 */
#ifndef OPENCUBE_EV3_STREAM_PACKETIZER_H
#define OPENCUBE_EV3_STREAM_PACKETIZER_H

#include <stdint.h>
#include <opencube_uart_if.h>
#include "messages.h"

// Feedback from the message processing function
typedef enum {
  // Process more messages
  PROC_CONTINUE,
  // Something bad happened, stop
  PROC_STOP
} message_proc_result;

// Functions for processing received messages
typedef message_proc_result (*message_callback)(void* context, const uint8_t *data, const size_t length);

// State of the message packetizer
typedef struct {
  message_callback callback;
  void* callback_context;
  partial_message message;
  uint8_t buffer[EV3_MSG_BUFFER_SIZE];
  bool waiting_for_header;
  bool ir_sensor_workaround;
} packetizer_state;

/**
 * Reset the state of the message packetizer.
 * @param st Packetizer state.
 */
extern void packetizer_reset(packetizer_state *st);

/**
 * Repeatedly read and reassemble messages from the UART port
 * until the RX FIFO is empty or an error occurs.
 *
 * This function calls st->callback for processing the
 * individual received messages.
 *
 * @param st Packetizer state.
 * @param uart UART to read from.
 */
extern void packetizer_poll(packetizer_state *st, opencube_uart_if *uart);

#endif //OPENCUBE_EV3_STREAM_PACKETIZER_H
