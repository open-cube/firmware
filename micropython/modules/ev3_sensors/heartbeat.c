/**
 * Helper module for implementing periodic sensor heartbeat.
 *
 * This allows the brick to periodically send NACK messages
 * as sensor watchdog keep-alives. Conversely, if the sensor
 * does not respond to the NACK with new data soon, it is
 * considered to be responding.
 */
#include <hardware/timer.h>
#include "heartbeat.h"
#include "messages.h"

static bool has_hb_period_elapsed(hb_state *hb);
static void update_hb_stats(hb_state *hb);
static bool timeout_was_exceeded(hb_state *hb);
static void send_heartbeat(hb_state *hb, opencube_uart_if *uart);

void hb_start(hb_state *hb) {
  hb->usec_of_last_beat = time_us_64();
  hb->data_packets_since_beat = 0;
  hb->beats_without_data = 0;
  hb->responding = true;
  hb->ultrasonic_singleshot_workaround = false;
}

hb_sensor_status hb_update(hb_state *hb, opencube_uart_if *uart) {
  if (hb->responding && has_hb_period_elapsed(hb)) {
    update_hb_stats(hb);

    if (!timeout_was_exceeded(hb)) {
      send_heartbeat(hb, uart);
    } else {
      hb->responding = false;
    }
  }
  return hb->responding ? SENSOR_OK : SENSOR_TIMED_OUT;
}

bool has_hb_period_elapsed(hb_state *hb) {
  uint64_t now = time_us_32();
  uint32_t elapsed_ms = (now - hb->usec_of_last_beat) / 1000;
  return elapsed_ms >= EV3_HEARTBEAT_PERIOD_MS;
}

void update_hb_stats(hb_state *hb) {
  if (hb->data_packets_since_beat == 0) {
    hb->beats_without_data += 1;
  } else {
    hb->beats_without_data = 0;
  }
  hb->data_packets_since_beat = 0;
}

bool timeout_was_exceeded(hb_state *hb) {
  unsigned int threshold;
  if (!hb->ultrasonic_singleshot_workaround) {
    threshold = EV3_MAX_HEARTBEATS_WITHOUT_DATA;
  } else {
    threshold = EV3_MAX_HEARTBEATS_WITHOUT_DATA_LAX;
  }
  return hb->beats_without_data > threshold;
}

void send_heartbeat(hb_state *hb, opencube_uart_if *uart) {
  hb->usec_of_last_beat = time_us_32();
  uint8_t beat_msg = EV3_MSG_NACK;
  opencube_uart_write(uart, &beat_msg, sizeof(beat_msg));
}
