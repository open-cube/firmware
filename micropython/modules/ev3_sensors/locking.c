#include <pico/critical_section.h>
#include "locking.h"

static bool ev3_lock_initialized = false;
static critical_section_t ev3_crit_section;

void ev3_lock(void) {
  if (!ev3_lock_initialized) {
    ev3_lock_initialized = true;
    critical_section_init(&ev3_crit_section);
  }
  critical_section_enter_blocking(&ev3_crit_section);
}

void ev3_unlock(void) {
  critical_section_exit(&ev3_crit_section);
}
