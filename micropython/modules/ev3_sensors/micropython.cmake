# Create an INTERFACE library for our C module.
add_library(usermod_ev3_sensors INTERFACE)

# Add our source files to the lib
target_sources(usermod_ev3_sensors INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}/messages.h
    ${CMAKE_CURRENT_LIST_DIR}/messages.c
    ${CMAKE_CURRENT_LIST_DIR}/data_msg.h
    ${CMAKE_CURRENT_LIST_DIR}/data_msg.c
    ${CMAKE_CURRENT_LIST_DIR}/stream_sync.h
    ${CMAKE_CURRENT_LIST_DIR}/stream_sync.c
    ${CMAKE_CURRENT_LIST_DIR}/stream_packetizer.h
    ${CMAKE_CURRENT_LIST_DIR}/stream_packetizer.c
    ${CMAKE_CURRENT_LIST_DIR}/heartbeat.h
    ${CMAKE_CURRENT_LIST_DIR}/heartbeat.c
    ${CMAKE_CURRENT_LIST_DIR}/write_queue.h
    ${CMAKE_CURRENT_LIST_DIR}/write_queue.c
    ${CMAKE_CURRENT_LIST_DIR}/ev3_sensor.h
    ${CMAKE_CURRENT_LIST_DIR}/ev3_sensor.c
    ${CMAKE_CURRENT_LIST_DIR}/sensor_info.h
    ${CMAKE_CURRENT_LIST_DIR}/sensor_info.c
    ${CMAKE_CURRENT_LIST_DIR}/locking.h
    ${CMAKE_CURRENT_LIST_DIR}/locking.c
    ${CMAKE_CURRENT_LIST_DIR}/micropython_api.c
)

# Add the current directory as an include directory.
target_include_directories(usermod_ev3_sensors INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}
)

target_link_libraries(usermod_ev3_sensors INTERFACE
    usermod_opencube_pindefs
    usermod_opencube_uart
)

# Link our INTERFACE library to the usermod target.
target_link_libraries(usermod INTERFACE usermod_ev3_sensors)
