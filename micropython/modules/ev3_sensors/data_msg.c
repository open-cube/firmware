#include <string.h>
#include "data_msg.h"
#include "locking.h"

void ev3_data_init(data_handler *dh) {
  dh->have_data = false;
}

void ev3_data_deinit(data_handler *dh) {}

void ev3_data_import(data_handler *dh, const uint8_t *msg, size_t len) {
  unsigned int avail_data_bytes = len - EV3_NORMAL_MSG_FRAME_OVERHEAD;
  unsigned int to_copy = avail_data_bytes > EV3_MAX_DATA_BYTES ? EV3_MAX_DATA_BYTES : avail_data_bytes;

  ev3_lock();
  dh->have_data = true;
  dh->last_msr.mode = subtype_of_message(msg[0]);
  dh->last_msr.received_at_usec = time_us_32();
  memcpy(&dh->last_msr.data[0], msg + 1, to_copy);
  ev3_unlock();
}

bool ev3_data_read(data_handler *dh, ev3_measurement *out) {
  bool ok = false;
  ev3_lock();
  if (dh->have_data) {
    ok = true;
    if (out) {
      *out = dh->last_msr;
    }
  }
  ev3_unlock();
  return ok;
}

void ev3_data_invalidate(data_handler *dh) {
  dh->have_data = false;
  __mem_fence_release();
}

bool should_ignore_data_checksum_error(uint8_t sensor_id, uint8_t msg_header) {
  return sensor_id == EV3_COLOR_SENSOR_ID &&
         type_of_message(msg_header) == MTYPE_DATA &&
         subtype_of_message(msg_header) == EV3_COLOR_SENSOR_RGB_RAW_MODE;
}
