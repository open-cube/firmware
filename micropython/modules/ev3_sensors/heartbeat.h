/**
 * Helper module for implementing periodic sensor heartbeat.
 *
 * This allows the brick to periodically send NACK messages
 * as sensor watchdog keep-alives. Conversely, if the sensor
 * does not respond to the NACK with new data soon, it is
 * considered to be responding.
 */
#ifndef OPENCUBE_HEARTBEAT_H
#define OPENCUBE_HEARTBEAT_H

#include <stdint.h>
#include <opencube_uart_if.h>

// State of the sensor heartbeat tracker
typedef struct {
  // timestamp at which the last NACK was sent
  uint32_t usec_of_last_beat;
  // How many data packets has the sensor sent since last NACK
  unsigned int data_packets_since_beat;
  // How many NACKs were sent without the sensor sending any DATA message back
  unsigned int beats_without_data;
  // Whether the sensor is still sending DATA messages quickly enough.
  bool responding;
  // Whether the NACK timeout should be made longer
  bool ultrasonic_singleshot_workaround;
} hb_state;

typedef enum {
  SENSOR_OK,
  SENSOR_TIMED_OUT,
} hb_sensor_status;

/**
 * Reinitialize the heartbeat state.
 * @param hb Heartbeat state.
 */
extern void hb_start(hb_state *hb);

/**
 * Perform various heartbeat timekeeping activities:
 *  - sending NACKs every 100 ms
 *  - check that the sensor is responding
 * @param hb Heartbeat state.
 * @param uart UART to which to write NACK messages
 * @return Whether the sensor is still considered to be plugged in or not.
 */
extern hb_sensor_status hb_update(hb_state *hb, opencube_uart_if *uart);

/**
 * Record that a sensor has sent a valid DATA message.
 * @param hb Heartbeat state.
 */
static inline void hb_got_message(hb_state *hb) {
  hb->data_packets_since_beat++;
}


#endif //OPENCUBE_HEARTBEAT_H
