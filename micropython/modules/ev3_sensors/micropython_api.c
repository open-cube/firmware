// based on https://github.com/micropython/micropython/blob/master/examples/usercmodule/cexample/examplemodule.c

#include <py/mphal.h>
#include "py/runtime.h"
#include "ev3_sensor.h"

#define COMMAND_SIZE_LIMIT 16
#define SENSOR_SWITCH_DELAY 1
#define SENSOR_SWITCH_MAX_ATTEMPTS 20
#define DEFAULT_WAIT_TIMEOUT_MS 3000
#define DISABLE_ID_CHECKS (-2) // -1 is returned from ev3_sensor_identify() when no sensor is present

MP_DEFINE_EXCEPTION(PortAlreadyOpenError, RuntimeError)
MP_DEFINE_EXCEPTION(SensorNotReadyError, RuntimeError)
MP_DEFINE_EXCEPTION(SensorMismatchError, RuntimeError)

/**
 * See <repo root>/micropython/typing/ev3_sensor_ll.pyi for API docs
 */

typedef struct {
  mp_obj_base_t base;
  ev3_sensor impl;
  ev3_measurement msr_space;
  bool is_closed;
  int proper_id;
} ev3_api_obj_t;

static mp_obj_t new_data_memoryview(ev3_api_obj_t *self);
static void raise_error_if_closed(ev3_api_obj_t *self);
static void try_wait_for_sensor(ev3_api_obj_t *self, uint timeout_ms);
static void try_wait_for_any_sensor(ev3_api_obj_t *self, uint timeout_ms);
static void try_check_sensor_id(ev3_api_obj_t *self);

STATIC mp_obj_t ev3_api_make_new(const mp_obj_type_t *type, size_t n_args, size_t n_kw, const mp_obj_t *args) {
  mp_arg_check_num(n_args, n_kw, 2, 2, false);

  int port = mp_obj_get_int(args[0]);
  if (port < 0 || port > 3) {
    mp_raise_msg_varg(
        &mp_type_ValueError,
        MP_ERROR_TEXT("Sensor port index %d is not valid"),
        port
    );
  }

  int sensor_id = DISABLE_ID_CHECKS;
  if (args[1] != mp_const_none) {
    sensor_id = mp_obj_get_int(args[1]);
    if (sensor_id < 0) {
      mp_raise_msg_varg(
          &mp_type_ValueError,
          MP_ERROR_TEXT("Sensor ID %d is not valid"),
          sensor_id
      );
    }
  }

  ev3_api_obj_t *self = m_new_obj_with_finaliser(ev3_api_obj_t);
  self->base.type = (mp_obj_type_t *) type;
  self->is_closed = false;
  self->proper_id = sensor_id;
  if (ev3_sensor_open(port, &self->impl) < 0) {
    self->is_closed = true;
    m_del_obj(ev3_api_obj_t, self);
    mp_raise_msg_varg(
        &mp_type_PortAlreadyOpenError,
        MP_ERROR_TEXT("Sensor port %d is already open"),
        port + 1 // more user-friendly
    );
  }

  return MP_OBJ_FROM_PTR(self);
}


STATIC mp_obj_t ev3_api_finalizer(mp_obj_t self_in) {
  ev3_api_obj_t *self = MP_OBJ_TO_PTR(self_in);

  if (!self->is_closed) {
    ev3_sensor_close(&self->impl);
    self->is_closed = true;
  }
  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(ev3_api_finalizer_obj, ev3_api_finalizer);


STATIC mp_obj_t ev3_api_is_connected(mp_obj_t self_in) {
  ev3_api_obj_t *self = MP_OBJ_TO_PTR(self_in);

  return mp_obj_new_bool(
      !self->is_closed && ev3_sensor_is_connected(&self->impl)
  );
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(ev3_api_is_connected_obj, ev3_api_is_connected);


STATIC mp_obj_t ev3_api_is_ready(mp_obj_t self_in) {
  ev3_api_obj_t *self = MP_OBJ_TO_PTR(self_in);

  return mp_obj_new_bool(
      !self->is_closed && ev3_sensor_is_ready(&self->impl)
  );
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(ev3_api_is_ready_obj, ev3_api_is_ready);


STATIC mp_obj_t ev3_api_set_mode(mp_obj_t self_in, mp_obj_t mode_in) {
  ev3_api_obj_t *self = MP_OBJ_TO_PTR(self_in);
  raise_error_if_closed(self);
  try_wait_for_sensor(self, DEFAULT_WAIT_TIMEOUT_MS);

  mp_int_t mode_number = mp_obj_get_int(mode_in);
  if (mode_number < 0 || mode_number >= MAX_SENSOR_MODE_COUNT) {
    mp_raise_msg_varg(
        &mp_type_ValueError,
        MP_ERROR_TEXT("Mode number %d is out of valid range"),
        mode_number
    );
  }

  if (!ev3_sensor_set_mode(&self->impl, mode_number)) {
    mp_raise_msg(&mp_type_RuntimeError, MP_ERROR_TEXT("Cannot set mode - write queue is full"));
  }
  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_2(ev3_api_set_mode_obj, ev3_api_set_mode);


STATIC mp_obj_t ev3_api_write_command(mp_obj_t self_in, mp_obj_t buffer_in) {
  ev3_api_obj_t *self = MP_OBJ_TO_PTR(self_in);
  raise_error_if_closed(self);
  try_wait_for_sensor(self, DEFAULT_WAIT_TIMEOUT_MS);

  mp_buffer_info_t buffer;
  mp_get_buffer_raise(buffer_in, &buffer, MP_BUFFER_READ);

  if (buffer.len > COMMAND_SIZE_LIMIT) {
    mp_raise_ValueError("The provided command buffer is too large");
  }

  if (!ev3_sensor_write_command(&self->impl, buffer.buf, buffer.len)) {
    mp_raise_msg(&mp_type_RuntimeError, MP_ERROR_TEXT("Cannot send direct command - write queue is full"));
  }
  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_2(ev3_api_write_command_obj, ev3_api_write_command);


STATIC mp_obj_t ev3_api_reset(mp_obj_t self_in) {
  ev3_api_obj_t *self = MP_OBJ_TO_PTR(self_in);
  raise_error_if_closed(self);

  ev3_sensor_reset(&self->impl);
  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(ev3_api_reset_obj, ev3_api_reset);


STATIC mp_obj_t ev3_api_read(mp_obj_t self_in) {
  ev3_api_obj_t *self = MP_OBJ_TO_PTR(self_in);
  raise_error_if_closed(self);
  try_wait_for_sensor(self, DEFAULT_WAIT_TIMEOUT_MS);

  if (ev3_sensor_read(&self->impl, &self->msr_space)) {
    mp_obj_t tuple_objects[2] = {
        mp_obj_new_int_from_uint(self->msr_space.mode),
        new_data_memoryview(self)
    };

    return mp_obj_new_tuple(MP_ARRAY_SIZE(tuple_objects), tuple_objects);
  } else {
    mp_raise_msg(&mp_type_SensorNotReadyError, "Sensor has not sent data yet");
  }
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(ev3_api_read_obj, ev3_api_read);


STATIC mp_obj_t ev3_api_read_mode(mp_obj_t self_in, mp_obj_t mode_in) {
  ev3_api_obj_t *self = MP_OBJ_TO_PTR(self_in);
  raise_error_if_closed(self);
  try_wait_for_sensor(self, DEFAULT_WAIT_TIMEOUT_MS);

  mp_int_t mode = mp_obj_get_int(mode_in);
  if (mode < 0 || mode > MAX_SENSOR_MODE_COUNT) {
    mp_raise_msg_varg(
        &mp_type_ValueError,
        MP_ERROR_TEXT("Mode number %d is out of valid range"),
        mode
    );
  }

  bool done = false;
  for (int i = 0; i < SENSOR_SWITCH_MAX_ATTEMPTS; i++) {
    if (ev3_sensor_read(&self->impl, &self->msr_space) && self->msr_space.mode == mode) {
      done = true;
      break;
    }
    if (ev3_sensor_is_connected(&self->impl)) {
      (void) ev3_sensor_set_mode(&self->impl, mode); // ignore: even if wq is full, try later again to reduce wait time
    } else {
      mp_raise_msg(&mp_type_SensorNotReadyError, "Sensor has been unplugged");
    }
    mp_hal_delay_ms(i*SENSOR_SWITCH_DELAY);
  }
  if (!done) {
    mp_raise_msg(&mp_type_SensorNotReadyError, "Sensor mode switch timed out");
  }

  return new_data_memoryview(self);
}

STATIC MP_DEFINE_CONST_FUN_OBJ_2(ev3_api_read_mode_obj, ev3_api_read_mode);


STATIC mp_obj_t ev3_api_get_sensor_id(mp_obj_t self_in) {
  ev3_api_obj_t *self = MP_OBJ_TO_PTR(self_in);
  raise_error_if_closed(self);

  int id = ev3_sensor_identify(&self->impl);
  if (id >= 0) {
    return mp_obj_new_int(id);
  } else {
    return mp_const_none;
  }
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(ev3_api_get_sensor_id_obj, ev3_api_get_sensor_id);

STATIC mp_obj_t ev3_api_waitfor(size_t n_args, const mp_obj_t *args) {
  ev3_api_obj_t *self = MP_OBJ_TO_PTR(args[0]);
  raise_error_if_closed(self);

  int max_ms = DEFAULT_WAIT_TIMEOUT_MS;
  if (n_args == 2) {
    max_ms = mp_obj_get_int(args[1]);
  }

  try_wait_for_sensor(self, max_ms);
  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(ev3_api_waitfor_obj, 1, 2, ev3_api_waitfor);


mp_obj_t new_data_memoryview(ev3_api_obj_t *self) {
  data_miniinfo format;
  if (!ev3_sensor_describe_data(&self->impl, self->msr_space.mode, &format)) {
    mp_raise_msg(&mp_type_SensorNotReadyError, "Data description is not ready");
  }

  char typecode;
  switch (format.format) {
    case FORMAT_INT_8:
    default:
      typecode = 'b';
      break;
    case FORMAT_INT_16:
      typecode = 'h';
      break;
    case FORMAT_INT_32:
      typecode = 'l';
      break;
    case FORMAT_FLOAT:
      typecode = 'f';
      break;
  }
  if (!format.is_signed) {
    typecode += 'A' - 'a';
  }

  return mp_obj_new_memoryview(typecode, format.num_values, self->msr_space.data);
}

void raise_error_if_closed(ev3_api_obj_t *self) {
  if (self->is_closed) {
    mp_raise_msg(&mp_type_SensorNotReadyError, "Sensor object has been closed");
  }
}

void try_wait_for_sensor(ev3_api_obj_t *self, uint timeout_ms) {
  if (!ev3_sensor_is_ready(&self->impl)) {
    try_wait_for_any_sensor(self, timeout_ms);
  }
  if (self->proper_id != DISABLE_ID_CHECKS) {
    try_check_sensor_id(self);
  }
}

void try_wait_for_any_sensor(ev3_api_obj_t *self, uint timeout_ms) {
  bool succeeded;
  uint started_at = mp_hal_ticks_ms();
  do {
    mp_hal_delay_ms(1);
    succeeded = ev3_sensor_is_ready(&self->impl);
  } while (!succeeded && (mp_hal_ticks_ms() - started_at) < timeout_ms);

  if (!succeeded) {
    mp_raise_msg_varg(
        &mp_type_SensorNotReadyError,
        MP_ERROR_TEXT("No sensor is connected to port %d"),
        (int) self->impl.uart.port + 1
    );
  }
}

void try_check_sensor_id(ev3_api_obj_t *self) {
  int received_id = ev3_sensor_identify(&self->impl);
  if (received_id != self->proper_id) {
    mp_raise_msg_varg(
        &mp_type_SensorMismatchError,
        MP_ERROR_TEXT("Incorrect sensor is connected to port %d: got ID %d, expected ID %d"),
        (int) self->impl.uart.port + 1,
        received_id,
        self->proper_id
    );
  }
}

STATIC const mp_rom_map_elem_t ev3_api_locals_dict_table[] = {
    {MP_ROM_QSTR(MP_QSTR_close),               MP_ROM_PTR(&ev3_api_finalizer_obj)},
    {MP_ROM_QSTR(MP_QSTR___del__),             MP_ROM_PTR(&ev3_api_finalizer_obj)},
    {MP_ROM_QSTR(MP_QSTR_is_connected),        MP_ROM_PTR(&ev3_api_is_connected_obj)},
    {MP_ROM_QSTR(MP_QSTR_is_ready),            MP_ROM_PTR(&ev3_api_is_ready_obj)},
    {MP_ROM_QSTR(MP_QSTR_start_set_mode),      MP_ROM_PTR(&ev3_api_set_mode_obj)},
    {MP_ROM_QSTR(MP_QSTR_write_command),       MP_ROM_PTR(&ev3_api_write_command_obj)},
    {MP_ROM_QSTR(MP_QSTR_start_reset),         MP_ROM_PTR(&ev3_api_reset_obj)},
    {MP_ROM_QSTR(MP_QSTR_read_raw),            MP_ROM_PTR(&ev3_api_read_obj)},
    {MP_ROM_QSTR(MP_QSTR_read_raw_mode),       MP_ROM_PTR(&ev3_api_read_mode_obj)},
    {MP_ROM_QSTR(MP_QSTR_sensor_id),           MP_ROM_PTR(&ev3_api_get_sensor_id_obj)},
    {MP_ROM_QSTR(MP_QSTR_wait_for_connection), MP_ROM_PTR(&ev3_api_waitfor_obj)},
};
STATIC MP_DEFINE_CONST_DICT(ev3_api_locals_dict, ev3_api_locals_dict_table);

MP_DEFINE_CONST_OBJ_TYPE(
    ev3_api,
    MP_QSTR_EV3Sensor,
    MP_TYPE_FLAG_NONE,
    make_new, &ev3_api_make_new,
    locals_dict, &ev3_api_locals_dict
);

STATIC const mp_rom_map_elem_t ev3_module_globals_table[] = {
    {MP_ROM_QSTR(MP_QSTR___name__),             MP_ROM_QSTR(MP_QSTR_ev3_sensor_ll)},
    {MP_ROM_QSTR(MP_QSTR_EV3Sensor),            MP_ROM_PTR(&ev3_api)},
    {MP_ROM_QSTR(MP_QSTR_SensorNotReadyError),  MP_ROM_PTR(&mp_type_SensorNotReadyError)},
    {MP_ROM_QSTR(MP_QSTR_PortAlreadyOpenError), MP_ROM_PTR(&mp_type_PortAlreadyOpenError)},
    {MP_ROM_QSTR(MP_QSTR_SensorMismatchError),  MP_ROM_PTR(&mp_type_SensorMismatchError)},
};
STATIC MP_DEFINE_CONST_DICT(ev3_module_globals, ev3_module_globals_table);

const mp_obj_module_t ev3_module = {
    .base = {&mp_type_module},
    .globals = (mp_obj_dict_t *) &ev3_module_globals
};

MP_REGISTER_MODULE(MP_QSTR_ev3_sensor_ll, ev3_module);
