/**
 * Helper module for storing last measurement sent over by the sensor
 */
#ifndef OPENCUBE_EV3_DATA_MSG_H
#define OPENCUBE_EV3_DATA_MSG_H

#include <stdint.h>
#include <pico/types.h>
#include <pico/critical_section.h>
#include "messages.h"

// Structure for representing a single data packet from the sensor
typedef struct {
  // When was the packet received (might be useful for checking the age of the packet)
  uint32_t received_at_usec;
  // Measurement mode to which the data packet corresponds
  uint mode;
  // Raw data buffer from the sensor
  uint8_t data[EV3_MAX_DATA_BYTES];
} ev3_measurement;

// Helper object for holding the last received data message
typedef struct {
  // Last received data message
  ev3_measurement last_msr;
  // Whether last_msr contains a valid message
  bool have_data;
} data_handler;

/**
 * Initialize the data holder object.
 * @param dh Data holder object to use.
 */
extern void ev3_data_init(data_handler *dh);

/**
 * Release all memory held by the data holder object.
 * @param dh Data holder object to use.
 */
extern void ev3_data_deinit(data_handler *dh);

/**
 * Parse and store the contents of a DATA message from the sensor.
 * @param dh Data holder object to use.
 * @param msg Buffer containing the received message.
 * @param len Total length of the received message.
 */
extern void ev3_data_import(data_handler *dh, const uint8_t *msg, size_t len);

/**
 * Query the last reading received from the sensor.
 * @param dh Data holder object to use.
 * @param out Where to put the measurement.
 * @return Whether out was updated, i.e. if the sensor has sent any data.
 */
extern bool ev3_data_read(data_handler *dh, ev3_measurement *out);

/**
 * Consider the last received reading to be invalid.
 * This will make ev3_data_read() fail until
 * ev3_data_import() is called again.
 *
 * @param dh Data holder object to use.
 */
extern void ev3_data_invalidate(data_handler *dh);

/**
 * Check whether a given sensor mode can sometimes produce valid messages
 * with an invalid checksum.
 *
 * @param sensor_id ID of the sensor
 * @param msg_header Header of the received message
 * @return True if invalid checksum is to be expected.
 */
extern bool should_ignore_data_checksum_error(uint8_t sensor_id, uint8_t msg_header);

#endif //OPENCUBE_EV3_DATA_MSG_H
