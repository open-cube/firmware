/**
 * Simple queue for sensor write requests in DATA mode
 */
#ifndef OPENCUBE_EV3_WRITE_QUEUE
#define OPENCUBE_EV3_WRITE_QUEUE

#include <pico/critical_section.h>

#define EV3WQ_MAX_WRITE_BYTES 19 // produces nice alignment
#define EV3WQ_MAX_QUEUED_WRITES 2 // 1 mode change + 1 direct write

// Queue entry
typedef struct sensor_wq_entry {
  uint8_t data_len;
  uint8_t data[EV3WQ_MAX_WRITE_BYTES];
} sensor_wq_entry;

// Queue for write requests
typedef struct sensor_wq {
  // Preallocated slots
  sensor_wq_entry slots[EV3WQ_MAX_QUEUED_WRITES];
} sensor_wq;

/**
 * Initialize a write queue.
 */
extern void sensor_wq_init(sensor_wq *wq);

/**
 * Deinitialize a write queue and free its all resources.
 */
extern void sensor_wq_deinit(sensor_wq *wq);


/**
 * Enqueue a new write request.
 * @param data Request data
 * @param len Length of the data
 * @return True if the request could be queued, false otherwise
 */
extern bool sensor_wq_push(sensor_wq *wq, const uint8_t *data, uint8_t len) __attribute__((warn_unused_result));

/**
 * Dequeue a next pending write request.
 * @return NULL if the queue is empty; queue head otherwise.
 */
extern sensor_wq_entry *sensor_wq_peek(sensor_wq *wq) __attribute__((warn_unused_result));

/**
 * Release a pop-ed queue entry
 * @param e Entry to release
 */
extern void sensor_wq_pop(sensor_wq_entry *e);

/**
 * Remove and free all entries from the queue.
 */
extern void sensor_wq_clear(sensor_wq *wq);

#endif //OPENCUBE_EV3_WRITE_QUEUE
