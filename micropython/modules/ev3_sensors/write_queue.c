/**
 * Simple queue for sensor write requests in DATA mode
 */
#include <malloc.h>
#include <memory.h>
#include "write_queue.h"
#include "locking.h"

static sensor_wq_entry *find_first_free(sensor_wq *wq);
static sensor_wq_entry *find_first_queued(sensor_wq *wq);

void sensor_wq_init(sensor_wq *wq) {
  memset(wq, 0, sizeof(sensor_wq));
}

void sensor_wq_deinit(sensor_wq *wq) {
  sensor_wq_clear(wq);
}

bool sensor_wq_push(sensor_wq *wq, const uint8_t *data, uint8_t len) {
  // always fail big writes
  if (len > EV3WQ_MAX_WRITE_BYTES) return false;

  // null writes are noop
  if (len == 0) return true;

  // try to queue
  ev3_lock();
  sensor_wq_entry *e = find_first_free(wq);
  if (e != NULL) {
    memcpy(e->data, data, len);
    e->data_len = len;
  }
  ev3_unlock();

  return e != NULL;
}

sensor_wq_entry *sensor_wq_peek(sensor_wq *wq) {
  sensor_wq_entry *result = NULL;
  ev3_lock();
  result = find_first_queued(wq);
  ev3_unlock();
  return result;
}

void sensor_wq_pop(sensor_wq_entry *e) {
  ev3_lock();
  e->data_len = 0;
  ev3_unlock();
}

sensor_wq_entry *find_first_free(sensor_wq *wq) {
  for (int i = 0; i < EV3WQ_MAX_QUEUED_WRITES; i++) {
    if (wq->slots[i].data_len == 0) {
      return &wq->slots[i];
    }
  }
  return NULL;
}

sensor_wq_entry *find_first_queued(sensor_wq *wq) {
  for (int i = 0; i < EV3WQ_MAX_QUEUED_WRITES; i++) {
    if (wq->slots[i].data_len != 0) {
      return &wq->slots[i];
    }
  }
  return NULL;
}

void sensor_wq_clear(sensor_wq *wq) {
  ev3_lock();
  for (int i = 0; i < EV3WQ_MAX_QUEUED_WRITES; i++) {
    wq->slots[i].data_len = 0;
  }
  ev3_unlock();
}
