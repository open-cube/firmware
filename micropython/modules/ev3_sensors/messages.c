/**
 * Definitions and helper functions for decoding EV3 UART packets
 */
#include "messages.h"

static uint8_t make_message_payload_len(unsigned int payload_len, size_t *padded_len);


uint8_t calculate_message_checksum(const uint8_t *buffer, size_t length) {
  uint8_t csum = 0xFF;
  for (size_t i = 0; i < length; i++) {
    csum ^= buffer[i];
  }
  return csum;
}

uint8_t make_message_header(ev3_msg_type type, uint subtype, uint payload_length, size_t *alloc_length) {
  uint8_t header = 0;
  header |= (type & EV3_MESSAGE_TYPE_BITS) << EV3_MESSAGE_TYPE_SHIFT;
  header |= (subtype & EV3_MESSAGE_SUBTYPE_BITS) << EV3_MESSAGE_SUBTYPE_SHIFT;
  header |= make_message_payload_len(payload_length, NULL);
  if (alloc_length) {
    *alloc_length = full_length_of_message(header);
  }
  return header;
}

size_t full_length_of_message(uint8_t header) {
  ev3_msg_type mtype = type_of_message(header);

  if (mtype == MTYPE_SYSTEM) {
    return 1;
  }

  unsigned int payload_len = payload_len_of_message(header);

  if (mtype == MTYPE_INFO) {
    return payload_len + EV3_INFO_MSG_FRAME_OVERHEAD;
  } else {
    return payload_len + EV3_NORMAL_MSG_FRAME_OVERHEAD;
  }
}

uint8_t make_message_payload_len(unsigned int payload_len, size_t *padded_len) {
  int exponent;
  if (payload_len <= 1) {
    exponent = 0;
  } else if (payload_len <= 2) {
    exponent = 1;
  } else if (payload_len <= 4) {
    exponent = 2;
  } else if (payload_len <= 8) {
    exponent = 3;
  } else if (payload_len <= 16) {
    exponent = 4;
  } else if (payload_len <= 32) {
    exponent = 5;
  } else {
    panic("message too large");
  }
  if (padded_len != NULL) {
    *padded_len = 1 << exponent;
  }
  return (exponent & EV3_MESSAGE_LENGTH_BITS) << EV3_MESSAGE_LENGTH_SHIFT;
}
