/**
 * Helper module for finding the start of sensor handshake
 */
#ifndef OPENCUBE_STREAM_SYNC_H
#define OPENCUBE_STREAM_SYNC_H

#include <stdint.h>
#include <opencube_uart_if.h>

typedef struct {
  uint8_t message_window[3];
} synchronizer_state;

/**
 * Clear the current rolling window
 * @param sn Synchronizer state
 */
extern void synchronizer_reset(synchronizer_state *sn);

/**
 * Read bytes from the port until the FIFO is empty or
 * an initial message is detected.
 *
 * @param sn Synchronizer state
 * @param uart UART
 * @return -1 if the sync has not yet been acquired.
 *         Sensor ID (>= 0) if the initial message has been successfully decoded.
 */
extern int synchronizer_poll(synchronizer_state *sn, opencube_uart_if *uart);


#endif //OPENCUBE_STREAM_SYNC_H
