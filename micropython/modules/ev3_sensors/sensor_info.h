/**
 * Structures for storing sensor self-description information.
 */
#ifndef OPENCUBE_EV3_SENSOR_INFO_H
#define OPENCUBE_EV3_SENSOR_INFO_H

#include <stdint.h>
#include <stdbool.h>
#include "messages.h"

#define FULL_TYPEDATA 0

// Data type of values returned by the sensor for a given mode
typedef enum {
  FORMAT_INT_8 = 0,
  FORMAT_INT_16 = 1,
  FORMAT_INT_32 = 2,
  FORMAT_FLOAT = 3
} data_format;

// Scaling limits
typedef struct {
  float min;
  float max;
} value_limits;

#define MAX_SENSOR_NAME_LEN 8
// Information about a sensor mode
typedef struct {
#if FULL_TYPEDATA
  // Mode value range in SI units
  value_limits si_limit;
  // Mode value range in percent of full scale (does not have to be 0-100!)
  value_limits pct_limit;
  // Value range sent by the sensor
  value_limits raw_limit;
#endif
  // Data type received in the data message
  data_format format;
  // Number of values returned
  uint8_t values;
#if FULL_TYPEDATA
  // Total value width (can be used for printf specification). This is likely related to the SI value.
  uint figures;
  // How many places after a decimal point are valid (can be used for printf). This is likely related to the SI value.
  uint decimals;

  // Mode name (e.g. COL-REFLECT)
  char name[MAX_SENSOR_NAME_LEN+1];
  // SI unit (e.g. deg)
  char unit[MAX_SENSOR_NAME_LEN+1];
#endif
} sensor_mode;

#define MAX_SENSOR_MODE_COUNT 8
// Information about a sensor
typedef struct {
  // Communication speed used during the data phase. Specified in baud.
  uint32_t baud_rate;
  // ID of the sensor (see ev3_sensor_id)
  ev3_sensor_id id;
  // Number of modes supported by the sensor
  uint mode_count;
#if FULL_TYPEDATA
  // Number of modes that should be made visible to the user (this excludes
  // some advanced & factory-oriented modes)
  uint visible_mode_count;
#endif
  // Information about individual modes supported by the sensor
  sensor_mode modes[MAX_SENSOR_MODE_COUNT];
} sensor_info;

/**
 * Initialize an object for holding information about a sensor.
 * @param info Sensor information storage object.
 */
extern void sensor_info_init(sensor_info *info);

/**
 * Release all memory held in the sensor information object.
 * @param info Sensor information storage object.
 */
extern void sensor_info_deinit(sensor_info *info);

/**
 * Update sensor information based on a new INFO or CMD message received from the sensor.
 * @param info Sensor information storage object.
 * @param data Buffer containing the received message.
 * @param len Total length of the received message.
 */
extern void sensor_info_update(sensor_info *info, const uint8_t *data, size_t len);

/**
 * Initialize default values for information about a mode.
 * @param info Mode information storage object.
 */
extern void sensor_mode_init(sensor_mode *info);

/**
 * Update mode information based on a new INFO message received from the sensor.
 * @param info Mode information storage object.
 * @param data Buffer containing the received message.
 * @param len Total length of the received message.
 */
extern void sensor_mode_update(sensor_mode *info, const uint8_t *data, size_t len);

#endif //OPENCUBE_EV3_SENSOR_INFO_H
