/**
 * Helper module for splitting a flat byte stream into individual messages.
 */
#include "stream_packetizer.h"

typedef enum {
  NOTYET,
  DONE
} completion;

static completion try_read_header(packetizer_state *st, opencube_uart_if *uart);
static completion try_complete_message(packetizer_state *st, opencube_uart_if *uart);

void packetizer_reset(packetizer_state *st) {
  st->message.done_size = 0;
  st->message.total_size = 0;
  st->message.buffer = st->buffer;
  st->waiting_for_header = true;
  st->ir_sensor_workaround = false;
}

void packetizer_poll(packetizer_state *st, opencube_uart_if *uart) {
  while (true) {
    if (st->waiting_for_header && try_read_header(st, uart) == NOTYET) {
      break;
    }

    if (try_complete_message(st, uart) == DONE) {
      st->waiting_for_header = true;
      if (st->ir_sensor_workaround && st->message.buffer[0] == EV3_MSG_SYNC) {
        // skip broken SYNC packets from EV3 IR sensor
        continue;
      }
      if (st->callback(st->callback_context, st->message.buffer, st->message.total_size) == PROC_STOP) {
        break;
      }
    } else {
      // no more data
      st->waiting_for_header = false;
      break;
    }
  }
}

completion try_read_header(packetizer_state *st, opencube_uart_if *uart) {
  int header = opencube_uart_read_one(uart);
  if (header == -1) {
    // no more data
    return NOTYET;
  }

  st->message.buffer[0] = header;
  st->message.done_size = 1;

  if (st->ir_sensor_workaround && header == EV3_MSG_SYNC) {
    // The IR sensor sometimes sends 0x00 0xFF packets during the
    // handshake - they look like broken SYNC packages. They are useless,
    // so just detect them here and later skip them
    st->message.total_size = 2;
  } else {
    st->message.total_size = full_length_of_message(header);
    if (st->message.total_size > EV3_MSG_BUFFER_SIZE) {
      st->message.total_size = EV3_MSG_BUFFER_SIZE;
    }
  }
  return DONE;
}

completion try_complete_message(packetizer_state *st, opencube_uart_if *uart) {
  bool done = (st->message.total_size == st->message.done_size) ||
              opencube_uart_read_many(uart, &st->message);
  return done ? DONE : NOTYET;
}
