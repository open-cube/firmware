#ifndef OPENCUBE_EV3_SENSOR_H
#define OPENCUBE_EV3_SENSOR_H

#include <pico/time.h>
#include <pico/mutex.h>
#include "stream_sync.h"
#include "stream_packetizer.h"
#include "heartbeat.h"
#include "sensor_info.h"
#include "write_queue.h"
#include "data_msg.h"

// EV3 protocol state machine
typedef enum {
  // Transitioning to 2400 Bd (initialization comm speed)
  EV3_SETTING_LOW_BAUD,
  // Waiting for initial sensor message
  EV3_WAITING_FOR_SYNC,
  // Receiving sensor metadata
  EV3_PROCESSING_HANDSHAKE,
  // Transitioning to data-phase speed (typically 57600 Bd)
  EV3_SETTING_HIGH_BAUD,
  // Receiving data from the sensor
  EV3_DATA_RUNNING
} sensor_status;

typedef struct {
  opencube_uart_if uart;
  alarm_pool_t *pool;
  repeating_timer_t poll_callback;
  synchronizer_state synchronizer;
  packetizer_state packetizer;
  hb_state heartbeat;

  sensor_info sensor_metadata;
  data_handler msr;
  sensor_wq write_queue;

  sensor_status state;
  bool reset_requested;
  bool is_open;
} ev3_sensor;

// Compact description of values within a buffer
typedef struct {
  // Number of values contained therein
  uint num_values;
  // Which data type they are
  data_format format;
  // Whether integers should be interpreted as signed or unsigned
  bool is_signed;
} data_miniinfo;

// Open a new sensor object at the given port. Return value of 0 (=PICO_OK) means success.
extern int ev3_sensor_open(opencube_sensor_port port, ev3_sensor *out);

// Close the sensor object and release the UART port for other uses.
extern void ev3_sensor_close(ev3_sensor *link);

// Check whether a UART sensor has been detected
extern bool ev3_sensor_is_connected(ev3_sensor *link);

// Check whether a UART sensor is sending data
extern bool ev3_sensor_is_ready(ev3_sensor *link);

// Asynchronously switch the sensor to the specified mode. Returns false is the write queue is full
extern bool ev3_sensor_set_mode(ev3_sensor *link, unsigned int mode);

// Asynchronously send a direct WRITE command to the sensor. Returns false is the write queue is full
extern bool ev3_sensor_write_command(ev3_sensor *link, const uint8_t *buffer, size_t size);

// Query the last data message sent by the sensor. Returns false if the sensor has not sent a message yet.
extern bool ev3_sensor_read(ev3_sensor *link, ev3_measurement *msr);

// Re-enter the initialization phase. This will cause the sensor to reboot.
extern void ev3_sensor_reset(ev3_sensor *link);

// Obtain a compact description of data buffer layout for the given mode.
extern bool ev3_sensor_describe_data(ev3_sensor *link, uint mode, data_miniinfo *out);

// Query the ID of the sensor currently connected. Returns -1 if no sensor is connected.
extern int ev3_sensor_identify(ev3_sensor *link);

#endif //OPENCUBE_EV3_SENSOR_H
