/**
 * Global IRQ lock for EV3 sensors
 */
#ifndef OPENCUBE_EV3_LOCKING_H
#define OPENCUBE_EV3_LOCKING_H

/**
 * Obtain a lock on all EV3 sensor data.
 */
extern void ev3_lock(void);

/**
 * Releas a lock on all EV3 sensor data.
 */
extern void ev3_unlock(void);

#endif //OPENCUBE_EV3_LOCKING_H
