#include <malloc.h>
#include <string.h>
#include <hardware/sync.h>
#include "ev3_sensor.h"
#include "locking.h"

static void ev3_sensor_enter_handshake(ev3_sensor *link, bool reset_baud);
static void ev3_sensor_enter_data(ev3_sensor *link);
static bool ev3_sensor_refresh(repeating_timer_t *rt);
static message_proc_result ev3_sensor_handle_message(void *link_obj, const uint8_t *msg, size_t len);
static void ev3_sensor_handle_data(ev3_sensor *link, const uint8_t *msg, size_t len);
static void ev3_sensor_handle_handshake(ev3_sensor *link, const uint8_t *msg, size_t len);
static void process_queued_writes(ev3_sensor *link);
static bool is_open(ev3_sensor *link);
static bool is_ultrasonic_single_mode(ev3_sensor *link, unsigned mode);

int ev3_sensor_open(opencube_sensor_port port, ev3_sensor *link) {
  int result;

  result = opencube_uart_open(port, &link->uart);
  if (result != PICO_OK) {
    return result;
  }

  link->pool = alarm_pool_get_default();
  link->packetizer.callback = &ev3_sensor_handle_message;
  link->packetizer.callback_context = link;
  sensor_info_init(&link->sensor_metadata); // to prevent freeing garbage data
  ev3_data_init(&link->msr);
  sensor_wq_init(&link->write_queue);

  ev3_sensor_enter_handshake(link, false);
  alarm_pool_add_repeating_timer_ms(
      link->pool,
      EV3_MSG_POLL_PERIOD_MS,
      &ev3_sensor_refresh,
      link,
      &link->poll_callback
  );
  link->is_open = true;

  return result;
}

void ev3_sensor_close(ev3_sensor *link) {
  if (link->is_open) {
    alarm_pool_cancel_alarm(link->pool, link->poll_callback.alarm_id);
    sensor_info_deinit(&link->sensor_metadata);
    sensor_wq_deinit(&link->write_queue);
    ev3_data_deinit(&link->msr);
    opencube_uart_close(&link->uart);
    link->is_open = false;
  }
}


void ev3_sensor_enter_handshake(ev3_sensor *link, bool reset_baud) {
  ev3_lock();
  sensor_info_deinit(&link->sensor_metadata);
  sensor_info_init(&link->sensor_metadata);
  ev3_unlock();
  synchronizer_reset(&link->synchronizer);
  packetizer_reset(&link->packetizer);
  ev3_data_invalidate(&link->msr);
  sensor_wq_clear(&link->write_queue);
  opencube_uart_clear_rx(&link->uart);
  link->reset_requested = false;

  if (reset_baud) {
    link->state = EV3_SETTING_LOW_BAUD;
    opencube_uart_start_set_baud(&link->uart, EV3_UART_HANDSHAKE_BAUD);
  } else {
    link->state = EV3_WAITING_FOR_SYNC;
  }
}

void ev3_sensor_enter_data(ev3_sensor *link) {
  uint8_t ack = EV3_MSG_ACK;
  opencube_uart_write(&link->uart, &ack, sizeof(ack));
  opencube_uart_start_set_baud(&link->uart, link->sensor_metadata.baud_rate);
  link->state = EV3_SETTING_HIGH_BAUD;
  hb_start(&link->heartbeat);
}


bool ev3_sensor_refresh(repeating_timer_t *rt) {
  ev3_sensor *link = rt->user_data;
  opencube_uart_if *uart = &link->uart;

  if (opencube_uart_had_rx_overrun(uart) || link->reset_requested) {
    ev3_sensor_enter_handshake(link, true);
  }

  if (link->state == EV3_SETTING_LOW_BAUD) {
    if (opencube_uart_check_set_baud(uart)) {
      link->state = EV3_WAITING_FOR_SYNC;
    }
  }
  if (link->state == EV3_WAITING_FOR_SYNC) {
    int sensor_id_or_error = synchronizer_poll(&link->synchronizer, uart);

    if (sensor_id_or_error >= 0) { // got sync!
      link->sensor_metadata.id = sensor_id_or_error;
      link->state = EV3_PROCESSING_HANDSHAKE;
      link->packetizer.ir_sensor_workaround = sensor_id_or_error == EV3_INFRA_SENSOR_ID;
    }
  }
  if (link->state == EV3_PROCESSING_HANDSHAKE) {
    packetizer_poll(&link->packetizer, uart); // calls ev3_sensor_handle_message() internally
  }
  if (link->state == EV3_SETTING_HIGH_BAUD) {
    if (opencube_uart_check_set_baud(uart)) {
      link->state = EV3_DATA_RUNNING;
    }
  }
  if (link->state == EV3_DATA_RUNNING) {
    packetizer_poll(&link->packetizer, uart); // calls ev3_sensor_handle_message() internally
  }
  if (link->state == EV3_DATA_RUNNING) { // might not be the case if there is a checksum error
    if (hb_update(&link->heartbeat, uart) == SENSOR_TIMED_OUT) {
      ev3_sensor_enter_handshake(link, true);
    }
  }
  if (link->state == EV3_DATA_RUNNING) { // might not be the case if the sensor times out
    process_queued_writes(link);
  }
  return true;
}

message_proc_result ev3_sensor_handle_message(void *link_obj, const uint8_t *msg, size_t len) {
  ev3_sensor *link = link_obj;

  if (link->state == EV3_PROCESSING_HANDSHAKE) {
    ev3_sensor_handle_handshake(link, msg, len);
  } else if (link->state == EV3_DATA_RUNNING) {
    ev3_sensor_handle_data(link, msg, len);
  }

  if (link->state == EV3_PROCESSING_HANDSHAKE || link->state == EV3_DATA_RUNNING) {
    return PROC_CONTINUE;
  } else {
    return PROC_STOP;
  }
}

void ev3_sensor_handle_handshake(ev3_sensor *link, const uint8_t *msg, size_t len) {
  if (len == 0) {
    return;
  }

  if (len > 1) {
    // checksum error during handshake causes handshake restart
    bool csum_ok = calculate_message_checksum(msg, len) == 0;
    if (!csum_ok) {
      ev3_sensor_enter_handshake(link, false);
      return;
    }
  }

  if (msg[0] == EV3_MSG_ACK) {
    ev3_sensor_enter_data(link);
  } else {
    ev3_lock();
    sensor_info_update(&link->sensor_metadata, msg, len);
    ev3_unlock();
  }
}

void ev3_sensor_handle_data(ev3_sensor *link, const uint8_t *msg, size_t len) {
  if (type_of_message(msg[0]) != MTYPE_DATA) {
    // this parser only understands DATA messages after the handshake ends
    return;
  }
  if (len < EV3_NORMAL_MSG_FRAME_OVERHEAD) {
    // we require at least header + checksum
    return;
  }

  bool csum_ok = calculate_message_checksum(msg, len) == 0;
  bool ignore = should_ignore_data_checksum_error(link->sensor_metadata.id, msg[0]);
  if (!csum_ok && !ignore) {
    // checksum error; NACK makes the sensor send data again
    uint8_t nack = EV3_MSG_NACK;
    opencube_uart_write(&link->uart, &nack, sizeof(nack));
    return;
  }

  if (link->heartbeat.ultrasonic_singleshot_workaround) {
    if (is_ultrasonic_single_mode(link, subtype_of_message(msg[0]))) {
      // We received the first DATA message for the given SELECT.
      // Subsequent responses to NACK should be quick, so disable the
      // workaround now.
      // Note: this likely does not handle mixing of US-SI-CM/US-SI-IN;
      // this is ignored for keeping things simple.
      link->heartbeat.ultrasonic_singleshot_workaround = false;
    }
  }

  // receive the data
  hb_got_message(&link->heartbeat);
  ev3_data_import(&link->msr, msg, len);
}

void process_queued_writes(ev3_sensor *link) {
  sensor_wq_entry *e = NULL;
  while ((e = sensor_wq_peek(&link->write_queue)) != NULL) {
    opencube_uart_write(&link->uart, e->data, e->data_len);
    sensor_wq_pop(e);
  }
}

bool is_open(ev3_sensor *link) {
  return *(volatile bool *) &link->is_open;
}

bool ev3_sensor_is_connected(ev3_sensor *link) {
  sensor_status status = *(volatile sensor_status *) &link->state;
  return is_open(link) && !link->reset_requested && status != EV3_WAITING_FOR_SYNC && status != EV3_SETTING_LOW_BAUD;
}

bool ev3_sensor_is_ready(ev3_sensor *link) {
  return is_open(link) && !link->reset_requested && ev3_data_read(&link->msr, NULL);
}

#define SELECT_MODE_MSG_LENGTH 3

bool ev3_sensor_set_mode(ev3_sensor *link, unsigned int mode) {
  if (!is_open(link)) {
    return false;
  }

  // If we're SELECTing US-SI-CM/US-SI-IN, we may need to wait
  // quite a long time for the first DATA message. This is because
  // the sensor will hold off sending that DATA message until it can get
  // a valid measurement (which may take on the order of seconds, if
  // you put something non-sound-reflective in front of it).
  link->heartbeat.ultrasonic_singleshot_workaround = is_ultrasonic_single_mode(link, mode);

  uint8_t header = make_message_header(
      MTYPE_COMMAND,
      MTYPE_COMMAND_SELECT,
      sizeof(uint8_t),
      NULL
  );

  uint8_t msg[SELECT_MODE_MSG_LENGTH];
  msg[0] = header;
  msg[1] = mode;
  msg[2] = calculate_message_checksum(msg, SELECT_MODE_MSG_LENGTH - 1);
  return sensor_wq_push(&link->write_queue, msg, SELECT_MODE_MSG_LENGTH);
}

#define MAX_DIRECT_WRITE_BYTES (16)
#define DIRECT_MSG_BUFFER (MAX_DIRECT_WRITE_BYTES + EV3_NORMAL_MSG_FRAME_OVERHEAD)
static_assert(
    EV3WQ_MAX_WRITE_BYTES >= DIRECT_MSG_BUFFER,
    "Write queue buffers are too small for direct writes"
);

bool ev3_sensor_write_command(ev3_sensor *link, const uint8_t *buffer, size_t size) {
  if (!is_open(link)) {
    return false;
  }

  if (size > MAX_DIRECT_WRITE_BYTES) {
    return false;
  }

  size_t total_length = 0;
  uint8_t header = make_message_header(
      MTYPE_COMMAND,
      MTYPE_COMMAND_WRITE,
      size,
      &total_length
  );
  assert(total_length <= DIRECT_MSG_BUFFER);

  uint8_t msg[DIRECT_MSG_BUFFER];
  msg[0] = header;
  memcpy(&msg[1], buffer, size);
  msg[total_length - 1] = calculate_message_checksum(msg, total_length - 1);

  return sensor_wq_push(&link->write_queue, msg, total_length);
}

void ev3_sensor_reset(ev3_sensor *link) {
  *(volatile bool *) &link->reset_requested = true;
}

bool ev3_sensor_read(ev3_sensor *link, ev3_measurement *msr) {
  if (!is_open(link) || link->reset_requested) {
    return false;
  }

  return ev3_data_read(&link->msr, msr);
}

bool ev3_sensor_describe_data(ev3_sensor *link, uint mode, data_miniinfo *out) {
  bool result = false;
  if (!is_open(link) || link->reset_requested) {
    return false;
  }
  ev3_lock();
  if (mode < link->sensor_metadata.mode_count) {
    sensor_mode *mode_info = &link->sensor_metadata.modes[mode];
    out->format = mode_info->format;
    out->num_values = mode_info->values;
    // as per https://github.com/ev3dev/lego-linux-drivers/blob/ev3dev-buster/sensors/ev3_uart_sensor_defs.c
    // almost everything is signed
    out->is_signed = true;
    result = true;
  }
  ev3_unlock();
  return result;
}

int ev3_sensor_identify(ev3_sensor *link) {
  __mem_fence_acquire();
  int sensor_id = link->sensor_metadata.id;
  if (sensor_id == 0 || link->reset_requested) {
    sensor_id = -1;
  }
  return sensor_id;
}

bool is_ultrasonic_single_mode(ev3_sensor *link, unsigned mode) {
  return link->sensor_metadata.id == EV3_SONIC_SENSOR_ID &&
    (mode == EV3_ULTRASONIC_SENSOR_SINGLE_CM_MODE ||
     mode == EV3_ULTRASONIC_SENSOR_SINGLE_IN_MODE);
}
