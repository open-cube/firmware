#include <string.h>
#include "stream_sync.h"
#include "messages.h"

void synchronizer_reset(synchronizer_state *sn) {
  memset(sn->message_window, 0, sizeof(sn->message_window));
}

int synchronizer_poll(synchronizer_state *sn, opencube_uart_if *uart) {
  uint8_t *msg = sn->message_window;
  while (true) {
    if (msg[0] == EV3_INITIAL_MSG && (msg[0] ^ msg[1] ^ msg[2]) == 0xFF) {
      return msg[1];
    }

    int rx_byte = opencube_uart_read_one(uart);
    if (rx_byte < 0) {
      return -1;
    }
    msg[0] = msg[1];
    msg[1] = msg[2];
    msg[2] = rx_byte;
  }
}
