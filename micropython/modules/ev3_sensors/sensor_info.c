/**
 * Decoder for sensor self-description information.
 */
#include <string.h>
#include "sensor_info.h"

static void require_at_least_bytes(size_t in_len, size_t size);

#if FULL_TYPEDATA
static inline uint uimin(uint a, uint b) {
  return a < b ? a : b;
}
#endif

#define copy_into(container, buf, len) do { \
    require_at_least_bytes((len), sizeof((container))); \
    memcpy(&(container), (buf), sizeof((container))); \
  } while(0)

#define make_type(struct_type, buf, len) ({ \
  struct_type obj;                          \
  copy_into(obj, buf, len);                 \
  obj;                                      \
})

void sensor_info_init(sensor_info *info) {
  memset(info, 0, sizeof(sensor_info));
}

void sensor_info_deinit(sensor_info *info) {
  (void) info;
  // noop, malloc is gone
}

void sensor_info_update(sensor_info *info, const uint8_t *msg, size_t len) {
  uint8_t header = msg[0];
  ev3_msg_type mtype = type_of_message(header);

  if (mtype == MTYPE_COMMAND) {
    ev3_cmdmsg_type cmd = subtype_of_message(header);
    if (cmd == MTYPE_COMMAND_TYPE) {
      ev3_cmd_type parsed = make_type(ev3_cmd_type, msg, len);
      info->id = parsed.id;

    } else if (cmd == MTYPE_COMMAND_SPEED) {
      ev3_cmd_speed parsed = make_type(ev3_cmd_speed, msg, len);
      info->baud_rate = parsed.baud_rate;

    } else if (cmd == MTYPE_COMMAND_MODES) {
      ev3_cmd_modes parsed = make_type(ev3_cmd_modes, msg, len);

      // duplicate message: reject it to stay safe
      if (info->mode_count != 0) {
        return;
      }

      info->mode_count = parsed.mode_count + 1;
      if (info->mode_count > MAX_SENSOR_MODE_COUNT) {
        info->mode_count = MAX_SENSOR_MODE_COUNT;
      }

#if FULL_TYPEDATA
      info->visible_mode_count = parsed.visible_mode_count + 1;
      if (info->visible_mode_count > MAX_SENSOR_MODE_COUNT) {
        info->visible_mode_count = MAX_SENSOR_MODE_COUNT;
      }
#endif

      for (int i = 0; i < info->mode_count; i++) {
        sensor_mode_init(&info->modes[i]);
      }
    }
  } else if (mtype == MTYPE_INFO) {
    require_at_least_bytes(len, 2);
    unsigned int mode_no = subtype_of_message(msg[0]);
    if (mode_no < info->mode_count) {
      sensor_mode_update(&info->modes[mode_no], msg, len);
    }
  }
}

void sensor_mode_init(sensor_mode *info) {
#if FULL_TYPEDATA
  info->si_limit.min = 0.0f;
  info->si_limit.max = 1.0f;
  info->pct_limit.min = 0.0f;
  info->pct_limit.max = 100.0f;
  info->raw_limit.min = 0.0f;
  info->raw_limit.max = 1023.0f;
  info->figures = 4;
  info->decimals = 0;
#endif
  info->values = 1;
  info->format = FORMAT_INT_8;
}

void sensor_mode_update(sensor_mode *info, const uint8_t *msg, size_t len) {
  ev3_infomsg_type itype = msg[1];
  switch (itype) { // NOLINT(hicpp-multiway-paths-covered)
#if FULL_TYPEDATA
    case INFO_TYPE_NAME: {
      unsigned int supplied_size = len - EV3_INFO_MSG_FRAME_OVERHEAD;
      unsigned int copy_size = uimin(supplied_size, MAX_SENSOR_NAME_LEN);
      memcpy(info->name, &msg[2], copy_size);
      // null terminator already at the end
      break;
    }
    case INFO_TYPE_RAW: {
      ev3_info_limits parsed = make_type(ev3_info_limits, msg, len);
      info->raw_limit.min = parsed.lower;
      info->raw_limit.max = parsed.upper;
      break;
    }
    case INFO_TYPE_PCT: {
      ev3_info_limits parsed = make_type(ev3_info_limits, msg, len);
      info->pct_limit.min = parsed.lower;
      info->pct_limit.max = parsed.upper;
      break;
    }
    case INFO_TYPE_SI: {
      ev3_info_limits parsed = make_type(ev3_info_limits, msg, len);
      info->si_limit.min = parsed.lower;
      info->si_limit.max = parsed.upper;
      break;
    }
    case INFO_TYPE_SYMBOL: {
      unsigned int supplied_size = len - EV3_INFO_MSG_FRAME_OVERHEAD;
      unsigned int copy_size = uimin(supplied_size, MAX_SENSOR_NAME_LEN);
      memcpy(info->unit, &msg[2], copy_size);
      // null terminator already at the end
      break;
    }
#endif
    case INFO_TYPE_FORMAT: {
      ev3_info_format parsed = make_type(ev3_info_format, msg, len);
      info->values = parsed.values;
      info->format = parsed.format;
#if FULL_TYPEDATA
      info->decimals = parsed.decimals;
      info->figures = parsed.figures;
#endif
      break;
    }
    default:
      break;
  }
}

void require_at_least_bytes(size_t in_len, size_t size) {
  if (in_len < size) {
    panic("buffer overrun when parsing info message");
  }
}
