# Create an INTERFACE library for our C module.
add_library(usermod_opencube_pindefs INTERFACE)

# Add our source files to the lib
target_sources(usermod_opencube_pindefs INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}/opencube_hw.h
)

# Add the current directory as an include directory.
target_include_directories(usermod_opencube_pindefs INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}
)
