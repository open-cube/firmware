/**
 * C-side pin definitions for the Open-Cube driver code.
 */

#ifndef OPENCUBE_PINS_H
#define OPENCUBE_PINS_H

// CPU operating frequency. Source: datasheet & SDK clock setup code.
#define RP2040_CPU_FREQ 125000000

// internal peripheral bus
#define INTERNAL_I2C_SDA_PIN 14
#define INTERNAL_I2C_SCK_PIN 15
#define INTERNAL_I2C_BUS     i2c1
#define INTERNAL_I2C_BAUD_RATE 400000
#define PCF8575_I2C_ADDR     0x20
#define ICM20608_I2C_ADDR    0x69 // alt address
#define ICM42688_I2C_ADDR    0x68 
#define ADS1119_I2C_ADDR     0x40 // 1000000 (A0+A1=GND)

// ==== Misc pins ====
#define TURN_OFF_PIN   26
#define VBATT_ADC_PIN     27
#define VBATT_ADC_CHANNEL  1
#define ICM20608_DRDY_PIN  28
#define BUZZER_PWM_PIN    29

// ==== UARTs for sensor ports ====

// - port 1: HW UART
#define SENSOR1_UART_TX_PIN 4
#define SENSOR1_UART_RX_PIN 5
#define SENSOR1_UART_PERIPHERAL uart1

// - port 2: PIO UART
#define SENSOR2_UART_RX_PIN 7
#define SENSOR2_UART_RX_PIO pio1
#define SENSOR2_UART_RX_SM 0
#define SENSOR2_UART_TX_PIN 6
#define SENSOR2_UART_TX_PIO pio0
#define SENSOR2_UART_TX_SM 0

// - port 3: PIO UART
#define SENSOR3_UART_RX_PIN 9
#define SENSOR3_UART_RX_PIO pio1
#define SENSOR3_UART_RX_SM 1
#define SENSOR3_UART_TX_PIN 8
#define SENSOR3_UART_TX_PIO pio0
#define SENSOR3_UART_TX_SM 1

// - port 4: PIO UART
#define SENSOR4_UART_RX_PIN 11
#define SENSOR4_UART_RX_PIO pio1
#define SENSOR4_UART_RX_SM 2
#define SENSOR4_UART_TX_PIN 10
#define SENSOR4_UART_TX_PIO pio0
#define SENSOR4_UART_TX_SM 2

// ==== Sensor ports ADC channels and active pins for analog sensors ====

// - port 1
#define SENSOR1_ADC_CHANNEL 3
#define SENSOR1_ACTIVE_PIN SENSOR1_UART_TX_PIN

// - port 2
#define SENSOR2_ADC_CHANNEL 2
#define SENSOR2_ACTIVE_PIN SENSOR2_UART_TX_PIN

// - port 3
#define SENSOR3_ADC_CHANNEL 1
#define SENSOR3_ACTIVE_PIN SENSOR3_UART_TX_PIN

// - port 4
#define SENSOR4_ADC_CHANNEL 0
#define SENSOR4_ACTIVE_PIN SENSOR4_UART_TX_PIN

// ==== Motor pins ====

// - PWM output (INA on H-bridge)
#define MOTOR1_PWM_PIN 0
#define MOTOR2_PWM_PIN 1
#define MOTOR3_PWM_PIN 2
#define MOTOR4_PWM_PIN 3
#define MOTOR12_PWM_SLICE 0
#define MOTOR34_PWM_SLICE 1

// - Quadrature encoder input
#define MOTOR1_ENC_A_PIN 18
#define MOTOR1_ENC_B_PIN 19
#define MOTOR2_ENC_A_PIN 20
#define MOTOR2_ENC_B_PIN 21
#define MOTOR3_ENC_A_PIN 22
#define MOTOR3_ENC_B_PIN 23
#define MOTOR4_ENC_A_PIN 24
#define MOTOR4_ENC_B_PIN 25

// ==== PCF8575 remote pins (_RPIN suffix) ====

// H-bridge enable pin (PS)
#define MOTOR1_EN_RPIN 0
#define MOTOR2_EN_RPIN 1
#define MOTOR3_EN_RPIN 2
#define MOTOR4_EN_RPIN 3

// H-bridge direction pin (INB)
#define MOTOR1_DIR_RPIN 4
#define MOTOR2_DIR_RPIN 5
#define MOTOR3_DIR_RPIN 6
#define MOTOR4_DIR_RPIN 7

// Button pin inputs (active low)
#define BUTTON_POWER_RPIN 8
#define BUTTON_RIGHT_RPIN 9
#define BUTTON_DOWN_RPIN 10
#define BUTTON_OK_RPIN 11
#define BUTTON_UP_RPIN 12
#define BUTTON_LEFT_RPIN 13

// User-controllable LED
#define USER_LED_RPIN 14

// Active high for NXT UTZ communication
#define I2C_STRONG_PULL_RPIN 15

typedef enum {
  OPENCUBE_SENSOR_1,
  OPENCUBE_SENSOR_2,
  OPENCUBE_SENSOR_3,
  OPENCUBE_SENSOR_4
} opencube_sensor_port;

typedef enum {
  OPENCUBE_MOTOR_1,
  OPENCUBE_MOTOR_2,
  OPENCUBE_MOTOR_3,
  OPENCUBE_MOTOR_4
} opencube_motor_port;

#define NUM_SENSOR_PORTS 4
#define NUM_MOTOR_PORTS 4

// Battery voltage measurement
#define VBATT_SCALE_FACTOR 5.7f  // ADC input = resistive divider giving 1/4 of Vbatt
#define VBATT_REF_VOLTAGE 3      // ADC Vref voltage - for determining real battery voltage.
#define VBATT_ADC_MAX_CODE 4095  // ADC resolution
#endif // OPENCUBE_PINS_H
