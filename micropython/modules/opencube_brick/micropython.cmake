# Create an INTERFACE library for our C module.
add_library(usermod_opencube_brick INTERFACE)

# Add our source files to the lib
target_sources(usermod_opencube_brick INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}/battery.c
    ${CMAKE_CURRENT_LIST_DIR}/battery.h
    ${CMAKE_CURRENT_LIST_DIR}/ICM_api.c
    ${CMAKE_CURRENT_LIST_DIR}/ICM_api.h
    ${CMAKE_CURRENT_LIST_DIR}/buttons.c
    ${CMAKE_CURRENT_LIST_DIR}/buttons.h
    ${CMAKE_CURRENT_LIST_DIR}/led.c
    ${CMAKE_CURRENT_LIST_DIR}/led.h
    ${CMAKE_CURRENT_LIST_DIR}/brick_api.c
    ${CMAKE_CURRENT_BINARY_DIR}/opencube_brick_generated/opencube_fw_version.h
)

# Add the current directory as an include directory.
target_include_directories(usermod_opencube_brick INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}
    ${CMAKE_CURRENT_BINARY_DIR}/opencube_brick_generated
)

target_link_libraries(usermod_opencube_brick INTERFACE
    usermod_opencube_pindefs
    usermod_opencube_lib
)

# Link our INTERFACE library to the usermod target.
target_link_libraries(usermod INTERFACE usermod_opencube_brick)

add_custom_target(opencube_fw_version
    COMMENT "Generating version header"
    COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/opencube_brick_generated
    COMMAND python3 ${CMAKE_CURRENT_LIST_DIR}/generate_info.py ${CMAKE_CURRENT_BINARY_DIR}/opencube_brick_generated/opencube_fw_version.h
    WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
    BYPRODUCTS ${CMAKE_CURRENT_BINARY_DIR}/opencube_brick_generated/opencube_fw_version.h
)
