// Include MicroPython API.
#include <opencube_hw.h>
#include "py/runtime.h"
#include "hardware/adc.h"
#include "pico/stdlib.h"

#define VOLTAGE_REFRESH_PERIOD_MSEC 200

// This structure represents Battery instance objects.
typedef struct {
    mp_obj_base_t base;
    float last_value;
    float multiplier;
    alarm_pool_t *timer_pool;
    repeating_timer_t timer;
} battery_obj_t;

static void read_voltage(battery_obj_t *self);
static bool read_voltage_handler(repeating_timer_t *timer);
static void battery_print(const mp_print_t *print, mp_obj_t self_in);

void read_voltage(battery_obj_t *self) {
    adc_select_input(VBATT_ADC_CHANNEL);
    self->last_value = adc_read() * VBATT_SCALE_FACTOR * VBATT_REF_VOLTAGE / VBATT_ADC_MAX_CODE * self->multiplier;
}

bool read_voltage_handler(repeating_timer_t *timer) {
    battery_obj_t *self = timer->user_data;
    read_voltage(self);
    return true;
}

static void battery_print(const mp_print_t *print, mp_obj_t self_in) {
    //battery_obj_t *self = MP_OBJ_TO_PTR(self_in);
}

STATIC mp_obj_t battery_read_voltage(mp_obj_t self_in) {
    battery_obj_t *self = MP_OBJ_TO_PTR(self_in);
    read_voltage(self);
    return mp_obj_new_float(self->last_value);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(battery_read_voltage_obj, battery_read_voltage);

STATIC mp_obj_t battery_voltage(mp_obj_t self_in) {
    battery_obj_t *self = MP_OBJ_TO_PTR(self_in);
    return mp_obj_new_float(self->last_value);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(battery_voltage_obj, battery_voltage);

STATIC mp_obj_t battery_set_multiplier(mp_obj_t self_in, mp_obj_t multiplier) {
    battery_obj_t *self = MP_OBJ_TO_PTR(self_in);
    self->multiplier = mp_obj_get_float(multiplier);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(battery_set_multiplier_obj, battery_set_multiplier);

STATIC mp_obj_t battery_deinit(mp_obj_t self_in) {
    battery_obj_t *self = MP_OBJ_TO_PTR(self_in);
    alarm_pool_cancel_alarm(self->timer_pool, self->timer.alarm_id);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(battery_deinit_obj, battery_deinit);

// Called on Battery init
STATIC mp_obj_t battery_make_new(const mp_obj_type_t* type, size_t n_args, size_t n_kw, const mp_obj_t* args) {
    battery_obj_t *self = m_new_obj_with_finaliser(battery_obj_t);
    self->base.type = type;
    self->multiplier = 1.0;
    // Initialise the ADC peripheral if it's not already running.
    // (this check is borrowed from machine_adc.c)
    if (!(adc_hw->cs & ADC_CS_EN_BITS))
    {
        adc_init();
    }

    adc_gpio_init(VBATT_ADC_PIN);
    read_voltage(self);

    self->timer_pool = alarm_pool_get_default();
    alarm_pool_add_repeating_timer_ms(
        self->timer_pool,
        VOLTAGE_REFRESH_PERIOD_MSEC,
        &read_voltage_handler,
        self,
        &self->timer
    );

    return MP_OBJ_FROM_PTR(self);
}

// This collects all methods and other static class attributes of the Battery.
// The table structure is similar to the module table, as detailed below.
STATIC const mp_rom_map_elem_t battery_locals_dict[] = {
    { MP_ROM_QSTR(MP_QSTR_read_voltage), MP_ROM_PTR(&battery_read_voltage_obj) },
    { MP_ROM_QSTR(MP_QSTR_voltage), MP_ROM_PTR(&battery_voltage_obj) },
    { MP_ROM_QSTR(MP_QSTR_set_multiplier), MP_ROM_PTR(&battery_set_multiplier_obj) },
    { MP_ROM_QSTR(MP_QSTR_deinit), MP_ROM_PTR(&battery_deinit_obj) },
    { MP_ROM_QSTR(MP_QSTR___del__), MP_ROM_PTR(&battery_deinit_obj) },
};
STATIC MP_DEFINE_CONST_DICT(battery_locals_dict_obj, battery_locals_dict);

// This defines the type(Battery) object.
MP_DEFINE_CONST_OBJ_TYPE(
    opencube_battery_type,
    MP_QSTR_Battery,
    MP_TYPE_FLAG_NONE,
    make_new, &battery_make_new,
    locals_dict, &battery_locals_dict_obj,
    print, &battery_print
);