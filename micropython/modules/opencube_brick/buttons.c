/**
 * Module for capturing pcf button interrupts and keeping button state
 */

#include <hardware/gpio.h>
#include <hardware/irq.h>
#include <hardware/sync.h>
#include <opencube_hw.h>
#include "py/runtime.h"
#include "py/mphal.h"
#include "pico/stdlib.h"
#include "i2c_locking.h"
#include "PCF8575.h"
#include "buttons.h"

// This structure represents buttons instance objects.
typedef struct _buttons_obj_t {
    mp_obj_base_t base;
} buttons_obj_t;

//static void pcf_irq_handler(void);

// Called on buttons init
STATIC mp_obj_t buttons_make_new(const mp_obj_type_t* type, size_t n_args, size_t n_kw, const mp_obj_t* args) {
    buttons_obj_t *self = m_new_obj(buttons_obj_t);
    self->base.type = type;
    opencube_lock_i2c_or_raise();
    PCF8575_init();
    opencube_unlock_i2c();
    return MP_OBJ_FROM_PTR(self);
}

STATIC void buttons_print(const mp_print_t *print, mp_obj_t self_in, mp_print_kind_t kind) {
  mp_printf(print, "Buttons");
}

STATIC mp_obj_t buttons_pressed(mp_obj_t self_in) {
    uint16_t pcf_last_read = PCF8575_read_value();
    uint16_t pcf_last_write = PCF8575_read_function();
    mp_obj_t tuple[6] = {
            tuple[0] = mp_obj_new_bool((pcf_last_read & (1u << BUTTON_POWER_RPIN)) == 0 
                                    && (pcf_last_write & (1u << BUTTON_POWER_RPIN)) != 0),
            tuple[1] = mp_obj_new_bool((pcf_last_read & (1u << BUTTON_LEFT_RPIN)) == 0),
            tuple[2] = mp_obj_new_bool((pcf_last_read & (1u << BUTTON_RIGHT_RPIN)) == 0),
            tuple[3] = mp_obj_new_bool((pcf_last_read & (1u << BUTTON_OK_RPIN)) == 0),
            tuple[4] = mp_obj_new_bool((pcf_last_read & (1u << BUTTON_UP_RPIN)) == 0),
            tuple[5] = mp_obj_new_bool((pcf_last_read & (1u << BUTTON_DOWN_RPIN)) == 0),
        };
    return mp_obj_new_tuple(6, tuple);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(buttons_pressed_obj, buttons_pressed);

STATIC mp_obj_t buttons_pressed_since(mp_obj_t self_in) {
    uint16_t pcf_last_read = PCF8575_read_value_missed();
    mp_obj_t tuple[6] = {
            tuple[0] = mp_obj_new_bool(0),
            tuple[1] = mp_obj_new_bool((pcf_last_read & (1u << BUTTON_LEFT_RPIN)) == 0),
            tuple[2] = mp_obj_new_bool((pcf_last_read & (1u << BUTTON_RIGHT_RPIN)) == 0),
            tuple[3] = mp_obj_new_bool((pcf_last_read & (1u << BUTTON_OK_RPIN)) == 0),
            tuple[4] = mp_obj_new_bool((pcf_last_read & (1u << BUTTON_UP_RPIN)) == 0),
            tuple[5] = mp_obj_new_bool((pcf_last_read & (1u << BUTTON_DOWN_RPIN)) == 0),
        };
    return mp_obj_new_tuple(6, tuple);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(buttons_pressed_since_obj, buttons_pressed_since);

STATIC mp_obj_t buttons_read_value(mp_obj_t self_in) {
    if (opencube_try_lock_i2c())
    {
        PCF8575_read();
        opencube_unlock_i2c();
    }
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(buttons_read_value_obj, buttons_read_value);

STATIC mp_obj_t buttons_set_pin(mp_obj_t self_in, mp_obj_t pin, mp_obj_t value) {
    uint8_t pin_num = mp_obj_get_int(pin);
    bool pin_value = mp_obj_get_int(value);
    if (opencube_try_lock_i2c())
    {
        PCF8575_write_pin(pin_num, pin_value);
        opencube_unlock_i2c();
    }
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_3(buttons_set_pin_obj, buttons_set_pin);

STATIC mp_obj_t buttons_turn_off_cube(mp_obj_t self_in) {
    gpio_put(TURN_OFF_PIN, true);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(buttons_turn_off_cube_obj, buttons_turn_off_cube);


// This collects all methods and other static class attributes of the buttons.
STATIC const mp_rom_map_elem_t buttons_locals_dict[] = {
    { MP_ROM_QSTR(MP_QSTR_pressed), MP_ROM_PTR(&buttons_pressed_obj) },
    { MP_ROM_QSTR(MP_QSTR_pressed_since), MP_ROM_PTR(&buttons_pressed_since_obj) },
    { MP_ROM_QSTR(MP_QSTR_read_value), MP_ROM_PTR(&buttons_read_value_obj) },
    { MP_ROM_QSTR(MP_QSTR_set_pin), MP_ROM_PTR(&buttons_set_pin_obj) },
    { MP_ROM_QSTR(MP_QSTR_turn_off_cube), MP_ROM_PTR(&buttons_turn_off_cube_obj) },
};
STATIC MP_DEFINE_CONST_DICT(buttons_locals_dict_obj, buttons_locals_dict);

// This defines the type(buttons) object.
MP_DEFINE_CONST_OBJ_TYPE(
    opencube_buttons_type,
    MP_QSTR_Buttons,
    MP_TYPE_FLAG_NONE,
    make_new, &buttons_make_new,
    locals_dict, &buttons_locals_dict_obj,
    print, &buttons_print
);