#!/usr/bin/env python3

import sys
import subprocess
import datetime
import os

def main():
    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} <header path>", file=sys.stderr)
        sys.exit(1)

    header_path = sys.argv[1]

    now = datetime.datetime.now()
    date = now.strftime("%d.%m.%Y")
    commit = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).decode().strip()

    header_content = f"""#ifndef OPENCUBE_FW_VERSION_H
#define OPENCUBE_FW_VERSION_H
#define OPENCUBE_FW_DATE   "{date}"
#define OPENCUBE_FW_COMMIT "{commit}"
#endif // OPENCUBE_FW_VERSION_H
"""

    if os.path.exists(header_path):
        with open(header_path, "r") as fp:
            existing_header_content = fp.read()
        if existing_header_content == header_content:
            return

    with open(header_path, "w") as fp:
        fp.write(header_content)

if __name__ == '__main__':
    main()
