/**
 * Module for capturing pcf button interrupts and keeping button state
 */
#include <opencube_hw.h>
#include "py/runtime.h"
#include "py/mphal.h"
#include "pico/stdlib.h"
#include "i2c_locking.h"
#include "PCF8575.h"
#include "led.h"

// This structure represents led instance objects.
typedef struct _led_obj_t {
    mp_obj_base_t base;
} led_obj_t;

//static void pcf_irq_handler(void);

// Called on led init
STATIC mp_obj_t led_make_new(const mp_obj_type_t* type, size_t n_args, size_t n_kw, const mp_obj_t* args) {
    led_obj_t *self = m_new_obj(led_obj_t);
    self->base.type = type;
    opencube_lock_i2c_or_raise();
    PCF8575_init();
    opencube_unlock_i2c();
    return MP_OBJ_FROM_PTR(self);
}

STATIC void led_print(const mp_print_t *print, mp_obj_t self_in, mp_print_kind_t kind) {
  mp_printf(print, "Led");
}

STATIC mp_obj_t led_on(mp_obj_t self_in) {
    opencube_lock_i2c_or_raise();
    PCF8575_write_pin(USER_LED_RPIN, false);
    opencube_unlock_i2c();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(led_on_obj, led_on);

STATIC mp_obj_t led_off(mp_obj_t self_in) {
    opencube_lock_i2c_or_raise();
    PCF8575_write_pin(USER_LED_RPIN, true);
    opencube_unlock_i2c();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(led_off_obj, led_off);

STATIC mp_obj_t led_toggle(mp_obj_t self_in) {
    opencube_lock_i2c_or_raise();
    PCF8575_toggle_pin(USER_LED_RPIN);
    opencube_unlock_i2c();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(led_toggle_obj, led_toggle);


// This collects all methods and other static class attributes of the led.
STATIC const mp_rom_map_elem_t led_locals_dict[] = {
    { MP_ROM_QSTR(MP_QSTR_on), MP_ROM_PTR(&led_on_obj) },
    { MP_ROM_QSTR(MP_QSTR_off), MP_ROM_PTR(&led_off_obj) },
    { MP_ROM_QSTR(MP_QSTR_toggle), MP_ROM_PTR(&led_toggle_obj) },
    { MP_ROM_QSTR(MP_QSTR___del__), MP_ROM_PTR(&led_off_obj) },
    { MP_ROM_QSTR(MP_QSTR_close), MP_ROM_PTR(&led_off_obj) },
};
STATIC MP_DEFINE_CONST_DICT(led_locals_dict_obj, led_locals_dict);

// This defines the type(led) object.
MP_DEFINE_CONST_OBJ_TYPE(
    opencube_led_type,
    MP_QSTR_Led,
    MP_TYPE_FLAG_NONE,
    make_new, &led_make_new,
    locals_dict, &led_locals_dict_obj,
    print, &led_print
);