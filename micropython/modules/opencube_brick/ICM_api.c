// Include MicroPython API.
#include "py/runtime.h"
#include "pico/stdlib.h"
#include <stdlib.h>
#include "i2c_locking.h"
#include "ICM42688.h"


// This structure represents ICM instance objects.
typedef struct _ICM_obj_t {
    mp_obj_base_t base;
    ICM42688_t* icm;
} ICM_obj_t;


static void ICM_print(const mp_print_t *print, mp_obj_t self_in);

// Called on ICM init
STATIC mp_obj_t ICM_make_new(const mp_obj_type_t* type, size_t n_args, size_t n_kw, const mp_obj_t* args) {
    ICM_obj_t *self = m_new_obj(ICM_obj_t);	
    self->base.type = type;
    self->icm = (ICM42688_t*)malloc(sizeof(ICM42688_t));
    opencube_lock_i2c_or_raise();
    ICM42688_init(self->icm);
    opencube_unlock_i2c();
    return MP_OBJ_FROM_PTR(self);
}

static void ICM_print(const mp_print_t *print, mp_obj_t self_in) {
    //ICM_obj_t *self = MP_OBJ_TO_PTR(self_in);
}

STATIC mp_obj_t ICM_read_value(mp_obj_t self_in) {
    ICM_obj_t *self = MP_OBJ_TO_PTR(self_in);	
    float accel_x, accel_y, accel_z, gyro_x, gyro_y, gyro_z;
    opencube_lock_i2c_or_raise();
    ICM42688_read_data(self->icm, 
                       &accel_x, &accel_y, &accel_z, 
                       &gyro_x, &gyro_y, &gyro_z);
    opencube_unlock_i2c();
    mp_obj_t tuple[6] = {
            tuple[0] = mp_obj_new_float(accel_x),
            tuple[1] = mp_obj_new_float(accel_y),
            tuple[2] = mp_obj_new_float(accel_z),
            tuple[3] = mp_obj_new_float(gyro_x),
            tuple[4] = mp_obj_new_float(gyro_y),
            tuple[5] = mp_obj_new_float(gyro_z),
        };
    return mp_obj_new_tuple(6, tuple);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(ICM_read_value_obj, ICM_read_value);

static mp_obj_t ICM_deinit(mp_obj_t self_in) {
    ICM_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    ICM42688_off(self->icm);
    self->icm = NULL;
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(ICM_deinit_obj, ICM_deinit);

// This collects all methods and other static class attributes of the ICM.
// The table structure is similar to the module table, as detailed below.
STATIC const mp_rom_map_elem_t ICM_locals_dict[] = {
    { MP_ROM_QSTR(MP_QSTR_read_value), MP_ROM_PTR(&ICM_read_value_obj) },
    { MP_ROM_QSTR(MP_QSTR_deinit), MP_ROM_PTR(&ICM_deinit_obj) },
    { MP_ROM_QSTR(MP_QSTR___del__), MP_ROM_PTR(&ICM_deinit_obj) },
    { MP_ROM_QSTR(MP_QSTR_close), MP_ROM_PTR(&ICM_deinit_obj) },
};
STATIC MP_DEFINE_CONST_DICT(ICM_locals_dict_obj, ICM_locals_dict);

// This defines the type(ICM) object.
MP_DEFINE_CONST_OBJ_TYPE(
    opencube_ICM_type,
    MP_QSTR_ICM,
    MP_TYPE_FLAG_NONE,
    make_new, &ICM_make_new,
    locals_dict, &ICM_locals_dict_obj,
    print, &ICM_print
);