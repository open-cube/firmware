#include <py/runtime.h>
#include <py/objstr.h>
#include "i2c_locking.h"
#include "battery.h"
#include "ICM_api.h"
#include "buttons.h"
#include "led.h"
#include "opencube_fw_version.h"

STATIC mp_obj_t i2c_lock_fun(void) {
    opencube_lock_i2c_or_raise();
    return mp_const_none;
}
STATIC mp_obj_t i2c_unlock_fun(void) {
    opencube_unlock_i2c();
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_0(i2c_lock_fun_obj, i2c_lock_fun);
MP_DEFINE_CONST_FUN_OBJ_0(i2c_unlock_fun_obj, i2c_unlock_fun);

MP_DEFINE_STR_OBJ(fw_date,   OPENCUBE_FW_DATE);
MP_DEFINE_STR_OBJ(fw_commit, OPENCUBE_FW_COMMIT);

static const mp_rom_map_elem_t brick_globals_dict[] = {
    {MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_bricks_ll)},
    {MP_ROM_QSTR(MP_QSTR_Battery), MP_ROM_PTR(&opencube_battery_type)},
    {MP_ROM_QSTR(MP_QSTR_ICM), MP_ROM_PTR(&opencube_ICM_type)},
    {MP_ROM_QSTR(MP_QSTR_Buttons), MP_ROM_PTR(&opencube_buttons_type)},
    {MP_ROM_QSTR(MP_QSTR_Led), MP_ROM_PTR(&opencube_led_type)},
    {MP_ROM_QSTR(MP_QSTR_lock_i2c), MP_ROM_PTR(&i2c_lock_fun_obj)},
    {MP_ROM_QSTR(MP_QSTR_unlock_i2c), MP_ROM_PTR(&i2c_unlock_fun_obj)},
    {MP_ROM_QSTR(MP_QSTR_InternalI2CBusyError), MP_ROM_PTR(&mp_type_InternalI2CBusyError)},
    {MP_ROM_QSTR(MP_QSTR_FW_DATE), MP_ROM_PTR(&fw_date)},
    {MP_ROM_QSTR(MP_QSTR_FW_COMMIT), MP_ROM_PTR(&fw_commit)},
};
MP_DEFINE_CONST_DICT(brick_globals_dict_obj, brick_globals_dict);

const mp_obj_module_t brick_module = {
    .base = {.type = &mp_type_module},
    .globals = (mp_obj_dict_t *) &brick_globals_dict_obj,
};

MP_REGISTER_MODULE(MP_QSTR_brick_ll, brick_module);
