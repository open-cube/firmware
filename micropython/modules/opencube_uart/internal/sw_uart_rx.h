/**
 * Receive-side EV3 UART I/F implementation for PIO-emulated UART
 */
#ifndef OPENCUBE_SW_UART_RX_H
#define OPENCUBE_SW_UART_RX_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <hardware/structs/pio.h>
#include <pico/critical_section.h>
#include "uart_ring.h"

// Receiver configuration
typedef struct {
  // Hardware PIO unit to use
  pio_hw_t *pio;
  // Which State Machine to use within the PIO block
  uint sm;
  // Initial baud rate
  uint baud;
  // GPIO pin to use for RX
  uint gpio_pin;
} opencube_swu_rx_config;

// Receiver state
typedef struct opencube_swu_rx {
  struct opencube_swu_rx *next_rx;
  pio_hw_t *pio;
  uint baud;
  uint sm;
  opencube_uart_ring ring;
} opencube_swu_rx;

/**
 * Initialize the given port.
 *
 * Beware! The state struct must *not* move in memory.
 * There may be globally reachable pointers pointing
 * to it once this function successfully returns.
 *
 * @param rx Where to store the state.
 * @param params How to configure the UART.
 * @return PICO_OK if the port was initialized successfully, some value <0 otherwise.
 */
extern int opencube_swu_rx_open(opencube_swu_rx *rx, const opencube_swu_rx_config *config);

/**
 * Deinitialize the given port.
 * @param rx State struct of the UART.
 */
extern void opencube_swu_rx_close(opencube_swu_rx *rx);

/**
 * Attempt to read one byte.
 * @param rx UART to read from.
 * @return Received byte or -1 if the input FIFO is empty.
 */
extern int opencube_swu_rx_read_one(opencube_swu_rx *rx);

/**
 * Attempt to read more of a message.
 * @param rx UART to read from.
 * @param win Message that is currently being read.
 * @return True if the message is complete, false otherwise. False is returned also if there is an overrun.
 */
extern bool opencube_swu_rx_read_many(opencube_swu_rx *rx, partial_message *win);

/**
 * Set the port baud rate.
 *
 * The RX-side change is instantaneous, there is no need to
 * poll the receive side of this port more.
 *
 * @param rx UART to configure.
 * @param new_rate New baud rate.
 */
extern void opencube_swu_rx_set_baud(opencube_swu_rx *rx, uint32_t new_rate);

/**
 * Clear the receive FIFO.
 * @param rx UART to clear.
 */
extern void opencube_swu_rx_clear(opencube_swu_rx *rx);

/**
 * Check if some bytes were lost during past receive operations.
 * @param uart UART to check.
 * @return True if data was lost, false if everything is OK.
 */
extern bool opencube_swu_rx_has_overrun(opencube_swu_rx *rx);

#endif //OPENCUBE_SW_UART_RX_H
