/**
 * EV3 UART I/F implementation for hardware RP2040 UART
 */
#include <hardware/sync.h>
#include <hardware/gpio.h>
#include <py/mpprint.h>
#include "hw_uart.h"

static bool uart_open[2] = {false, false};

static bool is_uart_busy(uart_inst_t *hw);

int opencube_hwu_open(opencube_hwu *uart, const opencube_hwu_config *params) {
  uint uart_no = uart_get_index(params->hw);
  if (uart_open[uart_no]) {
    mp_printf(MICROPY_ERROR_PRINTER, "ERROR: attempted to open HW UART #%d twice!\n", uart_no);
    // attempted to open UART twice!
    return PICO_ERROR_INVALID_ARG;
  }
  uart_open[uart_no] = true; // screw sync here, hopefully no races

  uart->hw = params->hw;
  uart->baud = params->baud;
  uart->baud_scheduled = params->baud;
  uart->overrun = false;
  uart_init(uart->hw, uart->baud);
  gpio_set_function(params->rx_gpio_pin, GPIO_FUNC_UART);
  gpio_set_function(params->tx_gpio_pin, GPIO_FUNC_UART);
  return PICO_OK;
}

void opencube_hwu_close(opencube_hwu *uart) {
  while (is_uart_busy(uart->hw)) {
    tight_loop_contents();
  }
  uart_deinit(uart->hw);
  uart_open[uart_get_index(uart->hw)] = false;
}

void opencube_hwu_start_set_baud(opencube_hwu *uart, uint new_baud) {
  uart->baud_scheduled = new_baud;
}

extern bool opencube_hwu_check_set_baud(opencube_hwu *uart) {
  bool done = false;
  if (uart->baud != uart->baud_scheduled) {
    if (!is_uart_busy(uart->hw)) {
      uart_set_baudrate(uart->hw, uart->baud_scheduled);
      uart->baud = uart->baud_scheduled;
      done = true;
    }
  } else {
    done = true;
  }
  return done;
}

int opencube_hwu_read_one(opencube_hwu *uart) {
  int result = -1;

  if (uart_is_readable(uart->hw)) {
    uint32_t fifo_word = uart_get_hw(uart->hw)->dr;
    if (fifo_word & UART_UARTDR_OE_BITS) {
      uart->overrun = true;
    }
    result = (int) (fifo_word & UART_UARTDR_DATA_BITS);
  }

  return result;
}

bool opencube_hwu_read_many(opencube_hwu *uart, partial_message *win) {
  while (uart_is_readable(uart->hw) && win->done_size < win->total_size) {
    uint32_t fifo_word = uart_get_hw(uart->hw)->dr;
    win->buffer[win->done_size++] = fifo_word & UART_UARTDR_DATA_BITS;
    if (fifo_word & UART_UARTDR_OE_BITS) {
      // fatal: exit early
      uart->overrun = true;
      return false;
    }
  }
  return win->total_size == win->done_size;
}

void opencube_hwu_send(opencube_hwu *uart, const uint8_t *data, size_t length) {
  uart_write_blocking(uart->hw, data, length);
}

void opencube_hwu_clear_rx(opencube_hwu *uart) {
  // drain hw fifo
  while (uart_is_readable(uart->hw)) {
    (void) uart_getc(uart->hw);
  }
}

bool opencube_hwu_has_overrun(opencube_hwu *uart) {
  return uart->overrun || (uart_get_hw(uart->hw)->rsr & UART_UARTRSR_OE_BITS) != 0;
}

bool is_uart_busy(uart_inst_t *hw) {
  return (uart_get_hw(hw)->fr & UART_UARTFR_BUSY_BITS) != 0;
}
