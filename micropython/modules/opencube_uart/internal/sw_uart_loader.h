/**
 * Helper functions for loading PIO programs into hardware.
 *
 * This module is needed as we can only write the PIO program
 * once to the write-only PIO memory. Writing it twice does
 * not return the previous program address, it will place the
 * program at a different PIO IP. To combat this, we save
 * the address returned by the first write and from then
 * only return that.
 */
#ifndef OPENCUBE_SW_UART_LOADER_H
#define OPENCUBE_SW_UART_LOADER_H

#include <hardware/structs/pio.h>

/**
 * Load the receiver program into the given PIO instance.
 * If the program is already loaded, just return its address.
 * @param pio PIO to load the program into
 * @return SM instruction pointer at which the program starts.
 */
extern uint opencube_swu_load_rx(pio_hw_t *pio);

/**
 * Load the transmitter program into the given PIO instance.
 * If the program is already loaded, just return its address.
 * @param pio PIO to load the program into
 * @return SM instruction pointer at which the program starts.
 */
extern uint opencube_swu_load_tx(pio_hw_t *pio);

#endif //OPENCUBE_SW_UART_LOADER_H
