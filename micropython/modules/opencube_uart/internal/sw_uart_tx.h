/**
 * Transmit-side EV3 UART I/F implementation for PIO-emulated UART
 */
#ifndef OPENCUBE_SW_UART_TX_H
#define OPENCUBE_SW_UART_TX_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <hardware/structs/pio.h>
#include "uart_ring.h"

// Status of the baud rate change process
typedef enum {
  // flushing TX FIFO
  SWU_WAITING_FOR_FIFO_DRAIN,
  // waiting for last character to be fully transmitted
  SWU_WAITING_FOR_LAST_CHAR,
  // baud rate change is done
  SWU_DONE
} opencube_swu_baud_change_state;

// Transmitter configuration
typedef struct {
  // Hardware PIO unit to use
  pio_hw_t *pio;
  // Which State Machine to use within the PIO block
  uint sm;
  // Initial baud rate
  uint baud;
  // GPIO pin to use for TX
  uint gpio_pin;
} opencube_swu_tx_config;

// State of the transmitter
typedef struct {
  pio_hw_t *pio;
  uint sm;
  uint baud;
  uint baud_scheduled;
  uint baud_drain_timestamp;
  opencube_swu_baud_change_state baud_state;
} opencube_swu_tx;

/**
 * Initialize the given port.
 *
 * Beware! The state struct must *not* move in memory.
 * There may be globally reachable pointers pointing
 * to it once this function successfully returns.
 *
 * @param tx Where to store the state.
 * @param params How to configure the UART.
 * @return PICO_OK if the port was initialized successfully, some value <0 otherwise.
 */
extern int opencube_swu_tx_open(opencube_swu_tx *tx, const opencube_swu_tx_config *params);

/**
 * Deinitialize the given port.
 * @param tx State struct of the UART.
 */
extern void opencube_swu_tx_close(opencube_swu_tx *tx);

/**
 * Synchronously send a block of data.
 * @param uart UART to send to.
 * @param data Data to send.
 * @param length Length of the data.
 */
extern void opencube_swu_tx_send(opencube_swu_tx *tx, const uint8_t *data, size_t length);

/**
 * Start the process of setting new baud rate.
 *
 * Beware! To advance the switching process, you *must*
 * periodically call opencube_swu_tx_check_set_baud(). With
 * this function you can also determine if the transition has
 * been completed.
 *
 * @param tx UART to configure.
 * @param new_rate Baud rate to set.
 */
extern void opencube_swu_tx_start_set_baud(opencube_swu_tx *tx, uint32_t new_rate);

/**
 * Check if the baud rate switching process has completed.
 * @param tx UART to configure.
 * @return True if the switch is finished, false otherwise.
 */
extern bool opencube_swu_tx_check_set_baud(opencube_swu_tx *tx);

#endif //OPENCUBE_SW_UART_TX_H
