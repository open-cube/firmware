/**
 * Helper functions for loading PIO programs into hardware.
 */
#include <hardware/pio.h>
#include "sw_uart_loader.h"

#ifndef NO_QSTR
// UART implementation
#include "sw_uart.pio.h"
#endif

static int rx_address[2] = {-1, -1};
static int tx_address[2] = {-1, -1};

uint opencube_swu_load_rx(pio_hw_t *pio) {
  uint idx = pio_get_index(pio);
  if (rx_address[idx] < 0) {
    rx_address[idx] = (int) pio_add_program(pio, &uart_rx_program);
  }
  return rx_address[idx];
}

uint opencube_swu_load_tx(pio_hw_t *pio) {
  uint idx = pio_get_index(pio);
  if (tx_address[idx] < 0) {
    tx_address[idx] = (int) pio_add_program(pio, &uart_tx_program);
  }
  return tx_address[idx];
}
