/**
 * EV3 UART I/F implementation for hardware RP2040 UART
 */
#ifndef OPENCUBE_HW_UART_H
#define OPENCUBE_HW_UART_H

#include <stddef.h>
#include <hardware/uart.h>
#include <pico/critical_section.h>
#include "uart_ring.h"

typedef struct {
  uart_inst_t *hw;
  uint rx_gpio_pin;
  uint tx_gpio_pin;
  uint baud;
} opencube_hwu_config;

/**
 * Software state of a hardware UART.
 */
typedef struct {
  uart_inst_t *hw;
  uint baud;
  uint baud_scheduled;
  bool overrun;
} opencube_hwu;

/**
 * Initialize the given port.
 *
 * Beware! The state struct must *not* move in memory.
 * There may be globally reachable pointers pointing
 * to it once this function successfully returns.
 *
 * @param uart State structure for the UART.
 * @param params Initial parameters to use.
 * @return PICO_OK if successful.
 */
extern int opencube_hwu_open(opencube_hwu *uart, const opencube_hwu_config *params);

/**
 * Deactivate the given port.
 * @param uart UART to close.
 */
extern void opencube_hwu_close(opencube_hwu *uart);

/**
 * Start the process of setting new baud rate.
 *
 * Beware! To advance the switching process, you *must*
 * periodically call opencube_hwu_check_set_baud(). With
 * this function you can also determine if the transition has
 * been completed.
 *
 * @param uart UART to configure.
 * @param new_baud Baud rate to set.
 */
extern void opencube_hwu_start_set_baud(opencube_hwu *uart, uint new_baud);

/**
 * Check if the baud rate switching process has completed.
 * @param uart UART to configure.
 * @return True if the switch is finished, false otherwise.
 */
extern bool opencube_hwu_check_set_baud(opencube_hwu *uart);

/**
 * Attempt to read one byte.
 * @param uart UART to read from.
 * @return Received byte or -1 if the input FIFO is empty.
 */
extern int opencube_hwu_read_one(opencube_hwu *uart);

/**
 * Attempt to read more of a message.
 * @param uart UART to read from.
 * @param win Message that is currently being read.
 * @return True if the message is complete, false otherwise. False is returned also if there is an overrun.
 */
extern bool opencube_hwu_read_many(opencube_hwu *uart, partial_message *win);

/**
 * Synchronously send a block of data.
 * @param uart UART to send to.
 * @param data Data to send.
 * @param length Length of the data.
 */
extern void opencube_hwu_send(opencube_hwu *uart, const uint8_t *data, size_t length);

/**
 * Clear the receive FIFO.
 * @param uart UART to clear.
 */
extern void opencube_hwu_clear_rx(opencube_hwu *uart);

/**
 * Check if some bytes were lost during past receive operations.
 * @param uart UART to check.
 * @return True if data was lost, false if everything is OK.
 */
extern bool opencube_hwu_has_overrun(opencube_hwu *uart);


#endif //OPENCUBE_HW_UART_H
