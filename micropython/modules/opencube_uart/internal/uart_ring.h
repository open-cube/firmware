/**
 * Ring buffer implementation for EV3 sensor UARTs.
 *
 * Note: These functions have no locking whatsoever.
 * Calling functions must ensure proper synchronization.
 */
#ifndef OPENCUBE_UART_RING_H
#define OPENCUBE_UART_RING_H

#include <stdint.h>
#include <stdbool.h>

// Size of the buffer in bytes
#define OPENCUBE_UART_RING_SIZE (1 << 5)
// Mask that wraps unbounded indices to fit within the buffer
#define OPENCUBE_UART_RING_MASK (OPENCUBE_UART_RING_SIZE - 1)

// Message that has been only partially read.
typedef struct {
  // Backing buffer to which the message is being read.
  uint8_t *buffer;
  // How many bytes the message has.
  uint32_t total_size;
  // How many bytes are already read into the buffer.
  uint32_t done_size;
} partial_message;

// Ring buffer for UART
// inspired by https://www.snellman.net/blog/archive/2016-12-13-ring-buffers/
typedef struct {
  // Backing array for the buffer.
  uint8_t buffer[OPENCUBE_UART_RING_SIZE];
  // Unbounded read index. This one can go all the way to 0xFF and then wrap to 0.
  uint32_t read_idx;
  // Unbounded write index. This one can go all the way to 0xFF and then wrap to 0.
  uint32_t write_idx;
  // Have some characters been lost?
  bool overrun;
} opencube_uart_ring;

/**
 * Read a single byte from the ring buffer.
 * @param buf Buffer to read from.
 * @return The byte or -1 if the buffer is empty.
 */
extern int opencube_uart_ring_pop(opencube_uart_ring *buf);

/**
 * Push a single byte to the ring buffer.
 *
 * This function is intended to be called from an interrupt handler.
 * @param buf Buffer to write to.
 * @param byte Byte to push.
 * @return True if there was space to push the buffer.
 */
extern bool opencube_uart_ring_push(opencube_uart_ring *buf, uint8_t byte);

/**
 * Try to read more of the given message.
 * @param buf Buffer to read from.
 * @param win Message to continue reading.
 * @return True if the message is now complete, false otherwise.
 */
extern bool opencube_uart_ring_readmsg(opencube_uart_ring *buf, partial_message *win);

/**
 * Clear the buffer contents & reset the overrun flag.
 * @param buf Buffer to clear.
 */
extern void opencube_uart_ring_clear(opencube_uart_ring *buf);

#endif //OPENCUBE_UART_RING_H
