/**
 * Ring buffer implementation for EV3 sensor UARTs.
 */
#include "uart_ring.h"

bool opencube_uart_ring_push(opencube_uart_ring *buf, uint8_t byte) {
  bool had_space = false;

  if ((buf->write_idx - buf->read_idx) < OPENCUBE_UART_RING_SIZE) {
    buf->buffer[buf->write_idx & OPENCUBE_UART_RING_MASK] = byte;
    buf->write_idx++;
    had_space = true;
  }
  if (!had_space) {
    buf->overrun = true;
  }

  return had_space;
}

int opencube_uart_ring_pop(opencube_uart_ring *buf) {
  int result = -1;

  if (buf->write_idx != buf->read_idx) {
    result = buf->buffer[buf->read_idx & OPENCUBE_UART_RING_MASK];
    buf->read_idx++;
  }

  return result;
}

bool opencube_uart_ring_readmsg(opencube_uart_ring *buf, partial_message *win) {
  uint32_t needed = win->total_size - win->done_size;
  uint32_t avail = (uint8_t)(buf->write_idx - buf->read_idx);
  uint32_t transfer_now = needed < avail ? needed : avail;

  for (uint32_t i = 0; i < transfer_now; i++) {
    win->buffer[win->done_size++] = buf->buffer[(buf->read_idx++) & OPENCUBE_UART_RING_MASK];
  }

  return win->total_size == win->done_size;
}

void opencube_uart_ring_clear(opencube_uart_ring *buf) {
  buf->write_idx = 0;
  buf->read_idx = 0;
  buf->overrun = false;
}
