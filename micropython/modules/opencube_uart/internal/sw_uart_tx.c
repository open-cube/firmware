/**
 * Transmit-side EV3 UART I/F implementation for PIO-emulated UART
 */
#include <string.h>
#include <hardware/sync.h>
#include <pico/time.h>
#include <opencube_hw.h>
#include "sw_uart_tx.h"
#include "sw_uart_loader.h"

#ifndef NO_QSTR
// UART implementation
#include "sw_uart.pio.h"
#endif

#define BAUD_FLUSH_DELAY_US 5000

static float calc_uart_multiplier(uint32_t baud) {
  return (float) RP2040_CPU_FREQ / (float) (uart_tx_multiplier * baud);
}

int opencube_swu_tx_open(opencube_swu_tx *tx, const opencube_swu_tx_config *params) {
  tx->pio = params->pio;
  tx->sm = params->sm;
  tx->baud = params->baud;
  tx->baud_scheduled = params->baud;
  tx->baud_state = SWU_DONE;

  pio_gpio_init(tx->pio, params->gpio_pin);
  pio_sm_claim(tx->pio, tx->sm);

  uint tx_ip = opencube_swu_load_tx(tx->pio);
  pio_sm_config txc = uart_tx_program_get_default_config(tx_ip);
  sm_config_set_out_pins(&txc, params->gpio_pin, 1);
  sm_config_set_sideset_pins(&txc, params->gpio_pin);
  sm_config_set_clkdiv(&txc, calc_uart_multiplier(tx->baud));
  sm_config_set_out_shift(&txc, true, false, 0);
  sm_config_set_fifo_join(&txc, PIO_FIFO_JOIN_TX);
  pio_sm_init(tx->pio, tx->sm, tx_ip, &txc);
  pio_sm_set_consecutive_pindirs(tx->pio, tx->sm, params->gpio_pin, 1, true);
  pio_sm_set_enabled(tx->pio, tx->sm, true);
  return PICO_OK;
}

void opencube_swu_tx_close(opencube_swu_tx *tx) {
  bool do_flush = false;
  while (!pio_sm_is_tx_fifo_empty(tx->pio, tx->sm)) {
    tight_loop_contents();
    do_flush = true;
  }
  if (do_flush) {
    sleep_us(BAUD_FLUSH_DELAY_US);
  }

  pio_sm_set_enabled(tx->pio, tx->sm, false);
  pio_sm_unclaim(tx->pio, tx->sm);
}

void opencube_swu_tx_send(opencube_swu_tx *tx, const uint8_t *data, size_t length) {
  for (size_t i = 0; i < length; i++) {
    // blocks if needed
    pio_sm_put_blocking(tx->pio, tx->sm, data[i]);
  }
}

void opencube_swu_tx_start_set_baud(opencube_swu_tx *tx, uint32_t new_rate) {
  if (tx->baud != new_rate || tx->baud_scheduled != new_rate) {
    tx->baud_scheduled = new_rate;
    tx->baud_state = SWU_WAITING_FOR_FIFO_DRAIN;
  }
}

bool opencube_swu_tx_check_set_baud(opencube_swu_tx *tx) {
  bool done = false;
  switch (tx->baud_state) {
    case SWU_WAITING_FOR_FIFO_DRAIN:
      if (pio_sm_is_tx_fifo_empty(tx->pio, tx->sm)) {
        tx->baud_state = SWU_WAITING_FOR_LAST_CHAR;
        tx->baud_drain_timestamp = time_us_32();
      }
      break;
    case SWU_WAITING_FOR_LAST_CHAR:
      if ((time_us_32() - tx->baud_drain_timestamp) > BAUD_FLUSH_DELAY_US) {
        pio_sm_set_clkdiv(tx->pio, tx->sm, calc_uart_multiplier(tx->baud_scheduled));
        tx->baud_state = SWU_DONE;
        tx->baud = tx->baud_scheduled;
        done = true;
      }
      break;
    case SWU_DONE:
      done = true;
      break;
  }
  return done;
}

