/**
 * Receive-side EV3 UART I/F implementation for PIO-emulated UART
 */
#include <string.h>
#include <hardware/sync.h>
#include <hardware/gpio.h>
#include <hardware/pio.h>
#include <opencube_hw.h>
#include "sw_uart_rx.h"
#include "sw_uart_loader.h"

#ifndef NO_QSTR
// UART implementation
#include "sw_uart.pio.h"
#endif


#define BULK_IRQ_THRESHOLD 7

static float calc_uart_multiplier(uint32_t baud);
static void register_irq_handler(opencube_swu_rx *rx);
static void unregister_irq_handler(opencube_swu_rx *rx);
static void pio_irq_handler(void);
static void sm_irq_handler(opencube_swu_rx *rx);
static void isr_ll_insert(opencube_swu_rx *rx);
static void isr_ll_remove(opencube_swu_rx *rx);

static critical_section_t irq_lock;
static bool irq_lock_initialized = false;
static bool irq_enabled_on_any_core = false;
static opencube_swu_rx *irq_handlers = NULL;

int opencube_swu_rx_open(opencube_swu_rx *rx, const opencube_swu_rx_config *config) {
  if (!irq_lock_initialized) {
    critical_section_init(&irq_lock);
    irq_lock_initialized = true;
  }

  rx->pio = config->pio;
  rx->sm = config->sm;
  rx->next_rx = NULL;
  rx->baud = config->baud;
  opencube_uart_ring_clear(&rx->ring);

  gpio_init(config->gpio_pin);
  pio_sm_claim(rx->pio, rx->sm);
  uint rx_ip = opencube_swu_load_rx(rx->pio);
  pio_sm_config rxc = uart_rx_program_get_default_config(rx_ip);
  sm_config_set_in_pins(&rxc, config->gpio_pin);
  sm_config_set_clkdiv(&rxc, calc_uart_multiplier(rx->baud));
  sm_config_set_jmp_pin(&rxc, config->gpio_pin);
  sm_config_set_in_shift(&rxc, true, false, 0);
  sm_config_set_fifo_join(&rxc, PIO_FIFO_JOIN_RX);
  sm_config_set_mov_status(&rxc, STATUS_RX_LESSTHAN, BULK_IRQ_THRESHOLD);
  pio_sm_init(rx->pio, rx->sm, rx_ip, &rxc);
  pio_sm_set_consecutive_pindirs(rx->pio, rx->sm, config->gpio_pin, 1, false);
  pio_set_irq1_source_enabled(rx->pio, pis_interrupt0 + rx->sm, true);

  register_irq_handler(rx);
  pio_sm_set_enabled(rx->pio, rx->sm, true);
  return PICO_OK;
}

void opencube_swu_rx_close(opencube_swu_rx *rx) {
  pio_sm_set_enabled(rx->pio, rx->sm, false);
  pio_sm_unclaim(rx->pio, rx->sm);
  unregister_irq_handler(rx);
}

int opencube_swu_rx_read_one(opencube_swu_rx *rx) {
  critical_section_enter_blocking(&irq_lock);

  // re-query FSM for queued data
  sm_irq_handler(rx);
  int result = opencube_uart_ring_pop(&rx->ring);
  critical_section_exit(&irq_lock);
  return result;
}

bool opencube_swu_rx_read_many(opencube_swu_rx *rx, partial_message *win) {
  critical_section_enter_blocking(&irq_lock);

  // re-query FSM for queued data
  sm_irq_handler(rx);

  // read data from the ring buffer
  bool result = opencube_uart_ring_readmsg(&rx->ring, win);
  critical_section_exit(&irq_lock);

  return result;
}

void opencube_swu_rx_set_baud(opencube_swu_rx *rx, uint32_t new_rate) {
  pio_sm_set_clkdiv(rx->pio, rx->sm, calc_uart_multiplier(new_rate));
  rx->baud = new_rate;
}

void opencube_swu_rx_clear(opencube_swu_rx *rx) {
  critical_section_enter_blocking(&irq_lock);

  // drain hw fifo
  while (!pio_sm_is_rx_fifo_empty(rx->pio, rx->sm)) {
    (void) pio_sm_get(rx->pio, rx->sm);
  }

  // clear buffer & reset overrun flag
  opencube_uart_ring_clear(&rx->ring);
  rx->pio->fdebug &= ~(1 << (rx->sm + PIO_FDEBUG_RXSTALL_LSB));
  critical_section_exit(&irq_lock);
}

bool opencube_swu_rx_has_overrun(opencube_swu_rx *rx) {
  bool result;
  critical_section_enter_blocking(&irq_lock);
  uint rx_stall_mask = (1 << (rx->sm + PIO_FDEBUG_RXSTALL_LSB));
  result = rx->ring.overrun || (rx->pio->fdebug & rx_stall_mask) != 0;
  critical_section_exit(&irq_lock);
  return result;
}

float calc_uart_multiplier(uint32_t baud) {
  return (float) RP2040_CPU_FREQ / (float) (uart_rx_multiplier * baud);
}

void register_irq_handler(opencube_swu_rx *rx) {
  bool enable_irqs_now = false;
  critical_section_enter_blocking(&irq_lock);

  enable_irqs_now = !irq_enabled_on_any_core;
  irq_enabled_on_any_core = true;

  isr_ll_insert(rx);

  critical_section_exit(&irq_lock);

  if (enable_irqs_now) {
    // Shared handlers are scarce (only 4 per program!).
    // To save them, here we claim the IRQ1 on PIO1.
    // MicroPython only uses IRQ0 on both PIO cores.
    irq_set_exclusive_handler(PIO1_IRQ_1, pio_irq_handler);
    irq_set_enabled(PIO1_IRQ_1, true);
  }
}

void unregister_irq_handler(opencube_swu_rx *rx) {
  critical_section_enter_blocking(&irq_lock);
  isr_ll_remove(rx);
  critical_section_exit(&irq_lock);
}

void pio_irq_handler(void) {
  critical_section_enter_blocking(&irq_lock);
  for (opencube_swu_rx *rx = irq_handlers;
       rx != NULL;
       rx = rx->next_rx) {

    if (pio_interrupt_get(rx->pio, rx->sm)) {
      pio_interrupt_clear(rx->pio, rx->sm);
      sm_irq_handler(rx);
    }
  }
  critical_section_exit(&irq_lock);
}

void sm_irq_handler(opencube_swu_rx *rx) {
  while (!pio_sm_is_rx_fifo_empty(rx->pio, rx->sm)) {
    uint8_t data = pio_sm_get(rx->pio, rx->sm) >> 24;
    opencube_uart_ring_push(&rx->ring, data);
  }
}

void isr_ll_insert(opencube_swu_rx *rx) {
  rx->next_rx = irq_handlers;
  irq_handlers = rx;
}

void isr_ll_remove(opencube_swu_rx *rx) {
  for (opencube_swu_rx **chaining_ptr = &irq_handlers;
       *chaining_ptr != NULL;
       chaining_ptr = &(*chaining_ptr)->next_rx) {
    if (*chaining_ptr == rx) {
      *chaining_ptr = rx->next_rx;
      rx->next_rx = NULL;
      break;
    }
  }
}
