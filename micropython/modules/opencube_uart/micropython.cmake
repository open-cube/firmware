# Create an INTERFACE library for our C module.
add_library(usermod_opencube_uart INTERFACE)

# Add our source files to the lib
target_sources(usermod_opencube_uart INTERFACE
        ${CMAKE_CURRENT_LIST_DIR}/internal/uart_ring.c
        ${CMAKE_CURRENT_LIST_DIR}/internal/uart_ring.h
        ${CMAKE_CURRENT_LIST_DIR}/internal/hw_uart.c
        ${CMAKE_CURRENT_LIST_DIR}/internal/hw_uart.h
        ${CMAKE_CURRENT_LIST_DIR}/internal/sw_uart_tx.h
        ${CMAKE_CURRENT_LIST_DIR}/internal/sw_uart_rx.c
        ${CMAKE_CURRENT_LIST_DIR}/internal/sw_uart_rx.h
        ${CMAKE_CURRENT_LIST_DIR}/internal/sw_uart_tx.c
        ${CMAKE_CURRENT_LIST_DIR}/internal/sw_uart_loader.c
        ${CMAKE_CURRENT_LIST_DIR}/internal/sw_uart_loader.h
        ${CMAKE_CURRENT_LIST_DIR}/opencube_uart_if.h
        ${CMAKE_CURRENT_LIST_DIR}/opencube_uart_if.c
)

# Add the current directory as an include directory.
target_include_directories(usermod_opencube_uart INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}
)

pico_generate_pio_header(usermod_opencube_uart
    ${CMAKE_CURRENT_LIST_DIR}/internal/sw_uart.pio
)

target_link_libraries(usermod_opencube_uart INTERFACE
    usermod_opencube_pindefs
)

# Link our INTERFACE library to the usermod target.
target_link_libraries(usermod INTERFACE usermod_opencube_uart)
