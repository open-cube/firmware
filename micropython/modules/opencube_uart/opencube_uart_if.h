/**
 * Unified interface for Open-Cube UART ports
 */
#ifndef OPENCUBE_UART_IF_H
#define OPENCUBE_UART_IF_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <opencube_hw.h>
#include <internal/hw_uart.h>
#include <internal/sw_uart_tx.h>
#include <internal/sw_uart_rx.h>
#include "internal/uart_ring.h"

// (Preferably opaque) UART port structure.
typedef struct {
  opencube_sensor_port port;
  union {
    opencube_hwu hw;
    struct opencube_swu {
      opencube_swu_tx tx;
      opencube_swu_rx rx;
    } pio;
  } impl;
} opencube_uart_if;

typedef struct opencube_swu opencube_swu;

/**
 * Open the given port and set it to handshake mode.
 *
 * Beware! The state struct must *not* move in memory.
 * There may be globally reachable pointers pointing
 * to it once this function successfully returns.
 *
 * @param port Port to which to bind.
 * @param out State structure for the UART.
 * @return PICO_OK if successful.
 */
extern int opencube_uart_open(opencube_sensor_port port, opencube_uart_if *out);

/**
 * Deactivate the given port.
 * @param uart UART to close.
 */
extern void opencube_uart_close(opencube_uart_if *uart);

/**
 * Start the process of setting new baud rate.
 *
 * Beware! To advance the switching process, you *must*
 * periodically call opencube_uart_check_set_baud(). With
 * this function you can also determine if the transition has
 * been completed.
 *
 * @param uart UART to configure.
 * @param new_rate Baud rate to set.
 */
extern void opencube_uart_start_set_baud(opencube_uart_if *uart, uint32_t new_rate);

/**
 * Check if the baud rate switching process has completed.
 * @param uart UART to configure.
 * @return True if the switch is finished, false otherwise.
 */
extern bool opencube_uart_check_set_baud(opencube_uart_if *uart);

/**
 * Attempt to read one byte.
 * @param uart UART to read from.
 * @return Received byte or -1 if the input FIFO is empty.
 */
extern int opencube_uart_read_one(opencube_uart_if *uart);

/**
 * Attempt to read more of a message.
 * @param uart UART to read from.
 * @param win Message that is currently being read.
 * @return True if the message is complete, false otherwise. False is returned also if there is an overrun.
 */
extern bool opencube_uart_read_many(opencube_uart_if *uart, partial_message *win);

/**
 * Synchronously send a block of data.
 * @param uart UART to send to.
 * @param data Data to send.
 * @param length Length of the data.
 */
extern void opencube_uart_write(opencube_uart_if *uart, const uint8_t *data, size_t length);

/**
 * Clear the receive FIFO.
 * @param uart UART to clear.
 */
extern void opencube_uart_clear_rx(opencube_uart_if *uart);

/**
 * Check if some bytes were lost during past receive operations.
 * @param uart UART to check.
 * @return True if data was lost, false if everything is OK.
 */
extern bool opencube_uart_had_rx_overrun(opencube_uart_if *uart);

#endif //OPENCUBE_UART_IF_H
