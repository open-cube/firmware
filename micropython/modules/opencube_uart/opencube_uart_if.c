/**
 * Unified interface for Open-Cube UART ports
 */
#include <hardware/structs/uart.h>
#include <hardware/pio.h>
#include <internal/hw_uart.h>
#include <internal/sw_uart_rx.h>
#include <internal/sw_uart_tx.h>
#include <opencube_hw.h>
#include "opencube_uart_if.h"

#define UART_HANDSHAKE_BAUD 2400

typedef struct {
  opencube_swu_rx_config rx;
  opencube_swu_tx_config tx;
} opencube_swu_config;

const struct {
  opencube_hwu_config port1;
  opencube_swu_config port2;
  opencube_swu_config port3;
  opencube_swu_config port4;
} default_config = {
    .port1 = {
        .hw = SENSOR1_UART_PERIPHERAL,
        .rx_gpio_pin = SENSOR1_UART_RX_PIN,
        .tx_gpio_pin = SENSOR1_UART_TX_PIN,
        .baud = UART_HANDSHAKE_BAUD,
    },
    .port2 = {
        .rx = {
            .pio = SENSOR2_UART_RX_PIO,
            .sm = SENSOR2_UART_RX_SM,
            .gpio_pin = SENSOR2_UART_RX_PIN,
            .baud = UART_HANDSHAKE_BAUD,
        },
        .tx = {
            .pio = SENSOR2_UART_TX_PIO,
            .sm = SENSOR2_UART_TX_SM,
            .gpio_pin = SENSOR2_UART_TX_PIN,
            .baud = UART_HANDSHAKE_BAUD,
        },
    },
    .port3 = {
        .rx = {
            .pio = SENSOR3_UART_RX_PIO,
            .sm = SENSOR3_UART_RX_SM,
            .gpio_pin = SENSOR3_UART_RX_PIN,
            .baud = UART_HANDSHAKE_BAUD,
        },
        .tx = {
            .pio = SENSOR3_UART_TX_PIO,
            .sm = SENSOR3_UART_TX_SM,
            .gpio_pin = SENSOR3_UART_TX_PIN,
            .baud = UART_HANDSHAKE_BAUD,
        },
    },
    .port4 = {
        .rx = {
            .pio = SENSOR4_UART_RX_PIO,
            .sm = SENSOR4_UART_RX_SM,
            .gpio_pin = SENSOR4_UART_RX_PIN,
            .baud = UART_HANDSHAKE_BAUD,
        },
        .tx = {
            .pio = SENSOR4_UART_TX_PIO,
            .sm = SENSOR4_UART_TX_SM,
            .gpio_pin = SENSOR4_UART_TX_PIN,
            .baud = UART_HANDSHAKE_BAUD,
        },
    },
};

static bool port_claimed[NUM_SENSOR_PORTS];

static int opencube_swu_open(opencube_swu *uart, const opencube_swu_config *config);
static inline bool is_hw_uart(opencube_uart_if *uart);

int opencube_uart_open(opencube_sensor_port port, opencube_uart_if *out) {
  if (port >= NUM_SENSOR_PORTS) {
    return PICO_ERROR_INVALID_ARG;
  }
  if (port_claimed[port]) {
    return PICO_ERROR_NOT_PERMITTED;
  }

  out->port = port;
  int result;
  switch (port) {
    case OPENCUBE_SENSOR_1:
      result = opencube_hwu_open(&out->impl.hw, &default_config.port1);
      break;
    case OPENCUBE_SENSOR_2:
      result = opencube_swu_open(&out->impl.pio, &default_config.port2);
      break;
    case OPENCUBE_SENSOR_3:
      result = opencube_swu_open(&out->impl.pio, &default_config.port3);
      break;
    case OPENCUBE_SENSOR_4:
      result = opencube_swu_open(&out->impl.pio, &default_config.port4);
      break;
    default:
      panic("check for sensor port index did not catch an unknown port");
  }

  if (result == PICO_OK) {
    port_claimed[port] = true;
  }
  return result;
}

int opencube_swu_open(opencube_swu *uart, const opencube_swu_config *config) {
  int result;
  result = opencube_swu_rx_open(&uart->rx, &config->rx);
  if (result == PICO_OK) {
    result = opencube_swu_tx_open(&uart->tx, &config->tx);
    if (result != PICO_OK) {
      opencube_swu_rx_close(&uart->rx);
    }
  }
  return result;
}

void opencube_uart_close(opencube_uart_if *uart) {
  if (is_hw_uart(uart)) {
    opencube_hwu_close(&uart->impl.hw);
  } else {
    opencube_swu_tx_close(&uart->impl.pio.tx);
    opencube_swu_rx_close(&uart->impl.pio.rx);
  }
  port_claimed[uart->port] = false;
}

int opencube_uart_read_one(opencube_uart_if *uart) {
  int result;
  if (is_hw_uart(uart)) {
    result = opencube_hwu_read_one(&uart->impl.hw);
  } else {
    result = opencube_swu_rx_read_one(&uart->impl.pio.rx);
  }
  return result;
}

bool opencube_uart_read_many(opencube_uart_if *uart, partial_message *win) {
  bool result;
  if (is_hw_uart(uart)) {
    result = opencube_hwu_read_many(&uart->impl.hw, win);
  } else {
    result = opencube_swu_rx_read_many(&uart->impl.pio.rx, win);
  }
  return result;
}

void opencube_uart_write(opencube_uart_if *uart, const uint8_t *data, size_t length) {
  if (is_hw_uart(uart)) {
    opencube_hwu_send(&uart->impl.hw, data, length);
  } else {
    opencube_swu_tx_send(&uart->impl.pio.tx, data, length);
  }
}

void opencube_uart_start_set_baud(opencube_uart_if *uart, uint32_t new_rate) {
  if (is_hw_uart(uart)) {
    opencube_hwu_start_set_baud(&uart->impl.hw, new_rate);
  } else {
    opencube_swu_rx_set_baud(&uart->impl.pio.rx, new_rate);
    opencube_swu_tx_start_set_baud(&uart->impl.pio.tx, new_rate);
  }
}

bool opencube_uart_check_set_baud(opencube_uart_if *uart) {
  bool result;
  if (is_hw_uart(uart)) {
    result = opencube_hwu_check_set_baud(&uart->impl.hw);
  } else {
    result = opencube_swu_tx_check_set_baud(&uart->impl.pio.tx);
  }
  return result;
}

void opencube_uart_clear_rx(opencube_uart_if *uart) {
  if (is_hw_uart(uart)) {
    opencube_hwu_clear_rx(&uart->impl.hw);
  } else {
    opencube_swu_rx_clear(&uart->impl.pio.rx);
  }
}

bool opencube_uart_had_rx_overrun(opencube_uart_if *uart) {
  bool result;
  if (is_hw_uart(uart)) {
    result = uart->impl.hw.overrun;
  } else {
    result = opencube_swu_rx_has_overrun(&uart->impl.pio.rx);
  }
  return result;
}

bool is_hw_uart(opencube_uart_if *uart) {
  return uart->port == OPENCUBE_SENSOR_1;
}
