#ifndef NXT_ANALOG_SENSOR_H
#define NXT_ANALOG_SENSOR_H

#include <hardware/gpio.h>
#include <opencube_hw.h>
#include <stdlib.h>
#include "pico/stdlib.h"

#include "ADS1119.h"

// Initialize all analog sensors, assign output pins and channels and initialize ADC
extern void opencube_analog_sensors_init(void);

// Initialize analog sensor on given port
extern void opencube_analog_sensor_init(uint8_t port);

// Deinitialize all analog sensors
extern void opencube_analog_sensors_deinit(void);

// Deinitialize analog sensor on given port
extern void opencube_analog_sensor_deinit(uint8_t port);

// Turn on given port output pin
extern void opencube_analog_sensor_turn_on(uint8_t port);

// Turn off given port output pin
extern void opencube_analog_sensor_turn_off(uint8_t port);

// Toggle given port output pin
extern void opencube_analog_sensor_toggle(uint8_t port);

// Set switching mode for continuous measurement on given port
extern void opencube_analog_sensor_set_switching(uint8_t port, bool switching);

// Set ADC channel for given port
extern void opencube_analog_sensor_set_channel(uint8_t port);

// Read value on given port, switch ADC channel if needed
extern void opencube_analog_sensor_read_value(uint8_t port);

// Read value on given port if needed or return last read value in continuous mode
// pin_on is used to indicate if read data is when output pin is on or off
extern uint16_t opencube_analog_sensor_get_value(uint8_t port, bool pin_on);

// Start continuous measurement with given period and wait time on ports with continuous measurement enabled
// Wait time is used to wait for analog senor to settle after switching active pin state
extern void opencube_analog_sensor_start_continuous(uint32_t period_us, uint32_t wait_time_us);

// Stop continuous measurement on all ports
extern void opencube_analog_sensor_stop_continuous(void);

// Check if new data is available on given port
// pin_on is used to indicate if read data are when pin is on or off
extern bool opencube_analog_sensor_new_data(uint8_t port, bool pin_on);

#endif