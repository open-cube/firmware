# Create an INTERFACE library for our C module.
add_library(usermod_analog_sensor INTERFACE)

# Add our source files to the lib
target_sources(usermod_analog_sensor INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}/analog_sensor.h
    ${CMAKE_CURRENT_LIST_DIR}/analog_sensor.c
    ${CMAKE_CURRENT_LIST_DIR}/analog_sensor_api.c
)

# Add the current directory as an include directory.
target_include_directories(usermod_analog_sensor INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}
)

target_link_libraries(usermod_analog_sensor INTERFACE
    usermod_opencube_pindefs
    usermod_opencube_lib
)

# Link our INTERFACE library to the usermod target.
target_link_libraries(usermod INTERFACE usermod_analog_sensor)