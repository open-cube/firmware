// MicroPython API for the AnalogSensor
#include <py/runtime.h>
#include <stdlib.h>
#include "pico/stdlib.h"
#include "analog_sensor.h"

static void analog_sensor_print(const mp_print_t *print, mp_obj_t self_in);

// This structure represents AnalogSensor instance objects.
typedef struct _analog_sensor_obj_t {
    mp_obj_base_t base;
    uint8_t port;
} analog_sensor_obj_t;

// Called on AnalogSensor init
// Initialize AnalogSensor on given port
STATIC mp_obj_t analog_sensor_make_new(const mp_obj_type_t* type, size_t n_args, size_t n_kw, const mp_obj_t* args) {
    mp_arg_check_num(n_args, n_kw, 1, 1, false);
    int port = mp_obj_get_int(args[0]);
    if (port < 0 || port > 3) {
        mp_raise_msg_varg(&mp_type_ValueError, "Port %d is not a valid sensor port", port+1);
    }
    analog_sensor_obj_t *self = m_new_obj_with_finaliser(analog_sensor_obj_t);

    self->port = port;
    self->base.type = type;

    opencube_analog_sensor_init(self->port);

    return MP_OBJ_FROM_PTR(self);
}

static void analog_sensor_print(const mp_print_t *print, mp_obj_t self_in) {
    analog_sensor_obj_t *self = MP_OBJ_TO_PTR(self_in);
    mp_printf(print, "Analog sensor(%d)", self->port + 1);
}

// Turn on output pin
STATIC mp_obj_t analog_sensor_on(mp_obj_t self_in) {
    analog_sensor_obj_t *self = MP_OBJ_TO_PTR(self_in);
    opencube_analog_sensor_turn_on(self->port);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(analog_sensor_on_obj, analog_sensor_on);

// Turn off output pin
STATIC mp_obj_t analog_sensor_off(mp_obj_t self_in) {
    analog_sensor_obj_t *self = MP_OBJ_TO_PTR(self_in);
    opencube_analog_sensor_turn_off(self->port);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(analog_sensor_off_obj, analog_sensor_off);

// Toggle output pin 
STATIC mp_obj_t analog_sensor_toggle(mp_obj_t self_in) {
    analog_sensor_obj_t *self = MP_OBJ_TO_PTR(self_in);
    opencube_analog_sensor_toggle(self->port);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(analog_sensor_toggle_obj, analog_sensor_toggle);

// In normal mode - measure and return analog value
// In continuous mode - return last measured analog value
// If pin_on is True - last value when output pin was on, False - last value when output pin was off
STATIC mp_obj_t analog_sensor_get_value(mp_obj_t self_in, mp_obj_t pin_on) {
    analog_sensor_obj_t *self = MP_OBJ_TO_PTR(self_in);
    bool on = (mp_obj_get_int(pin_on) == 1);

    uint16_t value = opencube_analog_sensor_get_value(self->port, on);
    return mp_obj_new_int(value);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(analog_sensor_get_value_obj, analog_sensor_get_value);

// Check if new data is available
STATIC mp_obj_t analog_sensor_new_data(mp_obj_t self_in, mp_obj_t led_on) {
    analog_sensor_obj_t *self = MP_OBJ_TO_PTR(self_in);
    bool led_on_bool = (mp_obj_get_int(led_on) == 1);
    return mp_obj_new_int(opencube_analog_sensor_new_data(self->port, led_on_bool));
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(analog_sensor_new_data_obj, analog_sensor_new_data);

// Set sensor port switching mode for continuous measurement.
// Output value is measured automaticaly with the output pin on and with the output pin off
STATIC mp_obj_t analog_sensor_set_switching(mp_obj_t self_in) {
    analog_sensor_obj_t *self = MP_OBJ_TO_PTR(self_in);
    opencube_analog_sensor_set_switching(self->port, true);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(analog_sensor_set_switching_obj, analog_sensor_set_switching);

// Set sensor port to non-switching mode for continuous measurement.
STATIC mp_obj_t analog_sensor_stop_switching(mp_obj_t self_in) {
    analog_sensor_obj_t *self = MP_OBJ_TO_PTR(self_in);
    opencube_analog_sensor_set_switching(self->port, false);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(analog_sensor_stop_switching_obj, analog_sensor_stop_switching);

// Set all analog sensors to continuous mode with given period (us)
// Wait given time (us) after switching output pin state in switching mode 
STATIC mp_obj_t analog_sensor_set_continuous(mp_obj_t self_in, mp_obj_t period_us, mp_obj_t wait_us) {
    uint32_t period = mp_obj_get_int(period_us);
    uint32_t wait = mp_obj_get_int(wait_us);
    opencube_analog_sensor_start_continuous(period, wait);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_3(analog_sensor_set_continuous_obj, analog_sensor_set_continuous);

// Stop continuous measurement of all analog sensors
STATIC mp_obj_t analog_sensor_stop_continuous(mp_obj_t self_in) {
    opencube_analog_sensor_stop_continuous();
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(analog_sensor_stop_continuous_obj, analog_sensor_stop_continuous);

// Deinitialize analog sensor
STATIC mp_obj_t analog_sensor_deinit(mp_obj_t self_in) {
    analog_sensor_obj_t *self = MP_OBJ_TO_PTR(self_in);
    opencube_analog_sensor_deinit(self->port);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(analog_sensor_deinit_obj, analog_sensor_deinit);

// This collects all methods and other static class attributes of the AnalogSensor.
STATIC const mp_rom_map_elem_t analog_sensor_locals_dict[] = {
    { MP_ROM_QSTR(MP_QSTR_on), MP_ROM_PTR(&analog_sensor_on_obj) },
    { MP_ROM_QSTR(MP_QSTR_off), MP_ROM_PTR(&analog_sensor_off_obj) },
    { MP_ROM_QSTR(MP_QSTR_toggle), MP_ROM_PTR(&analog_sensor_toggle_obj) },
    { MP_ROM_QSTR(MP_QSTR_get_value), MP_ROM_PTR(&analog_sensor_get_value_obj) },
    { MP_ROM_QSTR(MP_QSTR_new_data), MP_ROM_PTR(&analog_sensor_new_data_obj) },
    { MP_ROM_QSTR(MP_QSTR_set_switching), MP_ROM_PTR(&analog_sensor_set_switching_obj) },
    { MP_ROM_QSTR(MP_QSTR_stop_switching), MP_ROM_PTR(&analog_sensor_stop_switching_obj) },
    { MP_ROM_QSTR(MP_QSTR_set_continuous), MP_ROM_PTR(&analog_sensor_set_continuous_obj) },
    { MP_ROM_QSTR(MP_QSTR_stop_continuous), MP_ROM_PTR(&analog_sensor_stop_continuous_obj)},
    { MP_ROM_QSTR(MP_QSTR_deinit), MP_ROM_PTR(&analog_sensor_deinit_obj) },
};
STATIC MP_DEFINE_CONST_DICT(analog_sensor_locals_dict_obj, analog_sensor_locals_dict);

// This defines the type(AnalogSensor) object.
MP_DEFINE_CONST_OBJ_TYPE(
    opencube_analog_sensor,
    MP_QSTR_AnalogSensor,
    MP_TYPE_FLAG_NONE,
    make_new, &analog_sensor_make_new,
    locals_dict, &analog_sensor_locals_dict_obj,
    print, &analog_sensor_print
);

// Define all properties of the module.
// Table entries are key/value pairs of the attribute name (a string)
// and the MicroPython object reference.
// All identifiers and strings are written as MP_QSTR_xxx and will be
// optimized to word-sized integers by the build system (interned strings).
STATIC const mp_rom_map_elem_t analog_sensor_globals_dict[] = {
    { MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_analog_sensor) },
    { MP_ROM_QSTR(MP_QSTR_AnalogSensor), MP_ROM_PTR(&opencube_analog_sensor) },
};
STATIC MP_DEFINE_CONST_DICT(analog_sensor_globals_dict_obj, analog_sensor_globals_dict);

// Define module object.
const mp_obj_module_t analog_sensor = {
    .base = {.type = &mp_type_module },
    .globals = (mp_obj_dict_t*)&analog_sensor_globals_dict_obj,
};

// Register the module to make it available in Python.
MP_REGISTER_MODULE(MP_QSTR_analog_sensor, analog_sensor);