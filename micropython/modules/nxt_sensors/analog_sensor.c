// Include MicroPython API.

#include <hardware/gpio.h>

#include <stdlib.h>
#include "pico/stdlib.h"
#include <hardware/timer.h>
#include <pico/time.h>

#include <opencube_hw.h>
#include <ADS1119.h>
#include "i2c_locking.h"
#include "analog_sensor.h"

// Struct for analog sensor port
typedef struct {
    bool enabled;
    bool pin_state;
    bool switching;
    bool continuous_state;
    bool new_data_on;
    bool new_data_off;
    bool scanned;
    uint8_t pin;
    uint8_t channel;
    uint16_t last_value_on;
    uint16_t last_value_off;
} analog_port;

// Struct for all analog sensors
typedef struct {
    analog_port ports[NUM_SENSOR_PORTS];
    alarm_pool_t *timer_pool;
    repeating_timer_t timer;
    ADS1119_adc_t *adc;
    bool continuous;
    uint8_t continuous_state;
    uint8_t current_port;
    bool enabled;
    bool waiting;
    uint32_t wait_time;
    uint32_t wait_time_counter;
    uint32_t cycle_period;
} analog_sensors;

static analog_sensors sensors = {.enabled = false};

static bool opencube_analog_sensor_continuous(repeating_timer_t *timer);

void opencube_analog_sensors_init(void) {
    if (sensors.enabled) {
        return;
    }
    for (int i = 0; i < NUM_SENSOR_PORTS; i++)
    {
        sensors.ports[i].enabled = false;
        switch (i)
        {
        case OPENCUBE_SENSOR_1:
        sensors.ports[i].pin = SENSOR1_ACTIVE_PIN;
        sensors.ports[i].channel = SENSOR1_ADC_CHANNEL;
        break;
        case OPENCUBE_SENSOR_2:
        sensors.ports[i].pin = SENSOR2_ACTIVE_PIN;
        sensors.ports[i].channel = SENSOR2_ADC_CHANNEL;
        break;
        case OPENCUBE_SENSOR_3:
        sensors.ports[i].pin = SENSOR3_ACTIVE_PIN;
        sensors.ports[i].channel = SENSOR3_ADC_CHANNEL;
        break;
        case OPENCUBE_SENSOR_4:
        sensors.ports[i].pin = SENSOR4_ACTIVE_PIN;
        sensors.ports[i].channel = SENSOR4_ADC_CHANNEL;
        break;

        default:
        break;
        }
    }

    ADS1119_adc_t *adc = (ADS1119_adc_t*)malloc(sizeof(ADS1119_adc_t));
    sensors.adc = adc;
    opencube_lock_i2c_or_raise();
    ADS1119_init(sensors.adc);
    ADS1119_set_vref(sensors.adc, 1);
    ADS1119_set_data_rate(sensors.adc, 3);
    ADS1119_write_config(sensors.adc);
    opencube_unlock_i2c();
    sensors.current_port = 255;
    sensors.enabled = true;
}

void opencube_analog_sensor_init(uint8_t port) {
    opencube_analog_sensors_init();
    if (sensors.ports[port].enabled) {
        return;
    }
    sensors.ports[port].enabled = true;
    sensors.ports[port].switching = false;
    sensors.ports[port].continuous_state = false;
    sensors.ports[port].new_data_on = false;
    sensors.ports[port].new_data_off = false;
    sensors.ports[port].scanned = false;
    sensors.ports[port].last_value_on = 0;
    sensors.ports[port].last_value_off = 0;

    gpio_init(sensors.ports[port].pin);
    gpio_set_dir(sensors.ports[port].pin, GPIO_OUT);
}

void opencube_analog_sensors_deinit(void) {
    for (int i = 0; i < NUM_SENSOR_PORTS; i++)
    {
		opencube_analog_sensor_deinit(i);
	}
    sensors.adc = NULL;
    sensors.enabled = false;
}

void opencube_analog_sensor_deinit(uint8_t port) {
    sensors.ports[port].enabled = false;
    opencube_analog_sensor_turn_off(port);
    opencube_analog_sensor_set_switching(port, false);
    for (int i = 0; i < NUM_SENSOR_PORTS; i++)
    {
        if (sensors.ports[i].enabled) {
            return;
        }
    }
    sensors.current_port = 255;
}

void opencube_analog_sensor_turn_on(uint8_t port) {
    if (sensors.ports[port].enabled) {
        gpio_put(sensors.ports[port].pin, 1);
        sensors.ports[port].pin_state = true;
    }
}

void opencube_analog_sensor_turn_off(uint8_t port) {
    if (sensors.ports[port].enabled) {
        gpio_put(sensors.ports[port].pin, 0);
        sensors.ports[port].pin_state = false;
    }
}

void opencube_analog_sensor_toggle(uint8_t port) {
    if (sensors.ports[port].enabled) {
        if (sensors.ports[port].pin_state){
            gpio_put(sensors.ports[port].pin, 0);
        } else {
            gpio_put(sensors.ports[port].pin, 1);
        }
        sensors.ports[port].pin_state = !sensors.ports[port].pin_state;
    }
}

void opencube_analog_sensor_set_switching(uint8_t port, bool switching) {
        sensors.ports[port].switching = switching;
}

void opencube_analog_sensor_set_channel(uint8_t port) {
    if (sensors.ports[port].enabled) {
        ADS1119_set_channel(sensors.adc, sensors.ports[port].channel);
        ADS1119_write_config(sensors.adc);
        sensors.current_port = port;
    }
}

void opencube_analog_sensor_read_value(uint8_t port) {
    if (sensors.ports[port].enabled) {
        // update ADC channel if needed
        if (sensors.current_port != port) {
            opencube_lock_i2c_or_raise();
            opencube_analog_sensor_set_channel(port);
            opencube_unlock_i2c();
        }
        // blocking ADC value read
        uint16_t value = ADS1119_lockBusAndReadTwoBytes(sensors.adc);
        // if in switching mode update value depending on led state
        if (sensors.ports[port].switching) {
            if (sensors.ports[port].pin_state) {
                sensors.ports[port].last_value_on = value;
            } else {
                sensors.ports[port].last_value_off = value;
            }
        // update both values
        } else {
            sensors.ports[port].last_value_off = value;
            sensors.ports[port].last_value_on = value;
        }

    }
}

uint16_t opencube_analog_sensor_get_value(uint8_t port, bool pin_on) {
    if (sensors.ports[port].enabled) {
        // if not continuous get a measurement first
        if (!sensors.continuous) {   
            // blocking read
            opencube_analog_sensor_read_value(port);
        }

        // if switching mode is on it does not matter which value is returned
        if (pin_on) {
            sensors.ports[port].new_data_on = false;
            return sensors.ports[port].last_value_on;
        } else {
            sensors.ports[port].new_data_off = false;
            return sensors.ports[port].last_value_off;
        }
    } else {
        return 0;
    }
    
}

void opencube_analog_sensor_start_continuous(uint32_t period_us, uint32_t wait_time_us) {
    sensors.wait_time_counter = 0;
    sensors.wait_time = wait_time_us;
    sensors.cycle_period = period_us;
    if (sensors.continuous) {
        opencube_analog_sensor_stop_continuous();
    } 
    sensors.continuous = true;
    sensors.timer_pool = alarm_pool_get_default();
        alarm_pool_add_repeating_timer_us(
            sensors.timer_pool,
            sensors.cycle_period,
            &opencube_analog_sensor_continuous,
            NULL,
            &sensors.timer
        );
    
}

void opencube_analog_sensor_stop_continuous(void) {
    if (sensors.continuous) {
        alarm_pool_cancel_alarm(sensors.timer_pool, sensors.timer.alarm_id);
        sensors.continuous = false;
    }
}

bool opencube_analog_sensor_new_data(uint8_t port, bool pin_on) {
    if (pin_on) {
        return sensors.ports[port].new_data_on;
    }
    else {
        return sensors.ports[port].new_data_off;
    }
}

// Timer callback for continuous mode
// Timer period is given by period_us in continuos measurement function.
// Wait time is given by wait_time_us in continuous measurement function and is used to 
// skip a period if wait time has not been fullfiled to wait for the sensor to settle 
// after switching output pin state.
// Measure all initialized analog sensors.
// Can measure 1-4 connected sensors that can be in switching or non-switching mode
// if a sensor is in switching mode it will measure the value for both states of the output pin.
bool opencube_analog_sensor_continuous(repeating_timer_t *timer) {
    if (sensors.waiting) {
        sensors.wait_time_counter += sensors.cycle_period;
        if (sensors.wait_time_counter < sensors.wait_time) {
            return true; // nothing to do now, yield back to program
        }
    }
    if (opencube_try_lock_i2c()) {
        // i2c bus is free, update state to reflect that the code below will run
        sensors.waiting = false;
        sensors.wait_time_counter = 0;
    } else {
        // i2c bus is locked, try next time
        return true;
    }

    uint16_t value = ADS1119_read(sensors.adc);
    sensors.ports[sensors.current_port].scanned = true;

    // in switching mode save read value to desired variable
    if (sensors.ports[sensors.current_port].switching) {
        if (sensors.ports[sensors.current_port].pin_state) {
            sensors.ports[sensors.current_port].last_value_on = value;
            sensors.ports[sensors.current_port].new_data_on = true;
        } else {
            sensors.ports[sensors.current_port].last_value_off = value;
            sensors.ports[sensors.current_port].new_data_off = true;
            
        }
    // in non switching mode save read value to both variables
    } else {
        sensors.ports[sensors.current_port].new_data_on = true;
        sensors.ports[sensors.current_port].new_data_off = true;
        sensors.ports[sensors.current_port].last_value_on = value;
        sensors.ports[sensors.current_port].last_value_off = value;
    }
    

    // cycle through all sensors to find the first one enabled
    // change output pin state if port is in switching mode
    sensors.current_port = (sensors.current_port+1)%NUM_SENSOR_PORTS;
    bool start_next_read = true;
    for (uint8_t i = 0; i < NUM_SENSOR_PORTS; i++) {
        if (sensors.ports[sensors.current_port].enabled && !sensors.ports[sensors.current_port].scanned) {
            break;
        }
        sensors.current_port = (sensors.current_port+1)%NUM_SENSOR_PORTS;
        if (i+1 == NUM_SENSOR_PORTS) {
            sensors.current_port = 0;
            for (uint8_t j = 0; j < NUM_SENSOR_PORTS; j++) {
                sensors.ports[sensors.current_port].scanned = false;
                if(sensors.ports[sensors.current_port].enabled && sensors.ports[sensors.current_port].switching) {
                    if(sensors.ports[sensors.current_port].pin_state) {
                        opencube_analog_sensor_turn_off(sensors.current_port);
                    } else {
                        opencube_analog_sensor_turn_on(sensors.current_port);
                    }
                }
                sensors.current_port = (sensors.current_port+1)%NUM_SENSOR_PORTS;
            }
            sensors.waiting = true;
            start_next_read = false;
            break;
        }
    }
    // Switch to a different adc channel and start new conversion
    if (start_next_read) {
        opencube_analog_sensor_set_channel(sensors.current_port);
        ADS1119_commandStart(sensors.adc);
    }

    opencube_unlock_i2c();
    return true;
}