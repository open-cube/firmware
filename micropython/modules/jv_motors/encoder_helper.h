/**
 * Helper module for converting absolute motor position to a relative one
 */
#ifndef FIRMWARE_ENCODER_HELPER_H
#define FIRMWARE_ENCODER_HELPER_H

#include <stdint.h>
#include <stdbool.h>

// Relative encoder object
typedef struct {
  // Port number to use (used for pulling the position/speed from a lower-level API)
  int port;
  // "Absolute" position that corresponds to a relative zero of this object
  uint32_t ref_position;
  // Whether the relative encoder increments in the same direction as the absolute encoder value
  bool forward;
} adjustable_encoder;

/**
 * Initialize a new abs->rel encoder converter object.
 * @param self Encoder converter object
 * @param port Open-Cube port to use
 * @param forward Whether this encoder object will count up the same way the low-level API does
 */
extern void adjustable_encoder_init(adjustable_encoder *self, int port, bool forward);

/**
 * Deinitialize a new abs->rel encoder converter object.
 * @param self Encoder converter object
 */
extern void adjustable_encoder_deinit(adjustable_encoder *self);

/**
 * Adjust the zero point of this encoder object.
 * @param self Encoder converter object
 * @param rel_pos The angle that adjustable_encoder_get_angle() will
 *                return immediately after this function is called.
 */
extern void adjustable_encoder_set_angle(adjustable_encoder *self, int32_t rel_pos);

/**
 * Get the current relative motor position.
 * @param self Encoder converter object
 * @return Motor position in degrees. The output is adjusted
 *         for offset and sign as needed.
 */
extern int32_t adjustable_encoder_get_angle(adjustable_encoder *self);

/**
 * Get the current motor rotation speed.
 * @param self Encoder converter object
 * @return Motor speed in deg/s. The output is adjusted for sign as needed.
 */
extern int32_t adjustable_encoder_get_speed(adjustable_encoder *self);

#endif //FIRMWARE_ENCODER_HELPER_H
