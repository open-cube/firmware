/**
 * Internal PID controller implementation.
 */
#ifndef FIRMWARE_JV_PID_H
#define FIRMWARE_JV_PID_H

#include <stdbool.h>
#include "move_generator.h"
#include "encoder_helper.h"

// Default sampling time
#define REGULATOR_SAMPLING_MSEC (1)
#define REGULATOR_SAMPLING_USEC (REGULATOR_SAMPLING_MSEC * 1000)
#define REGULATOR_SAMPLING_SEC_FLOAT (REGULATOR_SAMPLING_MSEC / 1000.0f)

// Meaning of PID controller state
typedef enum {
  // State is for position regulation
  PID_MODE_POSITION,
  // State is for speed regulation
  PID_MODE_SPEED
} pid_mode_t;

typedef struct {
  // Position regulator: Proportional feedback
  float pos_kp;
  // Position regulator: Integral feedback
  float pos_ki;
  // Position regulator: Derivative feedback
  float pos_kd;
  // Speed regulator: Proportional feedback
  float speed_kp;
  // Speed regulator: Integral feedback
  float speed_ki;
  // All regulators: Speed feedforward
  float kf_speed;
  // Position regulator: Acceleration feedforward
  float kf_accel;
} pid_controller_config_t;

typedef struct {
  pid_controller_config_t config;
  // Integrated feedback controller action
  float integral;
  // Limit for controller action -- it will not output a value larger than this
  int max_power;
  // Whether the controller action is currently clipped
  bool overloaded;
  // Whether the current state relates to speed or position regulation
  pid_mode_t mode;
} pid_controller_t;

extern void pid_init(pid_controller_t *self, const pid_controller_config_t *config);
extern int pid_control_position(pid_controller_t *self, move_t *trajectory, adjustable_encoder *feedback);
extern int pid_control_speed(pid_controller_t *self, float speed_sp, float speed_pv);


/**
 * Reset the controller state (namely, the integrated action).
 * @param self Controller state
 */
extern void pid_reset(pid_controller_t *self);

/**
 * Default PID configuration for EV3 large motor
 */
extern const pid_controller_config_t large_motor_pid_template;

/**
 * Default PID configuration for EV3 medium motor
 */
extern const pid_controller_config_t medium_motor_pid_template;

#endif //FIRMWARE_JV_PID_H
