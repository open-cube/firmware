# Create an INTERFACE library for our C module.
add_library(usermod_jv_motors INTERFACE)

# Add our source files to the lib
target_sources(usermod_jv_motors INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}/motor_pins.c
    ${CMAKE_CURRENT_LIST_DIR}/motor_pins.h
    ${CMAKE_CURRENT_LIST_DIR}/motor_pwm.h
    ${CMAKE_CURRENT_LIST_DIR}/motor_pwm.c
    ${CMAKE_CURRENT_LIST_DIR}/jv_motor_api.c
    ${CMAKE_CURRENT_LIST_DIR}/move_generator.h
    ${CMAKE_CURRENT_LIST_DIR}/move_generator.c
    ${CMAKE_CURRENT_LIST_DIR}/motor_control.h
    ${CMAKE_CURRENT_LIST_DIR}/motor_control.c
    ${CMAKE_CURRENT_LIST_DIR}/encoder_helper.h
    ${CMAKE_CURRENT_LIST_DIR}/encoder_helper.c
    ${CMAKE_CURRENT_LIST_DIR}/pid.h
    ${CMAKE_CURRENT_LIST_DIR}/pid.c
)

# Add the current directory as an include directory.
target_include_directories(usermod_jv_motors INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}
)

target_link_libraries(usermod_jv_motors INTERFACE
    usermod_opencube_pindefs
    usermod_opencube_lib
    usermod_opencube_motors
    hardware_claim
)

# Link our INTERFACE library to the usermod target.
target_link_libraries(usermod INTERFACE usermod_jv_motors)
