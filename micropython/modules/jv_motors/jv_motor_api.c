// based on https://github.com/micropython/micropython/blob/master/examples/usercmodule/cexample/examplemodule.c

#include <i2c_locking.h>
#include "py/runtime.h"
#include "motor_pwm.h"
#include "motor_control.h"

/**
 * See <repo root>/micropython/typing/jv_motors.pyi for API docs
 */

typedef enum {
  DIRECTION_CLOCKWISE,
  DIRECTION_COUNTERCLOCKWISE
} motor_dir_t;

extern const mp_obj_type_t motor_type;

typedef struct {
  mp_obj_base_t base;
  regulated_motor_t motor;
  bool initialized;
} motor_obj_t;

static motor_obj_t *to_initialized_motor(mp_obj_t obj) {
  motor_obj_t *self = MP_OBJ_TO_PTR(obj);
  if (!self->initialized) {
    mp_raise_msg_varg(&mp_type_RuntimeError,
        MP_ERROR_TEXT("Motor instance for port %d has been closed"), self->motor.encoder.port);
  }
  return self;
}

STATIC mp_obj_t motor_make_new(const mp_obj_type_t *type, size_t n_args, size_t n_kw, const mp_obj_t *args) {
  enum {
    ARG_port, ARG_type, ARG_positive_direction
  };
  static const mp_arg_t allowed[] = {
      {MP_QSTR_port,               MP_ARG_INT | MP_ARG_REQUIRED},
      {MP_QSTR_type,               MP_ARG_INT, {.u_int = LARGE_MOTOR}},
      {MP_QSTR_positive_direction, MP_ARG_INT, {.u_int = DIRECTION_CLOCKWISE}}
  };

  mp_arg_check_num(n_args, n_kw, 1, 3, true);

  mp_map_t kw_args;
  mp_map_init_fixed_table(&kw_args, n_kw, args + n_args);

  mp_arg_val_t arg_vals[MP_ARRAY_SIZE(allowed)];
  mp_arg_parse_all(n_args, args, &kw_args, MP_ARRAY_SIZE(allowed), allowed, arg_vals);

  int port = arg_vals[ARG_port].u_int;
  if (port < 0 || port >= NUM_MOTOR_PORTS) {
    mp_raise_msg_varg(&mp_type_ValueError, MP_ERROR_TEXT("Motor port index out of range: %d"), port);
  }

  motor_type_t mtype = arg_vals[ARG_type].u_int;
  if (mtype != LARGE_MOTOR && mtype != MEDIUM_MOTOR) {
    mp_raise_msg_varg(&mp_type_ValueError, MP_ERROR_TEXT("Unknown motor type %d"), mtype);
  }

  motor_dir_t mdir = arg_vals[ARG_positive_direction].u_int;
  if (mdir != DIRECTION_CLOCKWISE && mdir != DIRECTION_COUNTERCLOCKWISE) {
    mp_raise_msg_varg(&mp_type_ValueError, MP_ERROR_TEXT("Unknown motor direction %d"), mdir);
  }

  motor_obj_t *obj = m_new_obj_with_finaliser(motor_obj_t);
  obj->base.type = type;
  if (!reg_motor_init(&obj->motor, port, mtype, mdir == DIRECTION_CLOCKWISE)) {
    m_del_obj(motor_obj_t, obj);
    mp_raise_msg_varg(&mp_type_RuntimeError, MP_ERROR_TEXT("Motor port %d already opened"), port + 1);
  }
  obj->initialized = true;
  return MP_OBJ_FROM_PTR(obj);
}

static void motor_print(const mp_print_t *print, mp_obj_t self_in, mp_print_kind_t kind) {
  motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
  mp_printf(print, "Motor(MOTOR_PORT%d)", self->motor.encoder.port + 1);
}

STATIC mp_obj_t motor_finalizer(mp_obj_t self_in) {
  motor_obj_t *self = MP_OBJ_TO_PTR(self_in);

  if (self->initialized) {
    reg_motor_deinit(&self->motor);
    self->initialized = false;
  }
  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(motor_finalizer_obj, motor_finalizer);

STATIC mp_obj_t motor_angle(mp_obj_t self_in) {
  motor_obj_t *self = to_initialized_motor(self_in);

  return mp_obj_new_int(reg_motor_get_angle(&self->motor));
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(motor_angle_obj, motor_angle);

STATIC mp_obj_t motor_reset_angle(size_t n, const mp_obj_t *objs) {
  motor_obj_t *self = to_initialized_motor(objs[0]);

  int32_t new_angle = 0;
  if (n > 1) {
    new_angle = mp_obj_get_int(objs[1]);
  }

  reg_motor_set_angle(&self->motor, new_angle);
  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(motor_reset_angle_obj, 1, 2, motor_reset_angle);

STATIC mp_obj_t motor_speed(mp_obj_t self_in) {
  motor_obj_t *self = to_initialized_motor(self_in);

  return mp_obj_new_int(reg_motor_get_speed(&self->motor));
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(motor_speed_obj, motor_speed);

STATIC mp_obj_t motor_get_cur_power(mp_obj_t self_in) {
  motor_obj_t *self = to_initialized_motor(self_in);

  return mp_obj_new_int(reg_motor_get_current_power(&self->motor));
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(motor_get_cur_power_obj, motor_get_cur_power);

STATIC mp_obj_t motor_attr(mp_obj_t self_in, qstr attribute, mp_obj_t *dest) {
  if (dest[0] == MP_OBJ_SENTINEL && dest[1] == MP_OBJ_NULL) {
    mp_raise_msg(&mp_type_NotImplementedError, MP_ERROR_TEXT("attribute deletion not supported"));
  }

  motor_obj_t *self;
  switch (attribute) {
    case MP_QSTR_max_acceleration:
      self = to_initialized_motor(self_in);

      if (dest[0] == MP_OBJ_NULL) {
        dest[0] = mp_obj_new_float_from_f(reg_motor_get_max_accel(&self->motor));
      } else {
        reg_motor_set_max_accel(&self->motor, mp_obj_get_float_to_f(dest[1]));
        dest[0] = MP_OBJ_NULL;
      }
      break;

    case MP_QSTR_max_power:
      self = to_initialized_motor(self_in);

      if (dest[0] == MP_OBJ_NULL) {
        dest[0] = mp_obj_new_int(reg_motor_get_max_power(&self->motor));
      } else {
        reg_motor_set_max_power(&self->motor, mp_obj_get_int(dest[1]));
        dest[0] = MP_OBJ_NULL;
      }
      break;

    case MP_QSTR_pid:
      self = to_initialized_motor(self_in);

      if (dest[0] == MP_OBJ_NULL) {
        float kp, ki, kd;
        reg_motor_get_pid(&self->motor, &kp, &ki, &kd);

        mp_obj_t items[] = {
            mp_obj_new_float_from_f(kp),
            mp_obj_new_float_from_f(ki),
            mp_obj_new_float_from_f(kd),
        };
        dest[0] = mp_obj_new_tuple(MP_ARRAY_SIZE(items), items);
      } else {
        mp_obj_t *items;
        mp_obj_get_array_fixed_n(dest[1], 3, &items);

        reg_motor_set_pid(
            &self->motor,
            mp_obj_get_float_to_f(items[0]),
            mp_obj_get_float_to_f(items[1]),
            mp_obj_get_float_to_f(items[2])
        );
        dest[0] = MP_OBJ_NULL;
      }
      break;

    case MP_QSTR_speed_pid:
      self = to_initialized_motor(self_in);

      if (dest[0] == MP_OBJ_NULL) {
        float kp, ki;
        reg_motor_get_speed_pid(&self->motor, &kp, &ki);

        mp_obj_t items[] = {
            mp_obj_new_float_from_f(kp),
            mp_obj_new_float_from_f(ki),
        };
        dest[0] = mp_obj_new_tuple(MP_ARRAY_SIZE(items), items);
      } else {
        mp_obj_t *items;
        mp_obj_get_array_fixed_n(dest[1], 2, &items);

        reg_motor_set_speed_pid(
            &self->motor,
            mp_obj_get_float_to_f(items[0]),
            mp_obj_get_float_to_f(items[1])
        );
        dest[0] = MP_OBJ_NULL;
      }
      break;

    case MP_QSTR_feedforward:
      self = to_initialized_motor(self_in);

      if (dest[0] == MP_OBJ_NULL) {
        float kv, ka;
        reg_motor_get_feedforward(&self->motor, &kv, &ka);

        mp_obj_t items[] = {
            mp_obj_new_float_from_f(kv),
            mp_obj_new_float_from_f(ka),
        };
        dest[0] = mp_obj_new_tuple(MP_ARRAY_SIZE(items), items);
      } else {
        mp_obj_t *items;
        mp_obj_get_array_fixed_n(dest[1], 2, &items);

        reg_motor_set_feedforward(
            &self->motor,
            mp_obj_get_float_to_f(items[0]),
            mp_obj_get_float_to_f(items[1])
        );
        dest[0] = MP_OBJ_NULL;
      }
      break;

    default:
      // we do not require the motor to be initialized here!
      // otherwise garbage collection will crash (it will try to load __del__ via this function).
      if (dest[0] == MP_OBJ_NULL) {
        // continue with lookup
        dest[1] = MP_OBJ_SENTINEL;
      } else {
        // noop => fail
      }
      break;
  }

  return mp_const_none;
}


STATIC mp_obj_t motor_is_complete(mp_obj_t self_in) {
  motor_obj_t *self = to_initialized_motor(self_in);

  return mp_obj_new_bool(reg_motor_is_complete(&self->motor));
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(motor_is_complete_obj, motor_is_complete);

STATIC mp_obj_t motor_wait_complete(mp_obj_t self_in) {
  motor_obj_t *self = to_initialized_motor(self_in);

  reg_motor_wait_complete(&self->motor);
  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(motor_wait_complete_obj, motor_wait_complete);

STATIC mp_obj_t motor_coast(mp_obj_t self_in) {
  motor_obj_t *self = to_initialized_motor(self_in);

  if (!reg_motor_coast(&self->motor)) {
    raise_i2c_locking_error();
  }

  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(motor_coast_obj, motor_coast);

STATIC mp_obj_t motor_brake(mp_obj_t self_in) {
  motor_obj_t *self = to_initialized_motor(self_in);

  if (!reg_motor_brake(&self->motor)) {
    raise_i2c_locking_error();
  }

  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(motor_brake_obj, motor_brake);

STATIC mp_obj_t motor_hold(mp_obj_t self_in) {
  motor_obj_t *self = to_initialized_motor(self_in);

  reg_motor_hold(&self->motor);
  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_1(motor_hold_obj, motor_hold);


STATIC mp_obj_t motor_run_unregulated(mp_obj_t self_in, mp_obj_t power) {
  motor_obj_t *self = to_initialized_motor(self_in);

  if (!reg_motor_run_unregulated(&self->motor, mp_obj_get_int(power))) {
    raise_i2c_locking_error();
  }

  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_2(motor_run_unregulated_obj, motor_run_unregulated);


STATIC mp_obj_t motor_run_to_angle(size_t n, const mp_obj_t *pos, mp_map_t *kws) {
  enum {
    ARG_speed,
    ARG_target_angle,
    ARG_then,
    ARG_wait
  };
  static const mp_arg_t allowed[] = {
      {MP_QSTR_speed,        MP_ARG_OBJ | MP_ARG_REQUIRED},
      {MP_QSTR_target_angle, MP_ARG_OBJ | MP_ARG_REQUIRED},
      {MP_QSTR_then,         MP_ARG_INT,  {.u_int = STOP_MODE_HOLD}},
      {MP_QSTR_wait,         MP_ARG_BOOL, {.u_bool = true}},
  };

  motor_obj_t *self = to_initialized_motor(pos[0]);
  mp_arg_val_t args[MP_ARRAY_SIZE(allowed)];
  mp_arg_parse_all(n - 1, pos + 1, kws, MP_ARRAY_SIZE(allowed), allowed, args);

  reg_motor_run_to_angle(
      &self->motor,
      mp_obj_get_float_to_f(args[ARG_target_angle].u_obj),
      mp_obj_get_float_to_f(args[ARG_speed].u_obj),
      args[ARG_then].u_int
  );
  if (args[ARG_wait].u_bool) {
    reg_motor_wait_complete(&self->motor);
  }

  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_KW(motor_run_to_angle_obj, 3, motor_run_to_angle);

STATIC mp_obj_t motor_run_by_angle(size_t n, const mp_obj_t *pos, mp_map_t *kws) {
  enum {
    ARG_speed, ARG_rotation_angle, ARG_then, ARG_wait
  };
  static const mp_arg_t allowed[] = {
      {MP_QSTR_speed,          MP_ARG_OBJ | MP_ARG_REQUIRED},
      {MP_QSTR_rotation_angle, MP_ARG_OBJ | MP_ARG_REQUIRED},
      {MP_QSTR_then,           MP_ARG_INT,  {.u_int = STOP_MODE_HOLD}},
      {MP_QSTR_wait,           MP_ARG_BOOL, {.u_bool = true}},
  };

  motor_obj_t *self = to_initialized_motor(pos[0]);
  mp_arg_val_t args[MP_ARRAY_SIZE(allowed)];
  mp_arg_parse_all(n - 1, pos + 1, kws, MP_ARRAY_SIZE(allowed), allowed, args);

  reg_motor_run_angle(
      &self->motor,
      mp_obj_get_float_to_f(args[ARG_rotation_angle].u_obj),
      mp_obj_get_float_to_f(args[ARG_speed].u_obj),
      args[ARG_then].u_int
  );
  if (args[ARG_wait].u_bool) {
    reg_motor_wait_complete(&self->motor);
  }

  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_KW(motor_run_by_angle_obj, 3, motor_run_by_angle);

STATIC mp_obj_t motor_run_timed(size_t n, const mp_obj_t *pos, mp_map_t *kws) {
  enum {
    ARG_speed, ARG_time, ARG_then, ARG_wait
  };
  static const mp_arg_t allowed[] = {
      {MP_QSTR_speed, MP_ARG_OBJ | MP_ARG_REQUIRED},
      {MP_QSTR_time,  MP_ARG_OBJ | MP_ARG_REQUIRED},
      {MP_QSTR_then,  MP_ARG_INT,  {.u_int = STOP_MODE_HOLD}},
      {MP_QSTR_wait,  MP_ARG_BOOL, {.u_bool = true}},
  };

  motor_obj_t *self = to_initialized_motor(pos[0]);
  mp_arg_val_t args[MP_ARRAY_SIZE(allowed)];
  mp_arg_parse_all(n - 1, pos + 1, kws, MP_ARRAY_SIZE(allowed), allowed, args);

  reg_motor_run_timed(
      &self->motor,
      mp_obj_get_float_to_f(args[ARG_time].u_obj) / 1000.0f,
      mp_obj_get_float_to_f(args[ARG_speed].u_obj),
      args[ARG_then].u_int
  );
  if (args[ARG_wait].u_bool) {
    reg_motor_wait_complete(&self->motor);
  }

  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_KW(motor_run_timed_obj, 3, motor_run_timed);

STATIC mp_obj_t motor_run_ext_ref(size_t n, const mp_obj_t *pos, mp_map_t *kws) {
  enum {
    ARG_target_angle,
    ARG_target_speed,
    ARG_target_accel,
  };
  static const mp_arg_t allowed[] = {
      {MP_QSTR_target_angle, MP_ARG_OBJ | MP_ARG_REQUIRED},
      {MP_QSTR_target_speed, MP_ARG_OBJ, {.u_rom_obj = MP_ROM_NONE}},
      {MP_QSTR_target_accel, MP_ARG_OBJ, {.u_rom_obj = MP_ROM_NONE}},
  };

  motor_obj_t *self = to_initialized_motor(pos[0]);
  mp_arg_val_t args[MP_ARRAY_SIZE(allowed)];
  mp_arg_parse_all(n - 1, pos + 1, kws, MP_ARRAY_SIZE(allowed), allowed, args);

  float target_angle = mp_obj_get_float_to_f(args[ARG_target_angle].u_obj);
  float target_speed = 0.0f;
  float target_accel = 0.0f;
  if (args[ARG_target_speed].u_obj != mp_const_none) {
    target_speed = mp_obj_get_float_to_f(args[ARG_target_speed].u_obj);
  }
  if (args[ARG_target_accel].u_obj != mp_const_none) {
    target_speed = mp_obj_get_float_to_f(args[ARG_target_accel].u_obj);
  }

  reg_motor_run_external_ref(&self->motor, target_angle, target_speed, target_accel);
  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_KW(motor_run_ext_ref_obj, 2, motor_run_ext_ref);

STATIC mp_obj_t motor_run_unlimited(mp_obj_t self_in, mp_obj_t speed) {
  motor_obj_t *self = to_initialized_motor(self_in);

  reg_motor_run_unlimited(&self->motor, mp_obj_get_float_to_f(speed));
  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_2(motor_run_unlimited_obj, motor_run_unlimited);


STATIC const mp_rom_map_elem_t motor_dict_table[] = {
    // lifetime management
    {MP_ROM_QSTR(MP_QSTR___del__),          MP_ROM_PTR(&motor_finalizer_obj)},
    {MP_ROM_QSTR(MP_QSTR_close),            MP_ROM_PTR(&motor_finalizer_obj)},

    // probing of motor state
    {MP_ROM_QSTR(MP_QSTR_angle),            MP_ROM_PTR(&motor_angle_obj)},
    {MP_ROM_QSTR(MP_QSTR_reset_angle),      MP_ROM_PTR(&motor_reset_angle_obj)},
    {MP_ROM_QSTR(MP_QSTR_speed),            MP_ROM_PTR(&motor_speed_obj)},
    {MP_ROM_QSTR(MP_QSTR_power),            MP_ROM_PTR(&motor_get_cur_power_obj)},

    // start/stop
    {MP_ROM_QSTR(MP_QSTR_brake),            MP_ROM_PTR(&motor_brake_obj)},
    {MP_ROM_QSTR(MP_QSTR_hold),             MP_ROM_PTR(&motor_hold_obj)},
    {MP_ROM_QSTR(MP_QSTR_stop),             MP_ROM_PTR(&motor_coast_obj)},
    {MP_ROM_QSTR(MP_QSTR_coast),            MP_ROM_PTR(&motor_coast_obj)},
    /* coast() == stop(), I find stop() bogus */

    // moves
    {MP_ROM_QSTR(MP_QSTR_dc),               MP_ROM_PTR(&motor_run_unregulated_obj)},
    {MP_ROM_QSTR(MP_QSTR_run),              MP_ROM_PTR(&motor_run_unlimited_obj)},
    {MP_ROM_QSTR(MP_QSTR_run_angle),        MP_ROM_PTR(&motor_run_by_angle_obj)},
    {MP_ROM_QSTR(MP_QSTR_run_target),       MP_ROM_PTR(&motor_run_to_angle_obj)},
    {MP_ROM_QSTR(MP_QSTR_run_time),         MP_ROM_PTR(&motor_run_timed_obj)},
    {MP_ROM_QSTR(MP_QSTR_track_target),     MP_ROM_PTR(&motor_run_ext_ref_obj)},

    // completion
    {MP_ROM_QSTR(MP_QSTR_is_complete),      MP_ROM_PTR(&motor_is_complete_obj)},
    {MP_ROM_QSTR(MP_QSTR_wait_complete),    MP_ROM_PTR(&motor_wait_complete_obj)},

    // constants
    {MP_ROM_QSTR(MP_QSTR_COAST),            MP_ROM_INT(STOP_MODE_COAST)},
    {MP_ROM_QSTR(MP_QSTR_BRAKE),            MP_ROM_INT(STOP_MODE_BRAKE)},
    {MP_ROM_QSTR(MP_QSTR_HOLD),             MP_ROM_INT(STOP_MODE_HOLD)},
    {MP_ROM_QSTR(MP_QSTR_MEDIUM_MOTOR),     MP_ROM_INT(MEDIUM_MOTOR)},
    {MP_ROM_QSTR(MP_QSTR_LARGE_MOTOR),      MP_ROM_INT(LARGE_MOTOR)},
    {MP_ROM_QSTR(MP_QSTR_CLOCKWISE),        MP_ROM_INT(DIRECTION_CLOCKWISE)},
    {MP_ROM_QSTR(MP_QSTR_COUNTERCLOCKWISE), MP_ROM_INT(DIRECTION_COUNTERCLOCKWISE)},
};
STATIC MP_DEFINE_CONST_DICT(motor_dict, motor_dict_table);


MP_DEFINE_CONST_OBJ_TYPE(
    motor_type,
    MP_QSTR_Motor,
    MP_TYPE_FLAG_NONE,
    make_new, &motor_make_new,
    locals_dict, &motor_dict,
    attr, &motor_attr,
    print, &motor_print
);

STATIC const mp_rom_map_elem_t jv_motors_module_globals_table[] = {
    {MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_jv_motors)},
    {MP_ROM_QSTR(MP_QSTR_Motor),    MP_ROM_PTR(&motor_type)},
};
STATIC MP_DEFINE_CONST_DICT(jv_motors_module_globals, jv_motors_module_globals_table);

const mp_obj_module_t jv_motors_module = {
    .base = {&mp_type_module},
    .globals = (mp_obj_dict_t *) &jv_motors_module_globals
};

MP_REGISTER_MODULE(MP_QSTR_jv_motors, jv_motors_module);
