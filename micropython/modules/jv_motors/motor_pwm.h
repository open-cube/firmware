/**
 * Module for controlling the H-bridge
 */
#ifndef FIRMWARE_JV_MOTOR_PWM_H
#define FIRMWARE_JV_MOTOR_PWM_H

#include <opencube_hw.h>

/**
 * Initialize the PWM module if it is not initialized yet.
 *
 * This can only be called from a Python context.
 */
extern void motor_pwm_init(void);

/**
 * Enable the given motor and run it at the given (signed) power.
 * @param port Motor port number (0-3)
 * @param power PWM duty cycle to apply. If negative,
 *              H-bridge will switch the motor in the
 *              opposite direction.
 * @return Whether the operation succeeded or not
 *         (I2C lock can be busy).
 */
extern bool motor_pwm_run(opencube_motor_port port, int power);

/**
 * Suspend the H-bridge at the given port. This will fully
 * unbrake the connected motor.
 *
 * @param port Motor port number (0-3)
 * @return Whether the operation succeeded or not
 *         (I2C lock can be busy).
 */
extern bool motor_pwm_poweroff(opencube_motor_port port);

/**
 * Suspend the H-bridge on all motor ports.
 *
 * @return Whether the operation succeeded or not
 *         (I2C lock can be busy).
 */
extern bool motor_pwm_poweroff_all(void);

#endif //FIRMWARE_JV_MOTOR_PWM_H
