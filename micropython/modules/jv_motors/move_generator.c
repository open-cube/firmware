#include <hardware/timer.h>
#include <math.h>
#include "move_generator.h"

static void start_ramp_up(move_t *self);
static void start_ramp_steady(move_t *self);
static void start_ramp_down(move_t *self);
static void start_smooth_stop(move_t *self);
static void update_ramp_up(move_t *self);
static void update_ramp_steady(move_t *self);
static void update_ramp_down(move_t *self);
static void update_smooth_stop(move_t *self);

static float seconds_since(uint64_t now, uint64_t then) {
  return ((float) (int64_t) (now - then)) / 1000000.0f;
}

static float seconds_until(uint64_t then, uint64_t now) {
  return ((float) (int64_t) (then - now)) / 1000000.0f;
}

static inline uint64_t time_now(void) {
  return time_us_64();
}

static float remaining_angle(move_t *self) {
  float raw_delta = self->limit.angle - self->scheduled_angle;
  return self->target_speed >= 0 ? +raw_delta : -raw_delta;
}

static float remaining_time(move_t *self, uint64_t now) {
  return seconds_until(self->limit.time, now);
}

void start_move_to_angle(move_t *self,
    float target_angle, float max_speed, float max_accel,
    float from_angle, float from_speed) {
  self->limit_type = LIMIT_BY_ANGLE;
  self->limit.angle = target_angle;
  self->max_acceleration = fabsf(max_accel);
  self->target_speed = copysignf(max_speed, target_angle - from_angle);
  self->scheduled_angle = from_angle;
  self->scheduled_speed = from_speed;
  self->scheduled_accel = 0;

  // if we're too fast, we must first stop and then run in reverse
  float ramp_down_angle = 0.5f * from_speed * from_speed / self->max_acceleration;
  float avail_angle = remaining_angle(self);
  if (avail_angle >= ramp_down_angle) {
    // normal operation
    start_ramp_up(self);
  } else {
    // stop and then reverse operation
    float stop_angle = from_angle + copysignf(ramp_down_angle, from_speed);
    self->target_speed = copysignf(max_speed, target_angle - stop_angle);
    start_smooth_stop(self);
  }
}

void start_timed_move(move_t *self,
    float secs, float max_speed, float max_accel,
    float from_angle, float from_speed) {
  self->limit_type = LIMIT_BY_TIME;
  self->limit.time = time_now() + (int64_t) (secs * 1000000.0f);
  self->max_acceleration = fabsf(max_accel);
  self->target_speed = max_speed;
  self->scheduled_angle = from_angle;
  self->scheduled_speed = from_speed;
  self->scheduled_accel = 0;
  start_ramp_up(self);
}

void start_unlimited_move(move_t *self,
    float speed, float max_accel,
    float from_angle, float from_speed) {
  self->limit_type = LIMIT_BY_NOTHING;
  self->max_acceleration = fabsf(max_accel);
  self->target_speed = speed;
  self->scheduled_angle = from_angle;
  self->scheduled_speed = from_speed;
  self->scheduled_accel = 0;
  start_ramp_up(self);
}

void patch_unlimited_move(move_t *self,
    float speed, float max_accel) {
  update_move(self);
  self->max_acceleration = fabsf(max_accel);
  self->target_speed = speed;
  self->limit.angle = copysignf(INFINITY, speed);
  start_ramp_up(self);
}

void start_noop_move(move_t *self, float angle, float speed, float accel) {
  self->limit_type = LIMIT_BY_NOTHING;
  self->phase = MOVE_DONE;
  self->scheduled_accel = accel;
  self->scheduled_speed = speed;
  self->scheduled_angle = angle;
}


void update_move(move_t *ramp) {
  switch (ramp->phase) {
    case MOVE_RAMP_UP:
      update_ramp_up(ramp);
      break;

    case MOVE_STEADY:
      update_ramp_steady(ramp);
      break;

    case MOVE_RAMP_DOWN:
      update_ramp_down(ramp);
      break;

    case MOVE_SMOOTH_STOP:
      update_smooth_stop(ramp);
      break;

    default:
    case MOVE_DONE:
      break;
  }
}

void start_ramp_up(move_t *self) {
  float speed_delta = self->target_speed - self->scheduled_speed;
  float acceleration = copysignf(self->max_acceleration, speed_delta);

  self->phase = MOVE_RAMP_UP;
  self->ramp.up.t0_time = time_now();
  self->ramp.up.t0_angle = self->scheduled_angle;
  self->ramp.up.t0_speed = self->scheduled_speed;
  self->ramp.up.t0_accel = acceleration;
  self->ramp.up.time_duration = speed_delta / acceleration;
}

void update_ramp_up(move_t *self) {
  uint64_t now = time_now();
  float elapsed = seconds_since(now, self->ramp.up.t0_time);
  float t0_angle = self->ramp.up.t0_angle;
  float t0_speed = self->ramp.up.t0_speed;
  float t0_accel = self->ramp.up.t0_accel;

  bool ramp_up_done;
  if (elapsed < self->ramp.up.time_duration) {
    self->scheduled_accel = t0_accel;
    self->scheduled_speed = t0_speed + t0_accel * elapsed;
    self->scheduled_angle = t0_angle + t0_speed * elapsed + 0.5f * t0_accel * elapsed * elapsed;
    if (self->limit_type == LIMIT_BY_ANGLE) {
      float ramp_down_angle = 0.5f * self->scheduled_speed * self->scheduled_speed / self->max_acceleration;
      ramp_up_done = remaining_angle(self) <= ramp_down_angle;

    } else if (self->limit_type == LIMIT_BY_TIME) {
      float ramp_down_time = fabsf(self->scheduled_speed / self->max_acceleration);
      ramp_up_done = remaining_time(self, now) < ramp_down_time;

    } else if (self->limit_type == LIMIT_BY_NOTHING) {
      ramp_up_done = false;

    } else {
      panic("bogus move limit type %d", self->limit_type);
    }
  } else {
    // calculate smooth transition
    float base_time = self->ramp.up.time_duration;
    float extra_time = elapsed - self->ramp.up.time_duration;
    float base_dist;
    if (base_time != 0.0f) {
      base_dist = t0_angle + t0_speed * base_time + 0.5f * t0_accel * base_time * base_time;
    } else {
      // avoid NaN when t0_accel is infinite and base_time is zero
      base_dist = t0_angle;
    }
    float extra_dist = self->target_speed * extra_time;
    self->scheduled_accel = 0;
    self->scheduled_speed = self->target_speed;
    self->scheduled_angle = base_dist + extra_dist;
    ramp_up_done = true;
  }

  if (ramp_up_done) {
    start_ramp_steady(self);
    update_ramp_steady(self);
  }
}

void start_ramp_steady(move_t *self) {
  self->phase = MOVE_STEADY;
  self->ramp.steady.t0_time = time_now();
  self->ramp.steady.t0_angle = self->scheduled_angle;

  if (self->limit_type == LIMIT_BY_ANGLE) {
    self->ramp.steady.ramp_down_reserve =
        0.5f * self->target_speed * self->target_speed / self->max_acceleration;

  } else if (self->limit_type == LIMIT_BY_TIME) {
    self->ramp.steady.ramp_down_reserve =
        fabsf(self->target_speed / self->max_acceleration);
  }
}

void update_ramp_steady(move_t *self) {
  bool steady_done;
  uint64_t now = time_now();
  float elapsed = seconds_since(now, self->ramp.steady.t0_time);

  if (self->limit_type == LIMIT_BY_ANGLE) {
    steady_done = remaining_angle(self) <= self->ramp.steady.ramp_down_reserve;
  } else if (self->limit_type == LIMIT_BY_TIME) {
    steady_done = remaining_time(self, now) <= self->ramp.steady.ramp_down_reserve;
  } else if (self->limit_type == LIMIT_BY_NOTHING) {
    steady_done = false;
  } else {
    panic("bogus move limit type %d", self->limit_type);
  }

  if (!steady_done) {
    self->scheduled_accel = 0;
    self->scheduled_speed = self->target_speed;
    self->scheduled_angle = self->ramp.steady.t0_angle + self->target_speed * elapsed;
  } else {
    start_ramp_down(self);
    update_ramp_down(self);
  }
}

void start_ramp_down(move_t *self) {
  float t0_angle = self->scheduled_angle;
  float t0_speed = self->scheduled_speed;

  self->phase = MOVE_RAMP_DOWN;
  self->ramp.down.t0_time = time_now();
  self->ramp.down.t0_angle = t0_angle;
  self->ramp.down.t0_speed = t0_speed;

  if (self->limit_type == LIMIT_BY_ANGLE) {
    if (t0_speed != 0) {
      self->ramp.down.time_duration = 2.0f * (self->limit.angle - t0_angle) / t0_speed;
      self->ramp.down.t0_accel = -t0_speed / self->ramp.down.time_duration;
    } else {
      // protect against division by zero
      start_noop_move(self, self->limit.angle, 0, 0);
    }
  } else if (self->limit_type == LIMIT_BY_TIME) {
    self->ramp.down.time_duration = fabsf(t0_speed / self->max_acceleration);
    self->ramp.down.t0_accel = copysignf(self->max_acceleration, -t0_speed);
  }
}

void update_ramp_down(move_t *self) {
  float elapsed = seconds_since(time_now(), self->ramp.down.t0_time);
  float t0_angle = self->ramp.down.t0_angle;
  float t0_speed = self->ramp.down.t0_speed;
  float t0_accel = self->ramp.down.t0_accel;

  if (elapsed < self->ramp.down.time_duration) {
    self->scheduled_accel = t0_accel;
    self->scheduled_speed = t0_speed + t0_accel * elapsed;
    self->scheduled_angle = t0_angle + t0_speed * elapsed + 0.5f * t0_accel * elapsed * elapsed;
  } else {
    float stop_angle = self->limit_type == LIMIT_BY_ANGLE ? self->limit.angle : self->scheduled_angle;
    start_noop_move(self, stop_angle, 0, 0);
  }
}

void start_smooth_stop(move_t *self) {
  self->phase = MOVE_SMOOTH_STOP;
  self->ramp.smooth_stop.t0_time = time_now();
  self->ramp.smooth_stop.t0_angle = self->scheduled_angle;
  self->ramp.smooth_stop.t0_speed = self->scheduled_speed;
  self->ramp.smooth_stop.t0_accel = copysignf(self->max_acceleration, -self->scheduled_speed);
  self->ramp.smooth_stop.time_duration = fabsf(self->scheduled_speed / self->max_acceleration);
  self->ramp.smooth_stop.continue_after = true;
}

void update_smooth_stop(move_t *self) {
  float elapsed = seconds_since(time_now(), self->ramp.smooth_stop.t0_time);
  float t0_angle = self->ramp.smooth_stop.t0_angle;
  float t0_speed = self->ramp.smooth_stop.t0_speed;
  float t0_accel = self->ramp.smooth_stop.t0_accel;

  if (elapsed < self->ramp.smooth_stop.time_duration) {
    self->scheduled_accel = t0_accel;
    self->scheduled_speed = t0_speed + t0_accel * elapsed;
    self->scheduled_angle = t0_angle + t0_speed * elapsed + 0.5f * t0_accel * elapsed * elapsed;
  } else {
    if (self->ramp.smooth_stop.continue_after) {
      start_ramp_up(self);
      update_ramp_up(self);
    } else {
      start_noop_move(self, self->scheduled_angle, 0, 0);
    }
  }
}
