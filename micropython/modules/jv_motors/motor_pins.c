#include "motor_pins.h"

const single_port_pins opencube_motor_pins[NUM_MOTOR_PORTS] = {
    [OPENCUBE_MOTOR_1] = {
        .pwm_pin = MOTOR1_PWM_PIN,
        .dir_pin = MOTOR1_DIR_RPIN,
        .en_pin = MOTOR1_EN_RPIN
    },
    [OPENCUBE_MOTOR_2] = {
        .pwm_pin = MOTOR2_PWM_PIN,
        .dir_pin = MOTOR2_DIR_RPIN,
        .en_pin = MOTOR2_EN_RPIN
    },
    [OPENCUBE_MOTOR_3] = {
        .pwm_pin = MOTOR3_PWM_PIN,
        .dir_pin = MOTOR3_DIR_RPIN,
        .en_pin = MOTOR3_EN_RPIN
    },
    [OPENCUBE_MOTOR_4] = {
        .pwm_pin = MOTOR4_PWM_PIN,
        .dir_pin = MOTOR4_DIR_RPIN,
        .en_pin = MOTOR4_EN_RPIN
    }
};
