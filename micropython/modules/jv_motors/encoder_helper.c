#include <encoder.h>
#include "encoder_helper.h"


void adjustable_encoder_init(adjustable_encoder *self, int port, bool forward) {
  opencube_encoders_init(port);
  self->port = port;
  self->forward = forward;
  self->ref_position = opencube_encoders_get_position(self->port);
}

void adjustable_encoder_deinit(adjustable_encoder *self) {
  opencube_encoders_deinit(self->port);
}


int32_t adjustable_encoder_get_angle(adjustable_encoder *self) {
  uint32_t abs_pos = opencube_encoders_get_position(self->port);
  uint32_t abs_ref = self->ref_position;
  int32_t rel_pos = (int32_t) (abs_pos - abs_ref);
  return self->forward ? +rel_pos : -rel_pos;
}

void adjustable_encoder_set_angle(adjustable_encoder *self, int32_t rel_pos) {
  uint32_t abs_pos = opencube_encoders_get_position(self->port);
  if (self->forward) {
    self->ref_position = abs_pos - rel_pos;
  } else {
    self->ref_position = abs_pos + rel_pos;
  }
}

int32_t adjustable_encoder_get_speed(adjustable_encoder *self) {
  int32_t speed = opencube_encoders_get_speed(self->port);
  return self->forward ? +speed : -speed;
}
