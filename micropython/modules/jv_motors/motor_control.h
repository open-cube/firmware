/**
 * C-side implementation of a general-purpose regulated motor object.
 */
#ifndef FIRMWARE_MOTOR_CONTROL_H
#define FIRMWARE_MOTOR_CONTROL_H

#include <stdint.h>
#include <stdbool.h>
#include <pico/time.h>
#include <math.h>
#include "move_generator.h"
#include "pid.h"
#include "encoder_helper.h"

// Which regulation mode is active
typedef enum {
  // Regulator is off, the motor is either released, passively braked or directly controlled
  REG_MODE_OFF,
  // Unlimited trajectory generator is active
  REG_MODE_SPEED,
  // The motor is following a trajectory to a target angle
  REG_MODE_POSITION,
  // The motor is being held at a constant position
  REG_MODE_HOLD,
  // The motor is following a trajectory limited by total movement time
  REG_MODE_TIME,
  // The motor is following a user-provided speed&position reference
  REG_MODE_EXTERNAL_REF
} motor_regmode_t;

// Mode used for stopping the motor
typedef enum {
  // No braking at all
  STOP_MODE_COAST,
  // Passive electric braking
  STOP_MODE_BRAKE,
  // Active regulator-assisted braking
  STOP_MODE_HOLD
} motor_stopmode_t;

typedef enum {
  LARGE_MOTOR,
  MEDIUM_MOTOR
} motor_type_t;

typedef struct {
  adjustable_encoder encoder;
  motor_regmode_t regulator;
  move_t current_move;
  pid_controller_t pid;
  motor_stopmode_t on_stop;

  // Per-motor maximum acceleration in deg/s^2
  float max_accel;

  // Currently configured output voltage in signed %
  int current_power;

  // Timer IRQ handle
  alarm_pool_t *timer_pool;
  alarm_id_t timer_id;
} regulated_motor_t;

// Initialize a regulated motor object on the given port, optionally with an inverted tacho
extern bool reg_motor_init(regulated_motor_t *self, int port, motor_type_t type, bool fwd);

// Release a port for other regulators
extern void reg_motor_deinit(regulated_motor_t *self);

// Set limit for output voltage (% of battery voltage)
extern void reg_motor_set_max_power(regulated_motor_t *self, int power);

// Fully un-brake the motor
extern bool reg_motor_coast(regulated_motor_t *self);

// Passively brake the motor
extern bool reg_motor_brake(regulated_motor_t *self);

// Start actively holding the motor at the current position
extern void reg_motor_hold(regulated_motor_t *self);

// Start an unlimited trajectory with the given target speed
extern void reg_motor_run_unlimited(regulated_motor_t *self, float speed);

// Start a smoothly accelerated move that will last for the given time
extern void reg_motor_run_timed(regulated_motor_t *self, float secs, float speed, motor_stopmode_t after);

// Start a smoothly accelerated move that will end at the given target angle
extern void reg_motor_run_to_angle(regulated_motor_t *self, float to_angle, float speed, motor_stopmode_t after);

// Start a smoothly accelerated move that will move the motor by the given angle relative to the current position
extern void reg_motor_run_angle(regulated_motor_t *self, float by_angle, float speed, motor_stopmode_t after);

// Stop the current move and just set raw output voltage (in %)
extern bool reg_motor_run_unregulated(regulated_motor_t *self, int power);

// Feed an external reference to the internal PID regulator
extern void reg_motor_run_external_ref(regulated_motor_t *self, float position, float speed, float accel);

// Check whether the trajectory generator is busy with a finite trajectory
extern bool reg_motor_is_complete(regulated_motor_t *self);

// Wait for the motor to finish the current finite trajectory
extern void reg_motor_wait_complete(regulated_motor_t *self);

// Configure PID regulator constants
extern void reg_motor_set_pid(regulated_motor_t *self, float kp, float ki, float kd);

// Query PID regulator constants
extern void reg_motor_get_pid(regulated_motor_t *self, float *kp, float *ki, float *kd);

// Configure speed PID regulator constants
extern void reg_motor_set_speed_pid(regulated_motor_t *self, float kp, float ki);

// Query speed PID regulator constants
extern void reg_motor_get_speed_pid(regulated_motor_t *self, float *kp, float *ki);

// Configure feedforward constants
extern void reg_motor_set_feedforward(regulated_motor_t *self, float kv, float ka);

// Query feedforward constants
extern void reg_motor_get_feedforward(regulated_motor_t *self, float *kv, float *ka);

// Get current motor angle
inline int32_t reg_motor_get_angle(regulated_motor_t *self) {
  return adjustable_encoder_get_angle(&self->encoder);
}

// Adjust encoder offset to make reg_motor_get_angle() return the given angle now
inline void reg_motor_set_angle(regulated_motor_t *self, int32_t value) {
  return adjustable_encoder_set_angle(&self->encoder, value);
}

// Get current motor speed
inline int32_t reg_motor_get_speed(regulated_motor_t *self) {
  return adjustable_encoder_get_speed(&self->encoder);
}

// Get current acceleration limit
inline float reg_motor_get_max_accel(regulated_motor_t *self) {
  return self->max_accel;
}

// Set current acceleration limit
inline void reg_motor_set_max_accel(regulated_motor_t *self, float accel) {
  self->max_accel = fabsf(accel);
}

// Get current output voltage limit (% of Open-Cube battery voltage)
inline int reg_motor_get_max_power(regulated_motor_t *self) {
  return (int) self->pid.max_power;
}

// Query the voltage that is currently being sent to the motor (% of Open-Cube battery voltage)
inline int reg_motor_get_current_power(regulated_motor_t *self) {
  return self->current_power;
}


#endif //FIRMWARE_MOTOR_CONTROL_H
