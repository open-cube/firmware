/**
 * Assignment of PCF8575 & RP2040 pins to individual motor ports
 */
#ifndef FIRMWARE_MOTOR_PINS_H
#define FIRMWARE_MOTOR_PINS_H

#include <pico/types.h>
#include <opencube_hw.h>

typedef struct
{
    // RP2040 PWM pin (INA on h-bridge)
    uint pwm_pin;
    // PCF8575 pin for direction control (INB on h-bridge)
    uint dir_pin;
    // PCF8575 pin for power control (PS on h-bridge)
    uint en_pin;
} single_port_pins;

// Per-port pin definitions
extern const single_port_pins opencube_motor_pins[NUM_MOTOR_PORTS];

// All PCF8575 h-bridge pins
#define MOTOR_PCF_PINS (     \
    (1 << MOTOR1_EN_RPIN)  | \
    (1 << MOTOR2_EN_RPIN)  | \
    (1 << MOTOR3_EN_RPIN)  | \
    (1 << MOTOR4_EN_RPIN)  | \
    (1 << MOTOR1_DIR_RPIN) | \
    (1 << MOTOR2_DIR_RPIN) | \
    (1 << MOTOR3_DIR_RPIN) | \
    (1 << MOTOR4_DIR_RPIN))

// All PCF8575 h-bridge enable pins
#define MOTOR_ENABLE_PCF_PINS ( \
    (1 << MOTOR1_EN_RPIN) | \
    (1 << MOTOR2_EN_RPIN) | \
    (1 << MOTOR3_EN_RPIN) | \
    (1 << MOTOR4_EN_RPIN))

#endif //FIRMWARE_MOTOR_PINS_H
