#include <math.h>
#include "pid.h"

const pid_controller_config_t large_motor_pid_template = {
    .pos_kp = 3.4747f,
    .pos_ki = 24.2517f,
    .pos_kd = 0.1245f,
    .speed_kp = 0.2200f,
    .speed_ki = 5.0000f,
    .kf_accel = 0.0070f,
    .kf_speed = 0.1183f,
};

const pid_controller_config_t medium_motor_pid_template = {
    .pos_kp = 1.7858f,
    .pos_ki = 16.5011f,
    .pos_kd = 0.0483f,
    .speed_kp = 0.14f,
    .speed_ki = 5.0000f,
    .kf_accel = 0.0025f,
    .kf_speed = 0.0735f,
};

int pid_control_position(pid_controller_t *self, move_t *trajectory, adjustable_encoder *feedback) {
  if (self->mode != PID_MODE_POSITION) {
    pid_reset(self);
    self->mode = PID_MODE_POSITION;
  }

  float pv_angle = (float) adjustable_encoder_get_angle(feedback);
  float pv_speed = (float) adjustable_encoder_get_speed(feedback);
  // round setpoint: prevent "impossible" positions
  // (i.e. the encoder will never report a 1.5 deg position)
  float sp_angle = roundf(get_scheduled_move_position(trajectory));
  float sp_speed = get_scheduled_move_speed(trajectory);
  float sp_accel = get_scheduled_move_acceleration(trajectory);

  float err = sp_angle - pv_angle;
  float d_err = sp_speed - pv_speed;

  float i_err = self->integral + err * REGULATOR_SAMPLING_SEC_FLOAT;
  float p_term = self->config.pos_kp * err;
  float i_term = self->config.pos_ki * i_err;
  float d_term = self->config.pos_kd * d_err;
  float ff_term = self->config.kf_speed * sp_speed + self->config.kf_accel * sp_accel;

  int action = (int) roundf(p_term + i_term + d_term + ff_term);

  bool overloaded = false;
  if (action > self->max_power) {
    action = self->max_power;
    overloaded = true;
  } else if (action < -self->max_power) {
    action = -self->max_power;
    overloaded = true;
  }

  if (!overloaded) {
    self->integral = i_err;
  }
  self->overloaded = overloaded;

  return action;
}

int pid_control_speed(pid_controller_t *self, float speed_sp, float speed_pv) {
  if (self->mode != PID_MODE_SPEED) {
    pid_reset(self);
    self->mode = PID_MODE_SPEED;
  }
  float err = speed_sp - speed_pv;

  float i_err = self->integral + err * REGULATOR_SAMPLING_SEC_FLOAT;
  float p_term = self->config.speed_kp * err;
  float i_term = self->config.speed_ki * i_err;

  int action = (int) roundf(p_term + i_term);

  bool overloaded = false;
  if (action > self->max_power) {
    action = self->max_power;
    overloaded = true;
  } else if (action < -self->max_power) {
    action = -self->max_power;
    overloaded = true;
  }

  if (!overloaded) {
    self->integral = i_err;
  }
  self->overloaded = overloaded;

  return action;
}

void pid_reset(pid_controller_t *self) {
  self->integral = 0;
  self->overloaded = false;
}

void pid_init(pid_controller_t *self, const pid_controller_config_t *config) {
  self->config = *config;
  self->integral = 0;
  self->overloaded = false;
  self->max_power = 100;
  self->mode = PID_MODE_POSITION;
}
