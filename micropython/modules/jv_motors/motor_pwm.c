#include <opencube_hw.h>
#include <hardware/pwm.h>
#include <hardware/gpio.h>
#include "motor_pwm.h"
#include "motor_pins.h"
#include <PCF8575.h>
#include "i2c_locking.h"


// higher frequency/resolution than this would violate H-bridge requirements for the shortest logic pulse
#define PWM_FREQUENCY 20000
#define MAX_DUTY_CYCLE 100
// The shortest logic pulse issue could be worked around by ensuring that
// the duty cycle cannot be smaller than some value. However, resolution
// in percent should be good enough for now.


static bool motor_pwm_initialized = false;
static uint16_t motor_pwm_pcf8575 = 0;
static bool motor_pwm_update_pcf(uint16_t set_hi, uint16_t set_lo);

void motor_pwm_init(void)
{
    if (!motor_pwm_initialized)
    {
        motor_pwm_initialized = true;
        pwm_config cnf = pwm_get_default_config();
        pwm_config_set_wrap(&cnf, MAX_DUTY_CYCLE - 1);
        pwm_config_set_phase_correct(&cnf, true);
        pwm_config_set_output_polarity(&cnf, true, true);
        pwm_config_set_clkdiv_mode(&cnf, PWM_DIV_FREE_RUNNING);
        // make PWM frequency 2x the base counter freq to maintain phase correct PWM
        pwm_config_set_clkdiv(&cnf, (float) RP2040_CPU_FREQ / (2 * PWM_FREQUENCY * MAX_DUTY_CYCLE));
        pwm_init(MOTOR12_PWM_SLICE, &cnf, true);
        pwm_init(MOTOR34_PWM_SLICE, &cnf, true);
        // duty cycle is set to zero in pwm_init()

        for (int i = 0; i < NUM_MOTOR_PORTS; i++)
        {
            gpio_set_function(opencube_motor_pins[i].pwm_pin, GPIO_FUNC_PWM);
        }

        opencube_lock_i2c_or_raise();
        motor_pwm_pcf8575 = 0;
        PCF8575_init(); // TODO handle this somehow better
        opencube_unlock_i2c();
    }
}

bool motor_pwm_run(opencube_motor_port port, int power)
{
    uint16_t pins_to_lo = 0;
    uint16_t pins_to_hi = 1 << opencube_motor_pins[port].en_pin;
    if (power >= 0)
    {
        pins_to_lo |= 1 << opencube_motor_pins[port].dir_pin;
    }
    else
    {
        pins_to_hi |= 1 << opencube_motor_pins[port].dir_pin;
    }

    uint16_t pwm_duty_cycle = power >= 0 ? +power : -power;
    if (pwm_duty_cycle > 100)
    {
        pwm_duty_cycle = 100;
    }

    if (!motor_pwm_update_pcf(pins_to_hi, pins_to_lo))
    {
        return false;
    }

    pwm_set_chan_level(
        pwm_gpio_to_slice_num(opencube_motor_pins[port].pwm_pin),
        pwm_gpio_to_channel(opencube_motor_pins[port].pwm_pin),
        pwm_duty_cycle
    );
    return true;
}

bool motor_pwm_poweroff(opencube_motor_port port)
{
    return motor_pwm_update_pcf(0, 1 << opencube_motor_pins[port].en_pin);
}

bool motor_pwm_poweroff_all(void)
{
    return motor_pwm_update_pcf(0, MOTOR_ENABLE_PCF_PINS);
}

bool motor_pwm_update_pcf(uint16_t set_hi, uint16_t set_lo)
{
    uint16_t old_pins = motor_pwm_pcf8575;
    uint16_t new_pins = set_hi | (old_pins & ~set_lo);

    if (old_pins != new_pins)
    {
        if (!opencube_try_lock_i2c())
        {
            return false;
        }
        motor_pwm_pcf8575 = new_pins;
        PCF8575_write_pin_mask(new_pins & MOTOR_PCF_PINS, MOTOR_PCF_PINS);
        opencube_unlock_i2c();
    }
    return true;
}
