#include <opencube_hw.h>
#include <encoder.h>
#include <py/mphal.h>
#include <i2c_locking.h>
#include "motor_control.h"
#include "motor_pwm.h"
#include "pid.h"
#include <hardware/claim.h>

#define REGULATOR_DEFAULT_ACCEL 6000

static bool reg_claimed[NUM_MOTOR_PORTS] = {};

static void reg_motor_start_regulation(regulated_motor_t *self, motor_regmode_t mode);
static void reg_motor_stop_regulation(regulated_motor_t *self);
static int64_t reg_motor_regulate(alarm_id_t id, void *user_data);
static uint32_t motor_lock(void);
static void motor_unlock(uint32_t cookie);
static void regulate_position(regulated_motor_t *self);
static void regulate_speed(regulated_motor_t *self);


bool reg_motor_init(regulated_motor_t *self, int port, motor_type_t type, bool fwd) {
  uint32_t save = motor_lock();
  bool was_claimed = reg_claimed[port];
  reg_claimed[port] = true;
  motor_unlock(save);

  if (was_claimed) {
    return false;
  }

  motor_pwm_init();
  if (!motor_pwm_poweroff(port)) {
    raise_i2c_locking_error();
  }

  adjustable_encoder_init(&self->encoder, port, fwd);
  self->regulator = REG_MODE_OFF;
  self->current_move = (move_t) {};
  pid_init(&self->pid, type == LARGE_MOTOR ? &large_motor_pid_template : &medium_motor_pid_template);
  self->on_stop = STOP_MODE_HOLD;
  self->max_accel = REGULATOR_DEFAULT_ACCEL;
  self->timer_pool = alarm_pool_get_default();
  self->timer_id = -1;

  return true;
}

void reg_motor_deinit(regulated_motor_t *self) {
  reg_motor_coast(self);
  adjustable_encoder_deinit(&self->encoder);
  self->timer_pool = NULL;

  uint32_t save = motor_lock();
  reg_claimed[self->encoder.port] = false;
  motor_unlock(save);
}

void reg_motor_start_regulation(regulated_motor_t *self, motor_regmode_t mode) {
  if (self->timer_id == -1) {
    uint64_t now_usec = time_us_64();
    uint64_t current_timeslot = now_usec / REGULATOR_SAMPLING_USEC;
    uint64_t next_timeslot = current_timeslot + 1;
    uint64_t start_at = next_timeslot * REGULATOR_SAMPLING_USEC;

    absolute_time_t start_at_timestamp;
    update_us_since_boot(&start_at_timestamp, start_at);
    self->timer_id = alarm_pool_add_alarm_at(
        self->timer_pool,
        start_at_timestamp,
        &reg_motor_regulate,
        self,
        true
    );
  }
  self->regulator = mode;
}

void reg_motor_stop_regulation(regulated_motor_t *self) {
  if (self->timer_id != -1) {
    alarm_pool_cancel_alarm(self->timer_pool, self->timer_id);
    self->timer_id = -1;
  }
  pid_reset(&self->pid);
  self->regulator = REG_MODE_OFF;
}

void reg_motor_set_max_power(regulated_motor_t *self, int power) {
  if (power < 0) {
    power = -power;
  }
  if (power > 100) {
    power = 100;
  }
  self->pid.max_power = power;
}


bool reg_motor_coast(regulated_motor_t *self) {
  uint32_t save = motor_lock();
  reg_motor_stop_regulation(self);
  self->current_power = 0;
  motor_unlock(save);
  return motor_pwm_poweroff(self->encoder.port);
}

bool reg_motor_brake(regulated_motor_t *self) {
  uint32_t save = motor_lock();
  reg_motor_stop_regulation(self);
  self->current_power = 0;
  motor_unlock(save);
  return motor_pwm_run(self->encoder.port, 0);
}

bool reg_motor_run_unregulated(regulated_motor_t *self, int power) {
  if (power > self->pid.max_power) {
    power = self->pid.max_power;
  }
  if (power < -self->pid.max_power) {
    power = -self->pid.max_power;
  }
  uint32_t save = motor_lock();
  reg_motor_stop_regulation(self);
  self->current_power = power;
  motor_unlock(save);
  return motor_pwm_run(self->encoder.port, self->encoder.forward ? +power : -power);
}

void reg_motor_hold(regulated_motor_t *self) {
  uint32_t save = motor_lock();
  start_noop_move(&self->current_move, (float) reg_motor_get_angle(self), 0.0f, 0.0f);
  reg_motor_start_regulation(self, REG_MODE_HOLD);
  motor_unlock(save);
}

void reg_motor_run_unlimited(regulated_motor_t *self, float speed) {
  float cur_angle = (float) reg_motor_get_angle(self);
  float cur_speed = (float) reg_motor_get_speed(self);

  uint32_t save = motor_lock();
  if (self->regulator == REG_MODE_SPEED) {
    patch_unlimited_move(&self->current_move, speed, self->max_accel);
  } else {
    start_unlimited_move(&self->current_move, speed, self->max_accel, cur_angle, cur_speed);
    reg_motor_start_regulation(self, REG_MODE_SPEED);
  }
  motor_unlock(save);
}

void reg_motor_run_timed(regulated_motor_t *self, float secs, float speed, motor_stopmode_t after) {
  float cur_angle = (float) reg_motor_get_angle(self);
  float cur_speed = (float) reg_motor_get_speed(self);

  uint32_t save = motor_lock();
  start_timed_move(&self->current_move, secs, speed, self->max_accel, cur_angle, cur_speed);
  reg_motor_start_regulation(self, REG_MODE_TIME);
  self->on_stop = after;
  motor_unlock(save);
}

void reg_motor_run_to_angle(regulated_motor_t *self, float to_angle, float speed, motor_stopmode_t after) {
  float cur_angle = (float) reg_motor_get_angle(self);
  float cur_speed = (float) reg_motor_get_speed(self);

  uint32_t save = motor_lock();
  start_move_to_angle(&self->current_move, to_angle, speed, self->max_accel, cur_angle, cur_speed);
  reg_motor_start_regulation(self, REG_MODE_POSITION);
  self->on_stop = after;
  motor_unlock(save);
}

void reg_motor_run_angle(regulated_motor_t *self, float by_angle, float speed, motor_stopmode_t after) {
  float cur_angle = (float) reg_motor_get_angle(self);
  float cur_speed = (float) reg_motor_get_speed(self);
  float tgt_angle = cur_angle + by_angle;

  uint32_t save = motor_lock();
  start_move_to_angle(&self->current_move, tgt_angle, speed, self->max_accel, cur_angle, cur_speed);
  reg_motor_start_regulation(self, REG_MODE_POSITION);
  self->on_stop = after;
  motor_unlock(save);
}

void reg_motor_run_external_ref(regulated_motor_t *self, float position, float speed, float accel) {
  uint32_t save = motor_lock();
  start_noop_move(&self->current_move, position, speed, accel);
  reg_motor_start_regulation(self, REG_MODE_EXTERNAL_REF);
  motor_unlock(save);
}

int64_t reg_motor_regulate(alarm_id_t id, void *user_data) {
  (void) id;
  regulated_motor_t *self = user_data;


  switch (self->regulator) {
    case REG_MODE_OFF:
      return 0; // do not reschedule

    case REG_MODE_SPEED:
      regulate_speed(self);
      return -REGULATOR_SAMPLING_USEC;

    default:
      regulate_position(self);
      return -REGULATOR_SAMPLING_USEC;
  }
}

void regulate_position(regulated_motor_t *self) {
  update_move(&self->current_move);

  int power = pid_control_position(&self->pid, &self->current_move, &self->encoder);
  self->current_power = power;
  motor_pwm_run(self->encoder.port, self->encoder.forward ? +power : -power); // ignore failure

  bool reg_can_finish = self->regulator == REG_MODE_POSITION || self->regulator == REG_MODE_TIME;
  if (reg_can_finish && is_move_finished(&self->current_move)) {
    switch (self->on_stop) {
      case STOP_MODE_COAST:
        reg_motor_coast(self);
        break;
      case STOP_MODE_BRAKE:
        reg_motor_brake(self);
        break;
      case STOP_MODE_HOLD:
        if (self->regulator == REG_MODE_POSITION) {
          start_noop_move(&self->current_move, self->current_move.limit.angle, 0, 0);
        } else {
          start_noop_move(&self->current_move, (float) reg_motor_get_angle(self), 0, 0);
        }
        self->regulator = REG_MODE_HOLD;
        break;
    }
  }
}

void regulate_speed(regulated_motor_t *self) {
  update_move(&self->current_move);

  // inspired by Pybricks: use trajectory, but only its speed
  float speed_sp = get_scheduled_move_speed(&self->current_move);
  float speed_pv = (float) adjustable_encoder_get_speed(&self->encoder);

  int power = pid_control_speed(&self->pid, speed_sp, speed_pv);
  self->current_power = power;
  motor_pwm_run(self->encoder.port, self->encoder.forward ? +power : -power); // ignore failure
}


bool reg_motor_is_complete(regulated_motor_t *self) {
  bool is_complete;
  uint32_t save = motor_lock();

  is_complete = self->regulator != REG_MODE_POSITION && self->regulator != REG_MODE_TIME;

  motor_unlock(save);
  return is_complete;
}

void reg_motor_wait_complete(regulated_motor_t *self) {
  while (!reg_motor_is_complete(self)) {
    mp_hal_delay_ms(REGULATOR_SAMPLING_MSEC);
  }
}

void reg_motor_set_pid(regulated_motor_t *self, float kp, float ki, float kd) {
  uint32_t save = motor_lock();
  self->pid.config.pos_kp = kp;
  self->pid.config.pos_ki = ki;
  self->pid.config.pos_kd = kd;
  motor_unlock(save);
}

void reg_motor_get_pid(regulated_motor_t *self, float *kp, float *ki, float *kd) {
  if (kp) *kp = self->pid.config.pos_kp;
  if (ki) *ki = self->pid.config.pos_ki;
  if (kd) *kd = self->pid.config.pos_kd;
}

void reg_motor_set_speed_pid(regulated_motor_t *self, float kp, float ki) {
  uint32_t save = motor_lock();
  self->pid.config.speed_kp = kp;
  self->pid.config.speed_ki = ki;
  motor_unlock(save);
}

void reg_motor_get_speed_pid(regulated_motor_t *self, float *kp, float *ki) {
  if (kp) *kp = self->pid.config.speed_kp;
  if (ki) *ki = self->pid.config.speed_ki;
}

void reg_motor_set_feedforward(regulated_motor_t *self, float kv, float ka) {
  uint32_t save = motor_lock();
  self->pid.config.kf_speed = kv;
  self->pid.config.kf_accel = ka;
  motor_unlock(save);
}

void reg_motor_get_feedforward(regulated_motor_t *self, float *kv, float *ka) {
  if (kv) *kv = self->pid.config.kf_speed;
  if (ka) *ka = self->pid.config.kf_accel;
}

// HACK: some available lock

uint32_t motor_lock(void) {
  return hw_claim_lock();
}

void motor_unlock(uint32_t cookie) {
  hw_claim_unlock(cookie);
}
