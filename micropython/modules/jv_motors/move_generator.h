/**
 * Library for generating smoothly accelerated trajectories.
 */
#ifndef FIRMWARE_MOVE_GENERATOR_H
#define FIRMWARE_MOVE_GENERATOR_H

#include <stdint.h>
#include <stdbool.h>

// Current trajectory generator state
typedef enum {
  // Motor was too fast, decelerating to change direction
  MOVE_SMOOTH_STOP,
  // Ramping to the target speed
  MOVE_RAMP_UP,
  // Maintaining constant speed
  MOVE_STEADY,
  // Ramping down to halt
  MOVE_RAMP_DOWN,
  // Motor is stopped at the end of the trajectory
  MOVE_DONE
} move_phase_t;

// How is the trajectory limited
typedef enum {
  // The trajectory is limited by target motor angle
  LIMIT_BY_ANGLE,
  // The trajectory is limited by overall maneuver time
  LIMIT_BY_TIME,
  // The trajectory is unlimited
  LIMIT_BY_NOTHING
} move_limit_t;

typedef struct {
  // Current phase of the trajectory
  move_phase_t phase;
  // How is the trajectory limited
  move_limit_t limit_type;
  union {
    float angle;
    uint64_t time;
  } limit;
  // Acceleration/speed limits
  float max_acceleration;
  float target_speed;
  // Scheduled trajectory output
  float scheduled_angle;
  float scheduled_speed;
  float scheduled_accel;
  union {
    // MOVE_RAMP_UP state
    struct {
      uint64_t t0_time;
      float t0_angle;
      float t0_speed;
      float t0_accel;
      float time_duration;
    } up;
    // MOVE_STEADY state
    struct {
      uint64_t t0_time;
      float t0_angle;
      float ramp_down_reserve;
    } steady;
    // MOVE_RAMP_DOWN state
    struct {
      uint64_t t0_time;
      float t0_angle;
      float t0_speed;
      float t0_accel;
      float time_duration;
    } down;
    // MOVE_SMOOTH_STOP state
    struct {
      uint64_t t0_time;
      float t0_angle;
      float t0_speed;
      float t0_accel;
      float time_duration;
      bool continue_after;
    } smooth_stop;
  } ramp;
} move_t;

/**
 * Start a new trajectory for moving the motor to the given target angle.
 * @param self Trajectory generator to use.
 * @param target_angle Motor angle at trajectory end (deg).
 * @param max_speed Maximum speed allowed during this move (deg/s).
 * @param max_accel Maximum rate of acceleration allowed during this move (deg/s^2).
 * @param from_angle Initial motor angle (deg).
 * @param from_speed Initial motor speed (deg/s).
 */
void start_move_to_angle(move_t *self,
    float target_angle, float max_speed, float max_accel,
    float from_angle, float from_speed);

/**
 * Start a new trajectory for running the motor for the given amount of time.
 * @param self Trajectory generator to use.
 * @param secs Total moving time of the motor for this trajectory.
 * @param max_speed Maximum speed allowed during this move (deg/s).
 * @param max_accel Maximum rate of acceleration allowed during this move (deg/s^2).
 * @param from_angle Initial motor angle (deg).
 * @param from_speed Initial motor speed (deg/s).
 */
void start_timed_move(move_t *self,
    float secs, float max_speed, float max_accel,
    float from_angle, float from_speed);

/**
 * Start a new unlimited trajectory for accelerating the motor to a given target speed.
 * @param self Trajectory generator to use.
 * @param speed Target speed for the motor.
 * @param max_accel Maximum rate of acceleration allowed during the ramp (deg/s^2).
 * @param from_angle Initial motor angle (deg).
 * @param from_speed Initial motor speed (deg/s).
 */
void start_unlimited_move(move_t *self,
    float speed, float max_accel,
    float from_angle, float from_speed);

/**
 * Re-ramp an unlimited trajectory to a different target speed.
 * @param self Trajectory generator to use.
 * @param speed New target speed for the motor.
 * @param max_accel Maximum rate of acceleration allowed during the ramp (deg/s^2).
 */
void patch_unlimited_move(move_t *self, float speed, float max_accel);

/**
 * Initialize a new already-finished trajectory that will always return the given values.
 * @param self Trajectory generator to use.
 * @param angle Final motor angle (deg).
 * @param speed Final motor speed (deg/s).
 */
void start_noop_move(move_t *self, float angle, float speed, float accel);

/**
 * Update scheduled position/speed for the current time instant.
 * @param ramp Trajectory generator to use.
 */
void update_move(move_t *ramp);

/**
 * Get the planned motor position calculated in update_move().
 * @param ramp Trajectory generator to use.
 * @return Motor angle (deg).
 */
inline float get_scheduled_move_position(move_t *ramp) {
  return ramp->scheduled_angle;
}

/**
 * Get the planned motor speed calculated in update_move().
 * @param ramp Trajectory generator to use.
 * @return Motor speed (deg/s).
 */
inline float get_scheduled_move_speed(move_t *ramp) {
  return ramp->scheduled_speed;
}

/**
 * Get the planned motor acceleration calculated in update_move().
 * @param ramp Trajectory generator to use.
 * @return Motor acceleration (deg/s^2).
 */
inline float get_scheduled_move_acceleration(move_t *ramp) {
  return ramp->scheduled_accel;
}

/**
 * Check whether the trajectory generator reached a motor-stopped state.
 * @param ramp Trajectory generator to use.
 * @return True if the motor is not moving anymore, false otherwise.
 */
inline bool is_move_finished(move_t *ramp) {
  return ramp->phase == MOVE_DONE;
}


#endif //FIRMWARE_MOVE_GENERATOR_H
