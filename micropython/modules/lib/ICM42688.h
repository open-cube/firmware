/*
Library for ICM42688 gyroscope and accelerometer
Based on the ICM42688 datasheet: https://invensense.tdk.com/wp-content/uploads/2022/12/DS-000347-ICM-42688-P-v1.7.pdf
*/
#ifndef ICM42688_h
#define ICM42688_h

#include "pico/stdlib.h"
#include "hardware/i2c.h"

// Device ID
#define ICM42688_WHO_AM_I 0x75

#define ICM42688_I2C_ADDRESS_0 0x68 // I2C address when AD0 pin is low
#define ICM42688_REG_BANK_SEL 0x76  // Register bank selection

/* Device registers*/
// Power Management
#define ICM42688_PWR_MGMT0 0x4E

// Configuration
#define ICM42688_DEVICE_CONFIG 0x11
#define ICM42688_GYRO_CONFIG0 0x4F
#define ICM42688_ACCEL_CONFIG0 0x50
#define ICM42688_GYRO_CONFIG1 0x51
#define ICM42688_GYROACCEL_CONFIG0 0x52
#define ICM42688_ACCEL_CONFIG1 0x53

// Accelerometer Measurements
#define ICM42688_ACCEL_XOUT_H 0x1F
#define ICM42688_ACCEL_XOUT_L 0x20
#define ICM42688_ACCEL_YOUT_H 0x21
#define ICM42688_ACCEL_YOUT_L 0x22
#define ICM42688_ACCEL_ZOUT_H 0x23
#define ICM42688_ACCEL_ZOUT_L 0x24

// Gyroscope Measurements
#define ICM42688_GYRO_XOUT_H 0x25
#define ICM42688_GYRO_XOUT_L 0x26
#define ICM42688_GYRO_YOUT_H 0x27
#define ICM42688_GYRO_YOUT_L 0x28
#define ICM42688_GYRO_ZOUT_H 0x29
#define ICM42688_GYRO_ZOUT_L 0x2A

// Offset registers
#define ICM42688_ACCEL_X_OFFSET_H 0x77
#define ICM42688_ACCEL_X_OFFSET_L 0x78
#define ICM42688_ACCEL_Y_OFFSET_H 0x7A
#define ICM42688_ACCEL_Y_OFFSET_L 0x7B
#define ICM42688_ACCEL_Z_OFFSET_H 0x7D
#define ICM42688_ACCEL_Z_OFFSET_L 0x7E
#define ICM42688_GYRO_X_OFFSET_H 0x13
#define ICM42688_GYRO_X_OFFSET_L 0x14
#define ICM42688_GYRO_Y_OFFSET_H 0x15
#define ICM42688_GYRO_Y_OFFSET_L 0x16
#define ICM42688_GYRO_Z_OFFSET_H 0x17
#define ICM42688_GYRO_Z_OFFSET_L 0x18

// Interrupt configuration
#define ICM42688_INT_STATUS 0x2D
#define ICM42688_INT_CONFIG 0x14
#define ICM42688_INT_CONFIG0 0x63
#define ICM42688_INT_CONFIG1 0x64
#define ICM42688_INT_SOURCE0 0x65
#define ICM42688_INT_SOURCE3 0x68

// Constants
#define ICM42688_WHO_AM_I_VALUE 0x47

typedef struct
{
    i2c_inst_t *i2c_port;  // I2C port of the sensor
    uint8_t address;       // I2C address of the sensor
    uint8_t dev_id;        // Device ID of the sensor
    uint8_t buffer[12];    // Buffer to hold sensor data
    float accel_scale;     // Accelerometer scale factor
    float gyro_scale;      // Gyroscope scale factor
    float accel_offset[3]; // Accelerometer offset values
    float gyro_offset[3];  // Gyroscope offset values
} ICM42688_t;

// Initialize ICM42688
void ICM42688_init(ICM42688_t *sensor);

// Write value to given register
void ICM42688_write_register(ICM42688_t *sensor, uint8_t reg, uint8_t value);

// Read value from given register
uint8_t ICM42688_read_register(ICM42688_t *sensor, uint8_t reg);

// Read multiple registers into buffer
void ICM42688_read_multiple_registers(ICM42688_t *sensor, uint8_t reg, uint8_t *buffer, uint8_t length);

// Select bank (0-4) to read/write from different registers
void ICM42688_bank_select(ICM42688_t *sensor, uint8_t bank);

// Turn sensor off
void ICM42688_off(ICM42688_t *sensor);

// Reset sensor
void ICM42688_reset(ICM42688_t *sensor);

// Convert accelerometer data from raw to float format using set scale (-)
void ICM42688_convert_accel_raw_to_float(ICM42688_t *sensor, int16_t accel_x_raw, int16_t accel_y_raw, int16_t accel_z_raw, float *accel_x, float *accel_y, float *accel_z);

// Convert gyroscope data from raw to float format using set scale (deg/s)
void ICM42688_convert_gyro_raw_to_float(ICM42688_t *sensor, int16_t gyro_x_raw, int16_t gyro_y_raw, int16_t gyro_z_raw, float *gyro_x, float *gyro_y, float *gyro_z);

// Read accelerometer and gyroscope data in raw format
void ICM42688_read_data_raw(ICM42688_t *sensor, int16_t *accel_x, int16_t *accel_y, int16_t *accel_z, int16_t *gyro_x, int16_t *gyro_y, int16_t *gyro_z);

// Read accelerometer and gyroscope data and convert to float format (- and deg/s)
void ICM42688_read_data(ICM42688_t *sensor, float *accel_x, float *accel_y, float *accel_z, float *gyro_x, float *gyro_y, float *gyro_z);


#endif