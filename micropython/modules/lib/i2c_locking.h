/**
 * Utilities for avoiding collisions on the internal I2C bus.
 *
 * Note: Locking should be performed by the code
 * using the libraries in this module. The libraries
 * here cannot know if the calling code wants to
 * raise exceptions or not.
 */
#ifndef OPENCUBE_I2C_LOCKING_H
#define OPENCUBE_I2C_LOCKING_H

#include <stdbool.h>
#include <pico/mutex.h>
#include <py/mphal.h>
#include <py/runtime.h>

/**
 * Exception type thrown when an I2C bus collision is detected.
 */
extern const mp_obj_type_t mp_type_InternalI2CBusyError;

/**
 * Try to acquire access to the I2C bus, possibly from
 * a non-Python IRQ context. If the bus is busy, just return false.
 *
 * This function will not block.
 *
 * @return True if the locking succeeded, false otherwise.
 */
extern bool opencube_try_lock_i2c(void);

/**
 * Try to acquire access to the I2C bus, possibly from
 * a non-Python IRQ context. If the bus is busy,
 * raise a InternalI2CBusyError exception from this code.
 *
 * This function will not block.
 *
 * This function is not safe to call from a non-Python
 * IRQ context (i.e. don't call this in the motor
 * regulator loop). This is because the function
 * does a nonlocal return. The return should (?) be
 * performed only when Python indirectly called this
 * function - the return rolls back the stack. Doing this in
 * an ISR when the ISR was not called from Python
 * looks dangerous (what will happen once the
 * CPU returns from the exception handler? it will
 * probably continue executing the previous code,
 * instead of jumping to the try-except block).
 */
extern void opencube_lock_i2c_or_raise(void);

/**
 * Release the acquired I2C bus lock.
 *
 * You can call this function from both an IRQ
 * and non-IRQ contexts.
 */
extern void opencube_unlock_i2c(void);

/**
 * Throw an I2C locking error to the user app.
 */
NORETURN extern void raise_i2c_locking_error(void);

#endif //OPENCUBE_I2C_LOCKING_H
