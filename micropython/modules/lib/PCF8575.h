/*
Library for PCF8575 16/bit I/O expander
Based on the PCF8575 datasheet: https://www.ti.com/lit/ds/symlink/pcf8575.pdf
*/
#ifndef PCF8575_h
#define PCF8575_h

#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/i2c.h"

// Quasi-bidirectional pins:
// * input  = weak high output, overridable by external circuits
// * output = strong low output
#define PCF8575_INPUT 1
#define PCF8575_OUTPUT_HIGH 1
#define PCF8575_OUTPUT_LOW 0

// Initialize the PCF8575
void PCF8575_init();

// Return all previously read bits from the PCF8575
uint16_t PCF8575_read_value();

// Return all previously read bits not yet read by user
uint16_t PCF8575_read_value_missed();

// Return all previously written bits to the PCF8575
uint16_t PCF8575_read_function();

// Read all bits of PCF8575 and store the result in the last_read array
void PCF8575_read();

// Write all bits from the last_write array to the PCF8575
void PCF8575_write();

// Read a single pin from the PCF8575
bool PCF8575_read_pin(uint8_t pin);

// Write a single pin to the PCF8575
void PCF8575_write_pin(uint8_t pin, bool value);

// Update multiple pins on PCF8575. Setting a bit high
// has a precedence over setting a bit low.
void PCF8575_write_pin_mask(uint16_t set_high, uint16_t set_low);

// Toggle a single pin on the PCF8575
void PCF8575_toggle_pin(uint8_t pin);

// Validate a pin number
bool PCF8575_validate_pin(uint8_t pin);

#endif


