/*
Library for ICM42688 gyroscope and accelerometer
Based on the ICM42688 datasheet: https://invensense.tdk.com/wp-content/uploads/2022/12/DS-000347-ICM-42688-P-v1.7.pdf
*/
#include <stdio.h>
#include <stdlib.h>
#include <opencube_hw.h>
#include "pico/stdlib.h"
#include "hardware/i2c.h"
#include "ICM42688.h"

void ICM42688_init(ICM42688_t *sensor)
{
    sensor->address = ICM42688_I2C_ADDR;
    sensor->i2c_port = INTERNAL_I2C_BUS;
    // Check the device ID
    ICM42688_bank_select(sensor, 0);
    sensor->dev_id = ICM42688_read_register(sensor, ICM42688_WHO_AM_I);
    if (sensor->dev_id != ICM42688_WHO_AM_I_VALUE)
    {
        printf("ICM42688 not found at address 0x%x\n", sensor->address);
        exit(1);
    }

    ICM42688_write_register(sensor, ICM42688_PWR_MGMT0, 0x0F); // Set to 0x0F to activate the sensor
    sleep_ms(10);  
    // Configure the accelerometer
    ICM42688_write_register(sensor, ICM42688_ACCEL_CONFIG0, 0x6F); // Set to 0x36 for +/- 2g range, 5 Hz ODR
    ICM42688_write_register(sensor, ICM42688_ACCEL_CONFIG1, 0x10); // Set UI filter to 3rd order

    // Configure the gyroscope
    ICM42688_write_register(sensor, ICM42688_GYRO_CONFIG0, 0x2F); // Set to 0x16 for +/- 1000 deg/s range, 5 Hz ODR
    ICM42688_write_register(sensor, ICM42688_GYRO_CONFIG1, 0x08); // Set UI filter to 3rd order
    
    ICM42688_write_register(sensor, ICM42688_GYROACCEL_CONFIG0, 0x00); // BW=ODR/2
    
    // Set the interrupt configuration
    ICM42688_write_register(sensor, ICM42688_INT_CONFIG, 0x00);  // pulsed mode, open drain, active low
    ICM42688_write_register(sensor, ICM42688_INT_CONFIG0, 0x00); // Clear drdy int on Status Bit Read
    ICM42688_write_register(sensor, ICM42688_INT_CONFIG1, 0x00); // Set int async reset to 0
    ICM42688_write_register(sensor, ICM42688_INT_SOURCE0, 0x08); // Set to 0x10 to enable drdy interrupt
    ICM42688_write_register(sensor, ICM42688_INT_SOURCE3, 0x08); // Set to 0x10 to enable drdy interrupt
    sleep_ms(20);
    for (int i = 0; i < 3; i++)
    {
        sensor->accel_offset[i] = 0.0;
        sensor->gyro_offset[i] = 0.0;
    }
    
    // Set the scale factors for the accelerometer and gyroscope
    sensor->accel_scale = 2.0 / 32768.0;   // +/- 2g range
    sensor->gyro_scale = 1000.0 / 32768.0; // +/- 250 deg/s range
}

void ICM42688_write_register(ICM42688_t *sensor, uint8_t reg, uint8_t value)
{
    uint8_t tx_data[2] = {reg, value};
    i2c_write_blocking(sensor->i2c_port, sensor->address, tx_data, 2, false);
}

uint8_t ICM42688_read_register(ICM42688_t *sensor, uint8_t reg)
{
    i2c_write_blocking(sensor->i2c_port, sensor->address, &reg, 1, true);
    i2c_read_blocking(sensor->i2c_port, sensor->address, sensor->buffer, 1, false);
    return sensor->buffer[0];
}

void ICM42688_read_multiple_registers(ICM42688_t *sensor, uint8_t reg, uint8_t *buffer, uint8_t length)
{
    i2c_write_blocking(sensor->i2c_port, sensor->address, &reg, 1, true);
    i2c_read_blocking(sensor->i2c_port, sensor->address, buffer, length, false);
}

void ICM42688_bank_select(ICM42688_t *sensor, uint8_t bank) {
    ICM42688_write_register(sensor, ICM42688_REG_BANK_SEL, bank);
}

void ICM42688_disable_interrupt(ICM42688_t *sensor) {
    ICM42688_bank_select(sensor, 0);
    ICM42688_write_register(sensor, ICM42688_INT_SOURCE0, 0x00); 
}
void ICM42688_off(ICM42688_t *sensor)
{
    ICM42688_disable_interrupt(sensor);
    ICM42688_bank_select(sensor, 0);
    ICM42688_write_register(sensor, ICM42688_PWR_MGMT0, 0x00); // Disable gyro and accelerometer
}

void ICM42688_reset(ICM42688_t *sensor)
{
    ICM42688_bank_select(sensor, 0);
    ICM42688_write_register(sensor, ICM42688_DEVICE_CONFIG, 0x01); // Set reset bit
    sleep_ms(2);  
    ICM42688_write_register(sensor, ICM42688_PWR_MGMT0, 0x0F); // Set to 0x0F to activate the sensor                                              
}

void ICM42688_convert_accel_raw_to_float(ICM42688_t *sensor, int16_t accel_x_raw, int16_t accel_y_raw, int16_t accel_z_raw, float *accel_x, float *accel_y, float *accel_z)
{
    // Convert the raw accelerometer data to float values in g
    *accel_x = ((float)accel_x_raw) * sensor->accel_scale;
    *accel_y = ((float)accel_y_raw) * sensor->accel_scale;
    *accel_z = ((float)accel_z_raw) * sensor->accel_scale;
    // Apply the accelerometer offsets
    *accel_x += sensor->accel_offset[0];
    *accel_y += sensor->accel_offset[1];
    *accel_z += sensor->accel_offset[2];
}

void ICM42688_convert_gyro_raw_to_float(ICM42688_t *sensor, int16_t gyro_x_raw, int16_t gyro_y_raw, int16_t gyro_z_raw, float *gyro_x, float *gyro_y, float *gyro_z)
{
    // Convert the raw gyroscope data to float values in dps
    *gyro_x = ((float)gyro_x_raw) * sensor->gyro_scale;
    *gyro_y = ((float)gyro_y_raw) * sensor->gyro_scale;
    *gyro_z = ((float)gyro_z_raw) * sensor->gyro_scale;
    // Apply the gyroscope offsets
    *gyro_x += sensor->gyro_offset[0];
    *gyro_y += sensor->gyro_offset[1];
    *gyro_z += sensor->gyro_offset[2];
}

void ICM42688_read_data_raw(ICM42688_t *sensor, int16_t *accel_x, int16_t *accel_y, int16_t *accel_z, int16_t *gyro_x, int16_t *gyro_y, int16_t *gyro_z)
{
    // Read the raw accelerometer and gyroscope data
    ICM42688_read_multiple_registers(sensor, ICM42688_ACCEL_XOUT_H, sensor->buffer, 12);
    *accel_x = ((int16_t)sensor->buffer[0] << 8) | sensor->buffer[1];
    *accel_y = ((int16_t)sensor->buffer[2] << 8) | sensor->buffer[3];
    *accel_z = ((int16_t)sensor->buffer[4] << 8) | sensor->buffer[5];
    *gyro_x = ((int16_t)sensor->buffer[6] << 8) | sensor->buffer[7];
    *gyro_y = ((int16_t)sensor->buffer[8] << 8) | sensor->buffer[9];
    *gyro_z = ((int16_t)sensor->buffer[10] << 8) | sensor->buffer[11];
}

void ICM42688_read_data(ICM42688_t *sensor, float *accel_x, float *accel_y, float *accel_z, float *gyro_x, float *gyro_y, float *gyro_z)
{
    // Read the accelerometer and gyroscope data in g and dps
    int16_t accel_x_raw, accel_y_raw, accel_z_raw;
    int16_t gyro_x_raw, gyro_y_raw, gyro_z_raw;
    ICM42688_read_data_raw(sensor, &accel_x_raw, &accel_y_raw, &accel_z_raw, &gyro_x_raw, &gyro_y_raw, &gyro_z_raw);
    ICM42688_convert_accel_raw_to_float(sensor, accel_x_raw, accel_y_raw, accel_z_raw, accel_x, accel_y, accel_z);
    ICM42688_convert_gyro_raw_to_float(sensor, gyro_x_raw, gyro_y_raw, gyro_z_raw, gyro_x, gyro_y, gyro_z);
}
