# Create an INTERFACE library for our C module.
add_library(usermod_opencube_lib INTERFACE)

# Add our source files to the lib
target_sources(usermod_opencube_lib INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}/i2c_locking.h
    ${CMAKE_CURRENT_LIST_DIR}/i2c_locking.c
    ${CMAKE_CURRENT_LIST_DIR}/ADS1119.h
    ${CMAKE_CURRENT_LIST_DIR}/ADS1119.c
    ${CMAKE_CURRENT_LIST_DIR}/PCF8575.h
    ${CMAKE_CURRENT_LIST_DIR}/PCF8575.c
    ${CMAKE_CURRENT_LIST_DIR}/ICM42688.h
    ${CMAKE_CURRENT_LIST_DIR}/ICM42688.c
)

# Add the current directory as an include directory.
target_include_directories(usermod_opencube_lib INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}
)
