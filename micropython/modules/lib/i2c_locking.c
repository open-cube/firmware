#include "i2c_locking.h"

auto_init_mutex(cube_i2c_lock);

MP_DEFINE_EXCEPTION(InternalI2CBusyError, RuntimeError)

bool opencube_try_lock_i2c(void)
{
    return mutex_try_enter(&cube_i2c_lock, NULL);
}

void opencube_lock_i2c_or_raise(void)
{
    if (!opencube_try_lock_i2c())
    {
        raise_i2c_locking_error();
    }
}

void opencube_unlock_i2c(void)
{
    return mutex_exit(&cube_i2c_lock);
}

void raise_i2c_locking_error(void)
{
    /**
     * This should happen only rarely. I can imagine two scenarios:
     *  - the user has broken locking (nested?),
     *  - multiple cores are trying to access the peripheral.
     *
     * Both cases can be fixed with changes to the user code:
     *  - don't acquire the lock multiple times from one core.
     *  - protect functions using I2C via a separate Python mutex.
     */
    mp_raise_msg(
      &mp_type_InternalI2CBusyError,
      MP_ERROR_TEXT("Attempted to access the internal I2C bus while it is used elsewhere at the moment")
    );
}
