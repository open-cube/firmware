/*
Library for ICM20608-G gyroscope and accelerometer
Based on the ICM20608-G datasheet: https://invensense.tdk.com/wp-content/uploads/2015/03/DS-000081-v1.01.pdf
and register map: https://invensense.tdk.com/download-pdf/icm-20608-g-register-map/
*/
#ifndef ICM20608_h
#define ICM20608_h

#include "pico/stdlib.h"
#include "hardware/i2c.h"

// Device ID
#define ICM20608G_WHO_AM_I 0x75

/* Device registers*/
// Power Management
#define ICM20608G_PWR_MGMT_1 0x6B
#define ICM20608G_PWR_MGMT_2 0x6C

// Configuration
#define ICM20608G_SMPLRT_DIV 0x19
#define ICM20608G_CONFIG 0x1A
#define ICM20608G_GYRO_CONFIG 0x1B
#define ICM20608G_ACCEL_CONFIG 0x1C

// Gyroscope Measurements
#define ICM20608G_GYRO_XOUT_H 0x43
#define ICM20608G_GYRO_XOUT_L 0x44
#define ICM20608G_GYRO_YOUT_H 0x45
#define ICM20608G_GYRO_YOUT_L 0x46
#define ICM20608G_GYRO_ZOUT_H 0x47
#define ICM20608G_GYRO_ZOUT_L 0x48

// Accelerometer Measurements
#define ICM20608G_ACCEL_XOUT_H 0x3B
#define ICM20608G_ACCEL_XOUT_L 0x3C
#define ICM20608G_ACCEL_YOUT_H 0x3D
#define ICM20608G_ACCEL_YOUT_L 0x3E
#define ICM20608G_ACCEL_ZOUT_H 0x3F
#define ICM20608G_ACCEL_ZOUT_L 0x40

// Offset registers
#define ICM20608G_ACCEL_X_OFFSET_H 0x77
#define ICM20608G_ACCEL_X_OFFSET_L 0x78
#define ICM20608G_ACCEL_Y_OFFSET_H 0x7A
#define ICM20608G_ACCEL_Y_OFFSET_L 0x7B
#define ICM20608G_ACCEL_Z_OFFSET_H 0x7D
#define ICM20608G_ACCEL_Z_OFFSET_L 0x7E
#define ICM20608G_GYRO_X_OFFSET_H 0x13
#define ICM20608G_GYRO_X_OFFSET_L 0x14
#define ICM20608G_GYRO_Y_OFFSET_H 0x15
#define ICM20608G_GYRO_Y_OFFSET_L 0x16
#define ICM20608G_GYRO_Z_OFFSET_H 0x17
#define ICM20608G_GYRO_Z_OFFSET_L 0x18

// Interrupt configuration
#define ICM20608G_INT_ENABLE 0x38
#define ICM20608G_INT_STATUS 0x3A
#define ICM20608G_INT_CONFIG 0x37

// Constants
#define ICM20608G_WHO_AM_I_VALUE 0xAF

typedef struct
{
    i2c_inst_t *i2c_port;  // I2C port of the sensor
    uint8_t address;       // I2C address of the sensor
    uint8_t dev_id;        // Device ID of the sensor
    uint8_t buffer[14];    // Buffer to hold sensor data
    float accel_scale;     // Accelerometer scale factor
    float gyro_scale;      // Gyroscope scale factor
    float accel_offset[3]; // Accelerometer offset values
    float gyro_offset[3];  // Gyroscope offset values
} ICM20608G_t;

// Initialize ICM20608G
void ICM20608G_init(ICM20608G_t *sensor);

// Write value to given register
void ICM20608G_write_register(ICM20608G_t *sensor, uint8_t reg, uint8_t value);

// Read value from given register
uint8_t ICM20608G_read_register(ICM20608G_t *sensor, uint8_t reg);

// Read multiple registers into buffer
void ICM20608G_read_multiple_registers(ICM20608G_t *sensor, uint8_t reg, uint8_t *buffer, uint8_t length);

// Calibrate sensor and save calibration values to sensor registers
void ICM20608G_calibrate(ICM20608G_t *sensor);

// Enable interrupt on pin GPIO 28
void ICM20608G_enable_interrupt(ICM20608G_t *sensor);

// Disable interrupt
void ICM20608G_disable_interrupt(ICM20608G_t *sensor);

// Turn sensor off
void ICM20608G_off(ICM20608G_t *sensor);

// Reset sensor
void ICM20608G_reset(ICM20608G_t *sensor);

// Put sensor to sleep
void ICM20608G_sleep(ICM20608G_t *sensor);

// Wake up sensor
void ICM20608G_wakeup(ICM20608G_t *sensor);

// Read accelerometer data in raw format
void ICM20608G_read_accel_raw(ICM20608G_t *sensor, int16_t *accel_x, int16_t *accel_y, int16_t *accel_z);

// Read gyroscope data in raw format
void ICM20608G_read_gyro_raw(ICM20608G_t *sensor, int16_t *gyro_x, int16_t *gyro_y, int16_t *gyro_z);

// Convert accelerometer data from raw to float format using set scale (-)
void ICM20608G_convert_accel_raw_to_float(ICM20608G_t *sensor, int16_t accel_x_raw, int16_t accel_y_raw, int16_t accel_z_raw, float *accel_x, float *accel_y, float *accel_z);

// Convert gyroscope data from raw to float format using set scale (deg/s)
void ICM20608G_convert_gyro_raw_to_float(ICM20608G_t *sensor, int16_t gyro_x_raw, int16_t gyro_y_raw, int16_t gyro_z_raw, float *gyro_x, float *gyro_y, float *gyro_z);

// Read accelerometer and gyroscope data in raw format
void ICM20608G_read_data_raw(ICM20608G_t *sensor, int16_t *accel_x, int16_t *accel_y, int16_t *accel_z, int16_t *gyro_x, int16_t *gyro_y, int16_t *gyro_z);

// Read accelerometer and gyroscope data and convert to float format (- and deg/s)
void ICM20608G_read_data(ICM20608G_t *sensor, float *accel_x, float *accel_y, float *accel_z, float *gyro_x, float *gyro_y, float *gyro_z);

// Read accelerometer data and convert to float format (-)
void ICM20608G_read_acceleration(ICM20608G_t *sensor, float *accel_x, float *accel_y, float *accel_z);

// Read gyroscope data and convert to float format (deg/s)
void ICM20608G_read_rotation(ICM20608G_t *sensor, float *gyro_x, float *gyro_y, float *gyro_z);

#endif