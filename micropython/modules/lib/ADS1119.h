/*
Library for ADS1119 4-channel, 16-bit, 1-kSPS ADC 
Based on the ADS1119 datasheet: https://www.ti.com/lit/ds/sbas925a/sbas925a.pdf?ts=1684993325025
*/
#ifndef ADS1119_h
#define ADS1119_h

#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/i2c.h"

// Struct with last saved configuration parameters
typedef struct ADS1119_config
{
	uint8_t mux;
	uint8_t gain;
	uint8_t data_rate;
	uint8_t conversion_mode;
	uint8_t vref;
} ADS1119_config_t;

// Struct with ADC parameters
typedef struct ADS1119_adc
{
	i2c_inst_t *i2c_port;
	uint8_t i2c_addr;
	uint16_t conversion_time;
	ADS1119_config_t config;
} ADS1119_adc_t;

// Parameters
#define ADS1119_RANGE ((uint16_t)32767)
#define ADS1119_INTERNAL_REFERENCE_VOLTAGE ((float)2.048)

// Commands
#define ADS1119_COMMAND_RESET 0B00000110
#define ADS1119_COMMAND_START 0B00001000
#define ADS1119_COMMAND_PWRDOWN 0B00000010
#define ADS1119_COMMAND_RDATA 0B00010000
#define ADS1119_COMMAND_RREG 0B00100000
#define ADS1119_COMMAND_WREG 0B01000000

// Register adress
#define ADS1119_REGISTER_CONFIG 0B0
#define ADS1119_REGISTER_STATUS 0B1
#define ADS1119_REGISTER_OFFSET 2
#define ADS1119_DATA_READY_OFFSET 7

// Channel selection
#define ADS1119_MUX_P_AIN0_N_AIN1 (0B000)
#define ADS1119_MUX_P_AIN2_N_AIN3 (0B001)
#define ADS1119_MUX_P_AIN1_N_AIN2 (0B010)
#define ADS1119_MUX_P_AIN0_N_AGND (0B011)
#define ADS1119_MUX_P_AIN1_N_AGND (0B100)
#define ADS1119_MUX_P_AIN2_N_AGND (0B101)
#define ADS1119_MUX_P_AIN3_N_AGND (0B110)
#define ADS1119_MUX_SHORTED_H_AVDD (0B111)
#define ADS1119_MUX_OFFSET 5

// Gain selection
#define ADS1119_GAIN_1 (0B0)
#define ADS1119_GAIN_4 (0B1)
#define ADS1119_GAIN_OFFSET 4

// Data rate selection
#define ADS1119_DR_20 (0B00)
#define ADS1119_DR_90 (0B01)
#define ADS1119_DR_330 (0B10)
#define ADS1119_DR_1000 (0B11)
#define ADS1119_DR_OFFSET 2

// Conversion mode selection
#define ADS1119_CM_SINGLE (0B0)
#define ADS1119_CM_CONTINUOUS (0B1)
#define ADS1119_CM_OFFSET 1

// Voltage reference selection
#define ADS1119_VREF_INTERNAL (0B0)
#define ADS1119_VREF_EXTERNAL (0B1)
#define ADS1119_VREF_OFFSET 0

// Initialize ADC
void ADS1119_init(ADS1119_adc_t *adc);

/*ADC1119 configuration*/
void ADS1119_set_channel(ADS1119_adc_t *adc, uint8_t channel);
void ADS1119_set_gain(ADS1119_adc_t *adc, uint8_t gain);
void ADS1119_set_data_rate(ADS1119_adc_t *adc, uint8_t data_rate);
void ADS1119_set_vref(ADS1119_adc_t *adc, uint8_t vref);

// Blocking read of analog value on current channel
// Waits for conversion to finish
// Do not call this from an interrupt handler!
uint16_t ADS1119_lockBusAndReadTwoBytes(ADS1119_adc_t *adc);

/**
This command resets the device to the default states. No delay time is required after the RESET
command is latched before starting to communicate with the device as long as the timing requirements
(see the I2C Timing Requirements table) for the (repeated) START and STOP conditions are met.
*/
void ADS1119_reset(ADS1119_adc_t *adc);

/**
The POWERDOWN command places the device into power-down mode.
This command shuts down all internal analog components, but holds all register values.
In case the POWERDOWN command is issued when a conversion is ongoing, the conversion completes
before the ADS1119 enters power-down mode. As soon as a START/SYNC command is issued, all analog
components return to their previous states.
*/
void ADS1119_powerDown(ADS1119_adc_t *adc);

// Send command to read analog value from ADC
void ADS1119_commandReadData(ADS1119_adc_t *adc);

// Send command to start new conversion on current channel
void ADS1119_commandStart(ADS1119_adc_t *adc);

// Get last read analog value from ADC
// (does not start new conversion)
uint16_t ADS1119_read(ADS1119_adc_t *adc);

// Write value to given register
void ADS1119_write(ADS1119_adc_t *adc, uint8_t registerValue, uint8_t value);

// Write byte to ADC
void ADS1119_writeByte(ADS1119_adc_t *adc, uint8_t value);

// Read configuration register from ADC
void ADS1119_read_config(ADS1119_adc_t *adc);

// Write configuration register to ADC
void ADS1119_write_config(ADS1119_adc_t *adc);

// Read status register from ADC and return true if data is ready
bool ADS1119_data_ready(ADS1119_adc_t *adc);

// Read from given register from ADC
uint8_t ADS1119_readRegister(ADS1119_adc_t *adc, uint8_t registerToRead);

#endif