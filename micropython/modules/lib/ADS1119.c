/*
Library for ADS1119 4-channel, 16-bit, 1-kSPS ADC
Based on the ADS1119 datasheet: https://www.ti.com/lit/ds/sbas925a/sbas925a.pdf?ts=1684993325025
*/
#include "ADS1119.h"
#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/i2c.h"
#include "i2c_locking.h"
#include <opencube_hw.h>

void ADS1119_init(ADS1119_adc_t *adc)
{
    adc->i2c_port = INTERNAL_I2C_BUS;
    adc->i2c_addr = ADS1119_I2C_ADDR;
    adc->conversion_time = 1.0;
    ADS1119_read_config(adc);
}

void ADS1119_set_channel(ADS1119_adc_t *adc, uint8_t channel)
{
    switch (channel)
    {
    case 0:
        adc->config.mux = ADS1119_MUX_P_AIN0_N_AGND;
        break;
    case 1:
        adc->config.mux = ADS1119_MUX_P_AIN1_N_AGND;
        break;
    case 2:
        adc->config.mux = ADS1119_MUX_P_AIN2_N_AGND;
        break;
    case 3:
        adc->config.mux = ADS1119_MUX_P_AIN3_N_AGND;
        break;
    default:
        break;
    }
}

void ADS1119_set_gain(ADS1119_adc_t *adc, uint8_t gain)
{
    switch (gain)
    {
    case 0:
        adc->config.gain = ADS1119_GAIN_1;
        break;
    case 1:
        adc->config.gain = ADS1119_GAIN_4;
        break;
    default:
        break;
    }
}

void ADS1119_set_data_rate(ADS1119_adc_t *adc, uint8_t data_rate)
{
    switch (data_rate)
    {
    case 0:
        adc->config.data_rate = ADS1119_DR_20;
        break;
    case 1:
        adc->config.data_rate = ADS1119_DR_90;
        break;
    case 2:
        adc->config.data_rate = ADS1119_DR_330;
        break;
    case 3:
        adc->config.data_rate = ADS1119_DR_1000;
        break;
    default:
        break;
    }
}

void ADS1119_set_vref(ADS1119_adc_t *adc, uint8_t vref)
{
    switch (vref)
    {
    case 0:
        adc->config.vref = ADS1119_VREF_INTERNAL;
        break;
    case 1:
        adc->config.vref = ADS1119_VREF_EXTERNAL;
        break;
    default:
        break;
    }
}

uint16_t ADS1119_lockBusAndReadTwoBytes(ADS1119_adc_t *adc)
{
    unsigned long conversion_time;

    // ADC conversion time in ms 
    switch (adc->config.data_rate)
    {
    case ADS1119_DR_20:
        conversion_time = 1000.0 / 20.0;
        break;
    case ADS1119_DR_90:
        conversion_time = 1000.0 / 90.0;
        break;
    case ADS1119_DR_330:
        conversion_time = 1000.0 / 330.0;
        break;
    default:
        conversion_time = 1.0;
        break;
    }

    // Start conversion
    opencube_lock_i2c_or_raise();
    ADS1119_commandStart(adc);
    opencube_unlock_i2c();

    // Wait for conversion to complete
    sleep_ms(conversion_time);
    opencube_lock_i2c_or_raise();
    while (!ADS1119_data_ready(adc))
    {
        sleep_us(10);
    }

    // Read conversion result
    uint16_t result = ADS1119_read(adc);
    opencube_unlock_i2c();
    return result;
}

void ADS1119_reset(ADS1119_adc_t *adc)
{
    ADS1119_writeByte(adc, ADS1119_COMMAND_RESET); // 0x06
}

void ADS1119_powerDown(ADS1119_adc_t *adc)
{
    ADS1119_writeByte(adc, ADS1119_COMMAND_PWRDOWN); // 0x02
}
void ADS1119_commandReadData(ADS1119_adc_t *adc)
{
    ADS1119_writeByte(adc, ADS1119_COMMAND_RDATA); // 0x10
}

void ADS1119_commandStart(ADS1119_adc_t *adc)
{
    ADS1119_writeByte(adc, ADS1119_COMMAND_START); // 0x08
}

uint16_t ADS1119_read(ADS1119_adc_t *adc)
{
    uint8_t dst[2];
    ADS1119_commandReadData(adc);
    i2c_read_blocking(adc->i2c_port, adc->i2c_addr, dst, 2, false);
    return (dst[0] << 8) | dst[1];
}

void ADS1119_write(ADS1119_adc_t *adc, uint8_t registerValue, uint8_t value)
{
    uint8_t src[2];
    src[0] = registerValue;
    src[1] = value;
    i2c_write_blocking(adc->i2c_port, adc->i2c_addr, src, 2, true);
}

void ADS1119_writeByte(ADS1119_adc_t *adc, uint8_t value)
{
    i2c_write_blocking(adc->i2c_port, adc->i2c_addr,
                       &value, 1, true);
}

void ADS1119_read_config(ADS1119_adc_t *adc)
{
    uint8_t conf = ADS1119_readRegister(adc, ADS1119_REGISTER_CONFIG);
    adc->config.mux = (conf >> ADS1119_MUX_OFFSET) & 0B111;          // XXX00000
    adc->config.gain = (conf >> ADS1119_GAIN_OFFSET) & 0B1;          // 000X0000
    adc->config.data_rate = (conf >> ADS1119_DR_OFFSET) & 0B11;      // 0000XX00
    adc->config.conversion_mode = (conf >> ADS1119_CM_OFFSET) & 0B1; // 000000X0
    adc->config.vref = (conf >> ADS1119_VREF_OFFSET) & 0B1;          // 0000000X
}

void ADS1119_write_config(ADS1119_adc_t *adc)
{
    uint8_t registerValue = ADS1119_COMMAND_WREG;
    uint8_t value = 0x0;

    value |= (adc->config.mux << ADS1119_MUX_OFFSET);            // XXX00000
    value |= (adc->config.gain << ADS1119_GAIN_OFFSET);          // 000X0000
    value |= (adc->config.data_rate << ADS1119_DR_OFFSET);       // 0000XX00
    value |= (adc->config.conversion_mode << ADS1119_CM_OFFSET); // 000000X0
    value |= (adc->config.vref << ADS1119_VREF_OFFSET);          // 0000000X
    ADS1119_write(adc, registerValue, value);
}

bool ADS1119_data_ready(ADS1119_adc_t *adc)
{
    uint8_t status = ADS1119_readRegister(adc, ADS1119_REGISTER_STATUS);
    status = (status >> ADS1119_DATA_READY_OFFSET);
    return status == 0B1;
}

uint8_t ADS1119_readRegister(ADS1119_adc_t *adc, uint8_t registerToRead)
{
    uint8_t byteToWrite = ADS1119_COMMAND_RREG | (registerToRead << ADS1119_REGISTER_OFFSET);
    i2c_write_blocking(adc->i2c_port, adc->i2c_addr,
                       &byteToWrite, 1, true);
    uint8_t data_read;
    i2c_read_blocking(adc->i2c_port, adc->i2c_addr, &data_read, 1, false);

    return data_read;
}