/*
Library for ICM20608-G gyroscope and accelerometer
Based on the ICM20608-G datasheet: https://invensense.tdk.com/wp-content/uploads/2015/03/DS-000081-v1.01.pdf
and register map: https://invensense.tdk.com/download-pdf/icm-20608-g-register-map/
*/
#include <stdio.h>
#include <stdlib.h>
#include <opencube_hw.h>
#include "pico/stdlib.h"
#include "hardware/i2c.h"
#include "ICM20608.h"

void ICM20608G_init(ICM20608G_t *sensor)
{
    sensor->address = ICM20608_I2C_ADDR;
    sensor->i2c_port = INTERNAL_I2C_BUS;
    // Check the device ID

    sensor->dev_id = ICM20608G_read_register(sensor, ICM20608G_WHO_AM_I);
    if (sensor->dev_id != ICM20608G_WHO_AM_I_VALUE)
    {
        printf("ICM20608G not found at address 0x%x\n", sensor->address);
        exit(1);
    }

    // Wake up the sensor
    ICM20608G_reset(sensor);
    ICM20608G_write_register(sensor, ICM20608G_PWR_MGMT_1, 0x01); // Set to 0x00 to activate the sensor
    ICM20608G_write_register(sensor, ICM20608G_PWR_MGMT_2, 0x00); // Set to 0x00 to activate the sensor

    // Set the sample rate divider
    ICM20608G_write_register(sensor, ICM20608G_SMPLRT_DIV, 0x02); // Set to 0x00 for fastest sample rate

    // Configure the accelerometer
    ICM20608G_write_register(sensor, ICM20608G_ACCEL_CONFIG, 0x00); // Set to 0x00 for +/- 2g range

    // Configure the gyroscope
    ICM20608G_write_register(sensor, ICM20608G_GYRO_CONFIG, 0x10); // Set to 0x10 for +/- 1000 deg/s range

    // Set the configuration
    ICM20608G_write_register(sensor, ICM20608G_CONFIG, 0x01); // Set to 0x01 for filtering 1khz

    // Set the interrupt configuration
    ICM20608G_write_register(sensor, ICM20608G_INT_CONFIG, 0x00);
    ICM20608G_write_register(sensor, ICM20608G_INT_ENABLE, 0x01);

    for (int i = 0; i < 3; i++)
    {
        sensor->accel_offset[i] = 0.0;
        sensor->gyro_offset[i] = 0.0;
    }
    
    // Set the scale factors for the accelerometer and gyroscope
    sensor->accel_scale = 2.0 / 32768.0;   // +/- 2g range
    sensor->gyro_scale = 1000.0 / 32768.0; // +/- 250 deg/s range
}

void ICM20608G_write_register(ICM20608G_t *sensor, uint8_t reg, uint8_t value)
{
    uint8_t tx_data[2] = {reg, value};
    i2c_write_blocking(sensor->i2c_port, sensor->address, tx_data, 2, false);
}

uint8_t ICM20608G_read_register(ICM20608G_t *sensor, uint8_t reg)
{
    i2c_write_blocking(sensor->i2c_port, sensor->address, &reg, 1, true);
    i2c_read_blocking(sensor->i2c_port, sensor->address, sensor->buffer, 1, false);
    return sensor->buffer[0];
}

void ICM20608G_read_multiple_registers(ICM20608G_t *sensor, uint8_t reg, uint8_t *buffer, uint8_t length)
{
    i2c_write_blocking(sensor->i2c_port, sensor->address, &reg, 1, true);
    i2c_read_blocking(sensor->i2c_port, sensor->address, buffer, length, false);
}

void ICM20608G_calibrate(ICM20608G_t *sensor)
{
    // Initialize the accelerometer and gyroscope offset values to 0
    for (int i = 0; i < 3; i++)
    {
        sensor->accel_offset[i] = 0.0;
        sensor->gyro_offset[i] = 0.0;
    }
    // Collect 1000 samples of accelerometer and gyroscope data
    int16_t accel_raw[3], gyro_raw[3];
    for (int i = 0; i < 1000; i++)
    {
        // Read the accelerometer data
        ICM20608G_read_accel_raw(sensor, &accel_raw[0], &accel_raw[1], &accel_raw[2]);
        // Read the gyroscope data
        ICM20608G_read_gyro_raw(sensor, &gyro_raw[0], &gyro_raw[1], &gyro_raw[2]);

        // Add the raw data to the offset values
        for (int j = 0; j < 3; j++)
        {
            sensor->accel_offset[j] += (double)accel_raw[j];
            sensor->gyro_offset[j] += (double)gyro_raw[j];
        }

        // Delay for 1 millisecond between samples
        sleep_ms(1);
    }

    // Calculate the average offset values
    for (int i = 0; i < 3; i++)
    {
        sensor->accel_offset[i] /= 1000.0;
        sensor->gyro_offset[i] /= 1000.0;
    }

    // Write the calibration values to the sensor
    
    ICM20608G_write_register(sensor, ICM20608G_ACCEL_X_OFFSET_H, (uint8_t)(sensor->accel_offset[0] / 8.0)); // Scale factor for +/- 2g range
    ICM20608G_write_register(sensor, ICM20608G_ACCEL_X_OFFSET_L, (uint8_t)(sensor->accel_offset[0] / 8.0));
    ICM20608G_write_register(sensor, ICM20608G_ACCEL_Y_OFFSET_H, (uint8_t)(sensor->accel_offset[1] / 8.0));
    ICM20608G_write_register(sensor, ICM20608G_ACCEL_Y_OFFSET_L, (uint8_t)(sensor->accel_offset[1] / 8.0));
    ICM20608G_write_register(sensor, ICM20608G_ACCEL_Z_OFFSET_H, (uint8_t)(sensor->accel_offset[2] / 8.0));
    ICM20608G_write_register(sensor, ICM20608G_ACCEL_Z_OFFSET_L, (uint8_t)(sensor->accel_offset[2] / 8.0));
    
    ICM20608G_write_register(sensor, ICM20608G_GYRO_X_OFFSET_H, ((uint8_t)(sensor->gyro_offset[0]) >> 8) & 0xFF); // Scale factor for +/- 250 deg/s range
    ICM20608G_write_register(sensor, ICM20608G_GYRO_X_OFFSET_L, (uint8_t)(sensor->gyro_offset[0]));
    ICM20608G_write_register(sensor, ICM20608G_GYRO_Y_OFFSET_H, ((uint8_t)(sensor->gyro_offset[1]) >> 8) & 0xFF);
    ICM20608G_write_register(sensor, ICM20608G_GYRO_Y_OFFSET_L, (uint8_t)(sensor->gyro_offset[1]) & 0xFF);
    ICM20608G_write_register(sensor, ICM20608G_GYRO_Z_OFFSET_H, ((uint8_t)(sensor->gyro_offset[2]) >> 8) & 0xFF);
    ICM20608G_write_register(sensor, ICM20608G_GYRO_Z_OFFSET_L, (uint8_t)(sensor->gyro_offset[2]) & 0xFF);
}

void ICM20608G_enable_interrupt(ICM20608G_t *sensor)
{
    ICM20608G_write_register(sensor, ICM20608G_INT_CONFIG, 0x20); // Enable active low interrupt on INT pin
    ICM20608G_write_register(sensor, ICM20608G_INT_ENABLE, 0x01); // Enable data ready interrupt
}

void ICM20608G_disable_interrupt(ICM20608G_t *sensor)
{
    ICM20608G_write_register(sensor, ICM20608G_INT_ENABLE, 0x00); // Disable data ready interrupt
}

void ICM20608G_off(ICM20608G_t *sensor)
{
    ICM20608G_disable_interrupt(sensor);
    ICM20608G_sleep(sensor);
    ICM20608G_write_register(sensor, ICM20608G_PWR_MGMT_2, 0x3F); // Disable gyro and accelerometer
}

void ICM20608G_reset(ICM20608G_t *sensor)
{
    ICM20608G_write_register(sensor, ICM20608G_PWR_MGMT_1, 0x80); // Set reset bit
    sleep_ms(100);                                                // Wait for reset to complete
}

void ICM20608G_sleep(ICM20608G_t *sensor)
{
    ICM20608G_write_register(sensor, ICM20608G_PWR_MGMT_1, 0x40); // Set sleep bit
}

void ICM20608G_wakeup(ICM20608G_t *sensor)
{
    ICM20608G_write_register(sensor, ICM20608G_PWR_MGMT_1, 0x00); // Clear sleep bit
}

void ICM20608G_read_accel_raw(ICM20608G_t *sensor, int16_t *accel_x, int16_t *accel_y, int16_t *accel_z)
{
    // Read the raw accelerometer data
    ICM20608G_read_multiple_registers(sensor, ICM20608G_ACCEL_XOUT_H, sensor->buffer, 6);
    *accel_x = ((int16_t)sensor->buffer[0] << 8) | sensor->buffer[1];
    *accel_y = ((int16_t)sensor->buffer[2] << 8) | sensor->buffer[3];
    *accel_z = ((int16_t)sensor->buffer[4] << 8) | sensor->buffer[5];
}

void ICM20608G_read_gyro_raw(ICM20608G_t *sensor, int16_t *gyro_x, int16_t *gyro_y, int16_t *gyro_z)
{
    // Read the raw gyroscope data
    ICM20608G_read_multiple_registers(sensor, ICM20608G_GYRO_XOUT_H, sensor->buffer, 6);
    *gyro_x = ((int16_t)sensor->buffer[0] << 8) | sensor->buffer[1];
    *gyro_y = ((int16_t)sensor->buffer[2] << 8) | sensor->buffer[3];
    *gyro_z = ((int16_t)sensor->buffer[4] << 8) | sensor->buffer[5];
}

void ICM20608G_convert_accel_raw_to_float(ICM20608G_t *sensor, int16_t accel_x_raw, int16_t accel_y_raw, int16_t accel_z_raw, float *accel_x, float *accel_y, float *accel_z)
{
    // Convert the raw accelerometer data to float values in g
    *accel_x = ((float)accel_x_raw) * sensor->accel_scale;
    *accel_y = ((float)accel_y_raw) * sensor->accel_scale;
    *accel_z = ((float)accel_z_raw) * sensor->accel_scale;
    // Apply the accelerometer offsets
    *accel_x += sensor->accel_offset[0];
    *accel_y += sensor->accel_offset[1];
    *accel_z += sensor->accel_offset[2];
}

void ICM20608G_convert_gyro_raw_to_float(ICM20608G_t *sensor, int16_t gyro_x_raw, int16_t gyro_y_raw, int16_t gyro_z_raw, float *gyro_x, float *gyro_y, float *gyro_z)
{
    // Convert the raw gyroscope data to float values in dps
    *gyro_x = ((float)gyro_x_raw) * sensor->gyro_scale;
    *gyro_y = ((float)gyro_y_raw) * sensor->gyro_scale;
    *gyro_z = ((float)gyro_z_raw) * sensor->gyro_scale;
    // Apply the gyroscope offsets
    *gyro_x += sensor->gyro_offset[0];
    *gyro_y += sensor->gyro_offset[1];
    *gyro_z += sensor->gyro_offset[2];
}

void ICM20608G_read_data_raw(ICM20608G_t *sensor, int16_t *accel_x, int16_t *accel_y, int16_t *accel_z, int16_t *gyro_x, int16_t *gyro_y, int16_t *gyro_z)
{
    // Read the raw accelerometer and gyroscope data
    ICM20608G_read_multiple_registers(sensor, ICM20608G_ACCEL_XOUT_H, sensor->buffer, 14);
    *accel_x = ((int16_t)sensor->buffer[0] << 8) | sensor->buffer[1];
    *accel_y = ((int16_t)sensor->buffer[2] << 8) | sensor->buffer[3];
    *accel_z = ((int16_t)sensor->buffer[4] << 8) | sensor->buffer[5];
    *gyro_x = ((int16_t)sensor->buffer[8] << 8) | sensor->buffer[9];
    *gyro_y = ((int16_t)sensor->buffer[10] << 8) | sensor->buffer[11];
    *gyro_z = ((int16_t)sensor->buffer[12] << 8) | sensor->buffer[13];
}

void ICM20608G_read_data(ICM20608G_t *sensor, float *accel_x, float *accel_y, float *accel_z, float *gyro_x, float *gyro_y, float *gyro_z)
{
    // Read the accelerometer and gyroscope data in g and dps
    int16_t accel_x_raw, accel_y_raw, accel_z_raw;
    int16_t gyro_x_raw, gyro_y_raw, gyro_z_raw;
    ICM20608G_read_data_raw(sensor, &accel_x_raw, &accel_y_raw, &accel_z_raw, &gyro_x_raw, &gyro_y_raw, &gyro_z_raw);
    ICM20608G_convert_accel_raw_to_float(sensor, accel_x_raw, accel_y_raw, accel_z_raw, accel_x, accel_y, accel_z);
    ICM20608G_convert_gyro_raw_to_float(sensor, gyro_x_raw, gyro_y_raw, gyro_z_raw, gyro_x, gyro_y, gyro_z);
}

void ICM20608G_read_acceleration(ICM20608G_t *sensor, float *accel_x, float *accel_y, float *accel_z)
{
    // Read the acceleration values in g
    int16_t accel_x_raw, accel_y_raw, accel_z_raw;
    ICM20608G_read_accel_raw(sensor, &accel_x_raw, &accel_y_raw, &accel_z_raw);
    ICM20608G_convert_accel_raw_to_float(sensor, accel_x_raw, accel_y_raw, accel_z_raw, accel_x, accel_y, accel_z);
}

void ICM20608G_read_rotation(ICM20608G_t *sensor, float *gyro_x, float *gyro_y, float *gyro_z)
{
    // Read the rotation values in dps
    int16_t gyro_x_raw, gyro_y_raw, gyro_z_raw;
    ICM20608G_read_gyro_raw(sensor, &gyro_x_raw, &gyro_y_raw, &gyro_z_raw);
    ICM20608G_convert_gyro_raw_to_float(sensor, gyro_x_raw, gyro_y_raw, gyro_z_raw, gyro_x, gyro_y, gyro_z);
}
