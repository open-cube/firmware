#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/i2c.h"
#include <hardware/gpio.h>
#include <opencube_hw.h>
#include "PCF8575.h"

// global pcf structure for interrupt read
typedef struct PCF8575
{
    i2c_inst_t *i2c_port;
    uint8_t i2c_addr;
    uint16_t last_read;
    uint16_t last_write;
    uint16_t last_read_missed;
    bool irq_enabled;
    bool initialized;
} PCF8575_t;

static PCF8575_t pcf;

void PCF8575_init()
{
    if (pcf.initialized)
        return;
    pcf.initialized = true;
    pcf.i2c_port = INTERNAL_I2C_BUS;
    pcf.i2c_addr = PCF8575_I2C_ADDR;
    pcf.last_write =
        (PCF8575_OUTPUT_LOW << MOTOR1_EN_RPIN) | // disable motors until needed
        (PCF8575_OUTPUT_LOW << MOTOR2_EN_RPIN) |
        (PCF8575_OUTPUT_LOW << MOTOR3_EN_RPIN) |
        (PCF8575_OUTPUT_LOW << MOTOR4_EN_RPIN) |
        (PCF8575_OUTPUT_LOW << MOTOR1_DIR_RPIN) | // set some default value
        (PCF8575_OUTPUT_LOW << MOTOR2_DIR_RPIN) |
        (PCF8575_OUTPUT_LOW << MOTOR3_DIR_RPIN) |
        (PCF8575_OUTPUT_LOW << MOTOR4_DIR_RPIN) |
        (PCF8575_INPUT << BUTTON_POWER_RPIN) | // enable brick buttons
        (PCF8575_INPUT << BUTTON_RIGHT_RPIN) |
        (PCF8575_INPUT << BUTTON_DOWN_RPIN) |
        (PCF8575_INPUT << BUTTON_OK_RPIN) |
        (PCF8575_INPUT << BUTTON_UP_RPIN) |
        (PCF8575_INPUT << BUTTON_LEFT_RPIN) |
        (PCF8575_OUTPUT_HIGH << USER_LED_RPIN) | // turn off user LED
        (PCF8575_OUTPUT_HIGH << I2C_STRONG_PULL_RPIN); // High for NXT UTZ communication
    PCF8575_write(); // among other things, configures pins as inputs
    PCF8575_read(); // reads back the real pin status
    PCF8575_read_value_missed(); // clears the missed bits
}

uint16_t PCF8575_read_value()
{
    return pcf.last_read;
}

uint16_t PCF8575_read_value_missed()
{
    pcf.last_read_missed = pcf.last_read_missed & pcf.last_read; 
    uint16_t missed = pcf.last_read_missed;
    pcf.last_read_missed = 0;
    pcf.last_read_missed = ~pcf.last_read_missed;
    return missed;
}

uint16_t PCF8575_read_function()
{
    return pcf.last_write;
}

void PCF8575_read(void)
{
    uint8_t message[2] = {};
    i2c_read_blocking(pcf.i2c_port, pcf.i2c_addr, message, sizeof(message), false);
    pcf.last_read = message[0] | (message[1] << 8u);
    pcf.last_read_missed = pcf.last_read_missed & pcf.last_read; 
}

void PCF8575_write(void)
{
    uint8_t message[2] = {};
    message[0] = pcf.last_write & 0xFFu;
    message[1] = pcf.last_write >> 8u;
    i2c_write_blocking(pcf.i2c_port, pcf.i2c_addr, message, sizeof(message), false);
}

bool PCF8575_read_pin(uint8_t pin)
{
    if (PCF8575_validate_pin(pin))
    {
        uint16_t pin_mask = 1u << pin;
        return (pcf.last_read & pin_mask) != 0;
    }
    return false;
}

void PCF8575_write_pin(uint8_t pin, bool value)
{
    if (PCF8575_validate_pin(pin))
    {
        uint16_t pin_mask = 1u << pin;
        if (value)
        {
            pcf.last_write |= pin_mask;
        }
        else
        {
            pcf.last_write &= ~pin_mask;
        }
        PCF8575_write();
    }
}

void PCF8575_toggle_pin(uint8_t pin)
{
    if (PCF8575_validate_pin(pin))
    {
        uint16_t pin_mask = 1u << pin;
        pcf.last_write ^= pin_mask;
        PCF8575_write();
    }
}

void PCF8575_write_pin_mask(uint16_t set_high, uint16_t set_low)
{
    uint16_t old_pins = pcf.last_write;
    uint16_t new_pins = (old_pins & (~set_low)) | set_high;
    pcf.last_write = new_pins;
    PCF8575_write();
}

bool PCF8575_validate_pin(uint8_t pin)
{
    return pin >= 0 && pin <= 15;
}
