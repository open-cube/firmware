/**
 * Module for capturing encoder interrupts & maintaining motor position based on them
 */
#ifndef OPENCUBE_ENCODER_H
#define OPENCUBE_ENCODER_H

#include <stdint.h>
#include <pico/critical_section.h>
#include <opencube_hw.h>


/**
 * Initialize necessary IRQs and shared data.
 */
extern void opencube_encoders_init(int port);

/**
 * Deinitialize motor port IRQs. This function has to be called
 * from the same core that opencube_encoders_init() was called on.
 */
extern void opencube_encoders_deinit(int port);

/**
 * Get the current position of a motor in the given motor port.
 * @return Position in encoder counts (NXT & EV3: these are directly equivalent to degrees).
 */
extern uint32_t opencube_encoders_get_position(opencube_motor_port port);

/**
 * Set the current position of a motor in the given motor port.
 * @param position Position in encoder counts (NXT & EV3: these are directly equivalent to degrees).
 */
extern void opencube_encoders_set_position(opencube_motor_port port, uint32_t position);

/**
 * Get the current speed of a motor in the given motor port.
 * @return Speed in encoder counts per second (NXT & EV3: these are directly equivalent to degrees per second).
 */
extern int32_t opencube_encoders_get_speed(opencube_motor_port port);

#endif //OPENCUBE_ENCODER_H
