#ifndef OPENCUBE_MOTOR_H
#define OPENCUBE_MOTOR_H

#include <stdint.h>
#include <opencube_hw.h>


#endif //OPENCUBE_MOTOR_H

// Initialize all motors variables with PWM, direction and enable pins
void opencube_motors_init(void);

// Initialize motor on given port, set motor PWM output settings and enable motor with default direction
// PWM frequency is 20 kHz with 3125 levels of duty cycle
void opencube_motor_init(uint8_t port);

// Initialize motor regulator default variables on given port
void opencube_motors_init_regulator_variables(uint8_t port);

// Enable motor on given port. Set motor enable pin to high.
void opencube_motor_enable(uint8_t port);

// Disable motor on given port. Set motor enable pin to low.
void opencube_motor_disable(uint8_t port);

// Deinitialize motor on given port. Stop and disable motor.
void opencube_motor_deinit(uint8_t port);

// Set motor direction on H-bridge for a motor on given port
void opencube_motor_set_direction(uint8_t port, bool direction);

// Set motor power for a motor on given port
void opencube_motor_set_power(uint8_t port, float power);

// Get motor encoder position for a motor on given port
uint32_t opencube_motor_get_position(uint8_t port);

// Get motor encoder estimated speed for a motor on given port
int32_t opencube_motor_get_speed(uint8_t port);

// Initialize motor encoders
void opencube_motor_encoder_init(void);

// Reset motor encoder position to 0
void opencube_motor_reset_encoder(uint8_t port);

// Deinitialize motor encoders
void opencube_motor_encoder_deinit(void);

// Initialize motor regulator on given port
void opencube_motor_regulator_init(uint8_t port);

// Set motor target position for a motor on given port
void opencube_motor_regulator_set_position(uint8_t port, float position);

// Set motor target speed for a motor on given port
void opencube_motor_regulator_set_speed(uint8_t port, float speed);

// Deinitialize motor regulator on given port
void opencube_motor_regulator_deinit(uint8_t port);

// Set motor position regulator PID parameters for a motor on given port
void opencube_motor_regulator_set_pos_pid(uint8_t port, float kp, float ki, float kd);

// Set motor speed regulator PID parameters for a motor on given port
void opencube_motor_regulator_set_speed_pid(uint8_t port, float kp, float ki, float kd);

// Get motor regulator error of regulator when position or speed regulator is running
float opencube_motor_regulator_get_error(uint8_t port);

// Get current motor power given by regulator when position or speed regulator is running
float opencube_motor_regulator_get_power(uint8_t port);

// Get current motor filtered speed when speed regulator is running
float opencube_motor_regulator_get_filt_speed(uint8_t port);
