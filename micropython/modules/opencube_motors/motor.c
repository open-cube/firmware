/**
 * Module for controlling motors. capturing motor encoder interrupts 
 * & maintaining motor position based on them using position or speed regulator
 */
#include <hardware/timer.h>
#include "hardware/pwm.h"
#include <pico/time.h>
#include "i2c_locking.h"
#include "pico/stdlib.h"
#include "motor.h"
#include "opencube_hw.h"
#include "PCF8575.h"
#include "encoder.h"

// Regulator constants
#define BASE_SAMPLING_PERIOD_US 10000
#define BASE_SAMPLING_PERIOD_S BASE_SAMPLING_PERIOD_US/1000000.0
#define MAX_POWER 100
#define MAX_DUTY_CYCLE 3125

// Motor PWM output frequency
#define PWM_FREQUENCY 20000

#define FORWARD false
#define BACKWARD true

#define POSITION_REGULATOR true
#define SPEED_REGULATOR false

// Struct for motor regulator on given port
typedef struct
{
  bool mode;
  float pos_P;
  float pos_I;
  float pos_D;
  float speed_P;
  float speed_I;
  float speed_D;
  float target_speed;
  float target_position;
  int32_t current_position;
  float prev_position;
  int32_t current_speed;
  float current_speed_filt;
  float prev_speed;
  float pos_prev_error;
  float speed_prev_error;
  float pos_integral;
  float speed_integral;
  float current_power;
  float regulator_error;
} motor_regulator;

// Struct for motor on given port
typedef struct
{
  bool enabled;
  bool regulator_enabled;
  bool direction;
  motor_regulator regulator;
  pwm_config config;
  uint16_t level;
  uint8_t pwm_pin;
  uint8_t dir_pin;
  uint8_t enable_pin;
} motor_port;

// Struct for motors on all ports
typedef struct
{
  motor_port ports[NUM_MOTOR_PORTS];
  alarm_pool_t *timer_pool;
  repeating_timer_t timer;
  bool enc_enabled;
  bool reg_enabled;
  bool initialized;
} motor_state;

static motor_state motors;

static bool motor_regulator_control(repeating_timer_t *timer);
float motor_position_regulator(uint8_t port);
float motor_speed_regulator(uint8_t port);
void opencube_motors_init_regulator_variables(uint8_t port);
static bool opencube_motor_set_power_internal(uint8_t port, float power);
static bool opencube_motor_try_set_power_from_irq(uint8_t port, float power);

void opencube_motors_init(void)
{
  if (motors.initialized)
  {
    return;
  }
  opencube_lock_i2c_or_raise();
  PCF8575_init();
  opencube_unlock_i2c();
  motors.reg_enabled = false;
  motors.enc_enabled = false;
  for (int i = 0; i < NUM_MOTOR_PORTS; i++)
  {
    opencube_motors_init_regulator_variables(i);
    motors.ports[i].enabled = false;
    motors.ports[i].direction = FORWARD;
    switch (i)
    {
    case OPENCUBE_MOTOR_1:
      motors.ports[i].pwm_pin = MOTOR1_PWM_PIN;
      motors.ports[i].dir_pin = MOTOR1_DIR_RPIN;
      motors.ports[i].enable_pin = MOTOR1_EN_RPIN;
      break;
    case OPENCUBE_MOTOR_2:
      motors.ports[i].pwm_pin = MOTOR2_PWM_PIN;
      motors.ports[i].dir_pin = MOTOR2_DIR_RPIN;
      motors.ports[i].enable_pin = MOTOR2_EN_RPIN;
      break;
    case OPENCUBE_MOTOR_3:
      motors.ports[i].pwm_pin = MOTOR3_PWM_PIN;
      motors.ports[i].dir_pin = MOTOR3_DIR_RPIN;
      motors.ports[i].enable_pin = MOTOR3_EN_RPIN;
      break;
    case OPENCUBE_MOTOR_4:
      motors.ports[i].pwm_pin = MOTOR4_PWM_PIN;
      motors.ports[i].dir_pin = MOTOR4_DIR_RPIN;
      motors.ports[i].enable_pin = MOTOR4_EN_RPIN;
      break;

    default:
      break;
    }
  }
  motors.initialized = true;
}

void opencube_motor_init(uint8_t port)
{
  opencube_motors_init();
  opencube_motors_init_regulator_variables(port);
  uint8_t slice_num = MOTOR34_PWM_SLICE;
  if (port == MOTOR1_PWM_PIN || port == MOTOR2_PWM_PIN)
  {
    slice_num = MOTOR12_PWM_SLICE;
  }
  motors.ports[port].enabled = true;

  gpio_set_function(motors.ports[port].pwm_pin, GPIO_FUNC_PWM);
  
  pwm_config c = pwm_get_default_config();
  pwm_config_set_phase_correct(&c, true);
  pwm_config_set_clkdiv_int(&c, 1);
  pwm_config_set_clkdiv_mode(&c, PWM_DIV_FREE_RUNNING);
  pwm_config_set_output_polarity(&c, true, true);
  pwm_config_set_wrap(&c, MAX_DUTY_CYCLE - 1);

  motors.ports[port].config = c;
  pwm_init(slice_num, &motors.ports[port].config, true);

  opencube_motor_set_power_internal(port, 0.0);
  opencube_lock_i2c_or_raise();
  opencube_motor_enable(port);
  opencube_motor_set_direction(port, motors.ports[port].direction);
  opencube_unlock_i2c();
  
}

void opencube_motor_deinit(uint8_t port)
{
  opencube_motor_regulator_deinit(port);
  opencube_motor_set_power(port, 0.0);
  opencube_lock_i2c_or_raise();
  opencube_motor_disable(port);
  opencube_unlock_i2c();
  motors.ports[port].enabled = false;
  for (int i = 0; i < NUM_MOTOR_PORTS; i++)
  {
    if (motors.ports[i].enabled)
    {
      return;
    }
  }
  motors.initialized = false;
}

void opencube_motors_init_regulator_variables(uint8_t port)
{
  motors.ports[port].regulator_enabled = false;
  motors.ports[port].regulator.pos_P = 0.9;
  motors.ports[port].regulator.pos_I = 0.1;
  motors.ports[port].regulator.pos_D = 0;
  motors.ports[port].regulator.speed_P = 0.1;
  motors.ports[port].regulator.speed_I = 0.8;
  motors.ports[port].regulator.speed_D = 0;
  motors.ports[port].regulator.target_speed = 0;
  motors.ports[port].regulator.target_position = 0;
  motors.ports[port].regulator.current_position = 0;
  motors.ports[port].regulator.current_speed = 0;
  motors.ports[port].regulator.current_speed_filt = 0;
  motors.ports[port].regulator.prev_speed = 0;
  motors.ports[port].regulator.current_power = 0;
  motors.ports[port].regulator.pos_prev_error = 0;
  motors.ports[port].regulator.speed_prev_error = 0;
  motors.ports[port].regulator.pos_integral = 0;
  motors.ports[port].regulator.speed_integral = 0;
  motors.ports[port].regulator.regulator_error = 0;
}

void opencube_motor_enable(uint8_t port)
{
    PCF8575_write_pin(motors.ports[port].enable_pin, true);
}

void opencube_motor_disable(uint8_t port)
{
	PCF8575_write_pin(motors.ports[port].enable_pin, false);
}

void opencube_motor_set_direction(uint8_t port, bool direction)
{
  motors.ports[port].direction = direction;
  PCF8575_write_pin(motors.ports[port].dir_pin, direction);
}

bool opencube_motor_set_power_internal(uint8_t port, float power)
{
  motors.ports[port].regulator.current_power = power;
  power = power * MAX_DUTY_CYCLE/MAX_POWER;
  uint16_t level = power >= 0 ? +power : -power;

  bool was_forward = motors.ports[port].direction == FORWARD;
  bool want_forward = power > 0;

  if (want_forward != was_forward)
  {
    if (!opencube_try_lock_i2c())
    {
      return false;
    }
    opencube_motor_set_direction(port, !want_forward);
    opencube_unlock_i2c();
  }

  motors.ports[port].level = level;
  pwm_set_gpio_level(motors.ports[port].pwm_pin, level);
  return true;
}

bool opencube_motor_try_set_power_from_irq(uint8_t port, float power)
{
  // this may fail if the user's code is doing something with I2C
  // (refreshing the display, ...)
  return opencube_motor_set_power_internal(port, power);
}

void opencube_motor_set_power(uint8_t port, float power)
{
  if (!opencube_motor_set_power_internal(port, power))
  {
    raise_i2c_locking_error();
  }
}

uint32_t opencube_motor_get_position(uint8_t port)
{
  return opencube_encoders_get_position(port);
}

void opencube_motor_reset_encoder(uint8_t port)
{
  opencube_encoders_set_position(port, 0);
}

int32_t opencube_motor_get_speed(uint8_t port)
{
  return opencube_encoders_get_speed(port);
}

void opencube_motor_encoder_init(void)
{
  if (motors.enc_enabled)
  {
    return;
  }
  motors.enc_enabled = true;
  for (int i = 0; i < NUM_MOTOR_PORTS; i++)
  {
    opencube_encoders_init(i);
  }
}

void opencube_motor_encoder_deinit(void)
{
  if (!motors.enc_enabled)
  {
    return;
  }
  motors.enc_enabled = false;
  for (int i = 0; i < NUM_MOTOR_PORTS; i++)
  {
    opencube_encoders_deinit(i);
  }
}

void opencube_motor_regulator_init(uint8_t port)
{
  if (motors.ports[port].regulator_enabled)
  {
    return;
  }
  motors.ports[port].regulator_enabled = true;
  if (motors.reg_enabled)
  {
    return;
  }
  motors.reg_enabled = true;
  opencube_motor_encoder_init();
  // init timer
  motors.timer_pool = alarm_pool_get_default();
  alarm_pool_add_repeating_timer_us(
      motors.timer_pool,
      -BASE_SAMPLING_PERIOD_US,
      &motor_regulator_control,
      NULL,
      &motors.timer);
}

void opencube_motor_regulator_deinit(uint8_t port)
{
  if (!motors.ports[port].regulator_enabled)
  {
    return;
  }
  motors.ports[port].regulator_enabled = false;
  opencube_motors_init_regulator_variables(port);
  for (int i = 0; i < NUM_MOTOR_PORTS; i++)
  {
    if (motors.ports[i].regulator_enabled)
    {
      return;
    }
  }
  alarm_pool_cancel_alarm(motors.timer_pool, motors.timer.alarm_id);
  motors.reg_enabled = false;
}

void opencube_motor_regulator_set_position(uint8_t port, float position)
{
  motors.ports[port].regulator.mode = POSITION_REGULATOR;
  motors.ports[port].regulator.target_position = position;
  motors.ports[port].regulator.pos_integral = 0;
  motors.ports[port].regulator.pos_prev_error = 0;
  motors.ports[port].regulator.current_position = opencube_encoders_get_position(port);
  float error = (motors.ports[port].regulator.target_position - motors.ports[port].regulator.current_position);
  motors.ports[port].regulator.regulator_error = error;
}

void opencube_motor_regulator_set_speed(uint8_t port, float speed)
{
  motors.ports[port].regulator.mode = SPEED_REGULATOR;
  motors.ports[port].regulator.target_speed = speed;
  float error = (motors.ports[port].regulator.target_speed - opencube_encoders_get_speed(port));
  motors.ports[port].regulator.regulator_error = error;
}

void opencube_motor_regulator_set_pos_pid(uint8_t port, float kp, float ki, float kd) 
{
  motors.ports[port].regulator.pos_P = kp;
  motors.ports[port].regulator.pos_I = ki;
  motors.ports[port].regulator.pos_D = kd;
}

void opencube_motor_regulator_set_speed_pid(uint8_t port, float kp, float ki, float kd) 
{
  motors.ports[port].regulator.speed_P = kp;
  motors.ports[port].regulator.speed_I = ki;
  motors.ports[port].regulator.speed_D = kd;
}

float opencube_motor_regulator_get_error(uint8_t port)
{
  return motors.ports[port].regulator.regulator_error;
}

float opencube_motor_regulator_get_power(uint8_t port)
{
  return motors.ports[port].regulator.current_power;
}

float opencube_motor_regulator_get_filt_speed(uint8_t port)
{
    return motors.ports[port].regulator.current_speed_filt;
}

// PID motor position regulator
float motor_position_regulator(uint8_t port)
{
  float error = (motors.ports[port].regulator.target_position - motors.ports[port].regulator.current_position);
  float power = (motors.ports[port].regulator.pos_P * error + motors.ports[port].regulator.pos_I * motors.ports[port].regulator.pos_integral * BASE_SAMPLING_PERIOD_S + motors.ports[port].regulator.pos_D * (error - motors.ports[port].regulator.pos_prev_error) / BASE_SAMPLING_PERIOD_S);

  motors.ports[port].regulator.pos_prev_error = error;
  bool int_ok = true;
  if (power > MAX_POWER)
  {
    if (error > 0)
    {
      int_ok = false;
    }
    power = MAX_POWER;
  }
  else if (power < -MAX_POWER)
  {
    if (error < 0)
    {
      int_ok = false;
    }
    power = -MAX_POWER;
  }
  if (int_ok)
  {
    motors.ports[port].regulator.pos_integral += error;
  }

  motors.ports[port].regulator.regulator_error = error;
  
  return power;
}

// PID motor speed regulator
float motor_speed_regulator(uint8_t port)
{
  float error = (motors.ports[port].regulator.target_speed - motors.ports[port].regulator.current_speed_filt);
  float power = (motors.ports[port].regulator.speed_P * error + motors.ports[port].regulator.speed_I * motors.ports[port].regulator.speed_integral * BASE_SAMPLING_PERIOD_S + motors.ports[port].regulator.speed_D * (error - motors.ports[port].regulator.speed_prev_error) / BASE_SAMPLING_PERIOD_S);

  motors.ports[port].regulator.speed_prev_error = error;
  bool int_ok = true;
  if (power > MAX_POWER)
  {
    if (error > 0)
    {
      int_ok = false;
    }
    power = MAX_POWER;
  }
  else if (power < -MAX_POWER)
  {
    if (error < 0)
    {
      int_ok = false;
    }
    power = -MAX_POWER;
  }
  if (int_ok)
  {
    motors.ports[port].regulator.speed_integral += error;
  }
  motors.ports[port].regulator.regulator_error = error;
  return power;
}

// Motor regulator timer callback
// This function is called with period of 0.01s 
// It is used to set motor powor to motors with either position or speed regulator on ports with regulation enabled
bool motor_regulator_control(repeating_timer_t *timer)
{
  for (int i = 0; i < NUM_MOTOR_PORTS; i++)
  {
    if (!motors.ports[i].regulator_enabled)
    {
      continue;
    }
    motors.ports[i].regulator.current_position = opencube_encoders_get_position(i);
    motors.ports[i].regulator.current_speed = opencube_encoders_get_speed(i);
    motors.ports[i].regulator.current_speed_filt = 0.854*motors.ports[i].regulator.current_speed_filt + 0.0728*motors.ports[i].regulator.current_speed + 0.0728*motors.ports[i].regulator.prev_speed;
    
    float power;
    if (motors.ports[i].regulator.mode == POSITION_REGULATOR)
    {
      power = motor_position_regulator(i);
    } else {
      power = motor_speed_regulator(i);
    }   

    motors.ports[i].regulator.prev_speed = motors.ports[i].regulator.current_speed;
    motors.ports[i].regulator.prev_position = motors.ports[i].regulator.current_position;
    // this may fail:
    opencube_motor_try_set_power_from_irq(i, power);
    // ignore the failure for now. Next regulation iteration should succeed.
  }
  return true;
}
