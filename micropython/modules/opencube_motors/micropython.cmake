# Create an INTERFACE library for our C module.
add_library(usermod_opencube_motors INTERFACE)

# Add our source files to the lib
target_sources(usermod_opencube_motors INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}/encoder.h
    ${CMAKE_CURRENT_LIST_DIR}/encoder.c
    ${CMAKE_CURRENT_LIST_DIR}/encoder_api.c
    ${CMAKE_CURRENT_LIST_DIR}/encoder_api.h
    ${CMAKE_CURRENT_LIST_DIR}/motor.c
    ${CMAKE_CURRENT_LIST_DIR}/motor.h
    ${CMAKE_CURRENT_LIST_DIR}/motor_api.c
    ${CMAKE_CURRENT_LIST_DIR}/motor_api.h
    ${CMAKE_CURRENT_LIST_DIR}/motors_api.c
)

# Add the current directory as an include directory.
target_include_directories(usermod_opencube_motors INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}
)

target_link_libraries(usermod_opencube_motors INTERFACE
    usermod_opencube_pindefs
    usermod_opencube_lib
)

# Link our INTERFACE library to the usermod target.
target_link_libraries(usermod INTERFACE usermod_opencube_motors)
