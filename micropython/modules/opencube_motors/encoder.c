/**
 * Module for capturing encoder interrupts & maintaining motor position based on them
 */
#include <hardware/gpio.h>
#include <hardware/timer.h>
#include <pico/time.h>
#include <hardware/sync.h>
#include <pico/mutex.h>
#include "encoder.h"
#include "opencube_hw.h"

// speed estimation is based on
// "An Embedded System for Position and Speed Measurement Adopting Incremental Encoders"
#define SPEED_SAMPLING_PERIOD_US 10000
#define MAXIMUM_SPEED_AGE_US (SPEED_SAMPLING_PERIOD_US*2)

typedef struct {
  // updated by GPIO IRQ
  volatile uint32_t position; // current encoder position
  volatile uint32_t timestamp; // timestamp of last pin5 transition
  // updated by periodic timer
  volatile int32_t speed; // estimated speed in degrees per second
  uint32_t position_at_last_speed_msr; // value of "position" when speed was last estimated
  uint32_t timestamp_at_last_speed_msr; // value of "timestamp" when speed was last estimated
  uint32_t speed_age_usec_at_last_irq; // How old is the last measurement
  bool motor_is_moving; // Whether speed can be calculated (previous tacho pulse arrived < cca. 20ms ago)
  int refcount; // How many objects use this port
} encoder_port;

typedef struct {
  encoder_port ports[NUM_MOTOR_PORTS];
  alarm_pool_t *timer_pool;
  repeating_timer_t timer;
  bool enabled;
} encoder_state;

static encoder_state encoders;
auto_init_mutex(encoders_lock);

static void catchall_gpio_irq_handler(void);
static void motor_interrupt_edge_handler(encoder_port *enc, uint int_pin, uint dir_pin);
static void gpio_set_irq_enabled_masked(uint gpio_mask, uint32_t events, bool enabled);
static bool estimate_all_speeds(repeating_timer_t *timer);
static void estimate_port_speed(encoder_port *enc);
static bool encoders_can_be_freed(void);

#define MOTOR_INTERRUPT_PINS ( \
  (1 << MOTOR1_ENC_A_PIN) | \
  (1 << MOTOR2_ENC_A_PIN) | \
  (1 << MOTOR3_ENC_A_PIN) | \
  (1 << MOTOR4_ENC_A_PIN))

#define MOTOR_DIRECTION_PINS ( \
  (1 << MOTOR1_ENC_B_PIN) | \
  (1 << MOTOR2_ENC_B_PIN) | \
  (1 << MOTOR3_ENC_B_PIN) | \
  (1 << MOTOR4_ENC_B_PIN))

void opencube_encoders_init(int port) {
  mutex_enter_blocking(&encoders_lock);
  encoders.ports[port].refcount++;
  if (!encoders.enabled) {
    encoders.enabled = true;
    for (int i = 0; i < NUM_MOTOR_PORTS; i++) {
      encoders.ports[i].position = 0;
      encoders.ports[i].timestamp = time_us_32();
      encoders.ports[i].speed = 0;
      encoders.ports[i].position_at_last_speed_msr = 0;
      encoders.ports[i].timestamp_at_last_speed_msr = 0;
      encoders.ports[i].speed_age_usec_at_last_irq = 0;
      encoders.ports[i].motor_is_moving = false;
    }
    // encoder interrupts
    gpio_init_mask(MOTOR_INTERRUPT_PINS | MOTOR_DIRECTION_PINS);
    gpio_set_irq_enabled_masked(MOTOR_INTERRUPT_PINS, GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true);
    gpio_add_raw_irq_handler_with_order_priority_masked(
        MOTOR_INTERRUPT_PINS,
        &catchall_gpio_irq_handler,
        PICO_SHARED_IRQ_HANDLER_HIGHEST_ORDER_PRIORITY
    );
    irq_set_enabled(IO_IRQ_BANK0, true);
    irq_set_priority(IO_IRQ_BANK0, 0xFF); // max priority

    // periodic speed estimation
    encoders.timer_pool = alarm_pool_get_default();
    alarm_pool_add_repeating_timer_us(
        encoders.timer_pool,
        SPEED_SAMPLING_PERIOD_US,
        &estimate_all_speeds,
        NULL,
        &encoders.timer
    );
  }
  mutex_exit(&encoders_lock);
}

void opencube_encoders_deinit(int port) {
  mutex_enter_blocking(&encoders_lock);
  encoders.ports[port].refcount--;
  if (encoders.enabled && encoders_can_be_freed()) {
    gpio_set_irq_enabled_masked(MOTOR_INTERRUPT_PINS, GPIO_IRQ_EDGE_RISE, false);
    gpio_remove_raw_irq_handler_masked(MOTOR_INTERRUPT_PINS, &catchall_gpio_irq_handler);
    alarm_pool_cancel_alarm(encoders.timer_pool, encoders.timer.alarm_id);
    encoders.enabled = false;
  }
  mutex_exit(&encoders_lock);
}

bool encoders_can_be_freed(void) {
  for (int mport = 0; mport < NUM_MOTOR_PORTS; mport++) {
    if (encoders.ports[mport].refcount > 0) {
      return false;
    }
  }
  return true;
}

uint32_t opencube_encoders_get_position(opencube_motor_port port) {
  // assume that word access is atomic
  return encoders.ports[port].position;
}

void opencube_encoders_set_position(opencube_motor_port port, uint32_t position) {
  // assume that word access is atomic
  encoders.ports[port].position = position;
}

int32_t opencube_encoders_get_speed(opencube_motor_port port) {
  // assume that word access is atomic
  return encoders.ports[port].speed;
}

// function copied from Pico SDK (gpio_init_mask) and adapted for enabling IRQs
void gpio_set_irq_enabled_masked(uint gpio_mask, uint32_t events, bool enabled) {
  for (uint i = 0; i < NUM_BANK0_GPIOS; i++) {
    if (gpio_mask & 1) {
      gpio_set_irq_enabled(i, events, enabled);
    }
    gpio_mask >>= 1;
  }
}

void catchall_gpio_irq_handler(void) {
  motor_interrupt_edge_handler(&encoders.ports[OPENCUBE_MOTOR_1], MOTOR1_ENC_A_PIN, MOTOR1_ENC_B_PIN);
  motor_interrupt_edge_handler(&encoders.ports[OPENCUBE_MOTOR_2], MOTOR2_ENC_A_PIN, MOTOR2_ENC_B_PIN);
  motor_interrupt_edge_handler(&encoders.ports[OPENCUBE_MOTOR_3], MOTOR3_ENC_A_PIN, MOTOR3_ENC_B_PIN);
  motor_interrupt_edge_handler(&encoders.ports[OPENCUBE_MOTOR_4], MOTOR4_ENC_A_PIN, MOTOR4_ENC_B_PIN);
}

void motor_interrupt_edge_handler(encoder_port *enc, uint int_pin, uint dir_pin) {
  uint edge_type = gpio_get_irq_event_mask(int_pin);
  if (edge_type != 0) {
    gpio_acknowledge_irq(int_pin, edge_type);
    enc->timestamp = time_us_32(); // poor man's input capture

    if (gpio_get(dir_pin)) {
      if (edge_type & GPIO_IRQ_EDGE_RISE) {
        enc->position--;
      } else {
        enc->position++;
      }
    } else {
      if (edge_type & GPIO_IRQ_EDGE_RISE) {
        enc->position++;
      } else {
        enc->position--;
      }
    }
  }
}

bool estimate_all_speeds(repeating_timer_t *timer) {
  (void) timer;

  for (int i = 0; i < NUM_MOTOR_PORTS; i++) {
    estimate_port_speed(&encoders.ports[i]);
  }

  return true;
}

void estimate_port_speed(encoder_port *enc) {
  // SYNCHRONIZATION: IRQ for encoder has higher priority => disable interrupts
  // IRQ *should* run on the same core as both the alarm and IRQ are set up at the same time.
  // => disabling interrupts should be enough, a multicore spinlock is not needed
  uint32_t irq = save_and_disable_interrupts();
  uint32_t new_position = enc->position;
  uint32_t new_timestamp = enc->timestamp;
  restore_interrupts(irq);

  // update age to match the present moment
  enc->speed_age_usec_at_last_irq += SPEED_SAMPLING_PERIOD_US;

  // check if a new encoder pulse arrived
  int32_t pulses = (int32_t) (new_position - enc->position_at_last_speed_msr);

  if (pulses != 0) {
    // new edge arrived

    if (enc->motor_is_moving) {
      // we have some reasonably-recent edge from a previous interrupt
      // => new speed estimate can be produced
      int32_t usec_between_edges = (int32_t) (new_timestamp - enc->timestamp_at_last_speed_msr);
      int32_t speed = 1000000 * pulses / usec_between_edges;

      enc->position_at_last_speed_msr = new_position;
      enc->timestamp_at_last_speed_msr = new_timestamp;
      enc->speed_age_usec_at_last_irq = 0;
      enc->speed = speed;

    } else {
      // no recent edges from last interrupts
      // => cannot yet estimate speed, wait for next pulse
      enc->motor_is_moving = true;
      enc->position_at_last_speed_msr = new_position;
      enc->timestamp_at_last_speed_msr = new_timestamp;
      enc->speed_age_usec_at_last_irq = 0;
      enc->speed = 0;
    }
  } else {
    // no new edge arrived

    if (enc->motor_is_moving) {
      // we still might be able to measure speed later - check if we can wait for the pulse
      if (enc->speed_age_usec_at_last_irq >= MAXIMUM_SPEED_AGE_US) {
        // motor is too slow, disable speed measurement
        enc->motor_is_moving = false;
        enc->speed_age_usec_at_last_irq = 0;
        enc->speed = 0;
      } else {
        // keep the speed as-is, it might be updated in following interrupts
      }
    } else {
      // motor is stopped and no new edges are arriving => cannot measure speed
      enc->speed_age_usec_at_last_irq = 0;
      enc->speed = 0;
    }
  }
}
