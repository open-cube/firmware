#ifndef OPENCUBE_MPY_ENCODER_API_H
#define OPENCUBE_MPY_ENCODER_API_H

#include <py/runtime.h>

extern const mp_obj_type_t opencube_encoder_type;

#endif //OPENCUBE_MPY_ENCODER_API_H
