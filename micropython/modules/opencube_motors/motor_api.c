// MicroPython API for the Motor
#include <py/runtime.h>
#include "motor_api.h"
#include "motor.h"

// This structure represents Motor instance objects.
typedef struct {
    mp_obj_base_t base;
    uint port;
} motor_obj_t;

// Called on Motor init
// Initialize Motor on given port
static mp_obj_t motor_make_new(const mp_obj_type_t *type, size_t n_args, size_t n_kw, const mp_obj_t *args) {
    mp_arg_check_num(n_args, n_kw, 1, 2, false);

    int port = mp_obj_get_int(args[0]);
    if (port < 0 || port > 3) {
        mp_raise_msg_varg(&mp_type_ValueError, "Port %d is not a valid motor port", port+1);
    }

    motor_obj_t *self = m_new_obj(motor_obj_t);
    self->base.type = type;
    self->port = port;
    opencube_motor_init(self->port);
    return MP_OBJ_FROM_PTR(self);
}

static void motor_print(const mp_print_t *print, mp_obj_t self_in, mp_print_kind_t kind) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    mp_printf(print, "Motor(%d)", self->port+1);
}

// Set motor power (-100 to 100) directly 
static mp_obj_t motor_set_power(mp_obj_t self_in, mp_obj_t power_in) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    float power = mp_obj_get_float(power_in);
    power = power > 100 ? 100 : power;
    power = power < -100 ? -100 : power;
    opencube_motor_set_power(self->port, power);
    return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_2(motor_set_power_obj, motor_set_power);

// Initialize motor encoder on all ports
static mp_obj_t motor_init_encoder(mp_obj_t self_in) {
    opencube_motor_encoder_init();
    return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(motor_init_encoder_obj, motor_init_encoder);

// Reset motor encoder position to 0
static mp_obj_t motor_reset_encoder(mp_obj_t self_in) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    opencube_motor_reset_encoder(self->port);
    return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(motor_reset_encoder_obj, motor_reset_encoder);

// Deinitialize motor encoder on all ports
static mp_obj_t motor_deinit_encoder(mp_obj_t self_in) {
    opencube_motor_encoder_deinit();
    return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(motor_deinit_encoder_obj, motor_deinit_encoder);

// Get motor encoder position in degrees
static mp_obj_t motor_position(mp_obj_t self_in) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    return mp_obj_new_int(opencube_motor_get_position(self->port));
}
static MP_DEFINE_CONST_FUN_OBJ_1(motor_position_obj, motor_position);

// Get motor encoder speed in degrees per second
static mp_obj_t motor_speed(mp_obj_t self_in) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    return mp_obj_new_int(opencube_motor_get_speed(self->port));
}
static MP_DEFINE_CONST_FUN_OBJ_1(motor_speed_obj, motor_speed);

// Initialize motor regulator
static mp_obj_t motor_init_regulator(mp_obj_t self_in) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    opencube_motor_regulator_init(self->port);
    return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(motor_init_regulator_obj, motor_init_regulator);

// Deinitialize motor regulator
static mp_obj_t motor_deinit_regulator(mp_obj_t self_in) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    opencube_motor_regulator_deinit(self->port);
    return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(motor_deinit_regulator_obj, motor_deinit_regulator);

// Set motor regulator to regulation of position with given target position in degrees
static mp_obj_t motor_set_regulator_position(mp_obj_t self_in, mp_obj_t position_in) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    int32_t position = mp_obj_get_float(position_in);
    opencube_motor_regulator_set_position(self->port, position);
    return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_2(motor_set_regulator_position_obj, motor_set_regulator_position);

// Set motor regulator to regulation of speed with given target speed in degrees per second
static mp_obj_t motor_set_regulator_speed(mp_obj_t self_in, mp_obj_t speed_in) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    int32_t speed = mp_obj_get_float(speed_in);
    opencube_motor_regulator_set_speed(self->port, speed);
    return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_2(motor_set_regulator_speed_obj, motor_set_regulator_speed);

// Set motor positon regulator PID constants
static mp_obj_t motor_regulator_pos_set_consts(size_t n_args, const mp_obj_t *args) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(args[0]);
    float kp = mp_obj_get_float(args[1]);
    float ki = mp_obj_get_float(args[2]);
    float kd = mp_obj_get_float(args[3]);
    opencube_motor_regulator_set_pos_pid(self->port, kp, ki, kd);
    return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(motor_regulator_pos_set_consts_obj,4,4, motor_regulator_pos_set_consts);

// Set motor speed regulator PID constants
static mp_obj_t motor_regulator_speed_set_consts(size_t n_args, const mp_obj_t *args) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(args[0]);
    float kp = mp_obj_get_float(args[1]);
    float ki = mp_obj_get_float(args[2]);
    float kd = mp_obj_get_float(args[3]);
    opencube_motor_regulator_set_speed_pid(self->port, kp, ki, kd);
    return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(motor_regulator_speed_set_consts_obj, 4,4, motor_regulator_speed_set_consts);

// Get motor regulator error in degrees in regulation of position or degrees per second in regulation of speed
static mp_obj_t motor_regulator_error(mp_obj_t self_in) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    return mp_obj_new_float(opencube_motor_regulator_get_error(self->port));
}
static MP_DEFINE_CONST_FUN_OBJ_1(motor_regulator_error_obj, motor_regulator_error);

// Get current motor power given by regulator (-100 to 100)
static mp_obj_t motor_regulator_power(mp_obj_t self_in) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    return mp_obj_new_float(opencube_motor_regulator_get_power(self->port));
}
static MP_DEFINE_CONST_FUN_OBJ_1(motor_regulator_power_obj, motor_regulator_power);

// Get current motor filtered speed when speed regulator is running
static mp_obj_t motor_regulator_speed(mp_obj_t self_in) {
    motor_obj_t* self = MP_OBJ_FROM_PTR(self_in);
    return mp_obj_new_float(opencube_motor_regulator_get_filt_speed(self->port));
}
static MP_DEFINE_CONST_FUN_OBJ_1(motor_regulator_speed_obj, motor_regulator_speed);

// Deinitialize motor
static mp_obj_t motor_deinit(mp_obj_t self_in) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    opencube_motor_deinit(self->port);
    return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(motor_deinit_obj, motor_deinit);

// Finalizer for Motor
static mp_obj_t motor_finalizer(mp_obj_t self_in) {
    motor_obj_t *self = MP_OBJ_FROM_PTR(self_in);
    opencube_motor_deinit(self->port);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(motor_finalizer_obj, motor_finalizer);

// This collects all methods and other static class attributes of the Motor.
static const mp_rom_map_elem_t motor_locals_dict[] = {
    { MP_ROM_QSTR(MP_QSTR_set_power), MP_ROM_PTR(&motor_set_power_obj) },
    { MP_ROM_QSTR(MP_QSTR_deinit), MP_ROM_PTR(&motor_deinit_obj) },
    { MP_ROM_QSTR(MP_QSTR_init_encoder), MP_ROM_PTR(&motor_init_encoder_obj) },
    { MP_ROM_QSTR(MP_QSTR_deinit_encoder), MP_ROM_PTR(&motor_deinit_encoder_obj) },
    { MP_ROM_QSTR(MP_QSTR_reset_encoder), MP_ROM_PTR(&motor_reset_encoder_obj) },
    { MP_ROM_QSTR(MP_QSTR_position), MP_ROM_PTR(&motor_position_obj) },
    { MP_ROM_QSTR(MP_QSTR_speed), MP_ROM_PTR(&motor_speed_obj) },
    { MP_ROM_QSTR(MP_QSTR_set_regulator_position), MP_ROM_PTR(&motor_set_regulator_position_obj) },
    { MP_ROM_QSTR(MP_QSTR_set_regulator_speed), MP_ROM_PTR(&motor_set_regulator_speed_obj) },
    { MP_ROM_QSTR(MP_QSTR_regulator_pos_set_consts), MP_ROM_PTR(&motor_regulator_pos_set_consts_obj) },
    { MP_ROM_QSTR(MP_QSTR_regulator_speed_set_consts), MP_ROM_PTR(&motor_regulator_speed_set_consts_obj) },
    { MP_ROM_QSTR(MP_QSTR_regulator_error), MP_ROM_PTR(&motor_regulator_error_obj) },
    { MP_ROM_QSTR(MP_QSTR_regulator_power), MP_ROM_PTR(&motor_regulator_power_obj) },
    { MP_ROM_QSTR(MP_QSTR_regulator_speed), MP_ROM_PTR(&motor_regulator_speed_obj) },
    { MP_ROM_QSTR(MP_QSTR_init_regulator), MP_ROM_PTR(&motor_init_regulator_obj) },
    { MP_ROM_QSTR(MP_QSTR_deinit_regulator), MP_ROM_PTR(&motor_deinit_regulator_obj) },
    { MP_ROM_QSTR(MP_QSTR_close), MP_ROM_PTR(&motor_finalizer_obj) },
    { MP_ROM_QSTR(MP_QSTR___del__),MP_ROM_PTR(&motor_finalizer_obj) },
};
static MP_DEFINE_CONST_DICT(motor_locals_dict_obj, motor_locals_dict);

// This defines the type(Motor) object.
MP_DEFINE_CONST_OBJ_TYPE(
    opencube_motor_type,
    MP_QSTR_Motor,
    MP_TYPE_FLAG_NONE,
    make_new, &motor_make_new,
    locals_dict, &motor_locals_dict_obj,
    print, &motor_print
);
