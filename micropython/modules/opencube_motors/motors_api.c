/* Collect Encoder and Motor types and put them to MicroPython motors_ll module*/
#include <py/runtime.h>
#include "encoder_api.h"
#include "motor_api.h"

static const mp_rom_map_elem_t motor_globals_dict[] = {
    {MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_motors_ll)},
    {MP_ROM_QSTR(MP_QSTR_Encoder), MP_ROM_PTR(&opencube_encoder_type)},
    {MP_ROM_QSTR(MP_QSTR_Motor), MP_ROM_PTR(&opencube_motor_type)},
};
MP_DEFINE_CONST_DICT(motor_globals_dict_obj, motor_globals_dict);

const mp_obj_module_t motors_module = {
    .base = {.type = &mp_type_module},
    .globals = (mp_obj_dict_t *) &motor_globals_dict_obj,
};

MP_REGISTER_MODULE(MP_QSTR_motors_ll, motors_module);
