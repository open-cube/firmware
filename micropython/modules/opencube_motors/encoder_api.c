#include <py/runtime.h>
#include "encoder_api.h"
#include "encoder.h"

typedef struct {
  mp_obj_base_t base;
  uint port;
  uint32_t ref_position;
  bool reverse;
} encoder_obj_t;

static mp_obj_t encoder_make_new(const mp_obj_type_t *type, size_t n_args, size_t n_kw, const mp_obj_t *args) {
  mp_arg_check_num(n_args, n_kw, 1, 2, false);

  int port = mp_obj_get_int(args[0]);
  if (port < 0 || port > 3) {
    mp_raise_msg_varg(&mp_type_ValueError, "Port %d is not a valid motor port", port+1);
  }

  opencube_encoders_init(port);
  encoder_obj_t *self = m_new_obj_with_finaliser(encoder_obj_t);
  self->base.type = type;
  self->port = port;
  self->ref_position = opencube_encoders_get_position(self->port);
  if (n_args > 1) {
    self->reverse = mp_obj_is_true(args[1]);
  } else {
    self->reverse = false;
  }

  return MP_OBJ_FROM_PTR(self);
}

static void encoder_print(const mp_print_t *print, mp_obj_t self_in, mp_print_kind_t kind) {
  encoder_obj_t *self = MP_OBJ_FROM_PTR(self_in);
  mp_printf(print, "Encoder(%d)", self->port+1);
}

static mp_obj_t encoder_get_position(mp_obj_t self_in) {
  encoder_obj_t *self = MP_OBJ_FROM_PTR(self_in);
  uint32_t abs_pos = opencube_encoders_get_position(self->port);
  uint32_t abs_ref = self->ref_position;
  int32_t rel_pos = (int32_t) (abs_pos - abs_ref);
  if (self->reverse) {
    rel_pos = -rel_pos;
  }
  return mp_obj_new_int(rel_pos);
}

static MP_DEFINE_CONST_FUN_OBJ_1(encoder_get_position_obj, encoder_get_position);

static mp_obj_t encoder_set_position(mp_obj_t self_in, mp_obj_t position_in) {
  encoder_obj_t *self = MP_OBJ_FROM_PTR(self_in);
  uint32_t abs_pos = opencube_encoders_get_position(self->port);
  int32_t rel_pos = mp_obj_get_int(position_in);
  if (self->reverse) {
    rel_pos = -rel_pos;
  }
  self->ref_position = abs_pos - rel_pos;
  return mp_const_none;
}

static MP_DEFINE_CONST_FUN_OBJ_2(encoder_set_position_obj, encoder_set_position);

static mp_obj_t encoder_get_speed(mp_obj_t self_in) {
  encoder_obj_t *self = MP_OBJ_FROM_PTR(self_in);
  int32_t speed = opencube_encoders_get_speed(self->port);
  if (self->reverse) {
    speed = -speed;
  }
  return mp_obj_new_int(speed);
}

static MP_DEFINE_CONST_FUN_OBJ_1(encoder_get_speed_obj, encoder_get_speed);

static mp_obj_t encoder_reset(mp_obj_t self_in) {
  encoder_obj_t *self = MP_OBJ_FROM_PTR(self_in);
  self->ref_position = opencube_encoders_get_position(self->port);
  return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(encoder_reset_obj, encoder_reset);

static mp_obj_t encoder_finalizer(mp_obj_t self_in) {
  encoder_obj_t *self = MP_OBJ_FROM_PTR(self_in);
  opencube_encoders_deinit(self->port);
  return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(encoder_finalizer_obj, encoder_finalizer);

static const mp_rom_map_elem_t encoder_locals_dict[] = {
    {MP_ROM_QSTR(MP_QSTR_get_position), MP_ROM_PTR(&encoder_get_position_obj)},
    {MP_ROM_QSTR(MP_QSTR_set_position), MP_ROM_PTR(&encoder_set_position_obj)},
    {MP_ROM_QSTR(MP_QSTR_get_speed),    MP_ROM_PTR(&encoder_get_speed_obj)},
    {MP_ROM_QSTR(MP_QSTR_reset),        MP_ROM_PTR(&encoder_reset_obj)},
    {MP_ROM_QSTR(MP_QSTR___del__),      MP_ROM_PTR(&encoder_finalizer_obj)},
};
static MP_DEFINE_CONST_DICT(encoder_locals_dict_obj, encoder_locals_dict);

MP_DEFINE_CONST_OBJ_TYPE(
    opencube_encoder_type,
    MP_QSTR_Encoder,
    MP_TYPE_FLAG_NONE,
    make_new, &encoder_make_new,
    locals_dict, &encoder_locals_dict_obj,
    print, &encoder_print
);
