class ICM20608:
    """
    Class for ICM20608 gyroscope and accelerometer.
    """

    def __init__(self):
        """
        Initialize ICM20608, configure and start the sensor.
        """
        pass

    def read_value(self) -> tuple:
        """
        Read accelerometer and gyroscope data.
        :return: Data in format (acc_x (-), acc_y (-), acc_z (-), gyro_x (deg/s), gyro_y (deg/s), gyro_z (deg/s))
        """
        pass

    def deinit(self):
        """
        Deinitialize ICM20608 and turn it off.
        """
        pass


class Buttons:
    """
    Class for reading state of cube buttons.
    """

    def __init__(self):
        """
        Initialize buttons.
        """
        pass

    def pressed(self) -> tuple:
        """
        Get button state.
        :return: Buttons state (power, left, right, ok, up, down)
        """
        pass

    def read_value(self):
        """
        Read current button state from PCF. State can be returned with get_value().
        """
        pass

    def turn_off_cube(self):
        """
        Release battery power hold and turn off cube.
        """
        pass


class Led:
    """
    Class for controlling cube LED.
    """

    def __init__(self):
        """
        Initialize LED.
        """
        pass

    def on(self):
        """
        Turn LED on.
        """
        pass

    def off(self):
        """
        Turn LED off.
        """
        pass

    def toggle(self):
        """
        Toggle LED.
        """
        pass


class Battery:
    """
    Class for reading battery voltage.
    """

    def __init__(self):
        """
        Initialize battery ADC with a repeating timer.
        """
        pass

    def deinit(self):
        """
        Remove ADC timer.
        """
        pass

    def voltage(self) -> float:
        """
        Return last converted battery voltage.
        :return: Battery voltage in volts.
        """
        pass

    def read_voltage(self) -> float:
        """
        Start a new conversion and return the result.
        :return: Battery voltage in volts.
        """
        pass

class InternalI2CBusyError(RuntimeError):
    """
    Exception type thrown when an I2C bus collision is detected.
    """
    pass

def lock_i2c():
    """
    Lock the I2C bus for use by user code. Throws InternalI2CBusyError
    if the bus is locked by some other code.
    """
    pass

def unlock_i2c():
    """
    Unlock the I2C bus.
    """
    pass


FW_DATE: str
'''
Build date of the OpenCube firmware
'''

FW_COMMIT: str
'''
Commit from which the OpenCube firmware was built
'''
