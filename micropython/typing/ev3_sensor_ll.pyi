from typing import Tuple, overload


class PortAlreadyOpenError(RuntimeError):
    """
    The specified port has been already used for another EV3 sensor.
    """
    pass


class SensorNotReadyError(RuntimeError):
    """
    The sensor is not connected, or it has not yet finished the initial handshake.
    """
    pass

class SensorMismatchError(RuntimeError):
    """
    Incorrect sensor type has been connected to Open-Cube.
    """
    pass

class EV3Sensor:
    """
    C-based implementation of the EV3 sensor communication protocol.
    This class is intended as a base for a more user-approachable class.
    """

    def __init__(self, port_number: int, proper_id: int | None):
        """
        Open the given Open-Cube port as a EV3 sensor port.

        Calling this function switches the pins to UART mode,
        activates necessary peripherals and brings up a EV3
        sensor protocol stack.

        The port must be in range 0-3. If the port is outside of
        these bounds, ValueError is thrown.

        If this port has already been used for a RawEV3Sensor and
        that sensor has not been closed, PortAlreadyOpenError is thrown.

        Otherwise, a new instance is returned.

        The proper_id argument can be used for checking that
        a sensor of the correct type is connected. The value needs
        to be a numeric sensor ID (e.g. 29 for an EV3 color sensor).
        To disable sensor type checking, set this to None.

        This constructor will not wait for the sensor to connect.
        """
        pass

    def close(self):
        """
        Release the sensor port for other uses.

        After this function is called, you should not call
        other sensor functions.
        """
        pass

    def is_connected(self) -> bool:
        """
        Check if any sensor is connected.

        This differs from the sensor being ready:
         - "is_connected" is true even during the initial handshae
         - "is_ready" is true only once the sensor sends the first data packet
        """
        pass

    def is_ready(self) -> bool:
        """
        Check if the sensor is connected and ready for reading.

        This differs from the sensor being connected:
         - "is_connected" is true even during the initial handshae
         - "is_ready" is true only once the sensor sends the first data packet
        """
        pass

    def start_set_mode(self, mode_number: int):
        """
        Send a request to the sensor to switch to the specified mode.

        This function does not wait for the switch. If you need to
        wait until new data arrives, use read_mode().

        If the mode number is <0 or >7, ValueError will be thrown.
        """
        pass

    def write_command(self, buffer: memoryview | bytearray):
        """
        Write a device command to the sensor.

        This is used for some special functionality, e.g.
        firing the ultrasonic sensor, calibrating the gyro and
        for confirming factory calibration.

        If the command is longer than 16 bytes, ValueError will be thrown.
        """
        pass

    def start_reset(self):
        """
        Start a full sensor reset.

        This works by resetting the protocol state machine on OpenCube.
        OpenCube then stops sending keepalives (NACK packets) to the sensor.
        The sensor watchdog times out after a ~second and then reboots the sensor.
        The sensor then starts communicating with OpenCube like it was just plugged in.

        This function does not wait for sensor reconnection, it just
        resets the OpenCube state machine.
        """
        pass

    def read_raw(self) -> Tuple[int, memoryview]:
        """
        Read data from the current sensor mode.

        This function returns a tuple. The first element
        is the current mode. The second element is the
        measurement data as an array.

        The data array contains individual values from
        the current mode. No scaling is performed on them (
        e.g. these values are "raw", not "si" or "pct"-scaled).
        """
        pass

    def read_raw_mode(self, mode_number: int) -> memoryview:
        """
        Read data from a specific sensor mode.

        If the sensor is in a different mode, this function will issue
        a mode switch command to it. It will then poll the sensor to see
        if the data from a new mode was returned.

        The returned array contains individual values from
        the given mode. No scaling is performed on them (
        e.g. these values are "raw", not "si" or "pct"-scaled).

        If new sensor data is not received within ~2 seconds, SensorNotReadyError will be thrown.
        """
        pass

    def sensor_id(self) -> int | None:
        """
        Identify the sensor that is connected.

        This returns the LEGO ID of the sensor, e.g. 29
        for the EV3 color sensor.

        If no sensor is connected, None is returned.
        """
        pass

    @overload
    def wait_for_connection(self):
        """
        Wait for the sensor to start sending data, but
        do not wait longer than 3 seconds.
        """
        pass

    @overload
    def wait_for_connection(self, wait_ms: int):
        """
        Wait for the sensor to start sending data, but
        do not wait longer than the provided time.
        """
        pass
