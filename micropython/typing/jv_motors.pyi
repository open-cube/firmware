from typing import Tuple


class Motor:
    """
    Pybricks-inspired class for controlling EV3 & NXT motors.
    """

    COAST = 0  # Let the motor freely spin
    BRAKE = 1  # Passively brake the motor
    HOLD = 2  # Actively hold the motor in the given position

    LARGE_MOTOR = 0  # Large EV3/NXT motor
    MEDIUM_MOTOR = 1  # Medium EV3 motor

    CLOCKWISE = 0  # Motor rotates forward by default
    COUNTERCLOCKWISE = 1  # Motor is reversed

    def __init__(self, port: int, type: int = LARGE_MOTOR, positive_direction: int = CLOCKWISE):
        """
        Initialize a motor at the given Open-Cube port.
        :param port: MOTOR_PORTx where the motor resides.
        :param type: Motor type - Motor.LARGE_MOTOR or Motor.MEDIUM_MOTOR. This setting
                     adjusts the PID controller coefficients accordingly.
        :param positive_direction: Whether to reverse the motor direction or not.
                                   One of Motor.CLOCKWISE or Motor.COUNTERCLOCKWISE.
        """
        pass

    def __del__(self):
        """
        Deinitialize the motor and free the port for other uses.
        """
        pass

    def close(self):
        """
        Deinitialize the motor and free the port for other uses.
        """
        pass

    @property
    def pid(self) -> Tuple[float, float, float]:
        """
        Query the PID controller coefficients (Kp, Ki, Kd).

        The controller is used for tracking position. Thus,
        the coefficients have the following uses:
         * Kp multiplies the position error
         * Kd multiplies the speed error
         * Ki multiplies the integral of the position error
        """
        pass

    @pid.setter
    def pid(self, val: Tuple[float, float, float]):
        """
        Adjust the PID controller coefficients (Kp, Ki, Kd).

        The controller is used for tracking position. Thus,
        the coefficients have the following uses:
         * Kp multiplies the position error
         * Kd multiplies the speed error
         * Ki multiplies the integral of the position error
        """
        pass

    @property
    def speed_pid(self) -> Tuple[float, float]:
        """
        Query the speed PID controller coefficients (Kp, Ki).

        The controller is used for tracking speed in run().
        The coefficients have the following uses:
         * Kp multiplies the speed error
         * Ki multiplies the integral of the speed error
        """
        pass

    @speed_pid.setter
    def speed_pid(self, val: Tuple[float, float]):
        """
        Adjust the speed PID controller coefficients (Kp, Ki).

        The controller is used for tracking speed. Thus,
        the coefficients have the following uses:
         * Kp multiplies the speed error
         * Ki multiplies the integral of the speed error
        """
        pass

    @property
    def feedforward(self) -> Tuple[float, float]:
        """
        Query the feedforward coefficients (Kv, Ka).
        The output of the feedforward block is added
        to the motor output power. This enabled the motor
        to react faster to input reference changes.

        These coefficients are used in position controller only.
        They have this meaning:
         * Kv multiplies the requested speed to get output power in %
         * Ka multiplies the requested acceleration to get output power in %
        """
        pass

    @feedforward.setter
    def feedforward(self, val: Tuple[float, float]):
        """
        Adjust the feedforward coefficients (Kv, Ka).
        The output of the feedforward block is added
        to the motor output power. This enabled the motor
        to react faster to input reference changes.

        These coefficients are used in position controller only.
        They have this meaning:
         * Kv multiplies the requested speed to get output power in %
         * Ka multiplies the requested acceleration to get output power in %
        """
        pass

    @property
    def max_acceleration(self) -> float:
        """
        Query the current maximum acceleration in degrees/s^2.
        """
        pass

    @max_acceleration.setter
    def max_acceleration(self, val: float):
        """
        Set the maximum acceleration (in degrees/s^2).
        """
        pass

    @property
    def max_power(self) -> int:
        """
        Query the current maximum output power (in percent).
        """
        pass

    @max_power.setter
    def max_power(self, val: int):
        """
        Set the maximum output power (in percent).
        """
        pass

    def angle(self) -> int:
        """
        Query the current motor axle position (in degrees).
        """
        pass

    def reset_angle(self, new_angle: int = 0):
        """
        Adjust the encoder counters to start returning this angle in the current motor position.
        """
        pass

    def speed(self) -> int:
        """
        Query the current motor axle speed (in degrees/s).
        """
        pass

    def power(self) -> int:
        """
        Query the current motor output power (in percent).
        """
        pass

    def coast(self):
        """
        Let the motor freely spin on its own (no braking, no movement).
        """
        pass

    def stop(self):
        """
        Let the motor freely spin on its own (no braking, no active movement).
        Equivalent to coast(); this method is here for compatibility
        with Pybricks.
        """
        pass

    def brake(self):
        """
        Passively brake the motor (the motor can be turned, but only with difficulty).
        """
        pass

    def hold(self):
        """
        Actively hold the motor in the current position (the motor can not be turned).
        """
        pass

    def dc(self, power: int):
        """
        Run the motor without regulation at the given output power.

        Beware, the max_power limit still applies.
        """
        pass

    def run(self, speed: float):
        """
        Run the motor with regulation at the given speed (degrees/s).

        :param speed: Speed to run at in degrees/s. Sign matters - it determines the direction of rotation.
        """
        pass

    def run_angle(self, speed: float, rotation_angle: float, then: int = HOLD, wait: bool = True):
        """
        Rotate the motor by the given angle from the current motor position.
        :param speed: Speed to run at in degrees/s. Sign does not matter.
        :param rotation_angle: By how many degrees to rotate.
        :param then: What to do after finishing the move (Motor.COAST/BRAKE/HOLD).
        :param wait: Whether to wait for the move to end.
        """
        pass

    def run_target(self, speed: float, target_angle: float, then: int = HOLD, wait: bool = True):
        """
        Rotate the motor to the given angle position.
        :param speed: Speed to run at in degrees/s. Sign does not matter.
        :param target_angle: To what position to rotate.
        :param then: What to do after finishing the move (Motor.COAST/BRAKE/HOLD).
        :param wait: Whether to wait for the move to end.
        """
        pass

    def run_time(self, speed: float, time: float, then: int = HOLD, wait: bool = True):
        """
        Rotate the motor for the given total time.
        :param speed: Speed to run at in degrees/s. Sign matters - it determines the direction of rotation.
        :param time: Number of milliseconds reserved for the entire move.
        :param then: What to do after finishing the move (Motor.COAST/BRAKE/HOLD).
        :param wait: Whether to wait for the move to end.
        """
        pass

    def track_target(self, target_angle: float, target_speed: float = 0):
        """
        Follow a custom trajectory by feeding a custom position/speed setpoint to the PID controller.
        :param target_angle: Target angle to which to run.
        :param target_speed: Derivative of target_angle. This is optional,
                             but then the PID controller performance will be limited.
        """
        pass

    def is_complete(self) -> bool:
        """
        Check whether the last started movement has finished.

        If no regulated movement is running, true is returned.
        """
        pass

    def wait_complete(self):
        """
        Wait until the last started movement finishes.
        """
        pass
