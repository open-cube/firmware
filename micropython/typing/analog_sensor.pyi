from typing import overload

class AnalogSensor:
    """
    Class for measuring analog data and changing value of output pin on a port. 
    """

    def __init__(self, port: int):
        """
        Initialize analog sensor of the specified sensor port.
        :param port: Port 0-3 of the brick.
        """
        pass

    def on(self):
        """
        Turn on output pin of sensor port.
        """
        pass

    def off(self):
        """
        Turn off output pin of sensor port.
        """
        pass

    def toggle(self):
        """
        Toggle output pin of sensor port.
        """
        pass

    def set_switching(self):
        """
        Set sensor port switching mode for continuous measurement. 
        Output value is measured automaticaly with the output pin on 
        and with the output pin off.
        """
        pass

    def stop_switching(self):
        """
        Set sensor port to non-switching mode for continuous measurement.
        """
        pass
    
    def get_value(self, pin_on: bool) -> int:
        """
        In normal mode - measure and return analog value. 
        In continuous mode - return last measured analog value.
        :param pin_on: True - last value when pin was on, False - last value when pin was off.
        :return: Raw analog value (0-32768).
        """
        pass
    
    def set_continuous(self, period_us: int, wait_us: int):
        """
        Set all analog sensors to continuous mode with given period. Wait given time after switching pin state. 
        :param period_us: Period in microseconds.
        :param wait_us: Wait time in microseconds.
        """
        pass

    def stop_continuous(self):
        """
        Stop continuous measurement on all analog sensors.
        """
        pass

    def deinit(self):
        """
        Deinitialize analog sensor.
        """
        pass
    