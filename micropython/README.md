# MicroPython s Open-Cube knihovnami

Pro zvýšení rychlosti zpracování je část knihoven psaná v jazyce C.
Tento dokument popisuje, jak získat MicroPython s těmito knihovnami zabudovanými.

## Stažení z GitLabu

Na GitLabu máme nastavené automatické sestavování při commitu.
Nejnovější sestavený MicroPython z větve `main` lze stáhnout zde:

 - [opencube_micropython_with_pylibs.uf2](https://gitlab.fel.cvut.cz/open-cube/firmware/builds/artifacts/main/raw/opencube_micropython_with_pylibs.uf2?job=build%20micropython%20firmware)
   je verze obsahující veškeré nezbytné knihovny (včetně menu a Python knihoven ve složce `lib`).
   Tato varianta je vhodná pro koncové uživatele, kteří nechtějí MicroPython na OpenCube vyvíjet.
 - [opencube_micropython_without_pylibs.uf2](https://gitlab.fel.cvut.cz/open-cube/firmware/builds/artifacts/main/raw/opencube_micropython_without_pylibs.uf2?job=build%20micropython%20firmware)
   je verze obsahující pouze knihovny v C. Python knihovny je nutné nahrát
   na kostku jiným způsobem. Tato varianta je výhodná pro vývojáře --
   Python moduly lze takto jednoduše měnit (u verze `with_pylibs` může
   občas být problém s nahrazením zabudovaného modulu).

## Jak sestavit MicroPython

1. Je potřeba mít k dispozici nějaké Linuxové prostředí.
   Postup níže je otestovaný na Ubuntu 22.04 nainstalovaném přímo
   na hardware. Identicky by ale měl fungovat například [Windows Subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/).
2. Dále je potřeba nainstalovat kompilátor a další nástroje pro sestavení MicroPythonu.
   Příkaz níže by měl zařídit vše potřebné (pro Ubuntu):
   ```sh
   sudo apt install  gcc-arm-none-eabi build-essential git python3 cmake
   ```
3. Nyní stačí naklonovat repozitář Open-Cube spolu s MicroPythonem:
   ```sh
   # naklonování OpenCube repozitáře
   git clone --depth 1  https://gitlab.fel.cvut.cz/open-cube/firmware.git
   cd firmware

   # naklonování "mpy" submodulu v této složce
   git submodule update --init

   # naklonování submodulů uvnitř MicroPythonu
   make -C micropython/mpy/ports/rp2 submodules
   ```
4. Nyní už jde MicroPython sestavit:
   ```sh
   # sestaveni nástroje, co dokáže zkompilovat Python kód do MicroPython bajtkódu
   make -C micropython/mpy/mpy-cross -j $(nproc)

   # sestavení MicroPythonu samotného
   make -C micropython/mpy/ports/rp2 \
       USER_C_MODULES=$(pwd)/micropython/modules/micropython.cmake \
       -j $(nproc)
   ```
5. Výsledný MicroPython `.uf2` soubor k nahrání na desku se nachází v cestě `micropython/mpy/ports/rp2/build-PICO/firmware.uf2`.


## Přidání "zmražených" modulů

MicroPython [dokáže](https://docs.micropython.org/en/latest/reference/manifest.html) do firmwaru
zabudovat Python moduly ve zkomprimované a optimalizované podobě. Toto
jde provést přidáním parametru `FROZEN_MANIFEST` při sestavování MicroPythonu:
```sh
make -C micropython/mpy/ports/rp2 \
    USER_C_MODULES=$(pwd)/micropython/modules/micropython.cmake \
    FROZEN_MANIFEST=$(pwd)/micropython/frozen_manifest.py \
    -j $(nproc)
```

Soubor [`frozen_manifest.py`](frozen_manifest.py) obsahuje definice
modulů, které se mají do firmwaru vložit. V současnosti obsahuje:
 - odkaz na standardní knihovny pro RPi Pico,
 - OpenCube knihovnu ve složce `lib`,
 - Menu ve složce `main`.

## Struktura adresářů

 - [`mpy`](mpy) - obsahuje MicroPython samotný jako [Git submodul](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
 - [`modules`](modules) - Moduly pro Open-Cube
 - [`modules/micropython.cmake`](modules/micropython.cmake) - Seznam modulů použitý při sestavování MicroPythonu, [více zde](https://docs.micropython.org/en/latest/develop/cmodules.html)
 - [`modules/opencube_pindefs`](modules/opencube_pindefs) - Definice pinů pro UART a motory
 - [`modules/opencube_uart`](modules/opencube_uart) - Implementace UARTu pro EV3 senzory
 - [`modules/ev3_sensors`](modules/ev3_sensors) - Implementace protokolu EV3 senzorů
 - [`typing`](typing) - Soubory popisující Python rozhraní poskytované C moduly
 - [`frozen_manifest.py`](frozen_manifest.py) - Seznam Python modulů pro "zmražení" do firmware, [více zde](https://docs.micropython.org/en/latest/reference/manifest.html)
 - [`Dockerfile`](Dockerfile) - Specifikace "virtuálního stroje" pro Docker, ve kterém se MicroPython sestavuje na Gitlabu

## Přidávání nových modulů

Při vytváření stávajících modulů jsem postupoval podle <https://docs.micropython.org/en/latest/develop/cmodules.html>.
Pro rychlé vytvoření modulu doporučuji zkopírovat <https://github.com/micropython/micropython/tree/master/examples/usercmodule/cexample>,
tam je obsažena základní kostra modulu. Jenom je potřeba přidat
modul do souboru [`modules/micropython.cmake`](modules/micropython.cmake):
```cmake
include(${CMAKE_CURRENT_LIST_DIR}/slozka_modulu/micropython.cmake)
```
