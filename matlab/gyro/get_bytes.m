function bytes=get_bytes(msg_type, values)
    bt_dividers = [10, 10, 10, 1000, 100000];
    bt_len = [1, 4, 3, 1, 2];
    ascii_offset = 48;
    bytes = [msg_type+ascii_offset 32];
    if length(values) == bt_len(msg_type+1)
        for m=1:1:length(values)
            digits = dec2base(abs(values(m))*bt_dividers(msg_type+1),10) - '0';
            digits_ascii = digits + ascii_offset;
            if values(m) < 0
                bytes = [bytes 45];
            end
            bytes = [bytes digits_ascii 32];
        end
        bytes = [bytes 10];
    end
end