clc
clear
format compact
device = bluetooth("OPEN-CUBE")
%%
KpP = 0.001;
KiP = 0.0001;

KpA = 22;

KpS = 15;
KiS = 9;
KdS = -1.4;
KfdS = 7;

Kcomp = 0.001;
refPosition = 200.0;
refRot = 0.0;

offsetAngle = 8.5;
msg0 = get_bytes(0, [KpA]);
msg1 = get_bytes(1, [KpS KiS KdS KfdS]);
msg2 = get_bytes(2, [offsetAngle refPosition, refRot]);
msg3 = get_bytes(3, [Kcomp]);
msg4 = get_bytes(4, [KpP KiP]);

send_bytes(device,msg0)
send_bytes(device,msg1)
send_bytes(device,msg2)
send_bytes(device,msg3)
send_bytes(device,msg4)