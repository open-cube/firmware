clear variables;

%% Sampling times
Ts = 0.001; % regulator
Ts_speed = 0.01; % speed measurement


%% Motor model for tuning
% Motor is here modelled as a first-order system from velocity PoV
% (position PoV just adds an integrator).
% Constants below correspond to a transfer function
% H = K/(sT+1) plus an additional *(1/s) for integration.

s = tf('s');
% LARGE MOTOR
K = 844.97/100;
T = 0.059163;
% MEDIUM MOTOR
% K = 1360.2/100;
% T = 0.033717;

% output 1 is velocity, output 2 is position
simple_model_ct = ss([-1/T, 0; 1, 0], [K/T; 0], eye(2), 0);
simple_model_dt = c2d(simple_model_ct, Ts, 'zoh');


%% Motor model for testing
% NXT-Unroller model is more precise (includes inductance), but it
% only exists for the large motor
Vbatt = 8.0;
motor_state_model = nxt_unroller_model * Vbatt/100;
%motor_state_model = simple_model_ct;
motor_deadzone = 3; % guesstimated via a separate experiment


%% Design motor controller constants

% design feedback constants
feedback_system = simple_model_dt(2);
opts = pidtuneOptions('DesignFocus', 'disturbance-rejection');
[C,~] = pidtune(feedback_system, 'PID', 20, opts);
Kp = C.Kp;
Ki = C.Ki;
Kd = C.Kd;

% design feedforward constants
feedforward_system = tf(simple_model_ct(1));
system_inversion = minreal(1/feedforward_system);
Ka = system_inversion.num{1}(1); % trajectory acceleration -> power
Kv = system_inversion.num{1}(2); % trajectory velocity     -> power


%% Generate reference trajectory
t = (0:Ts:5).';
[x, v, a] = generate_trajectory(t, 360*4, 360, 720);
traj_x = [t, round(x)];
traj_v = [t, v];
traj_a = [t, a];
