function [x,v,a] = generate_trajectory(time, angle, speed, accel)
%GENERATE_TRAJECTORY Generate a trapezoidal speed profile
% This function will generate a smoothly accelerated trajectory that
% will rotate the motor by <angle> degrees. The motor will not go
% faster than <speed> and it will not accelerate faster than <accel>.
%
% Inputs:
% - time:  Time points at which to generate the trajectory (s)
% - angle: Target motor angle (deg)
% - speed: Maximum motor speed (deg/s)
% - accel: Maximum motor acceleration (deg/s^2)
%
% Outputs:
% - x: Trajectory position at specified time points
% - v: Derivative of trajectory position (= velocity)
% - a: Derivative of velocity (= acceleration)

    speed = abs(speed);
    accel = abs(accel);

    ramp_time = speed/accel;
    ramp_angle = 1/2*accel*ramp_time^2;

    if angle > (2*ramp_angle)
        Trampup = ramp_time;
        Tsteady = (angle-2*ramp_angle)/speed;
        Trampdown = Trampup;
    else
        Trampup = sqrt(angle/accel);
        Tsteady = 0;
        Trampdown = Trampup;
    end

    rampup_end_angle = 1/2*accel*Trampup^2;
    steady_end_angle = rampup_end_angle + Tsteady*speed;
    rampdown_end_angle = angle;

    max_speed = accel * Trampup;

    is_rampup = time < Trampup;
    is_steady = time < (Trampup+Tsteady) & time >= Trampup;
    is_rampdown = time < (Trampup+Tsteady+Trampdown) & time >= (Trampup+Tsteady);
    is_idle = time >= (Trampup+Tsteady+Trampdown);

    x = is_rampup .* (1/2*accel*time.^2) + ...
        is_steady .* (rampup_end_angle + max_speed*(time-Trampup)) + ...
        is_rampdown .* (steady_end_angle + max_speed*(time-Trampup-Tsteady) - 1/2*accel*(time-Trampup-Tsteady).^2) + ...
        is_idle .* (rampdown_end_angle);

    v = is_rampup .* (+accel .* (time)) + ...
        is_steady .* (max_speed) + ...
        is_rampdown .* (max_speed-accel .* (time-Trampup-Tsteady));

    a = is_rampup .* (+accel) + ...
        is_rampdown * (-accel);
end
