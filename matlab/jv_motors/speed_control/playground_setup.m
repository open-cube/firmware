clear variables;

%% Sampling times
Ts = 0.001; % regulator
Ts_speed = 0.01; % speed measurement


%% Motor model for tuning
% Motor is here modelled as a first-order system from velocity PoV
% (position PoV just adds an integrator).
% Constants below correspond to a transfer function
% H = K/(sT+1) plus an additional *(1/s) for integration.

s = tf('s');
% LARGE MOTOR
K = 844.97/100;
T = 0.059163;
% MEDIUM MOTOR
% K = 1360.2/100;
% T = 0.033717;

% output 1 is velocity, output 2 is position
simple_model_ct = ss([-1/T, 0; 1, 0], [K/T; 0], eye(2), 0);
simple_model_dt = c2d(simple_model_ct, Ts, 'zoh');


%% Motor model for testing
% NXT-Unroller model is more precise (includes inductance), but it
% only exists for the large motor
Vbatt = 8.0;
% motor_state_model = nxt_unroller_model * Vbatt/100;
motor_state_model = simple_model_ct;
motor_deadzone = 3; % guesstimated via a separate experiment


%% Design motor controller constants

% design feedback constants
% pid tuner autonomous mode sucks
Kp = 0.22;
Ki = 5;

fprintf("m.speed_pid = (%.4f, %.4f);\n", Kp, Ki);