function [sys] = nxt_unroller_model
%NXT_UNROLLER_MODEL Get a linear model of the EV3 Large motor
%
% This model includes both inductance and inertia of the motor.
%
% Parameters come from http://nxt-unroller.blogspot.com/2015/03/mathematical-model-of-lego-ev3-motor.html

    R = 6.832749059810827;
    L = 0.00494;
    K = 0.459965726538748;
    B = 0.000726962269165;
    J = 0.001502739083882;
    F = 0.007776695904018;

    Afull = [
        -B/J, +K/J, 0;
        -K/L, -R/L, 0;
            1,   0, 0
    ];
    Bfull = [
        0; 1/L; 0
    ];
    Cfull = [
        180/pi, 0, 0;
        0, 0, 180/pi
    ];
    Dfull = 0;

    sys = ss(Afull,Bfull,Cfull,Dfull);
end
