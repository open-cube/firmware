/*
 * Open-Cube sensor mode definitions
 * oc_sensor_modes.c
 *
 *  Created on: Jul 2, 2024
 *      Author: jelinva4
 */

#include "oc_sensor_modes.h"

#include "oc_st_sensor_uart.h"
#include "messages.h"

const ev3_mode_info m_distance = {S_MODE_DISTANCE,
							    "DISTANCE", 8,
							    0, MAX_SENSOR_RANGE,
							    0, 0,
							    0, MAX_SENSOR_RANGE,
							    "mm", 2,
							    1, 1, 4, 0};

const sensor_info s_info = {SENSOR_ID, S_MODE_COUNT, S_MODE_COUNT, DATA_BAUD_RATE,
							{m_distance}};

