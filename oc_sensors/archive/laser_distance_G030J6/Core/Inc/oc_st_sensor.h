/**
 *  Open-Cube STM helper functions for sending EV3 UART protocol messages
 */
#ifndef OC_ST_SENSOR_MESSAGES_H
#define OC_ST_SENSOR_MESSAGES_H
#include "messages.h"

typedef struct {
	uint8_t type;
	uint8_t num_modes;
	uint8_t num_visible_modes;
	uint32_t baud_rate;
	ev3_mode_info modes[EV3_MAX_MODES];
} sensor_info;

extern void uart1_send_length(const void *data, uint16_t length);
extern void send_metadata(const sensor_info* s_info);
extern void send_data(int sensor_state, void* data, size_t size);

#endif //OC_ST_SENSOR_MESSAGES_H
