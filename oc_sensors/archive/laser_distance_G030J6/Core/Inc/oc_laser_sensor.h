#ifndef OC_LASER_SENSOR_H
#define OC_LASER_SENSOR_H

#include <stdint.h>
#include "messages.h"
#include "oc_st_sensor.h"

typedef struct {
	uint8_t sensorState;
	uint8_t dataReady;
	uint16_t compVAL;
	uint16_t dev;
	uint16_t Distance;
	uint16_t SignalRate;
	uint16_t AmbientRate;
	uint16_t SpadNum;
	uint8_t RangeStatus;

} oc_laser_sensor;

typedef enum {
	S_MODE_WAIT_ACK = -2,
	S_MODE_INIT = -1,
	S_MODE_NORMAL = 0,
	S_MODE_FOV = 1,
	S_MODE_SHORT = 2,
	S_MODE_COUNT = 3,
} sensor_mode;

int get_sensor_data(oc_laser_sensor* sensor);
int set_sensor_mode_normal(oc_laser_sensor* sensor);
int set_sensor_mode_short(oc_laser_sensor* sensor);
int set_sensor_mode_fov(oc_laser_sensor* sensor);
int change_sensor_settings(oc_laser_sensor* sensor, int sensor_state);

#endif //OC_LASER_SENSOR_H
