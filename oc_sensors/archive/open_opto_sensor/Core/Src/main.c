/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <math.h>
#include "messages.h"
#include "oc_st_sensor.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#define SENSOR_ID 193
#define DATA_BAUD_RATE 256000
#define PCT_MIN 0
#define PCT_MAX 100
#define ADC_MAX 32767

#define MEAS_CYCLES 10

typedef enum {
	S_MODE_WAIT_ACK = -2,
	S_MODE_INIT = -1,
	S_MODE_REFLECT = 0,
	S_MODE_AMBIENT = 1,
	S_MODE_COLOR = 2,
	S_MODE_REF_RAW = 3,
	S_MODE_RGB_RAW = 4,
	S_MODE_COUNT = 5,
} sensor_mode;

typedef enum {
	NONE = 0,
	BLACK = 1,
	BLUE = 2,
	GREEN = 3,
	YELLOW = 4,
	RED = 5,
	WHITE = 6,
	BROWN = 7,
} colors;

const ev3_mode_info m_reflect = {S_MODE_REFLECT,
							    "COL-REFLECT", 11,
							    0, 100,
							    0, 100,
							    0, 100,
							    "pct", 3,
							    1, 3, 15, 5};

const ev3_mode_info m_ambient = {S_MODE_AMBIENT,
							    "COL-AMBIENT", 11,
							    0, 100,
							    0, 100,
							    0, 100,
							    "pct", 3,
							    1, 3, 15, 5};

const ev3_mode_info m_color = {S_MODE_COLOR,
							    "COL-COLOR", 9,
							    0, 7,
							    0, 7,
							    0, 7,
							    "col", 3,
							    1, 1, 15, 0};

const ev3_mode_info m_reflect_raw = {S_MODE_REF_RAW,
							    "REF-RAW", 7,
							    0, ADC_MAX,
							    0, 100,
							    0, ADC_MAX,
							    "none", 4,
							    2, 1, 15, 0};

const ev3_mode_info m_rgb_raw = {S_MODE_RGB_RAW,
							    "RGB-RAW", 7,
							    0, ADC_MAX,
							    0, 100,
							    0, ADC_MAX,
							    "none", 4,
							    4, 1, 15, 5};


const sensor_info s_info = {SENSOR_ID, S_MODE_COUNT, S_MODE_COUNT, DATA_BAUD_RATE,
							{m_reflect, m_ambient, m_color, m_reflect_raw, m_rgb_raw}};


/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

TIM_HandleTypeDef htim1;

/* USER CODE BEGIN PV */
uint16_t adc_result=0;
uint16_t adc_off=0;
uint16_t red=0;
uint16_t green=0;
uint16_t blue=0;
uint32_t adc_off_sum=0;
uint32_t red_sum=0;
uint32_t green_sum=0;
uint32_t blue_sum=0;
uint16_t red_diff=0;
uint16_t green_diff=0;
uint16_t blue_diff=0;

float red_map = 0.0;
float green_map = 0.0;
float blue_map = 0.0;
float red_pct = 0.0;
float ambient = 0.0;
float h,s,v;

uint16_t r_min = 80;
uint16_t r_max = 5200;
uint16_t g_min = 36;
uint16_t g_max = 3500;
uint16_t b_min = 62;
uint16_t b_max = 4300;

uint16_t color=0;

int sensor_state = S_MODE_INIT;

volatile uint32_t nack_time;
uint16_t nack_diff;

uint8_t rx_buff[EV3_MSG_BUFFER_SIZE];
uint8_t rx_i = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM1_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */
void delay_us(uint16_t us);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
// UART sent given number of bytes from an array

//-------------------------------------------------------------
void delay_us(uint16_t us){

	__HAL_TIM_SET_COUNTER(&htim1,0);  // set the counter value a 0
	while (__HAL_TIM_GET_COUNTER(&htim1) < us);  // wait for the counter to reach the us input in the parameter
}
//------------------------------------------------------------
uint16_t meas_light(void){
	HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1,1000);
	return (uint16_t)HAL_ADC_GetValue(&hadc1);
}


float max(float a, float b, float c) {
   return ((a > b)? (a > c ? a : c) : (b > c ? b : c));
}
float min(float a, float b, float c) {
   return ((a < b)? (a < c ? a : c) : (b < c ? b : c));
}

void rgb_to_hsv(float r, float g, float b, float* h, float* s, float* v) {
   // R, G, B values are divided by 255
   // to change the range from 0..255 to 0..1:
   float cmax = max(r, g, b); // maximum of r, g, b
   float cmin = min(r, g, b); // minimum of r, g, b
   float diff = cmax-cmin; // diff of cmax and cmin.
   if (cmax == cmin)
      *h = 0;
   else if (cmax == r)
      *h = fmod((60 * ((g - b) / diff) + 360), 360.0);
   else if (cmax == g)
      *h = fmod((60 * ((b - r) / diff) + 120), 360.0);
   else if (cmax == b)
      *h = fmod((60 * ((r - g) / diff) + 240), 360.0);
   // if cmax equal zero
      if (cmax == 0)
         *s = 0;
      else
         *s = (diff / cmax) * 100;
   // compute v
   *v = cmax * 100;
}

uint8_t hsv_to_color(float h, float s, float v) {
	if (v < 15) {
		return BLACK;
	} else if (s < 15) {
		return WHITE;
	} else if (h > 160 && h <= 260) {
		return BLUE;
	} else if (h > 70 && h <= 160) {
		return GREEN;
	} else if (h > 40 && h <= 70) {
		return YELLOW;
	} else if (h > 10 && h <= 40) {
		return BROWN;
	} else {
		return RED;
	}
}

float MAP(uint16_t au16_IN, uint16_t au16_INmin, uint16_t au16_INmax, uint16_t au16_OUTmin, uint16_t au16_OUTmax)
{
	if(au16_IN < au16_INmin) {
		return (float) au16_OUTmin;
	}
	if(au16_IN > au16_INmax) {
		return (float) au16_OUTmax;
	}
    return ((((float) (au16_IN - au16_INmin)*(au16_OUTmax - au16_OUTmin))/ ((float) (au16_INmax - au16_INmin)) + au16_OUTmin));
}
void measure_data(uint8_t sensor_state) {
	switch (sensor_state) {
		case S_MODE_REFLECT:
		case S_MODE_REF_RAW:
			adc_off_sum = 0;
			red_sum = 0;
			for (int i=0; i < MEAS_CYCLES; i++) {
				BLUE_off();
				GREEN_off();
				RED_off();
				delay_us(200);
				adc_off_sum += meas_light();
				RED_on();
				delay_us(400);
				red_sum += meas_light();
				RED_off();
			}

			if (red_sum < adc_off_sum) {
				red_diff = 0;
			} else {
				red_diff = (red_sum - adc_off_sum)/MEAS_CYCLES;
			}
			adc_off = adc_off_sum/MEAS_CYCLES;
			red = red_sum/MEAS_CYCLES;

			red_pct = MAP(red_diff, r_min, r_max, PCT_MIN, PCT_MAX);
			break;
		case S_MODE_AMBIENT:
			BLUE_off();
			GREEN_off();
			RED_off();
			adc_off_sum = 0;
			for (int i=0; i < MEAS_CYCLES; i++) {
				adc_off_sum += meas_light();
				delay_us(100);
			}

			adc_off = adc_off_sum/MEAS_CYCLES;
			ambient = MAP(adc_off, 0, ADC_MAX, PCT_MIN, PCT_MAX);
			break;
		case S_MODE_COLOR:
		case S_MODE_RGB_RAW:
			BLUE_off();
			GREEN_off();
			RED_off();
			adc_off_sum = 0;
			red_sum = 0;
			green_sum = 0;
			blue_sum = 0;
			for (int i=0; i < MEAS_CYCLES; i++) {
				delay_us(200);
				adc_off_sum += meas_light();
				RED_on();
				delay_us(400);
				red_sum += meas_light();
				RED_off();
				GREEN_on();
				delay_us(400);
				green_sum += meas_light();
				GREEN_off();
				BLUE_on();
				delay_us(400);
				blue_sum += meas_light();
				BLUE_off();
			}
			if (red_sum < adc_off_sum) {
				red_diff = 0;
			} else {
				red_diff = (red_sum - adc_off_sum)/MEAS_CYCLES;
			}
			if (green_sum < adc_off_sum) {
				green_diff = 0;
			} else {
				green_diff = (green_sum - adc_off_sum)/MEAS_CYCLES;
			}
			if (blue_sum < adc_off_sum) {
				blue_diff = 0;
			} else {
				blue_diff = (blue_sum - adc_off_sum)/MEAS_CYCLES;
			}
			adc_off = adc_off_sum/MEAS_CYCLES;

			red_map = MAP(red_diff, r_min, r_max, 0, 1);
			green_map = MAP(green_diff, g_min, g_max, 0, 1);
			blue_map = MAP(blue_diff, b_min, b_max, 0, 1);

			rgb_to_hsv(red_map, green_map, blue_map, &h, &s, &v);
			break;
		default:
			break;
	}
}

void choose_send_data(uint8_t sensor_state) {
	switch (sensor_state) {
		case S_MODE_REFLECT:
			send_data(sensor_state, &red_pct, sizeof(red_pct));
			break;
		case S_MODE_REF_RAW: {
			uint16_t data[2];
			data[0] = red;
			data[1] = adc_off;
			send_data(sensor_state, data, 4);
			break;
		}
		case S_MODE_AMBIENT:
			send_data(sensor_state, &ambient, sizeof(ambient));
			break;
		case S_MODE_COLOR:
			color = hsv_to_color(h,s,v);
			send_data(sensor_state, &color, sizeof(color));
			break;
		case S_MODE_RGB_RAW: {
			uint16_t data_rgb[4];
			data_rgb[0] = red_diff;
			data_rgb[1] = green_diff;
			data_rgb[2] = blue_diff;
			data_rgb[3] = adc_off;
			send_data(sensor_state, data_rgb, 8);
			break;
		}
		default:
			break;
	}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

		while(sensor_state == S_MODE_INIT || sensor_state == S_MODE_WAIT_ACK) {
			RED_off();
			GREEN_off();
			BLUE_on();
			send_metadata(&s_info);
			if (sensor_state == S_MODE_INIT) {
				sensor_state = S_MODE_WAIT_ACK;
			}
			HAL_Delay(80);
		}
		BLUE_off();
		LL_USART_Disable(USART1);
		MX_USART1_UART_Init();
		nack_time = HAL_GetTick();
		while (sensor_state > S_MODE_INIT) {
			measure_data(sensor_state);
			choose_send_data(sensor_state);
			nack_diff = HAL_GetTick() - nack_time;
			if (nack_diff > 1000) {
				sensor_state = S_MODE_INIT;
			}
		}
		LL_USART_Disable(USART1);
		MX_USART1_UART_Init();
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.LowPowerAutoPowerOff = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.SamplingTimeCommon1 = ADC_SAMPLETIME_19CYCLES_5;
  hadc1.Init.SamplingTimeCommon2 = ADC_SAMPLETIME_19CYCLES_5;
  hadc1.Init.OversamplingMode = ENABLE;
  hadc1.Init.Oversampling.Ratio = ADC_OVERSAMPLING_RATIO_8;
  hadc1.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_NONE;
  hadc1.Init.Oversampling.TriggeredMode = ADC_TRIGGEREDMODE_SINGLE_TRIGGER;
  hadc1.Init.TriggerFrequencyMode = ADC_TRIGGER_FREQ_HIGH;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */
  HAL_ADCEx_Calibration_Start(&hadc1);
  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 63;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 65535;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */
  HAL_TIM_Base_Start(&htim1);
  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{
  /* USER CODE BEGIN USART1_Init 0 */
	uint32_t baud_rate = s_info.baud_rate;
	  if (sensor_state == S_MODE_INIT || sensor_state == S_MODE_WAIT_ACK) {
		  baud_rate = EV3_UART_HANDSHAKE_BAUD;
	  }
  /* USER CODE END USART1_Init 0 */

  LL_USART_InitTypeDef USART_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

  /* Peripheral clock enable */
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);

  LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
  /**USART1 GPIO Configuration
  PB7   ------> USART1_RX
  PB6   ------> USART1_TX
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_0;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* USART1 interrupt Init */
  NVIC_SetPriority(USART1_IRQn, 0);
  NVIC_EnableIRQ(USART1_IRQn);

  /* USER CODE BEGIN USART1_Init 1 */
  LL_USART_EnableIT_RXNE(USART1);
  /* USER CODE END USART1_Init 1 */
  USART_InitStruct.PrescalerValue = LL_USART_PRESCALER_DIV1;
  USART_InitStruct.BaudRate = baud_rate;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
  LL_USART_Init(USART1, &USART_InitStruct);
  LL_USART_SetTXFIFOThreshold(USART1, LL_USART_FIFOTHRESHOLD_1_8);
  LL_USART_SetRXFIFOThreshold(USART1, LL_USART_FIFOTHRESHOLD_1_8);
  LL_USART_DisableFIFO(USART1);
  LL_USART_ConfigAsyncMode(USART1);

  /* USER CODE BEGIN WKUPType USART1 */

  /* USER CODE END WKUPType USART1 */

  LL_USART_Enable(USART1);

  /* Polling USART1 initialisation */
  while((!(LL_USART_IsActiveFlag_TEACK(USART1))) || (!(LL_USART_IsActiveFlag_REACK(USART1))))
  {
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOB);
  LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);

  /**/
  LL_GPIO_ResetOutputPin(LED_RED_GPIO_Port, LED_RED_Pin);

  /**/
  LL_GPIO_ResetOutputPin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);

  /**/
  LL_GPIO_ResetOutputPin(LED_BLUE_GPIO_Port, LED_BLUE_Pin);

  /**/
  GPIO_InitStruct.Pin = LED_RED_Pin;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(LED_RED_GPIO_Port, &GPIO_InitStruct);

  /**/
  GPIO_InitStruct.Pin = LED_GREEN_Pin;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(LED_GREEN_GPIO_Port, &GPIO_InitStruct);

  /**/
  GPIO_InitStruct.Pin = LED_BLUE_Pin;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(LED_BLUE_GPIO_Port, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */
void USART1_IRQHandler(void){
	if(LL_USART_IsActiveFlag_RXNE(USART1)){
		rx_buff[rx_i] = LL_USART_ReceiveData8(USART1);
		uint8_t msg_type = type_of_message(rx_buff[0]);
		uint8_t msg_subtype = subtype_of_message(rx_buff[0]);
		uint8_t msg_full_len = full_length_of_message(rx_buff[0]);
		if (msg_full_len > EV3_MSG_BUFFER_SIZE) {
			rx_i = 0;
			return;
		}
		switch (msg_type) {
			case MTYPE_SYSTEM:
				if (msg_subtype == MTYPE_SYSTEM_ACK
					&& (sensor_state == S_MODE_WAIT_ACK || sensor_state == S_MODE_INIT)) {
					sensor_state = S_MODE_REFLECT;
				} else if (msg_subtype == MTYPE_SYSTEM_NACK && sensor_state > S_MODE_INIT) {
					nack_time = HAL_GetTick();
					//choose_send_data(sensor_state);
				}
				break;
			case MTYPE_COMMAND:
				if (msg_full_len == rx_i+1 && sensor_state > S_MODE_INIT) {
					uint8_t csum = calculate_message_checksum(rx_buff, rx_i);
					if (csum == rx_buff[rx_i]) {
						if(msg_subtype == MTYPE_COMMAND_SELECT) {
							if (rx_buff[1] != sensor_state
								&& rx_buff[1] > S_MODE_INIT
								&& rx_buff[1] < S_MODE_COUNT) {
								sensor_state = rx_buff[1];
								//change_sensor_settings(&laser_sensor, sensor_state);
							}
						} else if (msg_subtype == MTYPE_COMMAND_WRITE) {

						}
					}
					rx_i = 0;
				} else {
					rx_i++;
				}
				break;
			default:
				rx_i = 0;
		}
	 }
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
