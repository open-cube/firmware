/**
 *  Open-Cube sensor mode definitions
 */
#ifndef OC_SENSOR_MODES_H
#define OC_SENSOR_MODES_H

#include <stdint.h>

#define SENSOR_ID 193
#define DATA_BAUD_RATE 256000
#define PCT_MIN 0
#define PCT_MAX 100
#define ADC_MAX 65535
#define RAW_MIN 0
#define RAW_MAX 1023

#define MEAS_CYCLES 12
#define MEAS_CYCLES_RGB 6
#define MEAS_CYCLES_AMBIENT 15
#define COL_THS 0.1

typedef struct {
	volatile uint8_t data_ready;
	volatile uint8_t send_data;
	volatile uint8_t change_mode;
} oc_rgb_sensor;

typedef enum {
	S_MODE_WAIT_ACK = -2,
	S_MODE_INIT = -1,
	S_MODE_REFLECT = 0,
	S_MODE_AMBIENT = 1,
	S_MODE_COLOR = 2,
	S_MODE_REF_RAW = 3,
	S_MODE_RGB_RAW = 4,
	S_MODE_REF_RAW_GREEN = 5,
	S_MODE_REF_RAW_BLUE = 6,
	S_MODE_COUNT = 7,
} sensor_mode;

typedef enum {
	NONE = 0,
	BLACK = 1,
	BLUE = 2,
	GREEN = 3,
	YELLOW = 4,
	RED = 5,
	WHITE = 6,
	BROWN = 7,
} colors;
#endif //OC_SENSOR_MODES_H
