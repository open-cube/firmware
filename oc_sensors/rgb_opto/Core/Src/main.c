/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <math.h>
#include "messages.h"
#include "oc_sensor_modes.h"
#include "oc_st_sensor_uart.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define USART_TX LL_GPIO_PIN_9
#define USART_RX LL_GPIO_PIN_10
#define USART_PORT GPIOA
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim17;

/* USER CODE BEGIN PV */
volatile sensor_mode sensor_state = S_MODE_INIT;
extern const sensor_info s_info;

uint8_t rx_buff[EV3_MSG_BUFFER_SIZE];
uint8_t rx_i = 0;
ev3_msg_type msg_type;
unsigned int msg_subtype;
size_t msg_full_len;

oc_rgb_sensor sensor = {};

uint16_t adc_result=0;
uint16_t adc_off=0;
uint16_t red=0;
uint16_t green=0;
uint16_t blue=0;
uint32_t adc_off_sum=0;
uint32_t red_sum=0;
uint32_t green_sum=0;
uint32_t blue_sum=0;
uint16_t red_diff=0;
uint16_t green_diff=0;
uint16_t blue_diff=0;

float red_map = 0.0;
float green_map = 0.0;
float blue_map = 0.0;
float red_pct = 0.0;
float ambient = 0.0;
float h,s,v;

const uint16_t r_min = 0;
const uint16_t r_max = 62000;
const uint16_t g_min = 0;
const uint16_t g_max = 24000;
const uint16_t b_min = 0;
const uint16_t b_max = 53000;

uint8_t color=0;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM17_Init(void);
/* USER CODE BEGIN PFP */
void delay_us(uint16_t us);
void reset_communication(void);
uint16_t meas_light(void);
void red_on(void);
void red_off(void);
void green_on(void);
void green_off(void);
void blue_on(void);
void blue_off(void);
float max(float a, float b, float c);
float min(float a, float b, float c);
void rgb_to_hsv(float r, float g, float b, float* h, float* s, float* v);
uint8_t hsv_to_color(float h, float s, float v);
float MAP(uint16_t au16_IN, uint16_t au16_INmin, uint16_t au16_INmax, uint16_t au16_OUTmin, uint16_t au16_OUTmax);
void measure_data(void);
void choose_send_data(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void delay_us(uint16_t us){
	__HAL_TIM_SET_COUNTER(&htim1,0);  // set the counter value a 0
	while (__HAL_TIM_GET_COUNTER(&htim1) < us);  // wait for the counter to reach the us input in the parameter
}

void reset_communication() {
	LL_USART_DisableIT_RXNE(USART1);
	LL_USART_Disable(USART1);
	LL_GPIO_ResetOutputPin(USART_PORT, USART_TX);
	LL_GPIO_ResetOutputPin(USART_PORT, USART_RX);
	HAL_Delay(500);
	MX_USART1_UART_Init();
	LL_USART_EnableIT_RXNE(USART1);
}

uint16_t meas_light(void){
	HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1,1000);
	return (uint16_t)HAL_ADC_GetValue(&hadc1);
}

void USART1_Rec_Cal() {
	rx_buff[rx_i] = LL_USART_ReceiveData8(USART1);
	if (rx_i == 0) {
		msg_type = type_of_message(rx_buff[0]);
		msg_subtype = subtype_of_message(rx_buff[0]);
		msg_full_len = full_length_of_message(rx_buff[0]);
	}
	if (msg_full_len > EV3_MSG_BUFFER_SIZE) {
		rx_i = 0;
		return;
	}
	switch (msg_type) {
		case MTYPE_SYSTEM:
			if (msg_subtype == MTYPE_SYSTEM_ACK
				&& (sensor_state == S_MODE_WAIT_ACK || sensor_state == S_MODE_INIT)) {
				sensor_state = MODE_DEFAULT;
			} else if (msg_subtype == MTYPE_SYSTEM_NACK && sensor_state > S_MODE_INIT) {
				__HAL_TIM_SET_COUNTER(&htim17, 0);
				sensor.data_ready = 1;
			}
			break;
		case MTYPE_COMMAND:
			if (msg_full_len == rx_i+1 && sensor_state > S_MODE_INIT) {
				uint8_t csum = calculate_message_checksum(rx_buff, rx_i);
				if (csum == rx_buff[rx_i]) {
					if(msg_subtype == MTYPE_COMMAND_SELECT) {
						if (rx_buff[1] != sensor_state
							&& rx_buff[1] > S_MODE_INIT
							&& rx_buff[1] < S_MODE_COUNT) {
							sensor_state = rx_buff[1];
							sensor.change_mode = 1;
						}
					} else if (msg_subtype == MTYPE_COMMAND_WRITE) {
						// no command used
					}
				}
				rx_i = 0;
			} else {
				rx_i++;
			}
			break;
		default:
			rx_i = 0;
	}
}
// all outputs are set as OPEN-COLECTOR
// outputs are directly in parallel, use command to change the status at the same time
// => use GPIOx_BSRR register...
//
//-------------------------------------------------------
// set active low to PB5,6,7,8,9
void red_on(void){
	GPIOB->BSRR |= 0x3E00000;
};
//-------------------------------------------------------
// set PB5,6,7,8,9 to OPEN-COLECTOR
void red_off(void){
	GPIOB->BSRR |= 0x3E0;
};
//--------------------------------------------------------
// set active low to PA2,3,4,5,6
void green_on(void){
	GPIOA->BSRR |= 0x7C0000;
};
//---------------------------------------------------------
// set PA2,3,4,5,6 to OPEN-COLECTOR
void green_off(void){
	GPIOA->BSRR |= 0x7C;
};
//---------------------------------------------------------
//  set active low to PB0,1,2,3,4
void blue_on(void){
	GPIOB->BSRR |= 0x1F0000;
};
//---------------------------------------------------------
// set PB0,1,2,3,4 to OPEN-COLECTOR
void blue_off(void){
	GPIOB->BSRR |= 0x1F;
};


float max(float a, float b, float c) {
   return ((a > b)? (a > c ? a : c) : (b > c ? b : c));
}
float min(float a, float b, float c) {
   return ((a < b)? (a < c ? a : c) : (b < c ? b : c));
}

void rgb_to_hsv(float r, float g, float b, float* h, float* s, float* v) {
   // R, G, B values are divided by 255
   // to change the range from 0..255 to 0..1:
   float cmax = max(r, g, b); // maximum of r, g, b
   float cmin = min(r, g, b); // minimum of r, g, b
   float diff = cmax-cmin; // diff of cmax and cmin.
   if (cmax == cmin)
      *h = 0;
   else if (cmax == r)
      *h = fmod((60 * ((g - b) / diff) + 360), 360.0);
   else if (cmax == g)
      *h = fmod((60 * ((b - r) / diff) + 120), 360.0);
   else if (cmax == b)
      *h = fmod((60 * ((r - g) / diff) + 240), 360.0);
   // if cmax equal zero
      if (cmax == 0)
         *s = 0;
      else
         *s = (diff / cmax) * 100;
   // compute v
   *v = cmax * 100;
}

uint8_t hsv_to_color(float h, float s, float v) {
	if (v < 15) {
		return BLACK;
	} else if (s < 20) {
		return WHITE;
	} else if (h > 160 && h <= 260) {
		return BLUE;
	} else if (h > 70 && h <= 160) {
		return GREEN;
	} else if (h > 40 && h <= 70) {
		return YELLOW;
	} else if (h > 10 && h <= 40) {
		return BROWN;
	} else {
		return RED;
	}
}

float MAP(uint16_t au16_IN, uint16_t au16_INmin, uint16_t au16_INmax, uint16_t au16_OUTmin, uint16_t au16_OUTmax)
{
	if(au16_IN < au16_INmin) {
		return (float) au16_OUTmin;
	}
	if(au16_IN > au16_INmax) {
		return (float) au16_OUTmax;
	}
    return ((((float) (au16_IN - au16_INmin)*(au16_OUTmax - au16_OUTmin))/ ((float) (au16_INmax - au16_INmin)) + au16_OUTmin));
}

void measure_data(void) {
	blue_off();
	green_off();
	red_off();
	switch (sensor_state) {
		case S_MODE_REFLECT:
		case S_MODE_REF_RAW:
			adc_off_sum = 0;
			red_sum = 0;
			for (int i=0; i < MEAS_CYCLES; i++) {
				delay_us(50);
				adc_off_sum += meas_light();
				red_on();
				delay_us(50);
				red_sum += meas_light();
				red_off();
			}

			if (red_sum < adc_off_sum) {
				red_diff = 0;
			} else {
				red_diff = (red_sum - adc_off_sum)/MEAS_CYCLES;
			}
			adc_off = adc_off_sum/MEAS_CYCLES;
			red = red_sum/MEAS_CYCLES;

			red_pct = MAP(red_diff, r_min, r_max, PCT_MIN, PCT_MAX);
			break;
		case S_MODE_REF_RAW_GREEN:
			adc_off_sum = 0;
			green_sum = 0;
			for (int i=0; i < MEAS_CYCLES; i++) {
				delay_us(50);
				adc_off_sum += meas_light();
				green_on();
				delay_us(50);
				green_sum += meas_light();
				green_off();
			}
			adc_off = adc_off_sum/MEAS_CYCLES;
			green = green_sum/MEAS_CYCLES;
			break;
		case S_MODE_REF_RAW_BLUE:
			adc_off_sum = 0;
			blue_sum = 0;
			for (int i=0; i < MEAS_CYCLES; i++) {
				delay_us(50);
				adc_off_sum += meas_light();
				blue_on();
				delay_us(50);
				blue_sum += meas_light();
				blue_off();
			}
			adc_off = adc_off_sum/MEAS_CYCLES;
			blue = blue_sum/MEAS_CYCLES;
			break;
		case S_MODE_AMBIENT:
			adc_off_sum = 0;
			for (int i=0; i < MEAS_CYCLES_AMBIENT; i++) {
				adc_off_sum += meas_light();
				delay_us(10);
			}

			adc_off = adc_off_sum/MEAS_CYCLES_AMBIENT;
			ambient = MAP(adc_off, 0, ADC_MAX, PCT_MIN, PCT_MAX);
			break;
		case S_MODE_COLOR:
		case S_MODE_RGB_RAW:
			adc_off_sum = 0;
			red_sum = 0;
			green_sum = 0;
			blue_sum = 0;
			for (int i=0; i < MEAS_CYCLES_RGB; i++) {
				delay_us(50);
				adc_off_sum += meas_light();
				red_on();
				delay_us(50);
				red_sum += meas_light();
				red_off();
				green_on();
				delay_us(50);
				green_sum += meas_light();
				green_off();
				blue_on();
				delay_us(50);
				blue_sum += meas_light();
				blue_off();
			}
			red_on();
			green_on();
			blue_on();
			red_diff = red_sum / MEAS_CYCLES_RGB;
			green_diff = green_sum / MEAS_CYCLES_RGB;
			blue_diff = blue_sum / MEAS_CYCLES_RGB;
			adc_off = adc_off_sum / MEAS_CYCLES_RGB;

			red_map = MAP(red_diff, r_min, r_max, 0, 1);
			green_map = MAP(green_diff, g_min, g_max, 0, 1);
			blue_map = MAP(blue_diff, b_min, b_max, 0, 1);

			rgb_to_hsv(red_map, green_map, blue_map, &h, &s, &v);
			if (red_map < COL_THS && green_map < COL_THS && blue_map < COL_THS) {
				color = NONE;
			} else {
				color = hsv_to_color(h,s,v);
			}
			break;
		default:
			break;
	}
	sensor.data_ready = 1;
}

void choose_send_data(void) {
	switch (sensor_state) {
		case S_MODE_REFLECT:
			send_data(sensor_state, &red_pct, sizeof(red_pct), USART1);
			break;
		case S_MODE_REF_RAW: {
			uint16_t data[4] = {0};
			data[0] = red;
			data[2] = adc_off;
			send_data(sensor_state, data, 8, USART1);
			break;
		}
		case S_MODE_REF_RAW_GREEN: {
			uint16_t data_g[4] = {0};
			data_g[0] = green;
			data_g[2] = adc_off;
			send_data(sensor_state, data_g, 8, USART1);
			break;
		}
		case S_MODE_REF_RAW_BLUE: {
			uint16_t data_b[4] = {0};
			data_b[0] = blue;
			data_b[2] = adc_off;
			send_data(sensor_state, data_b, 8, USART1);
			break;
		}
		case S_MODE_AMBIENT:
			send_data(sensor_state, &ambient, sizeof(ambient), USART1);
			break;
		case S_MODE_COLOR:
			send_data(sensor_state, &color, sizeof(color), USART1);
			break;
		case S_MODE_RGB_RAW: {
			uint16_t data_rgb[8] = {0};
			data_rgb[0] = red_diff;
			data_rgb[2] = green_diff;
			data_rgb[4] = blue_diff;
			data_rgb[6] = adc_off;
			send_data(sensor_state, data_rgb, 16, USART1);
			break;
		}
		default:
			break;
	}
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_USART1_UART_Init();
  MX_TIM1_Init();
  MX_TIM17_Init();
  /* USER CODE BEGIN 2 */
  red_off();
  green_off();
  blue_off();

  // NACK timer
  	HAL_TIM_Base_Start(&htim17);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	red_off();
	green_on();
	blue_off();
	reset_communication();
	// Initial state - send metadata and wait for ACK
	while(sensor_state == S_MODE_INIT || sensor_state == S_MODE_WAIT_ACK) {
		red_on();
		green_on();
		blue_off();
		send_metadata(&s_info, USART1);
		if (sensor_state == S_MODE_INIT) {
			sensor_state = S_MODE_WAIT_ACK;
		}
		int delay_counter = 0;
		red_off();
		while(sensor_state == S_MODE_WAIT_ACK && delay_counter < EV3_METADATA_DELAY_MS) {
			delay_counter++;
			HAL_Delay(1);
		}
	}

	// Set faster UART communication baud rate and start sending data
	blue_off();
	green_off();
	LL_USART_DisableIT_RXNE(USART1);
	LL_USART_Disable(USART1);
	MX_USART1_UART_Init();
	choose_send_data();
	LL_USART_EnableIT_RXNE(USART1);
	__HAL_TIM_SET_COUNTER(&htim17, 0);
	while (sensor_state > S_MODE_INIT) {
		measure_data();

		// Send data if new data available or when NACK received
		if (sensor.data_ready) {
			sensor.data_ready = 0;
			choose_send_data();
		}


		// Check if NACK was received, if not, switch to initial state - sending metadata
		uint16_t nack_diff = __HAL_TIM_GET_COUNTER(&htim17);
		if (nack_diff > EV3_MAX_NACK_DELAY) {
			sensor_state = S_MODE_INIT;
		}
	}
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV8;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.LowPowerAutoPowerOff = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.SamplingTimeCommon1 = ADC_SAMPLETIME_39CYCLES_5;
  hadc1.Init.SamplingTimeCommon2 = ADC_SAMPLETIME_39CYCLES_5;
  hadc1.Init.OversamplingMode = ENABLE;
  hadc1.Init.Oversampling.Ratio = ADC_OVERSAMPLING_RATIO_16;
  hadc1.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_NONE;
  hadc1.Init.Oversampling.TriggeredMode = ADC_TRIGGEREDMODE_SINGLE_TRIGGER;
  hadc1.Init.TriggerFrequencyMode = ADC_TRIGGER_FREQ_HIGH;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 16-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 65535;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */
  HAL_TIM_Base_Start(&htim1);
  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM17 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM17_Init(void)
{

  /* USER CODE BEGIN TIM17_Init 0 */

  /* USER CODE END TIM17_Init 0 */

  /* USER CODE BEGIN TIM17_Init 1 */

  /* USER CODE END TIM17_Init 1 */
  htim17.Instance = TIM17;
  htim17.Init.Prescaler = 16000-1;
  htim17.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim17.Init.Period = 65535;
  htim17.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim17.Init.RepetitionCounter = 0;
  htim17.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim17) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM17_Init 2 */

  /* USER CODE END TIM17_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */
	uint32_t baud_rate = s_info.baud_rate;
	  if (sensor_state == S_MODE_INIT || sensor_state == S_MODE_WAIT_ACK) {
		  baud_rate = EV3_UART_HANDSHAKE_BAUD;
	  }
  /* USER CODE END USART1_Init 0 */

  LL_USART_InitTypeDef USART_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_SYSCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

  /* Peripheral clock enable */
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);

  LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
  /**USART1 GPIO Configuration
  PA9   ------> USART1_TX
  PA10   ------> USART1_RX
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_9;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_MEDIUM;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_10;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_MEDIUM;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USART1 interrupt Init */
  NVIC_SetPriority(USART1_IRQn, 2);
  NVIC_EnableIRQ(USART1_IRQn);

  /* USER CODE BEGIN USART1_Init 1 */
  /* USER CODE END USART1_Init 1 */
  USART_InitStruct.PrescalerValue = LL_USART_PRESCALER_DIV1;
  USART_InitStruct.BaudRate = baud_rate;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
  LL_USART_Init(USART1, &USART_InitStruct);
  LL_USART_SetTXFIFOThreshold(USART1, LL_USART_FIFOTHRESHOLD_1_8);
  LL_USART_SetRXFIFOThreshold(USART1, LL_USART_FIFOTHRESHOLD_1_8);
  LL_USART_DisableFIFO(USART1);
  LL_USART_ConfigAsyncMode(USART1);

  /* USER CODE BEGIN WKUPType USART1 */

  /* USER CODE END WKUPType USART1 */

  LL_USART_Enable(USART1);

  /* Polling USART1 initialisation */
  while((!(LL_USART_IsActiveFlag_TEACK(USART1))) || (!(LL_USART_IsActiveFlag_REACK(USART1))))
  {
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, RED1_Pin|BLUE3_Pin|BLUE4_Pin|BLUE5_Pin
                          |BLUE2_Pin|BLUE1_Pin|RED5_Pin|RED4_Pin
                          |RED3_Pin|RED2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GREEN1_Pin|GREEN2_Pin|GREEN3_Pin|GREEN4_Pin
                          |GREEN5_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : RED1_Pin BLUE3_Pin BLUE4_Pin BLUE5_Pin
                           BLUE2_Pin BLUE1_Pin RED5_Pin RED4_Pin
                           RED3_Pin RED2_Pin */
  GPIO_InitStruct.Pin = RED1_Pin|BLUE3_Pin|BLUE4_Pin|BLUE5_Pin
                          |BLUE2_Pin|BLUE1_Pin|RED5_Pin|RED4_Pin
                          |RED3_Pin|RED2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : GREEN1_Pin GREEN2_Pin GREEN3_Pin GREEN4_Pin
                           GREEN5_Pin */
  GPIO_InitStruct.Pin = GREEN1_Pin|GREEN2_Pin|GREEN3_Pin|GREEN4_Pin
                          |GREEN5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
