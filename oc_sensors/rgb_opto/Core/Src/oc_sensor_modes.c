/*
 * Open-Cube sensor mode definitions
 * oc_sensor_modes.c
 *
 *  Created on: Jul 2, 2024
 *      Author: jelinva4
 */

#include "oc_sensor_modes.h"

#include "oc_st_sensor_uart.h"
#include "messages.h"

const ev3_mode_info m_reflect = {S_MODE_REFLECT,
							    "COL-REFLECT", 11,
							    0, 100,
							    0, 0,
							    0, 0,
							    "pct", 3,
							    1, 3, 6, 3};

const ev3_mode_info m_ambient = {S_MODE_AMBIENT,
							    "COL-AMBIENT", 11,
							    0, 100,
							    0, 0,
							    0, 0,
							    "pct", 3,
							    1, 3, 6, 3};

const ev3_mode_info m_color = {S_MODE_COLOR,
							    "COL-COLOR", 9,
							    0, 8,
							    0, 0,
							    0, 0,
							    "col", 3,
							    1, 0, 1, 0};

const ev3_mode_info m_reflect_raw = {S_MODE_REF_RAW,
							    "REF-RAW", 7,
							    0, ADC_MAX,
							    0, 0,
							    0, 0,
							    "", 0,
							    2, 2, 5, 0};

const ev3_mode_info m_rgb_raw = {S_MODE_RGB_RAW,
							    "RGB-RAW", 7,
							    0, ADC_MAX,
							    0, 0,
							    0, 0,
							    "", 0,
							    4, 2, 5, 5};

const ev3_mode_info m_reflect_raw_green = {S_MODE_REF_RAW_GREEN,
							    "REF-RAW-G", 9,
							    0, ADC_MAX,
							    0, 0,
							    0, 0,
							    "", 0,
							    2, 2, 5, 0};

const ev3_mode_info m_reflect_raw_blue = {S_MODE_REF_RAW_BLUE,
							    "REF-RAW-B", 9,
							    0, ADC_MAX,
							    0, 0,
							    0, 0,
							    "", 0,
							    2, 2, 5, 0};

const sensor_info s_info = {SENSOR_ID, S_MODE_COUNT, S_MODE_COUNT, DATA_BAUD_RATE,
							{m_reflect, m_ambient, m_color, m_reflect_raw, m_rgb_raw, m_reflect_raw_green, m_reflect_raw_blue}};

