# Automatické sestavování firmware Open-Cube senzorů

GitLab CI sestavuje kromě MicroPythonu také firmwary Open-Cube senzorů.

## Jak to funguje

V souboru `.gitlab-ci.yml`, který GitLab používá pro určení sestavovacích
kroků, se nachází instrukce pro spuštění `makefile.ci` v tomto adresáři.
Ten spustí `makefile.ci` v podadresářích, která provedou samotné sestavení.
Výsledný firmware se pak zkopíruje do vhodné složky pro publikování na GitLab.

Soubory `makefile.ci` v projektových podadresářích jsou vytvořené pomocí
STM32CubeMX -- ten dovoluje vygenerovat kromě STM32CubeIDE projektu i
Makefile projekt. Soubory poté byly mírně upraveny tak, aby byly kompatibilní
s IDE projektem.

## Kde lze firmware stáhnout

Nejnovější verzi z větve `main` lze stáhnout zde:

 - LIDAR laserový dálkoměr (STM32G030K8): [ELF][1], [BIN][2]
 - RGB optický senzor (STM32G030K8): [ELF][3], [BIN][4]
 - Ultrazvukový dálkoměr (STM32G030K8): [ELF][5], [BIN][6]
 - AHRS gyroskop, akcelerometr, magnetometr (STM32G030K8): [ELF][7], [BIN][8]

[1]: https://gitlab.fel.cvut.cz/open-cube/firmware/builds/artifacts/main/raw/oc_sensors/laser_distance.elf?job=build%20sensor%20firmwares
[2]: https://gitlab.fel.cvut.cz/open-cube/firmware/builds/artifacts/main/raw/oc_sensors/laser_distance.bin?job=build%20sensor%20firmwares
[3]: https://gitlab.fel.cvut.cz/open-cube/firmware/builds/artifacts/main/raw/oc_sensors/rgb_opto.elf?job=build%20sensor%20firmwares
[4]: https://gitlab.fel.cvut.cz/open-cube/firmware/builds/artifacts/main/raw/oc_sensors/rgb_opto.bin?job=build%20sensor%20firmwares
[5]: https://gitlab.fel.cvut.cz/open-cube/firmware/builds/artifacts/main/raw/oc_sensors/ultrasonic.elf?job=build%20sensor%20firmwares
[6]: https://gitlab.fel.cvut.cz/open-cube/firmware/builds/artifacts/main/raw/oc_sensors/ultrasonic.bin?job=build%20sensor%20firmwares
[7]: https://gitlab.fel.cvut.cz/open-cube/firmware/builds/artifacts/main/raw/oc_sensors/gyro.elf?job=build%20sensor%20firmwares
[8]: https://gitlab.fel.cvut.cz/open-cube/firmware/builds/artifacts/main/raw/oc_sensors/gyro.bin?job=build%20sensor%20firmwares
