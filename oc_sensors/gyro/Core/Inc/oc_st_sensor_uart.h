/**
 *  Open-Cube STM helper functions for sending EV3 UART protocol messages
 */
#ifndef OC_ST_SENSOR_UART_H
#define OC_ST_SENSOR_UART_H
#include "stm32g0xx_ll_usart.h"
#include "messages.h"

#define MODE_DEFAULT 0

typedef struct {
	uint8_t type;
	uint8_t num_modes;
	uint8_t num_visible_modes;
	uint32_t baud_rate;
	ev3_mode_info modes[EV3_MAX_MODES];
} sensor_info;

extern void uart_send_length(const void *data, uint16_t length, USART_TypeDef* usart);
extern void send_metadata(const sensor_info* s_info, USART_TypeDef* usart);
extern void send_data(int sensor_state, void* data, size_t size, USART_TypeDef* usart);

#endif //OC_ST_SENSOR_UART_H
