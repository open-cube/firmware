/**
 *  Open-Cube sensor mode definitions
 */
#ifndef OC_SENSOR_MODES_H
#define OC_SENSOR_MODES_H

#define SENSOR_ID 195
#define DATA_BAUD_RATE 256000
#define RESET_YAW_STRING "RESET-ANGLES"
#define RESET_YAW_SIZE 12

typedef enum {
	S_MODE_WAIT_ACK = -2,
	S_MODE_INIT = -1,
	S_MODE_EULER = 0,
	S_MODE_EULER_MAG,
	S_MODE_QUATERNION,
	S_MODE_RAW,
	S_MODE_GYRO_CALIB,
	S_MODE_MAG_CALIB,
	S_MODE_COUNT
} sensor_mode;

#endif //OC_SENSOR_MODES_H
