/*
 * oc_gyro.h
 *
 *  Created on: Jul 1, 2024
 *      Author: jelinva4
 */

#ifndef INC_OC_GYRO_H_
#define INC_OC_GYRO_H_

#include "Fusion.h"
#include "oc_sensor_modes.h"


#define FLASH_GYRO_CALIB_START_ADD 0x0800F800
#define FLASH_MEM_CONTROL_NUM 12345
 /* MAG I2C address and register addresses */
#define MAG_I2C_ADDR			(uint16_t)(0x0D<<1)
#define MAG_DATA_START_REG		0x00
#define MAG_CTRL_REG1			0x09
#define MAG_CTRL_REG2			0x0A
#define MAG_SETRES_REG			0x0B

/* MAG I2C register values */
#define MAG_MODE_CONTINUOUS		0x01
#define MAG_ODR_100HZ			0x08
#define MAG_ODR_200HZ			0x0C

#define MAG_RNG_8G				0x10
#define MAG_RNG_2G				0x00

#define MAG_OSR_64				0xC0

#define MAG_SOFT_RESET			0x80
#define MAG_PTR_ROLLOVER		0x40
#define MAG_INT_ENABLE 			0x00
#define MAG_INT_DISABLE			0x01

#define MAG_SETRES_VAL			0x01

/* ICM SPI register values */
#define ICM_REG_WRITE			0x00
#define ICM_REG_READ			0x80

#define ICM_DEVICE_CONFIG_REG	0x11
#define ICM_CONFIG				0x14
#define ICM_PWR_MGMT0			0x4E
#define ICM_GYRO_CONFIG0		0x4F
#define ICM_ACCEL_CONFIG0		0x50
#define ICM_INT_CONFIG1			0x64
#define ICM_INT_SOURCE0			0x65

#define ICM_RESET				0x01
#define ICM_INT_PP_AL			0x12	/* INT1&INT2: push-pull, active-low */
#define ICM_GYRO_LN_MODE		0x0C
#define ICM_ACCEL_LN_MODE		0x03

#define ICM_GYRO_2000_FS		0x00
#define ICM_GYRO_1000_FS		0x01
#define ICM_GYRO_500_FS			0x02
#define ICM_GYRO_250_FS			0x03
#define ICM_GYRO_125_FS			0x04
#define ICM_GYRO_62_5_FS		0x05

#define ICM_GYRO_100HZ_ODR		0x08
#define ICM_GYRO_200HZ_ODR		0x07
#define ICM_GYRO_500HZ_ODR		0x0F
#define ICM_GYRO_1000HZ_ODR		0x06
#define ICM_GYRO_2000HZ_ODR		0x05

#define ICM_ACCEL_16_FS			0x00
#define ICM_ACCEL_8_FS			0x01
#define ICM_ACCEL_4_FS			0x02
#define ICM_ACCEL_2_FS			0x03

#define ICM_ACCEL_100HZ_ODR		0x08
#define ICM_ACCEL_200HZ_ODR		0x07
#define ICM_ACCEL_500HZ_ODR		0x0F
#define ICM_ACCEL_1000HZ_ODR	0x06
#define ICM_ACCEL_2000HZ_ODR	0x05

#define ICM_INT_DUR_LONG		0x00	/* NOTE: Changes if ODR > 4kHz */

#define ICM_INT1_DRDY			0x08

/* Range constants */
#define ACCEL_LSB_PER_G 		2048	/* NOTE: Changes with FS! */
#define GYRO_LSB_PER_DPS 		16.4	/* NOTE: Changes with FS! */
#define GYRO_LSB_PER_DPS_2 		32.8	/* NOTE: Changes with FS! */
#define ACCEL_LSB_PER_G_2 		4096	/* NOTE: Changes with FS! */

/* Other constants */
#define SAMPLE_RATE 			200		/* For 200Hz ODR of GYRO */
#define GYRO_CALIB_CYCLES 		(SAMPLE_RATE*2)
#define MAG_CALIB_MINMAX_THRS 10000
#define MAG_CALIB_POS_THRS 0.9
#define MAG_CALIB_POS_COUNT 6

 /* Bitfield holding all status variables */
typedef struct {
	/* Should data be read/written now? */
	uint8_t Should_Send_UART : 1;
	/* Variables to determine interval between gyroscope reads and if gyro data
	 * is available. Gyro availability triggers Fusion calculations... */
	uint8_t Gyro_Data_Available : 1;
	uint8_t Gyro_Timer_Running : 1;
	uint8_t Gyro_Calibrated : 1;
	uint8_t Mag_Calibrated : 1;
	uint8_t Reset_Angles : 1;
	float Gyro_Time_Delta;
	uint32_t Measure_Count;
	uint32_t Gyro_Count;
	uint32_t Gyro_Calib_Count;

} oc_gyro_status; 

/* Struct holding sensor output data */
typedef struct {
	/* MAG = { x_LSB, x_MSB, y_LSB, y_MSB, z_LSB, z_MSB, Status } */
	uint8_t MAG[7];

	/* ICM = {
	 * 	Empty, <-- First byte is empty!
	 * 	Ac_x_MSB, Ac_x_LSB, Ac_y_MSB, Ac_y_LSB, Ac_z_MSB, Ac_z_LSB
	 *  Gy_x_MSB, Gy_x_LSB, Gy_y_MSB, Gy_y_LSB, Gy_z_MSB, Gy_z_LSB
	 * }
	 * */
	uint8_t ICM[13];

} oc_gyro_data;

typedef struct {
	int16_t axes[3];
	int16_t min[3];
	int16_t max[3];
	uint8_t pos[MAG_CALIB_POS_COUNT];
} oc_gyro_calibration;

typedef struct {
	FusionQuaternion quaternion;
	FusionEuler euler;
} oc_gyro_fusion;

/* Register and value pair type */
typedef struct {
	uint8_t reg;
	uint8_t val;
} Reg_Val_Type;

/* Function prototypes */
void oc_gyro_init(void);
void oc_gyro_fusion_reset(void);
void oc_gyro_fusion_step(sensor_mode sensor_state);
uint8_t oc_gyro_choose_data(uint16_t* data_send, sensor_mode sensor_state);
void oc_gyro_flash_save_calib(float x, float y, float z, float xm, float ym, float zm);
void oc_gyro_flash_load_calib(void);
void oc_gyro_read_mag_data(void);
void oc_gyro_read_icm_data(void);
void oc_gyro_read_icm_done(void);
#endif /* INC_OC_GYRO_H_ */
