/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"
#include "stm32g0xx_ll_usart.h"
#include "stm32g0xx_ll_rcc.h"
#include "stm32g0xx_ll_bus.h"
#include "stm32g0xx_ll_cortex.h"
#include "stm32g0xx_ll_system.h"
#include "stm32g0xx_ll_utils.h"
#include "stm32g0xx_ll_pwr.h"
#include "stm32g0xx_ll_gpio.h"
#include "stm32g0xx_ll_dma.h"

#include "stm32g0xx_ll_exti.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ICM_CS_Pin GPIO_PIN_4
#define ICM_CS_GPIO_Port GPIOA
#define ICM_SCK_Pin GPIO_PIN_5
#define ICM_SCK_GPIO_Port GPIOA
#define ICM_SDO_Pin GPIO_PIN_6
#define ICM_SDO_GPIO_Port GPIOA
#define ICM_SDI_Pin GPIO_PIN_7
#define ICM_SDI_GPIO_Port GPIOA
#define ICM_INT2_Pin GPIO_PIN_1
#define ICM_INT2_GPIO_Port GPIOB
#define LED_G_Pin GPIO_PIN_2
#define LED_G_GPIO_Port GPIOB
#define LED_R_Pin GPIO_PIN_8
#define LED_R_GPIO_Port GPIOA
#define ICM_INT1_Pin GPIO_PIN_6
#define ICM_INT1_GPIO_Port GPIOC
#define ICM_INT1_EXTI_IRQn EXTI4_15_IRQn
#define QMC_INT_Pin GPIO_PIN_15
#define QMC_INT_GPIO_Port GPIOA
#define QMC_INT_EXTI_IRQn EXTI4_15_IRQn

/* USER CODE BEGIN Private defines */
#define RED_on() HAL_GPIO_WritePin(LED_R_GPIO_Port,  LED_R_Pin, SET)
#define RED_off() HAL_GPIO_WritePin(LED_R_GPIO_Port,  LED_R_Pin, RESET)
#define GREEN_on() HAL_GPIO_WritePin(LED_G_GPIO_Port,  LED_G_Pin, SET)
#define GREEN_off() HAL_GPIO_WritePin(LED_G_GPIO_Port,  LED_G_Pin, RESET)
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
