/*
 * oc_gyro.c
 *
 *  Created on: Jul 1, 2024
 *      Author: jelinva4
 */

#include "main.h"
#include "oc_gyro.h"

#define max(a,b) \
({ __typeof__ (a) _a = (a); \
   __typeof__ (b) _b = (b); \
 _a > _b ? _a : _b; })

#define min(a,b) \
({ __typeof__ (a) _a = (a); \
   __typeof__ (b) _b = (b); \
 _a > _b ? _b : _a; })

extern I2C_HandleTypeDef hi2c2;
extern SPI_HandleTypeDef hspi1;
extern TIM_HandleTypeDef htim14;

/* Fusion-algo variables */
FusionAhrs ahrs;
FusionVector latest_gyro_cal;
FusionVector latest_mag;
FusionVector latest_acc;
FusionOffset offset;

volatile oc_gyro_status Status = {0};

oc_gyro_data Data = {
	.MAG = {},
	.ICM = {}
};
oc_gyro_fusion Fusion = {
	.quaternion = {},
	.euler = {}
};

oc_gyro_calibration HardIronCal = {
	.axes = {260, 929, -610},
	.min = {MAG_CALIB_MINMAX_THRS, MAG_CALIB_MINMAX_THRS, MAG_CALIB_MINMAX_THRS},
	.max = {-MAG_CALIB_MINMAX_THRS, -MAG_CALIB_MINMAX_THRS, -MAG_CALIB_MINMAX_THRS},
	.pos = {0}
};

FusionVector GyroCalSum = {0};
FusionVector GyroCal = {0};
FusionQuaternion ResetQuat = FUSION_IDENTITY_QUATERNION;

static void Write_To_SPI1_Sync(uint8_t reg_addr, uint8_t* p_write,
	const uint8_t bytes_to_write);
static void Read_From_SPI1_Async(uint8_t reg_addr, uint8_t* p_read,
	const uint8_t bytes_to_read);
static float Get_Accel_Value_In_G(uint8_t* ptr);
static float Get_Gyro_Value_In_DPS(uint8_t* ptr);
static void Set_MAG_Register(Reg_Val_Type reg_val);
static void Set_ICM_Register(Reg_Val_Type reg_val);
static uint16_t data_to_prec_uint(float num, int mult);

void oc_gyro_init() {
	/*
   * To initialize magnetometer:
   * 1. Soft reset for repeatability
   * -> Set 0x0A to 0x80
   *
   * 2. Set control register 1 (0x09) to:
   * -> MODE:  		01 (for continuous) 		- bits 1 and 0
   * -> ODR:   		10 (for 100Hz output freq.) - bits 3 and 2
   * -> RNG:   		01 (for 8G FS) 				- bits 5 and 4
   * -> OSR:   		11 (for 64 oversample) 		- bits 7 and 6
   * -> Together 	11011101					- 0xD9
   *
   * 3. Set control register 2 (0x0A) to:
   * -> ROL_PNT: 0x01 to enable pointer rollover
   * -> INT_ENB: 0x00 to enable interrupt
   *
   * 4. Set register 0x0B to:
   * -> 0x01 (recommended in datasheet)
   *
   * 5. Set address pointer (for reading in loop) to:
   * -> 0x00 (x_LSB)
   */

	Reg_Val_Type MAG_Reset = {
		.reg = MAG_CTRL_REG2,
		.val = MAG_SOFT_RESET
	};

	Reg_Val_Type MAG_Settings[3] = { {
			.reg = MAG_SETRES_REG,
			.val = MAG_SETRES_VAL
		},
	  {.reg = MAG_CTRL_REG1,
		//	  .val = MAG_OSR_64 | MAG_RNG_8G | MAG_ODR_100HZ | MAG_MODE_CONTINUOUS /* ODR = 100Hz */
			  .val = MAG_OSR_64 | MAG_RNG_8G | MAG_ODR_200HZ | MAG_MODE_CONTINUOUS /* ODR = 200Hz */
		/* NOTE: MAG sensor cannot go higher than 200Hz */
	}, {
		.reg = MAG_CTRL_REG2,
		.val = MAG_PTR_ROLLOVER | MAG_INT_ENABLE
	}, };

	Set_MAG_Register(MAG_Reset);

	HAL_Delay(10);							/* Delay after reset */

	for (uint8_t i = 0; i < 3; ++i) {
		Set_MAG_Register(MAG_Settings[i]);
	}

	/* Set register pointer to data to prepare for loops */
	uint8_t addr_of_data = 0x00;
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c2, (uint16_t)(13 << 1),
		&addr_of_data, 1, 100)) {
		//Error_Handler();
	}

	/*
	 * To initialize accelerometer/gyroscope:
	 * -> All registers here are in register bank 0
	 *
	 * 1. Soft reset for repeatability
	 * 2. INT_CONFIG
	 * 3. PWR_MGMT0 to turn on accelerometer and gyro
	 * 4. GYRO_CONFIG0 to configure gyro
	 * 5. ACCEL_CONFIG0 to configure accel
	 * 6. INT_CONFIG1 set bit 4 to 0 for interrupts to work
	 * 7. INT_SOURCE0 to set source of INT1 to DRDY
	 */

	Reg_Val_Type ICM_Reset = {
		.reg = ICM_DEVICE_CONFIG_REG | ICM_REG_WRITE,
		.val = ICM_RESET
	};

	Reg_Val_Type ICM_Settings[] = { {
		.reg = ICM_CONFIG,
		.val = ICM_INT_PP_AL
	}, {
		.reg = ICM_PWR_MGMT0,
		.val = ICM_GYRO_LN_MODE | ICM_ACCEL_LN_MODE
	}, {
		.reg = ICM_GYRO_CONFIG0,
		//	  .val = ICM_GYRO_2000_FS | ICM_GYRO_100HZ_ODR /* ODR = 100Hz */
			  .val = (ICM_GYRO_1000_FS << 5) | ICM_GYRO_200HZ_ODR /* ODR = 200Hz */
			  //	  .val = ICM_GYRO_2000_FS | ICM_GYRO_500HZ_ODR /* ODR = 500Hz */
				}, {
					.reg = ICM_ACCEL_CONFIG0,
					//	  .val = ICM_ACCEL_16_FS | ICM_ACCEL_100HZ_ODR /* ODR = 100Hz */
						  .val = (ICM_ACCEL_8_FS << 5) | ICM_ACCEL_200HZ_ODR /* ODR = 200Hz */
						  //	  .val = ICM_ACCEL_16_FS | ICM_ACCEL_500HZ_ODR /* ODR = 500Hz */
							}, {
								.reg = ICM_INT_CONFIG1,
								.val = ICM_INT_DUR_LONG
							}, {
								.reg = ICM_INT_SOURCE0,
								.val = ICM_INT1_DRDY
							} };

	Set_ICM_Register(ICM_Reset);

	HAL_Delay(10);							/* Delay after reset */

	for (uint8_t i = 0; i < 6; ++i) {
		Set_ICM_Register(ICM_Settings[i]);
	}
	/* AHRS Setup */
	FusionOffsetInitialise(&offset, SAMPLE_RATE);
	FusionAhrsInitialise(&ahrs);				/* NOTE: Changes with FS! */
}

void oc_gyro_fusion_reset(void) {
	FusionAhrsReset(&ahrs);
	ResetQuat = FUSION_IDENTITY_QUATERNION;
}

void oc_gyro_fusion_step(sensor_mode sensor_state) {
	FusionVector accelerometer = { .axis = {
				.x = Get_Accel_Value_In_G(Data.ICM + 1),
				.y = Get_Accel_Value_In_G(Data.ICM + 3),
				.z = Get_Accel_Value_In_G(Data.ICM + 5)
		} };

	FusionVector gyroscope = { .axis = {
			.x = Get_Gyro_Value_In_DPS(Data.ICM + 7),
			.y = Get_Gyro_Value_In_DPS(Data.ICM + 9),
			.z = Get_Gyro_Value_In_DPS(Data.ICM + 11)
	} };

	/* Magnetometer readings can be in arbitrary units... */
	/* First casting to int16_t and then to float is needed! */
	FusionVector magnetometer = { .axis = {
			.x = (float)(int16_t)(((Data.MAG[1] << 8) | Data.MAG[0])),
			.y = (float)(int16_t)(((Data.MAG[3] << 8) | Data.MAG[2])),
			.z = (float)(int16_t)(((Data.MAG[5] << 8) | Data.MAG[4]))
	} };

	/* Update gyroscope offset */
	if (sensor_state == S_MODE_GYRO_CALIB) {
		if (!Status.Gyro_Calibrated) {
			for (int i=0; i < 3; i++) {
				GyroCalSum.array[i] += gyroscope.array[i];
			}
			Status.Gyro_Calib_Count++;
			if (Status.Gyro_Calib_Count >= GYRO_CALIB_CYCLES) {
				Status.Gyro_Calibrated = 1;
				Status.Gyro_Calib_Count = 0;
				FusionAhrsReset(&ahrs);
				FusionOffsetInitialise(&offset, SAMPLE_RATE);
				for (int i=0; i < 3; i++) {
					GyroCal.array[i] = GyroCalSum.array[i] / GYRO_CALIB_CYCLES;
					GyroCalSum.array[i] = 0;
				}
				oc_gyro_flash_save_calib(GyroCal.axis.x, GyroCal.axis.y, GyroCal.axis.z,
						HardIronCal.axes[0], HardIronCal.axes[1], HardIronCal.axes[2]);
			}
		}
	} else {
		for (int i=0; i < 3; i++) {
			gyroscope.array[i] -= GyroCal.array[i];
		}
		gyroscope = FusionOffsetUpdate(&offset, gyroscope);
		latest_gyro_cal = gyroscope;
	}

	if (sensor_state == S_MODE_MAG_CALIB) {
		if (!Status.Mag_Calibrated) {
			for (int i=0; i < 3; i++) {
				HardIronCal.max[i] = max(HardIronCal.max[i], magnetometer.array[i]);
				HardIronCal.min[i] = min(HardIronCal.min[i], magnetometer.array[i]);
			}
			FusionVector earth_acc = FusionAhrsGetEarthAcceleration(&ahrs);
			for (int i=0; i < 3; i++) {
				if (accelerometer.array[i] > MAG_CALIB_POS_THRS) {
					HardIronCal.pos[2*i] = 1;
				}
				if (accelerometer.array[i] < -MAG_CALIB_POS_THRS) {
					HardIronCal.pos[2*i + 1] = 1;
				}
			}
			uint8_t pos_count = 0;
			for (int i=0; i < MAG_CALIB_POS_COUNT; i++) {
				if (HardIronCal.pos[i]) {
					pos_count++;
				}
			}
			if (pos_count == MAG_CALIB_POS_COUNT) {
				Status.Mag_Calibrated = 1;
				for (int i=0; i < 3; i++) {
					HardIronCal.axes[i] = (HardIronCal.max[i] + HardIronCal.min[i])/2;
				}
				//reset arrays for next calib
				for (int i=0; i < 3; i++) {
					HardIronCal.max[i] = -MAG_CALIB_MINMAX_THRS;
					HardIronCal.min[i] = MAG_CALIB_MINMAX_THRS;
				}
				for (int i=0; i < MAG_CALIB_POS_COUNT; i++) {
					HardIronCal.pos[i] = 0;
				}
				oc_gyro_flash_save_calib(GyroCal.axis.x, GyroCal.axis.y, GyroCal.axis.z,
						HardIronCal.axes[0], HardIronCal.axes[1], HardIronCal.axes[2]);
			}
		}
	} else {
		for (int i=0; i < 3; i++) {
			magnetometer.array[i] -= HardIronCal.axes[i];
		}

	}

	/* Update fusion estimation
	 * (Status.Gyro_Time_Delta is set in ICM data availability interrupt)
	 * */
	if (sensor_state == S_MODE_EULER_MAG) {
		FusionAhrsUpdate(&ahrs, gyroscope, accelerometer, magnetometer,
			Status.Gyro_Time_Delta);
	}
	else {
		FusionAhrsUpdateNoMagnetometer(&ahrs, gyroscope, accelerometer,
				Status.Gyro_Time_Delta);
	}
	if (Status.Reset_Angles) {
		Status.Reset_Angles = 0;
		ResetQuat.array[0] = ahrs.quaternion.array[0];
		ResetQuat.array[1] = -ahrs.quaternion.array[1];
		ResetQuat.array[2] = -ahrs.quaternion.array[2];
		ResetQuat.array[3] = -ahrs.quaternion.array[3];

	}
	/* Get quaternion */
	FusionQuaternion q = FusionAhrsGetQuaternion(&ahrs);
	q = FusionQuaternionMultiply(q, ResetQuat);
	FusionQuaternionNormalise(q);
	const FusionEuler euler = FusionQuaternionToEuler(q);
	latest_acc = accelerometer;
	latest_mag = magnetometer;
	memcpy(&Fusion.quaternion, &q, sizeof(FusionQuaternion));
	memcpy(&Fusion.euler, &euler, sizeof(FusionEuler));
}

uint8_t oc_gyro_choose_data(uint16_t* data_send, sensor_mode sensor_state) {
	uint8_t data_len = 0;
	switch (sensor_state) {
		case S_MODE_EULER:
		case S_MODE_EULER_MAG:
			data_send[0] = data_to_prec_uint(Fusion.euler.angle.yaw, 10);
			data_send[1] = data_to_prec_uint(Fusion.euler.angle.roll, 10);//swap roll and pitch to match actual orientation
			data_send[2] = data_to_prec_uint(Fusion.euler.angle.pitch, 10);
			data_len = 6;
			break;
		case S_MODE_QUATERNION:
			data_send[0] = data_to_prec_uint(Fusion.quaternion.element.w, 10000);
			data_send[1] = data_to_prec_uint(Fusion.quaternion.element.x, 10000);
			data_send[2] = data_to_prec_uint(Fusion.quaternion.element.y, 10000);
			data_send[3] = data_to_prec_uint(Fusion.quaternion.element.z, 10000);
			data_len = 8;
			break;
		case S_MODE_RAW:
			data_send[0] = data_to_prec_uint(latest_gyro_cal.axis.x, 1);
			data_send[1] = data_to_prec_uint(latest_gyro_cal.axis.y, 1);
			data_send[2] = data_to_prec_uint(latest_gyro_cal.axis.z, 1);
			data_send[3] = data_to_prec_uint(latest_acc.axis.x, 1000);
			data_send[4] = data_to_prec_uint(latest_acc.axis.y, 1000);
			data_send[5] = data_to_prec_uint(latest_acc.axis.z, 1000);
			data_send[6] = data_to_prec_uint(latest_mag.axis.x, 1);
			data_send[7] = data_to_prec_uint(latest_mag.axis.y, 1);
			data_send[8] = data_to_prec_uint(latest_mag.axis.z, 1);
			data_len = 18;
			break;
		case S_MODE_GYRO_CALIB:
			data_send[0] = Status.Gyro_Calibrated;
			data_len = 2;
			break;
		case S_MODE_MAG_CALIB:
			data_send[0] = Status.Mag_Calibrated;
			data_len = 2;
			break;
		default:
			break;
	}
	return data_len;
}

void oc_gyro_flash_load_calib(void) {
	// Define pointers to the memory locations where data was stored
	uint64_t calib_data1 = *(uint64_t *)FLASH_GYRO_CALIB_START_ADD;
	uint64_t calib_data2 = *(uint64_t *)(FLASH_GYRO_CALIB_START_ADD + 8);
	uint64_t calib_data_m1 = *(uint64_t *)(FLASH_GYRO_CALIB_START_ADD+16);
	uint64_t calib_data_m2 = *(uint64_t *)(FLASH_GYRO_CALIB_START_ADD + 24);

	float x, y, z, xm, ym, zm;

	uint32_t x_bits = (uint32_t)(calib_data1 >> 32);
	uint32_t y_bits = (uint32_t)(calib_data1 & 0xFFFFFFFF);
	uint32_t z_bits = (uint32_t)(calib_data2 >> 32);
	uint32_t control_num = (uint32_t)(calib_data2 & 0xFFFFFFFF);
	uint32_t xm_bits = (uint32_t)(calib_data_m1 >> 32);
	uint32_t ym_bits = (uint32_t)(calib_data_m1 & 0xFFFFFFFF);
	uint32_t zm_bits = (uint32_t)(calib_data_m2 >> 32);
	uint32_t control_num_m = (uint32_t)(calib_data_m2 & 0xFFFFFFFF);
	// Convert the bit patterns back to float
	memcpy(&x, &x_bits, sizeof(x));
	memcpy(&y, &y_bits, sizeof(y));
	memcpy(&z, &z_bits, sizeof(z));
	memcpy(&xm, &xm_bits, sizeof(xm));
	memcpy(&ym, &ym_bits, sizeof(ym));
	memcpy(&zm, &zm_bits, sizeof(zm));

	if (control_num == FLASH_MEM_CONTROL_NUM && control_num_m == FLASH_MEM_CONTROL_NUM) {
		GyroCal.axis.x = x;
		GyroCal.axis.y = y;
		GyroCal.axis.z = z;
		HardIronCal.axes[0] = xm;
		HardIronCal.axes[1] = ym;
		HardIronCal.axes[2] = zm;
	} else {
		oc_gyro_flash_save_calib(0, 0, 0, 0, 0, 0);
	}


	//FLASH_CALIB_START_ADD
}

void oc_gyro_flash_save_calib(float x, float y, float z, float xm, float ym, float zm) {
	uint32_t x_bits, y_bits, z_bits, xm_bits, ym_bits, zm_bits;
	memcpy(&x_bits, &x, sizeof(x));
	memcpy(&y_bits, &y, sizeof(y));
	memcpy(&z_bits, &z, sizeof(z));
	memcpy(&xm_bits, &xm, sizeof(xm));
	memcpy(&ym_bits, &ym, sizeof(ym));
	memcpy(&zm_bits, &zm, sizeof(zm));
	uint64_t calib_data1 = ((uint64_t)x_bits << 32) | (uint64_t)y_bits;
	uint64_t calib_data2 = ((uint64_t)z_bits << 32) | FLASH_MEM_CONTROL_NUM;
	uint64_t calib_data_m1 = ((uint64_t)xm_bits << 32) | (uint64_t)ym_bits;
	uint64_t calib_data_m2 = ((uint64_t)zm_bits << 32) | FLASH_MEM_CONTROL_NUM;

	static FLASH_EraseInitTypeDef EraseInitStruct;
	uint32_t PAGEError;
	EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
	EraseInitStruct.NbPages = 1;
	EraseInitStruct.Banks = FLASH_BANK_1;
	EraseInitStruct.Page = 31;
	__disable_irq();
	HAL_FLASH_Unlock();
	if (HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError) != HAL_OK)
	{
	    HAL_FLASH_GetError ();
	}
	/*Write into flash*/
	HAL_StatusTypeDef status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, FLASH_GYRO_CALIB_START_ADD, calib_data1);
	status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, FLASH_GYRO_CALIB_START_ADD+8, calib_data2);
	status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, FLASH_GYRO_CALIB_START_ADD+16, calib_data_m1);
	status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, FLASH_GYRO_CALIB_START_ADD+24, calib_data_m2);
	HAL_FLASH_Lock();
	__enable_irq();
}

void oc_gyro_read_mag_data()
{
	/* Read 7 bytes of data (including status), so pointer in MAG
	 * automatically rolls over.
	 */
	HAL_I2C_Master_Receive_DMA(&hi2c2, MAG_I2C_ADDR, Data.MAG, 7);
}

void oc_gyro_read_icm_data()
{
	/* Save time delta between gyro measurements.
	 * Needs to be saved in seconds, timer runs every microsecond.
	 * If timer not yet started, start it.
	 */
	if (Status.Gyro_Timer_Running == 0) {
		Status.Gyro_Timer_Running = 1;
		HAL_TIM_Base_Start_IT(&htim14);
		/* If timer not yet running, assume sample rate timing.. */
		Status.Gyro_Time_Delta = (float)1 / (float)SAMPLE_RATE;
	}
	else {
		Status.Gyro_Time_Delta = (float)TIM14->CNT / (float)1000000;
	}
	TIM14->CNT = 0;					/* Reset counter */
	/* Read 12 bytes from 0x1F on ICM into Data.ICM */
	Read_From_SPI1_Async(0x1F, Data.ICM, 12);
}

void oc_gyro_read_icm_done(void) {
	/* Comms done -> Set CS high. */
	Status.Gyro_Count++;
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, GPIO_PIN_SET);
	Status.Gyro_Data_Available = 1;		/* Indicate gyro data available */
}

void Set_MAG_Register(Reg_Val_Type reg_val) {
	uint8_t r_data[2] = { reg_val.reg, reg_val.val };
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c2, MAG_I2C_ADDR, r_data, 2, 10)) {
		Error_Handler();
	}
}

void Set_ICM_Register(Reg_Val_Type reg_val)
{
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, GPIO_PIN_RESET);
	uint8_t write_buf[2] = { reg_val.reg, reg_val.val };
	HAL_SPI_Transmit(&hspi1, write_buf, 2, 10);
		HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, GPIO_PIN_SET);
}

void Read_From_SPI1_Async(uint8_t reg_addr, uint8_t* p_read, const uint8_t bytes_to_read)
{
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, GPIO_PIN_RESET);
	uint8_t rst_tx[bytes_to_read + 1];
	rst_tx[0] = reg_addr | ICM_REG_READ;
	HAL_StatusTypeDef stat = HAL_SPI_TransmitReceive_DMA(&hspi1, rst_tx, p_read, bytes_to_read + 1);
	/* bytes_to_read + 1 enables writing address first */
	/* CS pin is set to high in callback */
}

uint16_t data_to_prec_uint(float num, int mult) {
	uint16_t data = 0;
	int16_t temp = (int16_t)(num * mult);
	memcpy(&data, &temp, 2);
	return data;
}

float Get_Accel_Value_In_G(uint8_t* ptr)
{
	return ((float)(int16_t)(((*ptr) << 8) | *(ptr + 1))) / (float)ACCEL_LSB_PER_G_2;
}

float Get_Gyro_Value_In_DPS(uint8_t* ptr)
{
	return ((float)(int16_t)(((*ptr) << 8) | *(ptr + 1))) / (float)GYRO_LSB_PER_DPS_2;
}


