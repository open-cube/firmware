#include <oc_st_sensor_uart.h>


// UART sent given number of bytes from an array
void uart_send_length(const void *data, uint16_t length, USART_TypeDef* usart){
	uint8_t* sData = (uint8_t*)data;
	for (int i = 0; i < length; i++){
	/* Wait for TXE flag to be raised */
		while (!LL_USART_IsActiveFlag_TXE(usart)){}
			LL_USART_TransmitData8(usart, *sData);
			sData++;
	}
	while (!LL_USART_IsActiveFlag_TXE(usart)){}
}

void send_metadata(const sensor_info* s_info, USART_TypeDef* usart) {
	ev3_cmd_type msg_type = make_cmd_type_message(s_info->type);
	uart_send_length(&msg_type, sizeof(msg_type), usart);

	ev3_cmd_modes msg_modes = make_cmd_modes_message(s_info->num_modes - 1, s_info->num_visible_modes - 1);
	uart_send_length(&msg_modes, sizeof(msg_modes), usart);

	ev3_cmd_speed msg_speed = make_cmd_speed_message(s_info->baud_rate);
	uart_send_length(&msg_speed, sizeof(msg_speed), usart);

	for (int i = s_info->num_modes - 1; i >= 0; i--) {
		size_t total_size;
		uint8_t msg_name[EV3_MSG_BUFFER_SIZE] = {0};
		make_info_string_message(i, INFO_TYPE_NAME, s_info->modes[i].mode_name, s_info->modes[i].mode_name_len, &total_size, msg_name);
		uart_send_length(msg_name, total_size, usart);

		if(s_info->modes[i].raw_lim_low != 0 || s_info->modes[i].raw_lim_high != 0) {
			ev3_info_limits msg_raw = make_info_limits_message(i, INFO_TYPE_RAW, s_info->modes[i].raw_lim_low, s_info->modes[i].raw_lim_high);
			uart_send_length(&msg_raw, sizeof(msg_raw), usart);
		}

		if(s_info->modes[i].pct_lim_low != 0 || s_info->modes[i].pct_lim_high != 0) {
			ev3_info_limits msg_ptc = make_info_limits_message(i, INFO_TYPE_PCT, s_info->modes[i].pct_lim_low, s_info->modes[i].pct_lim_high);
			uart_send_length(&msg_ptc, sizeof(msg_ptc), usart);
		}

		if(s_info->modes[i].si_lim_low != 0 || s_info->modes[i].si_lim_high != 0) {
			ev3_info_limits msg_si = make_info_limits_message(i, INFO_TYPE_SI, s_info->modes[i].si_lim_low, s_info->modes[i].si_lim_high);
			uart_send_length(&msg_si, sizeof(msg_si), usart);
		}

		if (s_info->modes[i].symbol_len > 0) {
			uint8_t msg_symbol[EV3_MSG_BUFFER_SIZE] = {0};
			make_info_string_message(i, INFO_TYPE_SYMBOL, s_info->modes[i].symbol, s_info->modes[i].symbol_len, &total_size, msg_symbol);
			uart_send_length(msg_symbol, total_size, usart);
		}

		ev3_info_format msg_format = make_info_format_message(i, s_info->modes[i].values, s_info->modes[i].format, s_info->modes[i].digits, s_info->modes[i].decimals);
		uart_send_length(&msg_format, sizeof(msg_format), usart);
		HAL_Delay(10);
	}


	ev3_sys msg_ack = make_sys_message(MTYPE_SYSTEM_ACK);
	uart_send_length(&msg_ack, sizeof(msg_ack), usart);
}

void send_data(int sensor_state, void* data, size_t size, USART_TypeDef* usart) {
	if (sensor_state >= 0) {
		uint8_t msg_data[EV3_MSG_BUFFER_SIZE] = {0};
		msg_data[0] = make_message_header(MTYPE_DATA, sensor_state, size, NULL);
		size_t full_size = full_length_of_message(msg_data[0]);
		memcpy(&msg_data[1], data, size);
		msg_data[full_size-1] = calculate_message_checksum(msg_data, full_size-1);
		uart_send_length(msg_data, full_size, usart);
	}
}
