/*
 * Open-Cube sensor mode definitions
 * oc_sensor_modes.c
 *
 *  Created on: Jul 2, 2024
 *      Author: jelinva4
 */

#include "oc_sensor_modes.h"

#include "oc_st_sensor_uart.h"
#include "messages.h"

const ev3_mode_info m_euler = {S_MODE_EULER,
							    "EULER", 5,
							    -1800, 1800,
							    0, 0,
							    -180, 180,
							    "deg", 3,
							    3, 1, 5, 1};

const ev3_mode_info m_euler_mag = {S_MODE_EULER_MAG,
							    "EULER-MAG", 9,
							    -1800, 1800,
							    0, 0,
							    -180, 180,
							    "deg", 3,
							    3, 1, 5, 1};

const ev3_mode_info m_quaternion = {S_MODE_QUATERNION,
							    "QUATERNION", 10,
							    -10000, 10000,
							    0, 0,
							    -1, 1,
							    "", 4,
							    4, 1, 6, 4};

const ev3_mode_info m_raw = {S_MODE_RAW,
							    "RAW", 3,
							    -10000, 10000,
							    0, 0,
							    -10000, 10000,
							    "", 5,
							    9, 1, 5, 0};

const ev3_mode_info m_gyro_calib = {S_MODE_GYRO_CALIB,
							    "GYRO-CALIB", 10,
							    0, 0,
							    0, 0,
							    0, 0,
							    "", 4,
							    1, 1, 1, 0};

const ev3_mode_info m_mag_calib = {S_MODE_MAG_CALIB,
							    "MAG-CALIB", 9,
							    0, 0,
							    0, 0,
							    0, 0,
							    "", 4,
							    1, 1, 1, 0};

const sensor_info s_info = {SENSOR_ID, S_MODE_COUNT, S_MODE_COUNT, DATA_BAUD_RATE,
							{m_euler, m_euler_mag, m_quaternion, m_raw, m_gyro_calib, m_mag_calib}};

