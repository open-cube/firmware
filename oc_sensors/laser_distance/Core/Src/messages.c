/**
 * Definitions and helper functions for decoding EV3 UART packets
 */
#include "messages.h"

static uint8_t make_message_payload_len(unsigned int payload_len, size_t *padded_len);


uint8_t calculate_message_checksum(const void *buffer, size_t length) {
	uint8_t* buff = (uint8_t*) buffer;
  uint8_t csum = 0xFF;
  for (size_t i = 0; i < length; i++) {
    csum ^= buff[i];
  }
  return csum;
}

uint8_t make_message_header(ev3_msg_type type, uint8_t subtype, uint8_t payload_length, size_t *alloc_length) {
  uint8_t header = 0;
  header |= (type & EV3_MESSAGE_TYPE_BITS) << EV3_MESSAGE_TYPE_SHIFT;
  header |= (subtype & EV3_MESSAGE_SUBTYPE_BITS) << EV3_MESSAGE_SUBTYPE_SHIFT;
  header |= make_message_payload_len(payload_length, NULL);
  if (alloc_length) {
    *alloc_length = full_length_of_message(header);
  }
  return header;
}

ev3_sys make_sys_message(uint8_t subtype) {
	ev3_sys msg;
	msg.header = make_message_header(MTYPE_SYSTEM, subtype, 0, NULL);
	return msg;
}

ev3_cmd_type make_cmd_type_message(uint8_t sensor_type) {
	ev3_cmd_type msg;
	msg.header = make_message_header(MTYPE_COMMAND, MTYPE_COMMAND_TYPE, 1, NULL);
	msg.id = sensor_type;
	msg.csum = calculate_message_checksum(&msg, sizeof(msg)-1);
	return msg;
}

ev3_cmd_modes make_cmd_modes_message(uint8_t mode_count, uint8_t visible_mode_count) {
	ev3_cmd_modes msg;
	msg.header = make_message_header(MTYPE_COMMAND, MTYPE_COMMAND_MODES, 2, NULL);
	msg.mode_count = mode_count;
	msg.visible_mode_count = visible_mode_count;
	msg.csum = calculate_message_checksum(&msg, sizeof(msg)-1);
	return msg;
}

ev3_cmd_speed make_cmd_speed_message(uint32_t baud_rate) {
	ev3_cmd_speed msg;
	msg.header = make_message_header(MTYPE_COMMAND, MTYPE_COMMAND_SPEED, 4, NULL);
	msg.baud_rate = baud_rate;
	msg.csum = calculate_message_checksum(&msg, sizeof(msg)-1);
	return msg;
}

ev3_info_limits make_info_limits_message(uint8_t mode, uint8_t info_type, float lower, float upper) {
	ev3_info_limits msg;
	msg.header = make_message_header(MTYPE_INFO, mode, 8, NULL);
	msg.info_type = info_type;
	msg.lower = lower;
	msg.upper = upper;
	msg.csum = calculate_message_checksum(&msg, sizeof(msg)-1);
	return msg;
}

void make_info_string_message(uint8_t mode, uint8_t info_type, uint8_t* str, size_t str_len, size_t* total_size, uint8_t* msg) {
	msg[0] = make_message_header(MTYPE_INFO, mode, str_len, total_size);
	msg[1] = info_type;
	memcpy(&msg[2], str, str_len);
	msg[*total_size-1] = calculate_message_checksum(msg, *total_size-1);
}

ev3_info_format make_info_format_message(uint8_t mode,
										 uint8_t values,
										 uint8_t format,
										 uint8_t figures,
										 uint8_t decimals) {
	ev3_info_format msg;
	msg.header = make_message_header(MTYPE_INFO, mode, 4, NULL);
	msg.info_type = INFO_TYPE_FORMAT;
	msg.values = values;
	msg.format = format;
	msg.figures = figures;
	msg.decimals = decimals;
	msg.csum = calculate_message_checksum(&msg, sizeof(msg)-1);
	return msg;
}

size_t full_length_of_message(uint8_t header) {
  ev3_msg_type mtype = type_of_message(header);

  if (mtype == MTYPE_SYSTEM) {
    return 1;
  }

  unsigned int payload_len = payload_len_of_message(header);

  if (mtype == MTYPE_INFO) {
    return payload_len + EV3_INFO_MSG_FRAME_OVERHEAD;
  } else {
    return payload_len + EV3_NORMAL_MSG_FRAME_OVERHEAD;
  }
}

uint8_t make_message_payload_len(unsigned int payload_len, size_t *padded_len) {
  int exponent;
  if (payload_len <= 1) {
    exponent = 0;
  } else if (payload_len <= 2) {
    exponent = 1;
  } else if (payload_len <= 4) {
    exponent = 2;
  } else if (payload_len <= 8) {
    exponent = 3;
  } else if (payload_len <= 16) {
    exponent = 4;
  } else if (payload_len <= 32) {
    exponent = 5;
  } else {
	  exponent = 0;
    //panic("message too large");
  }
  if (padded_len != NULL) {
    *padded_len = 1 << exponent;
  }
  return (exponent & EV3_MESSAGE_LENGTH_BITS) << EV3_MESSAGE_LENGTH_SHIFT;
}
