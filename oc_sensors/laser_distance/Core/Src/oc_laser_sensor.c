#include "oc_laser_sensor.h"
#include "VL53L1X_api.h"
#include "VL53L1X_calibration.h"


int init_sensor(oc_laser_sensor* sensor) {
	int status = 0;
	sensor->dev = VL53L1_I2C_ADDR;
	while(sensor->sensorState==0){
		status = VL53L1X_BootState(sensor->dev, &sensor->sensorState);
		HAL_Delay(2);
	}
	/* This function must be called to initialize the sensor with the default setting  */
	status = VL53L1X_SensorInit(sensor->dev);
	return status;
}

int get_sensor_data(oc_laser_sensor* sensor) {
	int status = 0;
	while (sensor->dataReady == 0){
	  status = VL53L1X_CheckForDataReady(sensor->dev, &sensor->dataReady);
	  HAL_Delay(1);
	}
	sensor->dataReady = 0;
	status = VL53L1X_GetRangeStatus(sensor->dev, &sensor->RangeStatus);
	status = VL53L1X_GetDistance(sensor->dev, &sensor->Distance);
	status = VL53L1X_GetSignalRate(sensor->dev, &sensor->SignalRate);
	status = VL53L1X_GetAmbientRate(sensor->dev, &sensor->AmbientRate);
	status = VL53L1X_GetSpadNb(sensor->dev, &sensor->SpadNum);
	status = VL53L1X_ClearInterrupt(sensor->dev); //clear interrupt has to be called to enable next interrupt
	sensor->sendData = 1;
	return status;
}

int set_sensor_mode_normal(oc_laser_sensor* sensor) {
	int status = 0;
	/* Optional functions to be used to change the main ranging parameters according the application requirements to get the best ranging performances */
	status = VL53L1X_SetDistanceMode(sensor->dev, 2); /* 1=short, 2=long */
	status = VL53L1X_SetTimingBudgetInMs(sensor->dev, 50); /* in ms possible values [20, 50, 100, 200, 500] */
	status = VL53L1X_SetInterMeasurementInMs(sensor->dev, 50); /* in ms, IM must be > = TB */
	//  status = VL53L1X_SetOffset(sensor->dev,20); /* offset compensation in mm */
	status = VL53L1X_SetROI(sensor->dev, 4, 4); /* minimum ROI 4,4 */
	//	status = VL53L1X_CalibrateOffset(sensor->dev, 140, &offset); /* may take few second to perform the offset cal*/
	//	status = VL53L1X_CalibrateXtalk(sensor->dev, 1000, &xtalk); /* may take few second to perform the xtalk cal */
	//uart1_send((uint8_t *)"VL53L1X Ultra Lite Driver Example running ...\r\n");
	status = VL53L1X_StartRanging(sensor->dev);   /* This function has to be called to enable the ranging */
	return status;
}

int set_sensor_mode_short(oc_laser_sensor* sensor) {
	int status = 0;
	/* Optional functions to be used to change the main ranging parameters according the application requirements to get the best ranging performances */
	status = VL53L1X_SetDistanceMode(sensor->dev, 1); /* 1=short, 2=long */
	status = VL53L1X_SetTimingBudgetInMs(sensor->dev, 50); /* in ms possible values [20, 50, 100, 200, 500] */
	status = VL53L1X_SetInterMeasurementInMs(sensor->dev, 50); /* in ms, IM must be > = TB */
	status = VL53L1X_SetROI(sensor->dev, 4, 4); /* minimum ROI 4,4 */
	status = VL53L1X_StartRanging(sensor->dev);   /* This function has to be called to enable the ranging */
	return status;
}

int set_sensor_mode_fov(oc_laser_sensor* sensor) {
	int status = 0;
	status = VL53L1X_SetDistanceMode(sensor->dev, 2); /* 1=short, 2=long */
	status = VL53L1X_SetTimingBudgetInMs(sensor->dev, 50); /* in ms possible values [20, 50, 100, 200, 500] */
	status = VL53L1X_SetInterMeasurementInMs(sensor->dev, 50); /* in ms, IM must be > = TB */
	status = VL53L1X_SetROI(sensor->dev, 16, 16); /* minimum ROI 4,4 */
	status = VL53L1X_StartRanging(sensor->dev);   /* This function has to be called to enable the ranging */
	return status;
}

int change_sensor_settings(oc_laser_sensor* sensor, sensor_mode sensor_state) {
	int status = 0;
	switch (sensor_state) {
		case S_MODE_NORMAL:
			status = set_sensor_mode_normal(sensor);
			break;
		case S_MODE_FOV:
			status = set_sensor_mode_fov(sensor);
			break;
		case S_MODE_SHORT:
			status = set_sensor_mode_short(sensor);
			break;
		default:
			status = set_sensor_mode_normal(sensor);
	}
	return status;
}
