/*
 * Open-Cube sensor mode definitions
 * oc_sensor_modes.c
 *
 *  Created on: Jul 2, 2024
 *      Author: jelinva4
 */

#include "oc_sensor_modes.h"

#include "oc_st_sensor_uart.h"
#include "messages.h"

#define MAX_SENSOR_RANGE 4000 //MM

const ev3_mode_info m_normal = {S_MODE_NORMAL,
							    "Normal", 6,
							    0, MAX_SENSOR_RANGE,
							    0, 0,
							    0, MAX_SENSOR_RANGE,
							    "mm", 2,
							    1, 1, 4, 0};

const ev3_mode_info m_fov = {S_MODE_FOV,
							    "Large FOV", 9,
							    0, MAX_SENSOR_RANGE,
							    0, 0,
							    0, MAX_SENSOR_RANGE,
							    "mm", 2,
							    1, 1, 4, 0};

const ev3_mode_info m_short = {S_MODE_SHORT,
							    "short range", 11,
							    0, MAX_SENSOR_RANGE,
							    0, 0,
							    0, MAX_SENSOR_RANGE,
							    "mm", 2,
							    1, 1, 4, 0};

const sensor_info s_info = {SENSOR_ID, S_MODE_COUNT, S_MODE_COUNT, DATA_BAUD_RATE,
		 	 	 	 	 	{m_normal, m_fov, m_short}};

