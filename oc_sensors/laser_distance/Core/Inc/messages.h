/**
 * Definitions and helper functions for decoding EV3 UART packets
 */
#ifndef EV3_SENSOR_MESSAGES_H
#define EV3_SENSOR_MESSAGES_H

#include <stdint.h>
#include <string.h>


// Longest message that can be sent from a sensor
#define EV3_MSG_BUFFER_SIZE 35
// How often to poll UARTs
#define EV3_MSG_POLL_PERIOD_MS 1
// How often to send NACKs to sensors to keep their watchdogs in check
#define EV3_HEARTBEAT_PERIOD_MS 100
// Consider sensor dead after not sending any data after this many watchdog refreshes
#define EV3_MAX_HEARTBEATS_WITHOUT_DATA 6
// Similar to EV3_MAX_HEARTBEATS_WITHOUT_DATA, but for some quirky modes that are slow to get ready
// (NOT IMPLEMENTED)
#define EV3_MAX_HEARTBEATS_WITHOUT_DATA_LAX 100
// Maximum number of payload bytes in a DATA message
#define EV3_MAX_DATA_BYTES 32
// Speed of the initial handshake
#define EV3_UART_HANDSHAKE_BAUD 2400
#define EV3_MAX_MODES 8
#define EV3_MAX_NACK_DELAY (EV3_MAX_HEARTBEATS_WITHOUT_DATA*EV3_HEARTBEAT_PERIOD_MS)
#define EV3_METADATA_DELAY_MS 80

typedef enum {
  // System message. Used for low-level handshaking (ACK, NACK)
  MTYPE_SYSTEM = 0b00,
  // Command message. Doubles as a sensor-wide typedata and a real command message.
  MTYPE_COMMAND = 0b01,
  // Info message. Contains a bit of information about a single sensor mode.
  MTYPE_INFO = 0b10,
  // Data message. Contains data in a format described by previous INFO messages.
  MTYPE_DATA = 0b11,
} ev3_msg_type;

typedef enum {
  // Byte supposed to be sent at sensor start. Not sent.
  // According to LEGO this was intended for clock synchronization.
  MTYPE_SYSTEM_SYNC = 0b000,
  // Retransmission request for data & simultaneous sensor watchdog refresh
  MTYPE_SYSTEM_NACK = 0b010,
  // Confirmation that the brick received all sensor information
  MTYPE_SYSTEM_ACK = 0b100,
  // Reserved?
  MTYPE_SYSTEM_ESC = 0b110,
} ev3_sysmsg_type;

typedef enum {
  // Sensor info: type code of the sensor
  MTYPE_COMMAND_TYPE = 0b000,
  // Sensor info: number of supported modes
  MTYPE_COMMAND_MODES = 0b001,
  // Sensor info: supported DATA communication speed
  MTYPE_COMMAND_SPEED = 0b010,
  // Command for the sensor: switch to a different mode
  MTYPE_COMMAND_SELECT = 0b011,
  // Raw command for the sensor. The payload has no common format,
  // it is just a byte blob. It is used for e.g. firing the ultrasonic
  // sensor, recalibrating the gyro or for confirming factory calibration
  // on sensors.
  MTYPE_COMMAND_WRITE = 0b100,
} ev3_cmdmsg_type;

// Type of information about a sensor mode
typedef enum {
  // Sensor mode name (e.g. COL-COLOR)
  INFO_TYPE_NAME = 0b00000000,
  // SI symbol associated with this mode (e.g. "deg")
  INFO_TYPE_SYMBOL = 0b00000100,
  // How are values encoded in the DATA message & how many of them are there
  INFO_TYPE_FORMAT = 0b10000000,
  // Scaling coefficients corresponding directly to how data is encoded in DATA messages
  INFO_TYPE_RAW = 0b00000001,
  // Scaling coefficients for converting data to percent representation
  INFO_TYPE_PCT = 0b00000010,
  // Scaling coefficients for converting data to a SI unit-based representation
  INFO_TYPE_SI = 0b00000011,
} ev3_infomsg_type;

#define EV3_MESSAGE_TYPE_BITS 0b11
#define EV3_MESSAGE_TYPE_SHIFT 6
#define EV3_MESSAGE_LENGTH_BITS 0b111
#define EV3_MESSAGE_LENGTH_SHIFT 3
#define EV3_MESSAGE_SUBTYPE_BITS 0b111
#define EV3_MESSAGE_SUBTYPE_SHIFT 0

// Layout of the system message
typedef struct __attribute__((packed)) {
  uint8_t header;
} ev3_sys;

// Layout of the TYPE command message
typedef struct __attribute__((packed)) {
  uint8_t header;
  uint8_t id;
  uint8_t csum;
} ev3_cmd_type;

// Layout of the MODES command message
typedef struct __attribute__((packed)) {
  uint8_t header;
  uint8_t mode_count;
  uint8_t visible_mode_count;
  uint8_t csum;
} ev3_cmd_modes;

// Layout of the SPEED command message
typedef struct __attribute__((packed)) {
  uint8_t header;
  uint32_t baud_rate;
  uint8_t csum;
} ev3_cmd_speed;

// Layout of the INFO message containing scaling coefficients
typedef struct __attribute__((packed)) {
  uint8_t header;
  uint8_t info_type;
  float lower;
  float upper;
  uint8_t csum;
} ev3_info_limits;

// Layout of the INFO message containing data format
typedef struct __attribute__((packed)) {
  uint8_t header;
  uint8_t info_type;
  uint8_t values;
  uint8_t format;
  uint8_t figures;
  uint8_t decimals;
  uint8_t csum;
} ev3_info_format;

typedef struct {
	uint8_t mode;
	uint8_t mode_name[EV3_MAX_DATA_BYTES];
	uint8_t mode_name_len;
	float raw_lim_low;
	float raw_lim_high;
	float pct_lim_low;
	float pct_lim_high;
	float si_lim_low;
	float si_lim_high;
	uint8_t symbol[EV3_MAX_DATA_BYTES];
	uint8_t symbol_len;
	uint8_t values;
	uint8_t format;
	uint8_t digits;
	uint8_t decimals;
} ev3_mode_info;

typedef struct {
	uint8_t header;
	uint8_t data[EV3_MAX_DATA_BYTES];
	uint8_t csum;
} data_message;

static inline ev3_msg_type type_of_message(uint8_t header) {
  return (ev3_msg_type) ((header >> EV3_MESSAGE_TYPE_SHIFT) & EV3_MESSAGE_TYPE_BITS);
}

static inline unsigned int payload_len_of_message(uint8_t header) {
  int exponent = (header >> EV3_MESSAGE_LENGTH_SHIFT) & EV3_MESSAGE_LENGTH_BITS;
  // EV3 messages contain payload length only as a base-2 exponent
  return 1 << exponent;
}

static inline unsigned int subtype_of_message(uint8_t header) {
  return (header >> EV3_MESSAGE_SUBTYPE_SHIFT) & EV3_MESSAGE_SUBTYPE_BITS;
}

/**
 * Determine how many bytes have to be read/written from/to UART to transport this message.
 * @param header Header of the message.
 * @return Number of bytes in range 1-35.
 */
extern size_t full_length_of_message(uint8_t header);

/**
 * Make a new message header.
 * @param type Main message type.
 * @param subtype Message subtype.
 * @param payload_length Length of wanted payload, without padding.
 * @param alloc_length Total number of bytes needed for representing the message.
 * @return Header byte to place at the beginning of the message.
 */
extern uint8_t make_message_header(
    ev3_msg_type type,
	uint8_t subtype,
	uint8_t payload_length,
    size_t* alloc_length);

extern ev3_sys make_sys_message(uint8_t subtype);
extern ev3_cmd_type make_cmd_type_message(uint8_t sensor_type);
extern ev3_cmd_modes make_cmd_modes_message(uint8_t mode_count, uint8_t visible_mode_count);
extern ev3_cmd_speed make_cmd_speed_message(uint32_t baud_rate);
extern ev3_info_limits make_info_limits_message(uint8_t mode, uint8_t info_type, float lower, float upper);
extern void make_info_string_message(uint8_t mode, uint8_t info_type, uint8_t* str, size_t str_len, size_t* total_size, uint8_t* msg);
extern ev3_info_format make_info_format_message(uint8_t mode, uint8_t values, uint8_t format, uint8_t figures, uint8_t decimals);

/**
 * Calculate the XOR checksum of the EV3 packet.
 * @param buffer Data buffer over which to do the checksum.
 * @param length Length of the buffer.
 * @return XOR of all provided bytes and 0xFF.
 */
extern uint8_t calculate_message_checksum(const void *buffer, size_t length);

// how many bytes of the total is just fixed frame
#define EV3_NORMAL_MSG_FRAME_OVERHEAD 2 // initial byte + xor checksum at the end
#define EV3_INFO_MSG_FRAME_OVERHEAD 3 // initial byte + info type + xor checksum at the end

enum {
  // Retransmission request for data & simultaneous sensor watchdog refresh
  EV3_MSG_NACK = (MTYPE_SYSTEM << EV3_MESSAGE_TYPE_SHIFT) |
                 (MTYPE_SYSTEM_NACK << EV3_MESSAGE_SUBTYPE_SHIFT),
  // Confirmation that the brick received all sensor information
  EV3_MSG_ACK = (MTYPE_SYSTEM << EV3_MESSAGE_TYPE_SHIFT) |
                (MTYPE_SYSTEM_ACK << EV3_MESSAGE_SUBTYPE_SHIFT),
  // First byte of the sensor handshake
  EV3_INITIAL_MSG = (MTYPE_COMMAND << EV3_MESSAGE_TYPE_SHIFT) |
                    (MTYPE_COMMAND_TYPE << EV3_MESSAGE_SUBTYPE_SHIFT) |
                    (0 << EV3_MESSAGE_LENGTH_SHIFT)
};


#endif //EV3_SENSOR_MESSAGES_H
