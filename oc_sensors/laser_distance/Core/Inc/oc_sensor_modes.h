/**
 *  Open-Cube sensor mode definitions
 */
#ifndef OC_SENSOR_MODES_H
#define OC_SENSOR_MODES_H

#define SENSOR_ID 192
#define DATA_BAUD_RATE 256000

typedef enum {
	S_MODE_WAIT_ACK = -2,
	S_MODE_INIT = -1,
	S_MODE_NORMAL = 0,
	S_MODE_FOV,
	S_MODE_SHORT,
	S_MODE_COUNT
} sensor_mode;

#endif //OC_SENSOR_MODES_H
