#ifndef OC_LASER_SENSOR_H
#define OC_LASER_SENSOR_H

#include <stdint.h>
#include "oc_sensor_modes.h"
#include "oc_st_sensor_uart.h"

#define VL53L1_I2C_ADDR 0x52

typedef struct {
	volatile uint8_t sensorState;
	volatile uint8_t sendData;
	volatile uint8_t changeMode;
	volatile uint16_t compVAL;
	uint8_t dataReady;
	uint16_t dev;
	uint16_t Distance;
	uint16_t SignalRate;
	uint16_t AmbientRate;
	uint16_t SpadNum;
	uint8_t RangeStatus;
} oc_laser_sensor;



int init_sensor(oc_laser_sensor* sensor);
int get_sensor_data(oc_laser_sensor* sensor);
int set_sensor_mode_normal(oc_laser_sensor* sensor);
int set_sensor_mode_short(oc_laser_sensor* sensor);
int set_sensor_mode_fov(oc_laser_sensor* sensor);
int change_sensor_settings(oc_laser_sensor* sensor, sensor_mode sensor_state);

#endif //OC_LASER_SENSOR_H
