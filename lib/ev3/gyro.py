from micropython import const

from lib.ev3 import EV3UartSensor

GYRO_SENSOR_ID = const(32)

OFFSET_ANGLE = const(0)
OFFSET_ANGLERATE = const(1)
OFFSET_TILTANGLE = const(2)
OFFSET_INVALID = const(3)


class GyroSensor(EV3UartSensor):
    # source:
    # http://docs.ev3dev.org/projects/lego-linux-drivers/en/ev3dev-buster/sensor_data.html#lego-ev3-gyro
    MODE_ANGLE = const(0)
    MODE_RATE = const(1)
    MODE_FAST_RATE = const(2)
    MODE_ANGLE_AND_RATE = const(3)
    MODE_TILT_RATE = const(5)
    MODE_TILT_ANGLE = const(6)

    def __init__(self, port: int):
        """
        Initialize a new EV3 gyro sensor object at the given port.
        :param port: Port to which the sensor is attached. Use one of Port.S1~Port.S4.
        """
        super().__init__(port, GYRO_SENSOR_ID)
        self.offset_value = 0
        self.offset_mode = OFFSET_ANGLE  # sensor boots up in GYRO-ANG

    def angle(self) -> int:
        """
        Measure current sensor orientation.
        :return: Sensor angle in degrees
        """
        if self.offset_mode != OFFSET_ANGLE:
            self.offset_mode = OFFSET_ANGLE
            self.offset_value = 0
        return self.read_raw_mode(GyroSensor.MODE_ANGLE)[0] - self.offset_value

    def speed(self) -> int:
        """
        Measure current sensor rate of rotation.
        :return: Sensor angular speed in deg/s
        """
        self.offset_mode = OFFSET_INVALID
        return self.read_raw_mode(GyroSensor.MODE_RATE)[0]

    def reset_angle(self, angle: int):
        """
        Adjust Open-Cube maintained zero point of angle()/tilst_angle()/angle_and_rate() readings.
        :param angle: New value that the reading functions will return
                      (assuming that the sensor will not move)
        """
        if self.offset_mode == OFFSET_ANGLERATE:
            self.offset_value = self.read_raw_mode(GyroSensor.MODE_ANGLE_AND_RATE)[0] - angle
        elif self.offset_mode == OFFSET_TILTANGLE:
            self.offset_value = self.read_raw_mode(GyroSensor.MODE_TILT_ANGLE)[0] - angle
        else:  # OFFSET_ANGLE and OFFSET_INVALID
            self.offset_value = self.read_raw_mode(GyroSensor.MODE_ANGLE)[0] - angle
            self.offset_mode = OFFSET_ANGLE

    def angle_and_speed(self):
        """
        Measure both the sensor angle and rate of rotation at once.

        This is better than angle()+speed() as it uses a dedicated
        sensor mode for measuring both at once.

        :return: (angle, rate) tuple in (deg/s, deg).
        """
        if self.offset_mode != OFFSET_ANGLERATE:
            self.offset_mode = OFFSET_ANGLERATE
            self.offset_value = 0
        values = self.read_raw_mode(GyroSensor.MODE_ANGLE_AND_RATE)
        return values[0] - self.offset_value, values[1]

    def coarse_speed(self):
        """
        Measure current sensor rate of rotation with lower resolution, but higher range.
        :return: Sensor angular speed in deg/s
        """
        self.offset_mode = OFFSET_INVALID
        return self.read_raw_mode(GyroSensor.MODE_FAST_RATE)[0]

    def tilt_speed(self):
        """
        Measure current sensor rate of rotation along a second axis.

        Beware: not all sensors provide two-axis measurements.

        :return: Sensor angular speed in deg/s
        """
        self.offset_mode = OFFSET_INVALID
        return self.read_raw_mode(GyroSensor.MODE_TILT_RATE)[0]

    def tilt_angle(self):
        """
        Measure current sensor orientation along a second axis.

        Beware: not all sensors provide two-axis measurements.

        :return: Sensor angle in degrees
        """
        if self.offset_mode != OFFSET_TILTANGLE:
            self.offset_mode = OFFSET_TILTANGLE
            self.offset_value = 0
        return self.read_raw_mode(GyroSensor.MODE_TILT_ANGLE)[0] - self.offset_value
