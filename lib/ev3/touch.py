from machine import Pin

from lib.hw_defs.ports import SENSOR_PORT_PINS


class TouchSensor:
    def __init__(self, port: int):
        """
        Initialize a new EV3 touch sensor object at the given port.
        :param port: Port to which the sensor is attached. Use one of Port.S1~Port.S4.
        """
        if port < 0 or port > 3:
            raise ValueError(f"Sensor port index {port} is out of range")
        pins = SENSOR_PORT_PINS[port]
        self.btn_pin_no = pins.uart.rx_pin
        self.btn_pin = Pin(self.btn_pin_no, mode=Pin.IN)

    def pressed(self) -> bool:
        """
        Checks if the button is pressed.
        """
        return self.btn_pin.value() == 1
