from micropython import const

from lib.ev3 import EV3UartSensor


# source:
# http://docs.ev3dev.org/projects/lego-linux-drivers/en/ev3dev-stretch/sensor_data.html#lego-ev3-color-mode2-value0
class Color:
    BLACK = const(1)
    BLUE = const(2)
    GREEN = const(3)
    YELLOW = const(4)
    RED = const(5)
    WHITE = const(6)
    BROWN = const(7)


COLOR_SENSOR_ID = const(29)


class ColorSensorCalibration:
    """
    Calibration constants used for offset and gain correction for RGB mode measurements.
    """
    def __init__(self, max_r: float, max_g: float, max_b: float, min_r: float, min_g: float, min_b: float):
        """
        Convert observed min/max RGB values into scaling constants.
        :param max_r: Maximum measured red ADC value
        :param max_g: Maximum measured green ADC value
        :param max_b: Maximum measured blue ADC value
        :param min_r: Minimum measured red ADC value
        :param min_g: Minimum measured green ADC value
        :param min_b: Minimum measured blue ADC value
        """
        self.r_scale = 100 / (max_r - min_r)
        self.g_scale = 100 / (max_g - min_g)
        self.b_scale = 100 / (max_b - min_b)
        self.r_offset = min_r
        self.g_offset = min_g
        self.b_offset = min_b


class ColorSensor(EV3UartSensor):
    # source:
    # http://docs.ev3dev.org/projects/lego-linux-drivers/en/ev3dev-stretch/sensor_data.html#lego-ev3-color
    MODE_REFLECT = const(0)
    MODE_AMBIENT = const(1)
    MODE_COLOR = const(2)
    MODE_REFLECT_RAW = const(3)
    MODE_RGB_RAW = const(4)
    MODE_COL_CAL = const(5)

    # calibration by JV
    DEFAULT_RGB_CALIBRATION = ColorSensorCalibration(275, 334, 242, 0, 0, 0)

    # calibration from Pybricks (https://github.com/pybricks/pybricks-micropython/blob
    # /e30fd16fe7e40b7beff16d6738fb029cf17a2e96/pybricks/ev3devices/pb_type_ev3devices_colorsensor.c#L81)
    # recalculated to match the min/max format
    PYBRICKS_RGB_CALIBRATION = ColorSensorCalibration(388.8, 360.0, 198.3, 1.2, 2.9, 7.1)

    def __init__(self, port: int):
        """
        Initialize a new EV3 color sensor object at the given port.
        :param port: Port to which the sensor is attached. Use one of Port.S1~Port.S4.
        """
        super().__init__(port, COLOR_SENSOR_ID)
        self.rgb_calibration = ColorSensor.DEFAULT_RGB_CALIBRATION

    def reflection(self) -> int:
        """
        Measure surfare reflectivity for red light. The values
        are calibrated within the sensor itself.
        :return: Reflectivity in %
        """
        return self.read_raw_mode(ColorSensor.MODE_REFLECT)[0]

    def reflection_raw(self) -> int:
        """
        Measure surfare reflectivity for red light. The values
        are uncalibrated. Code using this function must thus perform
        range scaling/shifting manually.

        This method will likely provide higher resolution than
        reflection() and therefore it might be advantageous
        to use this function for line-following applications.
        :return: Reflectivity in sensor ADC units
        """
        items = self.read_raw_mode(ColorSensor.MODE_REFLECT_RAW)
        led_on = items[0]
        led_off = items[1]
        return max(led_off - led_on, 0)

    def ambient(self) -> int:
        """
        Measure ambient light intensity. The values are calibrated
        within the sensor itself.
        :return: Light intensity in percent of something
        """
        return self.read_raw_mode(ColorSensor.MODE_AMBIENT)[0]

    def rgb_raw(self) -> tuple:
        """
        Measure surface reflectivity for red, green and blue light.
        Returned values are not calibrated.
        :return: Uncalibrated reflectivity for all colors in sensor ADC units.
        """
        values = self.read_raw_mode(ColorSensor.MODE_RGB_RAW)
        return values[0], values[1], values[2]

    def rgb(self) -> tuple:
        """
        Measure surface reflectivity for red, green and blue light.
        Returned values are calibrated on Open-Cube according to
        currently configured `rgb_calibration`.
        :return: Reflectivity for all colors in % of maximum observed by the sensor
        """
        r, g, b = self.rgb_raw()
        cal = self.rgb_calibration

        return (
            max(min((r - cal.r_offset) * cal.r_scale, 100.0), 0.0),
            max(min((g - cal.g_offset) * cal.g_scale, 100.0), 0.0),
            max(min((b - cal.b_offset) * cal.b_scale, 100.0), 0.0),
        )

    def hsv(self) -> tuple:
        """
        Measure surface reflectivity for red, green and blue light.
        Then, convert it to the HSV colorspace.
        Returned values are calibrated on Open-Cube according to
        currently configured `rgb_calibration`.
        :return: Triplet (h,s,v) representing the surface color in the HSV colorspace.
                 Hue is in degrees, saturation and value are in percent.
        """
        # See https://dl.acm.org/doi/epdf/10.1145/965139.807361
        R, G, B = self.rgb()
        R = R / 100
        G = G / 100
        B = B / 100

        V = max(R, G, B)  # value [0-1]
        X = min(R, G, B)

        if V < 0.001:
            # nearly black, define hue and saturation as zero
            return 0, 0, V * 100

        span = (V - X)
        S = span / V  # saturation [0-1]

        if S < 0.001:
            # gray, define hue as zero
            return 0, S * 100, V * 100

        r = (V - R) / span
        g = (V - G) / span
        b = (V - B) / span

        # hue [0-6]
        if V == R:
            if X == G:
                H = 5 + b
            else:
                H = 1 - g
        elif V == G:
            if X == B:
                H = 1 + r
            else:
                H = 3 - b
        else:
            if X == R:
                H = 3 + g
            else:
                H = 5 - r

        return H * 60, S * 100, V * 100

    def color(self) -> int | None:
        """
        Measure the color of a LEGO brick.
        The measurement is provided by the sensor itself.
        If this works poorly for your use case, hsv() might be a better choice.
        :return: None if the sensor cannot identify a color, otherwise
                 one of lib.ev3.Color constants
        """
        code = self.read_raw_mode(ColorSensor.MODE_COLOR)[0]
        if code != 0:
            return code
        else:  # code == 0: COLOR_NONE => return None
            return None
