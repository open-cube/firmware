from micropython import const

from lib.ev3 import EV3UartSensor

INFRA_SENSOR_ID = const(33)


class InfraredSensor(EV3UartSensor):
    # source:
    # http://docs.ev3dev.org/projects/lego-linux-drivers/en/ev3dev-buster/sensor_data.html#lego-ev3-ir
    MODE_PROXIMITY = const(0)
    MODE_SEEK = const(1)
    MODE_REMOTE = const(2)

    def __init__(self, port: int):
        """
        Initialize a new EV3 infrared sensor object at the given port.
        :param port: Port to which the sensor is attached. Use one of Port.S1~Port.S4.
        """
        super().__init__(port, INFRA_SENSOR_ID)

    def distance(self) -> int:
        """
        Measure the relative proximity to the nearest surface in percent.
        :return: Proximity in percent
        """
        return self.read_raw_mode(InfraredSensor.MODE_PROXIMITY)[0]

    def seeker(self, channel: int) -> tuple:
        """
        Determine where the remote controller for the given channel exists.
        See also http://docs.ev3dev.org/projects/lego-linux-drivers/en/ev3dev-buster/sensor_data.html#lego-ev3-ir
        :param channel: Which channel to check (1-4).
        :return: Tuple (heading, distance). Heading ranges from -25 to +25, distance is 0-100.
        """
        items = self.read_raw_mode(InfraredSensor.MODE_SEEK)
        pair_offset = 2 * (channel - 1)
        return items[pair_offset + 0], items[pair_offset + 1]

    # FIXME add support for remote functions provided by Pybricks
