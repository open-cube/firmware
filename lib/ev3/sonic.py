from micropython import const

from lib.ev3 import EV3UartSensor

# source:
# https://github.com/mindboards/ev3sources/blob/78ebaf5b6f8fe31cc17aa5dce0f8e4916a4fc072/lms2012/lms2012/source/bytecodes.h#L1532
DEVCMD_FIRE = bytearray(b"\x11")

SONIC_SENSOR_ID = const(30)

class UltrasonicSensor(EV3UartSensor):
    # source:
    # http://docs.ev3dev.org/projects/lego-linux-drivers/en/ev3dev-buster/sensor_data.html#lego-ev3-us
    MODE_CONTINUOUS_CM = const(0)
    MODE_LISTEN = const(2)
    MODE_SINGLE_CM = const(3)

    def __init__(self, port: int):
        """
        Initialize a new EV3 ultrasonic sensor object at the given port.
        :param port: Port to which the sensor is attached. Use one of Port.S1~Port.S4.
        """
        super().__init__(port, SONIC_SENSOR_ID)

    def distance(self, silent=False) -> int:
        """
        Measure the distance to the nearest obstacle in front of the sensor.
        :param silent: If true, the ultrasonic sensor will not emit more
                       sound waves after the measurement finishes.
        :return: Distance in millimeters or a magic value of 2550 if the measurement fails.
        """
        value = self.read_raw_mode(UltrasonicSensor.MODE_CONTINUOUS_CM)[0]
        if silent:
            value = self.read_raw_mode(UltrasonicSensor.MODE_SINGLE_CM)[0]
        return value

    def presence(self) -> bool:
        """
        Check if another ultrasonic sensor is active within the hearing distance of this sensor.
        """
        return self.read_raw_mode(UltrasonicSensor.MODE_LISTEN)[0] > 0

    # this is likely not needed! distance(silent=True) seems to work OK-ish
    # def oneshot_fire(self):
    #     self.write_command(DEVCMD_FIRE)
    #
    # def oneshot_read(self) -> int:
    #     return self.read_raw_mode(UltrasonicSensor.MODE_SINGLE_CM)[0]
