import micropython

from lib.robot_consts import BAT_VOLTAGE_MAX, BAT_VOLTAGE_MIN

DISPLAY_TITLE_POS = 18
DISPLAY_PROG_POS = 42
DISPLAY_ARROW_GAP = 9
ESP_MAX_RESET_FAILED = 5

class Menu:
    PROGRAM: Menu = micropython.const(0)
    MOT_TEST: Menu = micropython.const(1)
    SEN_TEST: Menu = micropython.const(2)
    CUBE_TEST: Menu = micropython.const(3)
    BT: Menu = micropython.const(4)
    FW_VERSION: Menu = micropython.const(5)
    COUNT: Menu = micropython.const(6)

    SEN_TEST_PROGRAM: Menu = micropython.const(0)
    SEN_TEST_PORT: Menu = micropython.const(1)

# Fill fr
# amebuffer with battery voltage
def display_fill_battery(robot, bat_voltage):
    bat_percentage = int((bat_voltage - BAT_VOLTAGE_MIN) / (BAT_VOLTAGE_MAX - BAT_VOLTAGE_MIN) * 20) * 5 # Round to 5
    bat_percentage = 100 if bat_percentage > 100 else 0 if bat_percentage < 0 else bat_percentage

    robot.display.line(0, 13, 128, 13, 1)
    robot.display.text('{:3d}%'.format(bat_percentage), 3, 3, 1)
    robot.display.centered_text('bat', 3, 1)
    robot.display.text('{:.2f}V'.format(bat_voltage), 84, 3, 1)

# Fill framebuffer with programs menu 
def display_fill_programs(robot, program_list, current_program):
    robot.display.fill(0)
    robot.display.rect(0, 0, 128, 64, 1)
    robot.display.centered_text('< Programs >', DISPLAY_TITLE_POS, 1)
    if len(program_list) > 0:
        robot.display.centered_text(program_list[current_program][0][:-3], DISPLAY_PROG_POS, 1)
        display_fill_arrows(robot)
    else:
        robot.display.centered_text("cube is empty", DISPLAY_PROG_POS, 1)

# Fill framebuffer with bt menu
def display_fill_bt_menu(robot, esp_default_name, esp_reset_failed_counter):
    robot.display.fill(0)
    robot.display.centered_text('< BT pair >', DISPLAY_TITLE_POS, 1)
    if esp_reset_failed_counter > ESP_MAX_RESET_FAILED:
        robot.display.centered_text('ESP', DISPLAY_PROG_POS-12, 1)
        robot.display.centered_text('not responding', DISPLAY_PROG_POS-4, 1)
        robot.display.centered_text('restart cube', DISPLAY_PROG_POS+4, 1)
    else:
        robot.display.centered_text(esp_default_name, DISPLAY_PROG_POS, 1)

# Add bt connecting window to framebuffer 
def display_fill_bt_pin(robot, pin, connecting):
    robot.display.rect(6,16,116,42, 1)
    robot.display.rect(7,17,114,40, 1)
    robot.display.fill_rect(8,18,112,38, 0)
    if connecting:
        robot.display.centered_text('connecting', 20, 1)
    else:
        robot.display.centered_text('pair with', 20, 1)
        ##robot.display.text('device', 10, 30, 1)
        robot.display.centered_text(f'{pin:06d}', 30, 1)
        robot.display.text('no', 10, 45, 1)
        robot.display.centered_text('ok', 45, 1)

# Fill framebuffer with motor test menu
def display_fill_mot_test(robot, current_motor_test):
    robot.display.fill(0)
    robot.display.centered_text('< Motor test >', DISPLAY_TITLE_POS, 1)
    robot.display.centered_text(f'M{current_motor_test+1}', DISPLAY_PROG_POS, 1)
    display_fill_arrows(robot)

# Fill framebuffer with sensor test menu
def display_fill_sen_test(robot, current_sen_test, sensor_test_menu, sensor_test_menu_stype, sensor_test_name):
    robot.display.fill(0)
    if sensor_test_menu == Menu.SEN_TEST_PROGRAM:
        robot.display.centered_text('< Sensor test >', DISPLAY_TITLE_POS, 1)
        robot.display.centered_text(sensor_test_name, DISPLAY_PROG_POS, 1)
    else:
        robot.display.centered_text('< Sensor port', DISPLAY_TITLE_POS, 1)
        robot.display.centered_text(f'S{current_sen_test+1}', DISPLAY_PROG_POS, 1)
    display_fill_arrows(robot)

# Fill framebuffer with cube test menu
def display_fill_cube_test(robot, current_cube_test, cube_test_menu_name):
    robot.display.fill(0)
    robot.display.centered_text('< Cube test >', DISPLAY_TITLE_POS, 1)
    robot.display.centered_text(cube_test_menu_name, DISPLAY_PROG_POS, 1)
    display_fill_arrows(robot)

def display_fill_arrows(robot):
    y_pos = DISPLAY_PROG_POS - DISPLAY_ARROW_GAP
    x_pos = 64
    robot.display.draw_arrow(x_pos, y_pos, robot.display.UP, 1)
    y_pos = DISPLAY_PROG_POS + DISPLAY_ARROW_GAP + 7
    robot.display.draw_arrow(x_pos, y_pos, robot.display.DOWN, 1)

def display_fill_fw_version(robot, fw_version):
    robot.display.fill(0)
    robot.display.centered_text("< Firmware >", DISPLAY_TITLE_POS, 1)
    robot.display.centered_text("Open-Cube", DISPLAY_PROG_POS-10, 1)
    robot.display.centered_text(fw_version, DISPLAY_PROG_POS, 1)

# Show program error on display
def display_show_error(robot):
    robot.display.fill(0)
    robot.display.centered_text('Program', 20, 1)
    robot.display.centered_text('error!', 30, 1)
    robot.display.show()

def display_show_startup(robot):
    robot.display.fill(0)
    robot.display.centered_text('Turning', 20, 1)
    robot.display.centered_text('on', 30, 1)
    robot.display.show()

def display_show_program(robot):
    robot.display.fill(0)
    robot.display.centered_text('Program', 20, 1)
    robot.display.centered_text('running', 30, 1)
    robot.display.show()

def display_show_boot(robot):
    robot.display.fill(0)
    robot.display.centered_text('Load', 20, 1)
    robot.display.centered_text('firmware', 30, 1)
    robot.display.show()