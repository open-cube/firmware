import brick_ll


class I2CGuard:
    """
    Helper object for locking the I2C peripheral during
    accesses from user code.
    """
    def __init__(self):
        pass

    def __enter__(self):
        brick_ll.lock_i2c()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        brick_ll.unlock_i2c()
