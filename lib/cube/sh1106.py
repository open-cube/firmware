# MicroPython SH1106 OLED driver, I2C and SPI interfaces
#
# This code is extended version of open-source driver for OLED display from https://github.com/robert-hh/SH1106
# Library provides useful plotting functions, drawing more complex shapes such as triangles or circles
#
# Sample code sections 
# ------------ SPI ------------------
#
# from machine import Pin, SPI
# import sh1106

# spi = SPI(1, baudrate=1000000)
# display = sh1106.SH1106_SPI(128, 64, spi, Pin(5), Pin(2), Pin(4))
# display.sleep(False)
# display.fill(0)
# display.text('Testing 1', 0, 0, 1)
# display.show()
#
# --------------- I2C ------------------

# from machine import Pin, I2C
# import sh1106
# 
# i2c = I2C(scl=Pin(5), sda=Pin(4), freq=400000)
# display = sh1106.SH1106_I2C(128, 64, i2c, Pin(16), 0x3c)
# display.sleep(False)
# display.fill(0)
# display.text('Testing 1', 0, 0, 1)
# display.show()


from micropython import const
from math import sin, cos
import utime as time
import framebuf


# Register definitions
_SET_CONTRAST        = const(0x81)
_SET_NORM_INV        = const(0xa6)
_SET_DISP            = const(0xae)
_SET_SCAN_DIR        = const(0xc0)
_SET_SEG_REMAP       = const(0xa0)
_LOW_COLUMN_ADDRESS  = const(0x00)
_HIGH_COLUMN_ADDRESS = const(0x10)
_SET_PAGE_ADDRESS    = const(0xB0)
_SET_OFFSET          = const(0xD3)

# Function to generate n equidistant points in range <a,b>
def linspace(a, b, n):    
    if n < 2:        
        return b
    diff = (float(b) - a)/(n - 1)
    return [diff * i + a  for i in range(n)]

class SH1106(framebuf.FrameBuffer):
    LEFT = 0
    UP = 1
    RIGHT = 2
    DOWN = 3

    def __init__(self, width, height, external_vcc, rotate=0):
        self.width = width
        self.height = height
        self.external_vcc = external_vcc
        self.flip_en = rotate == 180 or rotate == 270
        self.rotate90 = rotate == 90 or rotate == 270
        self.pages = self.height // 8
        self.bufsize = self.pages * self.width
        self.renderbuf = bytearray(self.bufsize)
        self.pages_to_update = 0

        if self.rotate90:
            self.displaybuf = bytearray(self.bufsize)
            # HMSB is required to keep the bit order in the render buffer
            # compatible with byte-for-byte remapping to the display buffer,
            # which is in VLSB. Else we'd have to copy bit-by-bit!
            super().__init__(self.renderbuf, self.height, self.width,
                             framebuf.MONO_HMSB)
        else:
            self.displaybuf = self.renderbuf
            super().__init__(self.renderbuf, self.width, self.height,
                             framebuf.MONO_VLSB)

        # flip() was called rotate() once, provide backwards compatibility.
        self.rotate = self.flip
        self.init_display()
        
        self.pix = 0
        self.piy = 0
        self.plx = 0
        self.ply = 0
        self.prx = 0
        self.pry = 0
        self.data_points = 0 #no of data points in continuous chart

    def init_display(self):
        self.reset()
        self.fill(0)
        self.show()
        self.poweron()
        # rotate90 requires a call to flip() for setting up.
        self.flip(self.flip_en)

    def poweroff(self):
        self.write_cmd(_SET_DISP | 0x00)

    def poweron(self):
        self.write_cmd(_SET_DISP | 0x01)

    def flip(self, flag=None, update=True):
        if flag is None:
            flag = not self.flip_en
        mir_v = flag ^ self.rotate90
        mir_h = flag
        self.write_cmd(_SET_SEG_REMAP | (0x01 if mir_v else 0x00))
        self.write_cmd(_SET_SCAN_DIR | (0x08 if mir_h else 0x00))
        self.flip_en = flag
        if update:
            self.show()

    def sleep(self, value):
        self.write_cmd(_SET_DISP | (not value))

    def contrast(self, contrast):
        self.write_cmd(_SET_CONTRAST)
        self.write_cmd(contrast)

    def invert(self, invert):
        self.write_cmd(_SET_NORM_INV | (invert & 1))

    def show(self, full_update = False):
        # self.* lookups in loops take significant time (~4fps).
        (w, p, db, rb) = (self.width, self.pages,
                          self.displaybuf, self.renderbuf)
        if self.rotate90:
            for i in range(self.bufsize):
                db[w * (i % p) + (i // p)] = rb[i]
        if full_update:
            pages_to_update = (1 << self.pages) - 1
        else:
            pages_to_update = self.pages_to_update
        #print("Updating pages: {:08b}".format(pages_to_update))
        for page in range(self.pages):
            if (pages_to_update & (1 << page)):
                self.write_cmd(_SET_PAGE_ADDRESS | page)
                self.write_cmd(_LOW_COLUMN_ADDRESS | 2)
                self.write_cmd(_HIGH_COLUMN_ADDRESS | 0)
                self.write_data(db[(w*page):(w*page+w)])
        self.pages_to_update = 0

    def register_updates(self, y0, y1=None):
        # this function takes the top and optional bottom address of the changes made
        # and updates the pages_to_change list with any changed pages
        # that are not yet on the list
        start_page = max(0, y0 // 8)
        end_page = max(0, y1 // 8) if y1 is not None else start_page
        # rearrange start_page and end_page if coordinates were given from bottom to top
        if start_page > end_page:
            start_page, end_page = end_page, start_page
        for page in range(start_page, end_page+1):
            self.pages_to_update |= 1 << page

    def reset(self, res):
        if res is not None:
            res(1)
            time.sleep_ms(1)
            res(0)
            time.sleep_ms(20)
            res(1)
            time.sleep_ms(20)
            
    def pixel(self, x, y, color=None):
        if color is None:
            return super().pixel(x, y)
        else:
            super().pixel(x, y , color)
            page = y // 8
            self.pages_to_update |= 1 << page

    def text(self, text, x, y, color=1):
        super().text(text, x, y, color)
        self.register_updates(y, y+7)

    def line(self, x0, y0, x1, y1, color):
        super().line(x0, y0, x1, y1, color)
        self.register_updates(y0, y1)

    def hline(self, x, y, w, color):
        super().hline(x, y, w, color)
        self.register_updates(y)

    def vline(self, x, y, h, color):
        super().vline(x, y, h, color)
        self.register_updates(y, y+h-1)

    def fill(self, color):
        super().fill(color)
        self.pages_to_update = (1 << self.pages) - 1

    def blit(self, fbuf, x, y, key=-1, palette=None):
        super().blit(fbuf, x, y, key, palette)
        self.register_updates(y, y+self.height)

    def scroll(self, x, y):
        # my understanding is that scroll() does a full screen change
        super().scroll(x, y)
        self.pages_to_update =  (1 << self.pages) - 1

    def fill_rect(self, x, y, w, h, color):
        super().fill_rect(x, y, w, h, color)
        self.register_updates(y, y+h-1)

    def rect(self, x, y, w, h, color):
        super().rect(x, y, w, h, color)
        self.register_updates(y, y+h-1)

    def draw_bar_chart_v(self, current_val, x, y, w, h, low_lim=0, high_lim=100, no_of_ticks=5, label= None, redraw=False):      
        if(redraw):
            self.init_display()
            redraw = False
            self.fill_rect(0,0,127,10,1)
            extra_pad = 0
            if(len(label)%2 == 1):
                extra_pad = 2
            self.text(label,64-int(len(label)/2)*8-extra_pad,1,0)       
            ticks = linspace(low_lim,high_lim,no_of_ticks)
            ticks = ticks[::-1]
            ticks_px = linspace(y-h,y-1,no_of_ticks)
            for i in range(len(ticks)):           
                self.hline(x+w+1,int(ticks_px[i]),5,1)
                self.text(str(int(ticks[i])),x+w+7,int(ticks_px[i])-4,1)               
        level = int(h * (((current_val-low_lim)/(high_lim-low_lim))))        
        self.rect(x, y - h, w, h, 1);
        self.fill_rect(x, y - h, w, h - level,  0);
        self.rect(x, y - h, w, h, 1);
        self.fill_rect(x, y - level, w,  level, 1);
        self.show(True)
        return redraw
        
    def draw_bar_chart_h(self, current_val, x, y, w, h, low_lim=0, high_lim=100, no_of_ticks=5, label= None, redraw=False):      
        if(redraw):
            self.init_display()
            redraw = False
            self.fill_rect(0,0,127,10,1)
            extra_pad = 0
            if(len(label)%2 == 1):
                extra_pad = 2
            self.text(label,64-int(len(label)/2)*8-extra_pad,1,0)            
            ticks = linspace(low_lim,high_lim,no_of_ticks)            
            ticks_px = linspace(x,x+w-1,no_of_ticks)
            for i in range(len(ticks)):                
                self.vline(int(ticks_px[i]),y+1,5,1)
                num_str = str(int(ticks[i]))
                add_pad = len(num_str)*4
                self.text(num_str,int(ticks_px[i])-add_pad,y+7,1)               
        level = int(w * (((current_val-low_lim)/(high_lim-low_lim))))       
        self.fill_rect(x + level, y - h, w - level, h,  0);
        self.rect(x, y - h, w,  h, 1);
        self.fill_rect(x, y - h, level,  h, 1);
        self.show(True)
        return redraw
        
    def draw_dial(self, curval, cx, cy, r, loval, hival, no_of_steps, sa, label, Redraw):         
        degtorad = .0174532778;
        if (Redraw):
            Redraw = False
            self.init_display()
            # draw the dial only one time--this will minimize flicker
            self.fill_rect(0,0,127,10,1)
            extra_pad = 0
            if(len(label)%2 == 1):
                extra_pad = 2
            self.text(label,64-int(len(label)/2)*8-extra_pad,1,0)  
            #center the scale about the vertical axis--and use this to offset the needle, and scale text
            Offset = (270 +  sa / 2) * degtorad
            # find hte scale step value based on the hival low val and the scale sweep angle
            # deducting a small value to eliminate round off errors
            # this val may need to be adjusted            
            # draw the scale and numbers
            # note draw this each time to repaint where the needle was
            ticks = linspace(0,sa,no_of_steps)
            labels = linspace(loval,hival,no_of_steps)
            for i in range(no_of_steps):
              angle = ( ticks[i]  * degtorad)
              angle = Offset - angle 
              ox =  int((r - 2) * cos(angle) + cx)
              oy =  int((r - 2) * sin(angle) + cy)
              ix =  int((r - 10) * cos(angle) + cx)
              iy =  int((r - 10) * sin(angle) + cy)
              tx =  int((r + 10) * cos(angle) + cx + 8)
              ty =  int((r + 10) * sin(angle) + cy)
              self.line(ox, oy, ix, iy, 1)        
              if i == 0 or i == no_of_steps-1:
                  num_str = str(int(labels[no_of_steps-i-1]))
                  self.text(num_str,tx-13,ty-15)          
            
            for i in range(sa):
              angle = ( i  * degtorad)
              angle = Offset - angle 
              ox =  int((r - 2) * cos(angle) + cx)
              oy =  int((r - 2) * sin(angle) + cy)
              self.pixel(ox, oy, 1)
            
        Offset = (270 +  sa / 2) * degtorad  
        # compute and draw the needle
        angle = (sa * (1 - (((curval - loval) / (hival - loval)))))
        angle = angle * degtorad
        angle = Offset - angle 
        ix =  int((r - 10) * cos(angle) + cx)
        iy =  int((r - 10) * sin(angle) + cy)
        # draw a triangle for the needle (compute and store 3 vertiticies)
        lx =  int(2 * cos(angle - 90 * degtorad) + cx)
        ly =  int(2 * sin(angle - 90 * degtorad) + cy)
        rx =  int(2 * cos(angle + 90 * degtorad) + cx)
        ry =  int(2 * sin(angle + 90 * degtorad) + cy);
        llim_x = cx-r
        hlim_x = cx+r
        llim_y = cy-r
        hlim_y = cy+r
   
        if ix > hlim_x or lx > hlim_x or rx > hlim_x or ix < llim_x or lx < llim_x or rx < llim_x: 
            return
        if iy > hlim_y or ly > hlim_y or ry > hlim_y or iy < llim_y or ly < llim_y or ry < llim_y: 
            return
       
        self.line(self.pix, self.piy, self.plx, self.ply, 0)
        self.line(self.pix, self.piy, self.prx, self.pry, 0)       
        self.line(ix, iy, lx, ly, 1)
        self.line(ix, iy, rx, ry, 1)        
        self.fill_circle(cx, cy, int(r/10), 1)
        self.pix = ix
        self.piy = iy
        self.plx = lx
        self.ply = ly
        self.prx = rx
        self.pry = ry     
        self.show(True)
        return Redraw
    
    def continuous_graph(self, x, y, gx, gy, w, h, xlo, xhi, ylo, yhi, label, Redraw):
        if (Redraw == True):
            self.init_display()
            Redraw = False            
            self.fill_rect(0,0,127,10,1)
            extra_pad = 0
            if(len(label)%2 == 1):
                extra_pad = 2
            self.text(label,64-int(len(label)/2)*8-extra_pad,1,0)
            self.ox = (x - xlo) * ( w) / (xhi - xlo) + gx;
            self.oy = (y - ylo) * (gy - h - gy) / (yhi - ylo) + gy
            # draw y scale
            y_scale = linspace(ylo,yhi,3)
            for i in y_scale:               
              temp =  (i - ylo) * (gy - h - gy) / (yhi - ylo) + gy
              if (i == 0):
                self.hline(int(gx - 3), int(temp),int( w + 3), 1)              
              else:
                self.hline(int(gx - 3), int(temp), 3, 1)                          
              self.text(str(int(i)),int(gx - 18)-(len(str(int(i)))-2)*8, int(temp - 3))     #gx-27 
            #xscale
            x_scale = linspace(xlo,xhi,3)       
            for i in x_scale:
              #compute the transform         
              temp =  (i - xlo) * ( w) / (xhi - xlo) + gx
              if (i == 0):
                self.vline(int(temp), int(gy - h), int(h + 3), 1)                  
              else:
                self.vline(int(temp), int(gy), 3, 1)              
              self.text(str(int(i)), int(temp)-6,int(gy) + 6);
            
        #graph drawn now plot the data
        x =  (x - xlo) * ( w) / (xhi - xlo) + gx;
        y =  (y - ylo) * (gy - h - gy) / (yhi - ylo) + gy;
        self.line(int(self.ox), int(self.oy), int(x), int(y), 1);
        self.line(int(self.ox), int(self.oy - 1), int(x), int(y - 1), 1);
        self.ox = x;
        self.oy = y;
        self.data_points += 1
        if self.data_points == w+10:
            Redraw = True
            self.data_points = 0
        # up until now print sends data to a video buffer NOT the screen
        # this call sends the data to the screen
        self.show(True);        
        return Redraw

    def fill_circle(self, x0, y0, radius, color):
        # Filled circle drawing function.  Will draw a filled circule with
        # center at x0, y0 and the specified radius.
        self.vline(int(x0), int(y0 - radius), int(2*radius + 1), color)
        f = 1 - radius
        ddF_x = 1
        ddF_y = -2 * radius
        x = 0
        y = radius
        while x < y:
            if f >= 0:
                y -= 1
                ddF_y += 2
                f += ddF_y
            x += 1
            ddF_x += 2
            f += ddF_x
            self.vline(int(x0 + x), int(y0 - y), int(2*y + 1), color)
            self.vline(int(x0 + y), int(y0 - x), int(2*x + 1), color)
            self.vline(int(x0 - x), int(y0 - y), int(2*y + 1), color)
            self.vline(int(x0 - y), int(y0 - x), int(2*x + 1), color)
        self.show(True)
            
    def fill_triangle(self, x0, y0, x1, y1, x2, y2, color):
        # Filled triangle drawing function.  Will draw a filled triangle around
        # the points (x0, y0), (x1, y1), and (x2, y2).
        if y0 > y1:
            y0, y1 = y1, y0
            x0, x1 = x1, x0
        if y1 > y2:
            y2, y1 = y1, y2
            x2, x1 = x1, x2
        if y0 > y1:
            y0, y1 = y1, y0
            x0, x1 = x1, x0
        a = 0
        b = 0
        y = 0
        last = 0
        if y0 == y2:
            a = x0
            b = x0
            if x1 < a:
                a = x1
            elif x1 > b:
                b = x1
            if x2 < a:
                a = x2
            elif x2 > b:
                b = x2
            self.hline(int(a), int(y0), int(b-a+1), color)
            return
        dx01 = x1 - x0
        dy01 = y1 - y0
        dx02 = x2 - x0
        dy02 = y2 - y0
        dx12 = x2 - x1
        dy12 = y2 - y1
        if dy01 == 0:
            dy01 = 1
        if dy02 == 0:
            dy02 = 1
        if dy12 == 0:
            dy12 = 1
        sa = 0
        sb = 0
        if y1 == y2:
            last = y1
        else:
            last = y1-1
        for y in range(y0, last+1):
            a = x0 + sa // dy01
            b = x0 + sb // dy02
            sa += dx01
            sb += dx02
            if a > b:
                a, b = b, a
            self.hline(a, y, b-a+1, color)
        sa = dx12 * (y - y1)
        sb = dx02 * (y - y0)
        while y <= y2:
            a = x1 + sa // dy12
            b = x0 + sb // dy02
            sa += dx12
            sb += dx02
            if a > b:
                a, b = b, a
            self.hline(a, y, b-a+1, color)
            y += 1
        self.show(True)

    def draw_arrow(self, x: int, y: int, dir: int, c: int = 1):
        j_max = 2
        if dir == SH1106.LEFT: #left
            for i in range(4):
                if i == 3:
                    j_max = 3
                for j in range(j_max):
                    self.pixel(x+i+j, y+i, c)
                    self.pixel(x+i+j, y-i, c)
        elif dir == SH1106.UP: #up
            for i in range(4):
                if i == 3:
                    j_max = 3
                for j in range(j_max):
                    self.pixel(x+i, y+i+j, c)
                    self.pixel(x-i, y+i+j, c)
        elif dir == SH1106.RIGHT: #right
            for i in range(4):
                if i == 3:
                    j_max = 3
                for j in range(j_max):
                    self.pixel(x-i-j, y+i, c)
                    self.pixel(x-i-j, y-i, c)
        elif dir == SH1106.DOWN: #down
            for i in range(4):
                if i == 3:
                    j_max = 3
                for j in range(j_max):
                    self.pixel(x+i, y-i-j, c)
                    self.pixel(x-i, y-i-j, c)

            

    def centered_text(self, s: str, y: int, c: int = 1):
        x = max((self.width - 8 * len(s)) // 2, 0)
        self.text(s, x, y, c)


class SH1106_I2C(SH1106):
    def __init__(self, width, height, i2c, i2c_lock, res=None, addr=0x3c,
                 rotate=0, external_vcc=False):
        self.i2c = i2c
        self.i2c_lock = i2c_lock
        self.addr = addr
        self.res = res
        self.temp = bytearray(2)
        if res is not None:
            res.init(res.OUT, value=1)
        super().__init__(width, height, external_vcc, rotate)

    def write_cmd(self, cmd):
        self.temp[0] = 0x80  # Co=1, D/C#=0
        self.temp[1] = cmd
        with self.i2c_lock:
            self.i2c.writeto(self.addr, self.temp)

    def write_data(self, buf):
        with self.i2c_lock:
            self.i2c.writeto(self.addr, b'\x40'+buf)

    def reset(self):
        super().reset(self.res)


class SH1106_SPI(SH1106):
    def __init__(self, width, height, spi, dc, res=None, cs=None,
                 rotate=0, external_vcc=False):
        self.rate = 10 * 1000 * 1000
        dc.init(dc.OUT, value=0)
        if res is not None:
            res.init(res.OUT, value=0)
        if cs is not None:
            cs.init(cs.OUT, value=1)
        self.spi = spi
        self.dc = dc
        self.res = res
        self.cs = cs
        super().__init__(width, height, external_vcc, rotate)

    def write_cmd(self, cmd):
        self.spi.init(baudrate=self.rate, polarity=0, phase=0)
        if self.cs is not None:
            self.cs(1)
            self.dc(0)
            self.cs(0)
            self.spi.write(bytearray([cmd]))
            self.cs(1)
        else:
            self.dc(0)
            self.spi.write(bytearray([cmd]))

    def write_data(self, buf):
        self.spi.init(baudrate=self.rate, polarity=0, phase=0)
        if self.cs is not None:
            self.cs(1)
            self.dc(1)
            self.cs(0)
            self.spi.write(buf)
            self.cs(1)
        else:
            self.dc(1)
            self.spi.write(buf)

    def reset(self):
        super().reset(self.res)