from machine import PWM, Pin

from lib.hw_defs.pins import BUZZER_PWM_PIN


MAX_DUTY = 65535

# Object for controlling the buzzer inside cube
# Buzzer is controlled by PWM signal with given frequency and duty cycle
class Buzzer:
    def __init__(self):
        self.buzzer = PWM(Pin(BUZZER_PWM_PIN))
    
    # Set frequency and duty cycle (0-1) of the buzzer
    def set_freq_duty(self, freq: int, duty: float):
        duty = 0 if duty < 0 else duty
        duty = 1 if duty > 1 else duty
        self.buzzer.freq(freq)
        self.buzzer.duty_u16(round(MAX_DUTY-MAX_DUTY*duty))

    def off(self):
        self.buzzer.duty_u16(0)