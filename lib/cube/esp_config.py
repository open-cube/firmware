from micropython import const
from machine import UART, Timer, Pin
import utime
import struct

from lib.hw_defs.pins import INTERNAL_UART_HW_ID, INTERNAL_UART_TX_PIN, INTERNAL_UART_RX_PIN, BUTTON_POWER_RPIN_BIT

class Esp:
    MAX_PAYLOAD = const(255)

    COM_RESET = const(0b10101111)
    COM_ACK = const(0b10110000)
    COM_NACK = const(0b10110001)
    COM_NAME = const(0b00000011)
    COM_PASSWORD = const(0b0000010)

    COM_BT = const(0b0000100)
    COM_BT_PIN = const(0b0000111)
    COM_BT_PAIR = const(0b0001000)
    COM_BT_CANCEL = const(0b0001001)
    COM_BT_SUCCESS = const(0b0001011)

    COM_WIFI = const(0b0001010)
    COM_WIFI_BUTTONS = const(0b0001100)
    COM_WIFI_INDICATORS = const(0b0001101)
    COM_WIFI_SWITCHES = const(0b0001110)
    COM_WIFI_BUTTONS_STR = const(0b0001111)
    COM_WIFI_SWITCHES_STR = const(0b0010000)
    COM_WIFI_INDICATORS_STR = const(0b0010001)
    COM_WIFI_NUMBERS = const(0b0010010)
    COM_WIFI_NUMBERS_STR = const(0b0010011)

    ACK = const(0)
    NACK = const(1)
    TIMEOUT = const(2)
    
    WIFI_PAYLOAD_IND_ON = const(0b00000001)
    WIFI_PAYLOAD_IND_OFF = const(0b00000010)

    NACK_ATTEMPTS = const(5)

    NUM_INDICATORS = const(5)
    NUM_BUTTONS = const(9)
    NUM_SWITCHES = const(5)
    NUM_NUMBERS = const(6)

    MODE_BT = const(0)
    MODE_WIFI = const(1)
    MODE_CMD = const(2)

    def __init__(self, pcf_buttons, baud_rate=115200):
        self.uart = UART(INTERNAL_UART_HW_ID, baudrate=baud_rate, bits=8, parity=None, stop=1, 
                         tx=Pin(INTERNAL_UART_TX_PIN), rx=Pin(INTERNAL_UART_RX_PIN), flow=0, invert=0)
        self.uart_timer = Timer(-1)
        self.uart_timer.init(mode=Timer.PERIODIC, period=10, callback=self.UART_RX_handler)
        self.pcf_buttons = pcf_buttons
        # CONFIG
        self.name = ""
        self.header = 0
        self.length = 0
        self.message_idx = 0
        self.message_cmd = bytes()
        self.message_tmp_cmd = bytes()
        self.payload = bytes()
        self.new_cmd = False
        self.esp_running = False
        self.esp_mode = Esp.MODE_BT
        self.esp_mode_prev = Esp.MODE_BT
        self.not_responding = 0
        # BT
        self.messageTmp = ""
        self.message = ""
        self.binary_message = bytes()
        self.binary_mode = False
        self.binary_header = []
        self.header = []
        self.header_idx = 0
        self.header_read = False
        self.data_size = 0
        self.data_idx = 0
        self.new_message = False
        # WIFI
        self.wifi_indicators = tuple([False]*Esp.NUM_INDICATORS)
        self.wifi_buttons = tuple([False]*Esp.NUM_BUTTONS)
        self.wifi_switches = tuple([False]*Esp.NUM_SWITCHES)
    

    # ************ USER FUNCTIONS ************
    # ---------------- ESP BT ----------------
    # Return received message form ESP if available
    # In ASCII mode return string message
    # In binary mode return bytes message
    def bt_read(self):
        if self.esp_mode == Esp.MODE_BT and self.bt_message_ready():
            self.new_message = False
            if self.binary_mode:
                return self.binary_message
            else:
                return self.message
        else:
            return None

    # Send message to ESP
    # In ASCII mode send string message
    # In binary mode send bytes message with previously set header
    def bt_write(self, message):
        if self.esp_mode == Esp.MODE_BT:
            if self.binary_mode:
                self.uart.write(self.binary_header+message)
            else:
                self.uart.write(message)
    
    # 
    def bt_message_ready(self):
        if self.esp_mode == Esp.MODE_BT:
            return self.new_message
        else:
            return None
    
    # Set binary mode if header is not none
    # Header is tuple of numbers (0-255)
    # Data size is number of bytes in message (without header)
    def bt_set_binary(self, header=None, data_size=0):
        if header:
            self.header = header
            header_format = ''
            for i in header:
                header_format += "B"
            self.binary_header = struct.pack('<{}'.format(header_format), *header)
            self.data_size = data_size
            self.binary_mode = True
        else:
            self.binary_mode = False

    # --------------- ESP WIFI ---------------
    def make_bytes_from_bools(self, byte_len: int, bools: list):
        bytes_t = bytes()
        for b in range(byte_len):
            next_byte = 0
            for i, boo in enumerate(bools):
                if boo:
                    next_byte |= (1 << i)
            bytes_t += self.i2b(next_byte)
        return bytes_t

    def make_bools_from_bytes(self, bools_len: int, bytes_t: bytes):
        bools = list()
        counter = 0
        for b in range(bools_len // 8 + 1):
            for i in range(8):
                bools.append((bytes_t[b] & (1 << i)) != 0)
                counter += 1
                if counter == bools_len:
                    break
        return bools

    def wifi_set_indicators(self, indicators: list):
        if len(indicators) != Esp.NUM_INDICATORS:
            return
        message = self.i2b(Esp.COM_WIFI_INDICATORS)
        payload_len = Esp.NUM_INDICATORS // 8 + 1
        message += self.i2b(payload_len)
        message += self.make_bytes_from_bools(payload_len, indicators)
        self.send_message(message)

    def wifi_set_numbers(self, numbers: list):
        if len(numbers) != Esp.NUM_NUMBERS:
            return
        message = self.i2b(Esp.COM_WIFI_NUMBERS)
        payload_len = Esp.NUM_NUMBERS*4
        message += self.i2b(payload_len)
        for number in numbers:
            message += struct.pack('<f', number)
        self.send_message(message)

    def wifi_set_buttons_labels(self, labels: list):
        if len(labels) != Esp.NUM_BUTTONS:
            return
        for i in range(Esp.NUM_BUTTONS):
            self.wifi_set_label(Esp.COM_WIFI_BUTTONS_STR, i, labels[i])
    
    def wifi_set_switches_labels(self, labels: list):
        if len(labels) != Esp.NUM_SWITCHES:
            return
        for i in range(Esp.NUM_SWITCHES):
            self.wifi_set_label(Esp.COM_WIFI_SWITCHES_STR, i, labels[i])

    def wifi_set_indicators_labels(self, labels: list):
        if len(labels) != Esp.NUM_INDICATORS:
            return
        for i in range(Esp.NUM_INDICATORS):
            self.wifi_set_label(Esp.COM_WIFI_INDICATORS_STR, i, labels[i])
    
    def wifi_set_numbers_labels(self, labels: list):
        if len(labels) != Esp.NUM_NUMBERS:
            return
        for i in range(Esp.NUM_NUMBERS):
            self.wifi_set_label(Esp.COM_WIFI_NUMBERS_STR, i, labels[i])

    def wifi_set_label(self, cmd, id: int, label: str):
        if len(label) > 9:
            return
        message = self.i2b(cmd)
        message += self.i2b(len(label)+1)
        message += self.i2b(id)
        message += bytes(label, 'utf-8')
        self.send_message(message)

    def wifi_get_buttons(self):
        return tuple(self.wifi_buttons)

    def wifi_get_switches(self):
        return tuple(self.wifi_switches)

    def wifi_new_cmd(self, header, payload):
        if header == Esp.COM_WIFI_BUTTONS:
            self.wifi_buttons = self.make_bools_from_bytes(Esp.NUM_BUTTONS, payload)
        elif header == Esp.COM_WIFI_SWITCHES:
            self.wifi_switches = self.make_bools_from_bytes(Esp.NUM_SWITCHES, payload)
        
    # -------------- ESP CONFIG --------------
    def reset(self, timeout=2000):
        self.cmd_start()
        self.esp_mode_prev = Esp.MODE_BT
        self.flush()
        time = utime.ticks_ms()
        while utime.ticks_diff(utime.ticks_ms(), time) < timeout:
            self.send_reset()
            utime.sleep_ms(5)
            if self.ack_received() == Esp.ACK:
                self.esp_running = True
                self.cmd_stop()
                return True
        self.esp_running = False
        self.cmd_stop()
        return False
    
    def reset_non_blocking(self):
        self.cmd_start()
        self.esp_mode_prev = Esp.MODE_BT
        self.flush()
        self.send_reset()
        self.esp_running = True
        self.cmd_stop()

    def bt(self):
        if self.esp_mode == Esp.MODE_WIFI:
            self.esp_mode = Esp.MODE_BT
            self.cmd_start()
            response = self.repeat_cmd(Esp.COM_BT_PIN, 50)
            self.cmd_stop()
            return response
        else:
            return None

    def wifi(self):
        response = None
        if self.esp_mode == Esp.MODE_BT:
            self.esp_mode = Esp.MODE_WIFI
            self.cmd_start()
            response = self.repeat_cmd(Esp.COM_WIFI, 50)
            self.cmd_stop()
            utime.sleep(0.6)
        return response

    def set_name(self, name: str, timeout=50):
        self.name = name
        self.cmd_start()
        response = self.repeat_str(Esp.COM_NAME, name, timeout)
        self.cmd_stop()
        utime.sleep(0.6)
        return response

    def set_name_nonblocking(self, name: str, timeout=50):
        self.name = name
        self.cmd_start()
        response = self.repeat_str(Esp.COM_NAME, name, timeout)
        self.cmd_stop()
        return response
    
    def get_name(self):
        return self.name
    
    def set_password(self, password: str, timeout=50):
        self.cmd_start()
        response = self.repeat_str(Esp.COM_PASSWORD, password, timeout)
        self.cmd_stop()
        utime.sleep(0.6)
        return response

    # ********** INTERNAL FUNCTIONS **********
    def deinit(self):
        self.uart_timer.deinit()
        self.uart.deinit()

    def cmd_start(self):
        if self.esp_mode != Esp.MODE_CMD:
            self.esp_mode_prev = self.esp_mode
            self.esp_mode_prev = self.esp_mode
            self.esp_mode = Esp.MODE_CMD
            self.pcf_buttons.set_pin(BUTTON_POWER_RPIN_BIT, False)
            utime.sleep(0.01)
            self.flush()
       
    def cmd_stop(self):
        if self.esp_mode == Esp.MODE_CMD:
            utime.sleep(0.01)
            self.flush()
            self.esp_mode = self.esp_mode_prev
            self.pcf_buttons.set_pin(BUTTON_POWER_RPIN_BIT, True)
            utime.sleep(0.01)

    def timeout(self):
        self.not_responding += 1
        if self.not_responding > 5:
            self.esp_mode = Esp.MODE_BT
            self.esp_running = False
            self.not_responding = 0
        else:
            self.flush()

    def running(self):
        return self.esp_running
    
    def flush(self):
        while self.uart.any():
            self.uart.read()
        self.message_idx = 0
        self.new_cmd = False
        self.message_tmp_cmd = bytes()

    def message_cmd_ready(self):
        return self.new_cmd
    
    def read_message(self):
        if self.message_cmd_ready():
            self.new_cmd = False
            return self.message_cmd
        else:
            return None
        
    def wait_for_ack(self, timeout=100):
        time = utime.ticks_ms()
        while utime.ticks_diff(utime.ticks_ms(), time) < timeout:
            utime.sleep_us(100)
            ack = self.ack_received()
            if ack != Esp.TIMEOUT:
                return ack
        return Esp.TIMEOUT
    
    def ack_received(self):
        if self.new_cmd:
            if self.header == Esp.COM_ACK:
                self.new_cmd = False
                return Esp.ACK
            if self.header == Esp.COM_NACK:
                self.new_cmd = False
                return Esp.NACK
        else:
            return Esp.TIMEOUT

    def calculate_checksum(self, data: bytes) -> int:
        csum = 0xFF
        for byte in data:
            csum = csum ^ byte
        return csum

    def i2b(self, number: int) -> bytes:
        return bytes([number])

    def b2i(self, array: bytes) -> int:
        return array[0]
    
    def req_bt_pin(self, timeout=100):
        self.cmd_start()
        time = utime.ticks_ms()
        self.send_message(self.i2b(Esp.COM_BT_PIN)+self.i2b(0))
        while utime.ticks_diff(utime.ticks_ms(), time) < timeout:
            utime.sleep_ms(1)
            if self.ack_received() == Esp.NACK:
                self.cmd_stop()
                return None
            if self.new_cmd and self.header == Esp.COM_BT_PIN:
                self.new_cmd = False
                return struct.unpack('<I', self.payload)[0]
        self.cmd_stop()
        return -1

    def send_reset(self):
        self.send_message(self.i2b(Esp.COM_RESET)+self.i2b(0))

    def send_ack(self):
        self.send_message(self.i2b(Esp.COM_ACK)+self.i2b(0))
    
    def send_nack(self):
        self.send_message(self.i2b(Esp.COM_NACK)+self.i2b(0))
    
    def send_pair(self):
        self.cmd_start()
        response = self.repeat_cmd(Esp.COM_BT_PAIR, 50)
        self.cmd_stop()
        return response

    def send_cancel(self):
        self.cmd_start()
        response = self.repeat_cmd(Esp.COM_BT_CANCEL, 50)
        self.cmd_stop()
        return response

    def repeat_cmd(self, command: int, timeout=50):
        for i in range(Esp.NACK_ATTEMPTS):
            self.send_message(self.i2b(command)+self.i2b(0))
            receive = self.wait_for_ack(timeout)
            if receive != Esp.TIMEOUT:
                break
        return receive
    
    def repeat_str(self, command: int, str: str, timeout=50):
        for i in range(Esp.NACK_ATTEMPTS):
            self.send_str(command, str)
            receive = self.wait_for_ack(timeout)
            if receive != Esp.TIMEOUT:
                break
        return receive
    
    def send_str(self, command: int, str: str):
        str_len = len(str)
        if str_len > Esp.MAX_PAYLOAD:
            return
        message = self.i2b(command)
        message += self.i2b(str_len)
        message += bytes(str, 'utf-8')
        self.send_message(message)

    def send_message(self, message: bytes):
        csum = self.calculate_checksum(message)
        message += self.i2b(csum)
        self.uart.write(message)
    
    # ************ UART HANDLER ************
    def config_message_receive(self, b_ch):
        #print(chr(struct.unpack('<B', b_ch)[0]), end="")
        self.message_tmp_cmd += b_ch
        if self.message_idx == 0:
            self.header = struct.unpack('<B', b_ch)[0]
            self.message_idx += 1
            if (self.esp_mode == Esp.MODE_WIFI 
                and self.header != Esp.COM_WIFI_BUTTONS 
                and self.header != Esp.COM_WIFI_SWITCHES):
                self.flush()
        elif self.message_idx == 1:
            self.length = struct.unpack('<B', b_ch)[0]
            self.message_idx += 1
        elif self.message_idx == self.length + 2:
            self.message_idx = 0
            if self.calculate_checksum(self.message_tmp_cmd) == 0:
                self.message_cmd = self.message_tmp_cmd
                self.payload = self.message_cmd[2:-1]
                self.message_tmp_cmd = bytes()
                self.new_cmd = True
                self.message_idx = 0
                if self.esp_mode == Esp.MODE_WIFI:
                    self.wifi_new_cmd(self.header, self.payload)
            else:
                self.send_nack()
        else:
            self.message_idx += 1

    def bt_message_receive(self, b_ch):
        if self.binary_mode:
            if self.header_read:
                self.binary_message += b_ch
                self.data_idx += 1
                if self.data_idx == self.data_size:       
                    self.new_message = True
                    self.header_read = False
                    self.data_idx = 0
            else:
                u_ch = struct.unpack('<B', b_ch)[0]
                if u_ch == self.header[self.header_idx]:
                    self.header_idx += 1
                    if len(self.header) == self.header_idx:
                        self.header_read = True
                        self.binary_message = bytes()
                        self.header_idx = 0
                else:
                    self.header_idx = 0
        else:
            ascii_code = int.from_bytes(b_ch, 'big')
            character = chr(ascii_code)
            if ascii_code == 10 or ascii_code == 13:
                self.message = self.messageTmp
                self.messageTmp = ""
                self.new_message = True
            else:
                self.messageTmp = self.messageTmp + character

    def UART_RX_handler(self, p):
        while self.uart.any():
            b_ch = self.uart.read(1)
            if self.esp_mode == Esp.MODE_CMD or self.esp_mode == Esp.MODE_WIFI:
                self.config_message_receive(b_ch)
            elif self.esp_mode == Esp.MODE_BT:
                self.bt_message_receive(b_ch)