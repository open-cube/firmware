from machine import I2C, Pin

from lib.hw_defs.pins import UTZ_I2C_SDA_PIN, UTZ_I2C_SCK_PIN, I2C_STRONG_PULL_RPIN_BIT 
from lib.robot_consts import I2C_MULTICUBE_FREQ

class I2C_master:
    def __init__(self, pcf_buttons, general_add=0x41):
        self.general_add = general_add
        self.pcf_buttons = pcf_buttons
        self.data_received = None
        self.data_sending = None
        self.start()

    def start(self):
        self.pcf_buttons.set_pin(I2C_STRONG_PULL_RPIN_BIT, False)
        self.i2c = I2C(id=0,scl=Pin(UTZ_I2C_SCK_PIN), 
                       sda=Pin(UTZ_I2C_SDA_PIN), freq=I2C_MULTICUBE_FREQ)

    def read(self, add, len=10):
        self.data_received = None
        try:
            self.data_received = self.i2c.readfrom(add, len)
        finally:
            return self.data_received

    def write(self, add, message):
        self.data_sending = message
        try:
            self.i2c.writeto(add, message)
        except Exception as e:
            return False
        return True

    def write_general(self, message):
        try:
            self.i2c.writeto(self.general_add, message)
        except:
            return False
        return True
    
    def get_general_add(self):
        return self.general_add
    
    def deinit(self):
        self.i2c = None
        self.pcf_buttons.set_pin(I2C_STRONG_PULL_RPIN_BIT, True)