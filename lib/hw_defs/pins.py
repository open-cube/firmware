"""
Open-Cube pin assignments.

Source: Open-Cube schematics (LEGOkostka.pdf)
+ Fixed a bug with missing power button input.
"""
from micropython import const

# RP2040 local pins
MOTOR1_PWM_PIN = const(0)
MOTOR2_PWM_PIN = const(1)
MOTOR3_PWM_PIN = const(2)
MOTOR4_PWM_PIN = const(3)

SENSOR1_UART_TX_PIN = const(4)
SENSOR1_UART_RX_PIN = const(5)
SENSOR1_UART_HW_ID = const(1)

SENSOR2_UART_TX_PIN = const(6)
SENSOR2_UART_RX_PIN = const(7)

SENSOR3_UART_TX_PIN = const(8)
SENSOR3_UART_RX_PIN = const(9)

SENSOR4_UART_TX_PIN = const(10)
SENSOR4_UART_RX_PIN = const(11)

INTERNAL_UART_TX_PIN = const(12)
INTERNAL_UART_RX_PIN = const(13)
INTERNAL_UART_HW_ID = const(0)

INTERNAL_I2C_SDA_PIN = const(14)
INTERNAL_I2C_SCK_PIN = const(15)
UTZ_I2C_SDA_PIN = const(16)
UTZ_I2C_SCK_PIN = const(17)
MOTOR1_ENC_A_PIN = const(18)
MOTOR1_ENC_B_PIN = const(19)
MOTOR2_ENC_A_PIN = const(20)
MOTOR2_ENC_B_PIN = const(21)
MOTOR3_ENC_A_PIN = const(22)
MOTOR3_ENC_B_PIN = const(23)
MOTOR4_ENC_A_PIN = const(24)
MOTOR4_ENC_B_PIN = const(25)

TURN_OFF_PIN = const(26)
VBATT_ADC_PIN = const(27)
ICM_IRQ_PIN = const(28)
BUZZER_PWM_PIN = const(29)

# PCF8575 remote pins (_RPIN suffix)
MOTOR1_EN_RPIN = const(0)
MOTOR2_EN_RPIN = const(1)
MOTOR3_EN_RPIN = const(2)
MOTOR4_EN_RPIN = const(3)
MOTOR1_DIR_RPIN = const(4)
MOTOR2_DIR_RPIN = const(5)
MOTOR3_DIR_RPIN = const(6)
MOTOR4_DIR_RPIN = const(7)

# pin numbers as specified in datasheet
BUTTON_POWER_RPIN = const(10)
BUTTON_RIGHT_RPIN = const(11)
BUTTON_DOWN_RPIN = const(12)
BUTTON_OK_RPIN = const(13)
BUTTON_UP_RPIN = const(14)
BUTTON_LEFT_RPIN = const(15)
USER_LED_RPIN = const(16)
I2C_STRONG_PULL_RPIN = const(17)

# pin bit offsets in returned words
BUTTON_POWER_RPIN_BIT = const(8)
BUTTON_RIGHT_RPIN_BIT = const(9)
BUTTON_DOWN_RPIN_BIT = const(10)
BUTTON_OK_RPIN_BIT = const(11)
BUTTON_UP_RPIN_BIT = const(12)
BUTTON_LEFT_RPIN_BIT = const(13)
USER_LED_RPIN_BIT = const(14)
I2C_STRONG_PULL_RPIN_BIT = const(15)

# ADS1117 analog input pins (_APIN suffix)
SENSOR1_APIN = const(3)
SENSOR2_APIN = const(2)
SENSOR3_APIN = const(1)
SENSOR4_APIN = const(0)

# internal I2C addresses
IMU_I2C_ADDR = const(0x68)  # ICM-20608 gyro+accelerometer
ADC_I2C_ADDR = const(0x40)  # ADS1119 ADC converter
PCF_I2C_ADDR = const(0x20)  # PCF8575 pin extender
DISPLAY_I2C_ADDR = const(0x3C)  # SH1106-compatible display

# Battery voltage measurement
VBATT_SCALE_FACTOR = const(4)  # ADC input = resistive divider giving 1/4 of Vbatt
VBATT_REF_VOLTAGE_NUM = const(305)  # ADC Vref voltage - for determining real battery voltage. Fraction numerator
VBATT_REF_VOLTAGE_DEN = const(100)  # Fraction denominator (ref voltage in volts = numerator / denominator)
VBATT_ADC_MAX_CODE = const(4095)  # ADC resolution
VBATT_ADC_MAX_CODE_U16 = const(65535)  # Max ADC code returned by MicroPython's read_u16
