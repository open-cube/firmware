from micropython import const

# PIO 0
SM_SENSOR2_UART_RX = const(0)  # receiver for UART2_RX
SM_SENSOR3_UART_RX = const(1)  # receiver for UART3_RX
SM_SENSOR4_UART_RX = const(2)  # receiver for UART4_RX
SM_ULTRASONIC_READ1 = const(3)  # UTZ software I2C - program 1

# PIO 1
SM_SENSOR2_UART_TX = const(4)  # transmitter for UART2_TX
SM_SENSOR3_UART_TX = const(5)  # transmitter for UART3_TX
SM_SENSOR4_UART_TX = const(6)  # transmitter for UART4_TX
SM_ULTRASONIC_READ2 = const(7)  # UTZ software I2C - program 2
