"""
Open-Cube port pin maps.
"""
from lib.hw_defs.pins import *
from lib.hw_defs.pio import *


class MotorPortPins:
    def __init__(self, pwm_pin, enc_a_pin, enc_b_pin, en_rpin, dir_rpin):
        self.pwm_pin = pwm_pin
        self.enc_a_pin = enc_a_pin
        self.enc_b_pin = enc_b_pin
        self.en_rpin = en_rpin
        self.dir_rpin = dir_rpin


class SensorPortPins:
    def __init__(self, uart, analog_apin):
        self.uart = uart
        self.analog_apin = analog_apin


class SensorHwUart:
    def __init__(self, uart_id: int, rx_pin: int, tx_pin: int):
        self.uart_id = uart_id
        self.rx_pin = rx_pin
        self.tx_pin = tx_pin


class SensorPioUart:
    def __init__(self, rx_sm_id: int, tx_sm_id: int, rx_pin: int, tx_pin: int):
        self.rx_sm_id = rx_sm_id
        self.tx_sm_id = tx_sm_id
        self.rx_pin = rx_pin
        self.tx_pin = tx_pin


class I2cPortPins:
    def __init__(self, sda, sck):
        self.sda_pin = sda
        self.scl_pin = sck


MOTOR_PORT_PINS = [
    MotorPortPins(
        MOTOR1_PWM_PIN,
        MOTOR1_ENC_A_PIN,
        MOTOR1_ENC_B_PIN,
        MOTOR1_EN_RPIN,
        MOTOR1_DIR_RPIN,
    ),
    MotorPortPins(
        MOTOR2_PWM_PIN,
        MOTOR2_ENC_A_PIN,
        MOTOR2_ENC_B_PIN,
        MOTOR2_EN_RPIN,
        MOTOR2_DIR_RPIN,
    ),
    MotorPortPins(
        MOTOR3_PWM_PIN,
        MOTOR3_ENC_A_PIN,
        MOTOR3_ENC_B_PIN,
        MOTOR3_EN_RPIN,
        MOTOR3_DIR_RPIN,
    ),
    MotorPortPins(
        MOTOR4_PWM_PIN,
        MOTOR4_ENC_A_PIN,
        MOTOR4_ENC_B_PIN,
        MOTOR4_EN_RPIN,
        MOTOR4_DIR_RPIN,
    ),
]

SENSOR_PORT_PINS = [
    SensorPortPins(
        SensorHwUart(
            SENSOR1_UART_HW_ID,
            SENSOR1_UART_RX_PIN,
            SENSOR1_UART_TX_PIN,
        ),
        SENSOR1_APIN,
    ),
    SensorPortPins(
        SensorPioUart(
            SM_SENSOR2_UART_RX,
            SM_SENSOR2_UART_TX,
            SENSOR2_UART_RX_PIN,
            SENSOR2_UART_TX_PIN,
        ),
        SENSOR2_APIN,
    ),
    SensorPortPins(
        SensorPioUart(
            SM_SENSOR3_UART_RX,
            SM_SENSOR3_UART_TX,
            SENSOR3_UART_RX_PIN,
            SENSOR3_UART_TX_PIN,
        ),
        SENSOR3_APIN,
    ),
    SensorPortPins(
        SensorPioUart(
            SM_SENSOR4_UART_RX,
            SM_SENSOR4_UART_TX,
            SENSOR4_UART_RX_PIN,
            SENSOR4_UART_TX_PIN,
        ),
        SENSOR4_APIN,
    ),
]

ULTRASONIC_PORT_PINS = I2cPortPins(UTZ_I2C_SDA_PIN, UTZ_I2C_SCK_PIN)
