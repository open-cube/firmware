from micropython import const

from lib.OC import EV3UartSensor

LASER_SENSOR_ID = const(192)


class LaserSensor(EV3UartSensor):
    MODE_NORMAL = const(0)
    MODE_FOV = const(1)
    MODE_SHORT = const(2)

    def __init__(self, port: int):
        """
        Initialize a new OC laser sensor object at the given port.
        :param port: Port to which the sensor is attached. Use one of Port.S1~Port.S4.
        """
        super().__init__(port, LASER_SENSOR_ID)

    def distance(self) -> int:
        """
        Measure the proximity to the nearest object in milimetres.
        :return: Proximity in milimetres (0-4000)
        """
        return self.read_raw_mode(LaserSensor.MODE_NORMAL)[0]
    
    def distance_fov(self) -> int:
        """
        Measure the proximity to the nearest object in milimetres with sensor large FOV.
        :return: Proximity in milimetres (0-4000)
        """
        return self.read_raw_mode(LaserSensor.MODE_FOV)[0]
    
    def distance_short(self) -> int:
        """
        Measure the proximity to the nearest object in milimetres. Better for short distances with strong ambient light.
        :return: Proximity in milimetres (0-1300)
        """
        return self.read_raw_mode(LaserSensor.MODE_SHORT)[0]
    
    def set_led_distance(self, distance: int):
        """
        Set the distance at which the laser sensor LED turns on.
        :param distance: Distance in milimetres (0-4000)
        """
        self.write_command(distance.to_bytes(2, 'little'))

