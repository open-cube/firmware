from micropython import const

from lib.OC import EV3UartSensor

SONIC_SENSOR_ID = const(194)


class UltrasonicSensor(EV3UartSensor):
    MODE_DISTANCE = const(0)

    def __init__(self, port: int):
        """
        Initialize a new OC ultrasonic sensor object at the given port.
        :param port: Port to which the sensor is attached. Use one of Port.S1~Port.S4.
        """
        super().__init__(port, SONIC_SENSOR_ID)

    def distance(self) -> int:
        """
        Measure the proximity to the nearest object in milimetres.
        :return: Proximity in milimetres (50-9999)
        """
        return self.read_raw_mode(UltrasonicSensor.MODE_DISTANCE)[0]
    
    def set_led_distance(self, distance: int):
        """
        Set the distance at which the laser sensor LED turns on.
        :param distance: Distance in milimetres (0-9999)
        """
        self.write_command(distance.to_bytes(2, 'little'))

