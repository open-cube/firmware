from micropython import const

from lib.ev3 import EV3UartSensor

GYRO_SENSOR_ID = const(195)


class GyroSensor(EV3UartSensor):
    MODE_EULER = const(0)
    MODE_EULER_MAG = const(1)
    MODE_QUATERNION = const(2)
    MODE_RAW = const(3)
    MODE_GYRO_CALIB = const(4)
    MODE_MAG_CALIB = const(5)

    EULER_CONVERSION = 10
    QUATERNION_CONVERSION = 10000
    ACC_CONVERSION = 1000

    RESET_ANGLES = "RESET-ANGLES"

    def __init__(self, port: int):
        """
        Initialize a new OC gyro sensor object at the given port.
        :param port: Port to which the sensor is attached. Use one of Port.S1~Port.S4.
        """
        super().__init__(port, GYRO_SENSOR_ID)

    def euler_angles(self) -> int:
        """
        Get Euler angles
        :return: Yaw, pitch, roll angles in deg
        """
        values = self.read_raw_mode(GyroSensor.MODE_EULER)
        return (values[0]/self.EULER_CONVERSION, values[1]/self.EULER_CONVERSION, values[2]/self.EULER_CONVERSION)
    
    def reset_angles(self):
        """
        Reset angles to zero while in EULER, EULER_MAG or QUATERNION mode
        """
        self.write_command(GyroSensor.RESET_ANGLES)

    def euler_angles_mag(self) -> int:
        """
        Get Euler angles including magnetometer data
        :return: Yaw, pitch, roll angles in deg
        """
        values = self.read_raw_mode(GyroSensor.MODE_EULER_MAG)
        return (values[0]/self.EULER_CONVERSION, values[1]/self.EULER_CONVERSION, values[2]/self.EULER_CONVERSION)
    
    def quaternions(self) -> int:
        """
        Get Quaternions
        :return: Quaternions
        """
        values = self.read_raw_mode(GyroSensor.MODE_QUATERNION)
        return (values[0]/self.QUATERNION_CONVERSION, values[1]/self.QUATERNION_CONVERSION, values[2]/self.QUATERNION_CONVERSION, values[3]/self.QUATERNION_CONVERSION)
    
    def raw(self) -> int:
        """
        Get raw values from gyro, acc and mag
        :return: Angular velocity deg/s, acceleration g, magnetic field miligauss
        """
        values = self.read_raw_mode(GyroSensor.MODE_RAW)
        out = ((values[0], values[1], values[2]), 
               (values[3]/self.ACC_CONVERSION, values[4]/self.ACC_CONVERSION, values[5]/self.ACC_CONVERSION), 
               (values[6], values[7], values[8]))
        return out
    
    def calibrate_gyro(self) -> int:
        """
        Calibrate gyroscope offset, don't move the sensor during calibration
        :return: 1 when calibration complete
        """
        return self.read_raw_mode(GyroSensor.MODE_GYRO_CALIB)[0]

    def calibrate_mag(self) -> int:
        """
        Calibrate magnetometer hard iron offset, move the sensor in a figure 8 pattern during calibration
        :return: 1 when calibration complete
        """
        return self.read_raw_mode(GyroSensor.MODE_MAG_CALIB)[0]

