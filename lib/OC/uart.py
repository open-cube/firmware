import ev3_sensor_ll


class EV3UartSensor(ev3_sensor_ll.EV3Sensor):
    def __init__(self, port: int, proper_id: int | None = None):
        """
        Initialize a new generic EV3 sensor object at the given port.
        :param port: Port to which the sensor is attached. Use one of Port.S1~Port.S4.
        :param proper_id: If set, check that the connected EV3 sensor is using the given ID.
        """
        super().__init__(port, proper_id)
