'''
Constants for easier acces to the cube hardware components, sensors and motors
'''

from micropython import const

from lib.hw_defs.pins import ICM_IRQ_PIN
from brick_ll import FW_DATE as FW_VERSION
from brick_ll import FW_COMMIT

class Sensor:
    NO_SENSOR: Sensor = const(0)
    NXT: Sensor = const(1)
    NXT_LIGHT: Sensor = const(2)
    NXT_ULTRASONIC: Sensor = const(3)
    NXT_TOUCH: Sensor = const(4)
    NXT_SOUND: Sensor = const(5)
    GYRO_ACC: Sensor = const(6)
    EV3: Sensor = const(7)
    EV3_COLOR: Sensor = const(8)
    EV3_GYRO: Sensor = const(9)
    EV3_INFRARED: Sensor = const(10)
    EV3_TOUCH: Sensor = const(11)
    EV3_ULTRASONIC: Sensor = const(12)
    OC_LASER: Sensor = const(13)
    OC_COLOR: Sensor = const(14)
    OC_ULTRASONIC: Sensor = const(15)
    OC_GYRO: Sensor = const(16)
    OC_TOUCH: Sensor = const(17)

class Port:
    M1: Port = const(0)
    M2: Port = const(1)
    M3: Port = const(2)
    M4: Port = const(3)
    M_COUNT: Port = const(4)

    S1: Port = const(0)
    S2: Port = const(1)
    S3: Port = const(2)
    S4: Port = const(3)
    S_COUNT: Port = const(4)

class Button:
    POWER: Button = const(0)
    LEFT: Button = const(1)
    RIGHT: Button = const(2)
    OK: Button = const(3)
    UP: Button = const(4)
    DOWN: Button = const(5)

class Light:
    OFF: Light = const(0)
    ON: Light = const(1)

class GyroAcc:
    AX: GyroAcc = const(0)
    AY: GyroAcc = const(1)
    AZ: GyroAcc = const(2)

    GX: GyroAcc = const(3)
    GY: GyroAcc = const(4)
    GZ: GyroAcc = const(5)

    IRQ_PIN: GyroAcc = const(ICM_IRQ_PIN)

class Color:
    NO_COLOR: Color = const(0)
    BLACK: Color = const(1)
    BLUE: Color = const(2)
    GREEN: Color = const(3)
    YELLOW: Color = const(4)
    RED: Color = const(5)
    WHITE: Color = const(6)
    BROWN: Color = const(7)


BAT_MEASURE_PERIOD = const(1000)	# ms
BAT_VOLTAGE_MAX = 8.4
BAT_VOLTAGE_MIN = 6.2
BAT_VOLTAGE_TURNOFF = (BAT_VOLTAGE_MIN+0.1)			# Volts

PCF_CHECK_PERIOD = const(50)		# ms
I2C_SLAVE_READ_PERIOD = const(1)	# ms

I2C_FREQ = const(400000)			# Hz
I2C_NXT_UTZ_FREQ = const(30000)             # Hz
I2C_MULTICUBE_FREQ = const(100000)

ESP_BAUDRATE = const(115200)		# bps
ESP32_COMMAND_PIN = const(8)
