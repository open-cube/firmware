from machine import Pin, I2C, Timer
from time import sleep, ticks_ms

from lib.cube import Buzzer, I2CGuard, SH1106_I2C, Esp, I2C_master, I2C_slave
from lib import nxt, ev3, OC

from lib.hw_defs.ports import SENSOR_PORT_PINS, MOTOR_PORT_PINS
from lib.hw_defs.pins import *
from lib.robot_consts import Sensor,Button, BAT_VOLTAGE_TURNOFF, BAT_MEASURE_PERIOD, I2C_FREQ, PCF_CHECK_PERIOD, FW_VERSION

import motors_ll
import brick_ll

SEN_NUM = len(SENSOR_PORT_PINS)
MOTOR_NUM = len(MOTOR_PORT_PINS)


# Global object robot to access all hardware components
# Object is initialized in the main program (main.py) after the startup of the cube and can be used in user programs
class Robot:
    def __init__(self):
        self.start_time = ticks_ms()
        # Set pins 0, 1 to input to correctly initialize HW UART0
        Pin(0, mode=Pin.IN)
        Pin(1, mode=Pin.IN)
        self.i2c = I2C(1,sda=Pin(INTERNAL_I2C_SDA_PIN), 
            scl=Pin(INTERNAL_I2C_SCK_PIN), freq=I2C_FREQ)
        self.i2c_lock = I2CGuard()
        self.mutex = None
        self.sensors = Sensors()
        self.motors = [EmptyObject() for i in range(len(MOTOR_PORT_PINS))]

        self.i2c_master = EmptyObject()
        self.i2c_slave = EmptyObject()

        # Initialize brick components
        self.battery = brick_ll.Battery()
        self.buzzer = Buzzer()
        self.led = brick_ll.Led()
        self.display = SH1106_I2C(128, 64, self.i2c, self.i2c_lock, rotate=180)
        self.buttons = brick_ll.Buttons()
        self.buttons.pressed_since()
        self.esp = Esp(self.buttons)

        # Ensure pin that turns off the cube is low
        self.turn_off_pin = Pin(TURN_OFF_PIN, Pin.OUT)
        self.turn_off_pin.value(0)

        # Initialize periodic cube buttons state read on PCF 
        self.cube_startup = True
        self.pcf_timer = Timer(-1)
        self.pcf_timer.init(mode=Timer.PERIODIC, period=PCF_CHECK_PERIOD,
                            callback=self.read_buttons)
        
        # Timer is used to check if the batteries are charged enough, turn off cube if not 
        self.battery_timer = Timer(-1)
        self.battery_timer.init(mode=Timer.PERIODIC, period=BAT_MEASURE_PERIOD,
                                callback=self.check_battery_low)
    
    
    # Read cube buttons state on timer interrupt ant turn off cube if power button pressed
    def read_buttons(self, p):
        self.buttons.read_value()
        pressed = self.buttons.pressed()
        if self.cube_startup:
            if not pressed[Button.POWER]:
                self.cube_startup = False
        elif pressed[Button.POWER]:
            self.display.fill(0)
            self.display.centered_text('Turning off', 30, 1)
            self.display.show()
            sleep(0.3)
            self.turn_off_pin.value(1)
            sleep(0.096)
            self.led.on()
            self.turn_off_pin.value(0)
            sleep(1)
            while True:
                self.turn_off_pin.value(1)
                
    def fw_version(self):
        return FW_VERSION
    
    """ Initialize and deinitialize motors and sensors """
    def init_motor(self, port=None):
            if (port is not None and port >=0 and port < MOTOR_NUM and
                isinstance(self.motors[port], EmptyObject)):
                self.motors[port] = motors_ll.Motor(port)

    def deinit_motor(self, port=None):
        if (port is not None and port >=0 and port < MOTOR_NUM and
            not isinstance(self.motors[port], EmptyObject)):
            self.motors[port].deinit_regulator()
            self.motors[port].deinit_encoder()
            self.motors[port].deinit()
            self.motors[port] = EmptyObject()

    def deinit_motors(self):
        for i in range(len(MOTOR_PORT_PINS)):
            self.deinit_motor(i)

    def init_sensor(self, sensor_type=None, port=None):
        return self.sensors.init_sensor(sensor_type, port)

    def deinit_sensor(self, sensor_type=None, port=None):
        return self.sensors.deinit_sensor(sensor_type, port)

    def deinit_sensors(self):
        return self.sensors.deinit_sensors()
    
    """ Mutex functions for multicore use
        Multicore is possible but not recommended in MicroPython"""
    def set_mutex(self, mutex):
        self.mutex = mutex

    def delete_mutex(self):
        self.mutex = None

    # Deinitialize all possible interrupt requests (only ICM gyro and accel for now)
    def deinit_irq(self):
        gyro_irq_pin = Pin(ICM_IRQ_PIN)
        gyro_irq_pin.irq(handler=None)

    def init_i2c_master(self):
        if isinstance(self.i2c_master, EmptyObject):
            self.i2c_master = I2C_master(self.buttons)

    def deinit_i2c_master(self):
        if not isinstance(self.i2c_master, EmptyObject):
            self.i2c_master.deinit()
            self.i2c_master = EmptyObject()

    def init_i2c_slave(self, address):
        if isinstance(self.i2c_slave, EmptyObject):
            self.i2c_slave = I2C_slave(slaveAddress=address)
    
    def deinit_i2c_slave(self):
        if not isinstance(self.i2c_slave, EmptyObject):
            self.i2c_slave.deinit()
            self.i2c_slave = EmptyObject()

    # Deinitialize all hardware components initialized by the user program
    def deinit_all(self):
        self.led.off()
        self.buzzer.off()
        self.deinit_irq()
        self.deinit_i2c_master()
        self.deinit_i2c_slave()
        self.deinit_motors()
        self.deinit_sensors()
        self.delete_mutex()
        self.buttons.pressed_since()
        
    # Check if the battery voltage is too low,
    # if it is make buzzer sound, show info on the display and turn off the cube
    def check_battery_low(self, p):
        if self.battery.voltage() < BAT_VOLTAGE_TURNOFF:
            self.buzzer.set_freq_duty(1000, 0.05)
            self.display.fill(0)
            self.display.text('Battery low!', 15, 30, 1)
            self.display.show()
            sleep(1)
            self.buzzer.off()
            sleep(3)
            self.deinit_all()
            self.buttons.turn_off_cube()


# Helper object for storing, initializing and deinitializing sensor objects
class Sensors:
    def __init__(self):
        self.active_ports = [Sensor.NO_SENSOR for i in range(SEN_NUM)]
        self.light = [EmptyObject() for i in range(SEN_NUM)]
        self.touch = [EmptyObject() for i in range(SEN_NUM)]
        self.analog = [EmptyObject() for i in range(SEN_NUM)]
        self.sound = [EmptyObject() for i in range(SEN_NUM)]
        self.ultrasonic = [EmptyObject() for i in range(SEN_NUM)]
        self.infrared = [EmptyObject() for i in range(SEN_NUM)]
        self.gyro = [EmptyObject() for i in range(SEN_NUM)]
        self.laser = [EmptyObject() for i in range(SEN_NUM)]
        self.ultra_nxt = EmptyObject()
        self.gyro_acc = EmptyObject()

    # Initialize sensor with given type on given port
    # NXT Ultrasonic and ICM gyro and acc sensors don't need port number
    def init_sensor(self, sensor_type=None, port=None):
        if sensor_type is not None:
            if sensor_type == Sensor.NXT_ULTRASONIC:
                if isinstance(self.ultra_nxt, EmptyObject):
                    self.ultra_nxt = nxt.UltrasonicSensor(periodic = True, use_nonblocking=False)
            elif sensor_type == Sensor.GYRO_ACC:
                if isinstance(self.gyro_acc, EmptyObject):
                    self.gyro_acc = brick_ll.ICM()
            elif port >= 0 and port < SEN_NUM:
                if self.active_ports[port] == Sensor.NO_SENSOR:
                    if sensor_type == Sensor.NXT_LIGHT:
                        self.light[port] = nxt.LightSensor(port)
                    elif sensor_type ==  Sensor.NXT_TOUCH:
                        self.touch[port] = nxt.TouchSensor(port)
                    elif sensor_type ==  Sensor.NXT_SOUND:
                        self.sound[port] = nxt.SoundSensor(port)
                    elif sensor_type ==  Sensor.EV3_COLOR:
                        self.light[port] = ev3.ColorSensor(port)
                    elif sensor_type ==  Sensor.EV3_TOUCH or sensor_type == Sensor.OC_TOUCH:
                        self.touch[port] = ev3.TouchSensor(port)
                    elif sensor_type ==  Sensor.EV3_INFRARED:
                        self.infrared[port] = ev3.InfraredSensor(port)
                    elif sensor_type ==  Sensor.EV3_ULTRASONIC:
                        self.ultrasonic[port] = ev3.UltrasonicSensor(port)
                    elif sensor_type ==  Sensor.EV3_GYRO:
                        self.gyro[port] = ev3.GyroSensor(port)
                    elif sensor_type ==  Sensor.OC_COLOR:
                        self.light[port] = OC.ColorSensor(port)
                    elif sensor_type ==  Sensor.OC_LASER:
                        self.laser[port] = OC.LaserSensor(port)
                    elif sensor_type ==  Sensor.OC_ULTRASONIC:
                        self.ultrasonic[port] = OC.UltrasonicSensor(port)
                    elif sensor_type ==  Sensor.OC_GYRO:
                        self.gyro[port] = OC.GyroSensor(port)
                    else:
                        return
                    self.active_ports[port] = sensor_type

    # Deinit sensor with given type on given port
    # If port is None, deinit all sensors of given type
    # If sensor_type is None, deinit all sensors on given port
    # If both are None, deinit all sensors
    def deinit_sensor(self, sensor_type=None, port=None): 
        if port is None and sensor_type is None:
            self.deinit_sensors()
            return

        if port is not None:
            if port < 0 or port > SEN_NUM:
                return
            sensor_type = self.active_ports[port]
        
        if port is None:
            if sensor_type == Sensor.GYRO_ACC and not isinstance(self.gyro_acc, EmptyObject):
                self.gyro_acc.deinit()
                self.gyro_acc = EmptyObject()
            elif sensor_type == Sensor.NXT_ULTRASONIC and not isinstance(self.ultra_nxt, EmptyObject):
                self.ultra_nxt.deinit()
                self.ultra_nxt = EmptyObject()
            elif sensor_type != Sensor.NO_SENSOR:
                # Deinit all sensors of given type
                for port_index in range(SEN_NUM):
                    if self.active_ports[port_index] == sensor_type:
                        self.deinit_sensor(sensor_type, port_index)

        elif self.active_ports[port] != Sensor.NO_SENSOR:
            self.active_ports[port] = Sensor.NO_SENSOR
            if sensor_type ==  Sensor.NXT_LIGHT:
                self.light[port].off()
                self.light[port].deinit()
                self.light[port] = EmptyObject()
            elif sensor_type ==  Sensor.NXT_TOUCH:
                self.touch[port].deinit()
                self.touch[port] = EmptyObject()
            elif sensor_type ==  Sensor.NXT_SOUND:
                self.sound[port].deinit()
                self.sound[port] = EmptyObject()
            elif sensor_type ==  Sensor.EV3_COLOR:
                self.light[port].close()
                self.light[port] = EmptyObject()
            elif sensor_type ==  Sensor.EV3_TOUCH:
                self.touch[port] = EmptyObject()
            elif sensor_type ==  Sensor.EV3_INFRARED:
                self.infrared[port].close()
                self.infrared[port] = EmptyObject()
            elif sensor_type ==  Sensor.EV3_ULTRASONIC:
                self.ultrasonic[port].close()
                self.ultrasonic[port] = EmptyObject()
            elif sensor_type ==  Sensor.EV3_GYRO:
                self.gyro[port].close()
                self.gyro[port] = EmptyObject()
            elif sensor_type ==  Sensor.OC_COLOR:
                self.light[port].close()
                self.light[port] = EmptyObject()
            elif sensor_type ==  Sensor.OC_LASER:
                self.laser[port].close()
                self.laser[port] = EmptyObject()
            elif sensor_type ==  Sensor.OC_ULTRASONIC:
                self.ultrasonic[port].close()
                self.ultrasonic[port] = EmptyObject()
            elif sensor_type ==  Sensor.OC_GYRO:
                self.gyro[port].close()
                self.gyro[port] = EmptyObject()

    # Deinit all sensors
    def deinit_sensors(self):
        for port_index in range(SEN_NUM):
            self.deinit_sensor(port=port_index)
        self.deinit_sensor(sensor_type=Sensor.GYRO_ACC, port=None)
        self.deinit_sensor(sensor_type=Sensor.NXT_ULTRASONIC, port=None)


# Helper empty object for checking if the object is initialized
# If a sensor or a motor is not initialized, print a warning without breaking the program
class EmptyObject:
    def __getattr__(self, attr):
        print("Object not initialized.")
