import analog_sensor


ADC_MID_VALUE = 16384

# Helper object for the NXT touch sensor
class TouchSensor(analog_sensor.AnalogSensor):
    def __init__(self, port: int):
        super().__init__(port)
        
    # True if the sensor is pressed, false otherwise
    def pressed(self) -> bool:
        return self.get_value(False) < ADC_MID_VALUE