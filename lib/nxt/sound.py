import analog_sensor


# Helper object for the NXT sound sensor
class SoundSensor(analog_sensor.AnalogSensor):
    def __init__(self, port: int):
        super().__init__(port)

    # Returns the intensity of the sound 0-32768 with 0 being the loudest
    def intensity(self) -> int:
        return self.get_value(False)