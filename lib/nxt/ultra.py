from machine import Timer, I2C, Pin
import utime
import rp2

from lib.hw_defs.pins import UTZ_I2C_SCK_PIN, UTZ_I2C_SDA_PIN
from lib.robot_consts import I2C_NXT_UTZ_FREQ

NXT_UTZ_I2C_ADDR = 0x01

@rp2.asm_pio(autopush=False, autopull=False, out_shiftdir=rp2.PIO.SHIFT_RIGHT, sideset_init=rp2.PIO.OUT_HIGH, out_init=rp2.PIO.OUT_HIGH)
def I2C_ReadSensor():
    wrap_target()           # Acting as while loop
    pull()                  # Wait until there is address to write
    
    out(pindirs,1)          # Change pin directions
    out(pins,1)             # Pull SDA low as start of the transfer
    set(x, 7) [5]           # We want to write 7 bits + R/W
    nop().side(0) [3]       # Few Nops
    
    # Write address
    label("bitloop")        # Jump back here
    out(pins,1)[1]          # Write 1 bit
    nop().side(1)[3]        # Pull SCK High
    nop().side(0)           # Pull SCK Low
    jmp(x_dec, "bitloop")   # jmp to bitloop
    
    # Read ACK bit - ignore for now
    out(pins,1)[1]          # Pull SDA High
    nop().side(1)[3]        # Pull SCK High
    nop().side(0)           # Pull SCK Low
    irq(block, rel(0))      # After receiving all bits put IRQ
    wrap()
    
    
@rp2.asm_pio(autopush=False, autopull=False, in_shiftdir=rp2.PIO.SHIFT_LEFT, out_shiftdir=rp2.PIO.SHIFT_RIGHT, sideset_init=rp2.PIO.OUT_HIGH, out_init=rp2.PIO.OUT_HIGH)
def I2C_ReadSensor2():
    wrap_target()
    set(x, 7)
    pull()
    
    out(pindirs,1)      # Change directions of pins 0 means input, 1 means output
    label("loop")       # Jump back here
    out(pins,1)[1]      # Write 1 bit
    nop().side(1)[1]    # Pull SCK High
    in_(pins,1)[1]      # Read 1 bit  
    nop().side(0)       # Pull SCK Low
    jmp(x_dec, "loop")  # jmp to bitloop
    
    #Read ACK bit - ignore for now
    out(pins,1)[1]
    nop().side(1)[3]    # Pull SCK High
    nop().side(0)[3]    # Pull SCK Low
    
    # 1 additional clock pulse needed for the communication to work
    nop().side(1)[3]    # Pull SCK High
    nop().side(0)[3]    # Pull SCK Low
    
    push()[2]           # Push the outcome
    nop().side(1)[2]
    irq(block, rel(0))  # After receiving all bits put IRQ
    wrap()


# Object for reading measured data from the NXT ultrasonic sensor
class UltrasonicSensor:
    def __init__(self, only_first_target = True, periodic = True, use_nonblocking = True, freq = 10):
        # Define if one or eight values should be read from the sensor
        if(use_nonblocking == True):
            only_first_target = True
        self.oft = only_first_target
        # Define if data should be read periodicaly or only when data is needed
        self.periodic = periodic
        self.use_nonblocking = use_nonblocking
        self.clock_pulse = int(1/I2C_NXT_UTZ_FREQ*2*1000000)

        # Inicialization of I2C, recommended baud rate is 9600, capable of 30000
        if(self.use_nonblocking == True):
            SDA = Pin(UTZ_I2C_SDA_PIN,mode=Pin.OPEN_DRAIN, pull=Pin.PULL_UP)
            SCL = Pin(UTZ_I2C_SCK_PIN,mode=Pin.OPEN_DRAIN, pull=Pin.PULL_UP)
            self.sm0 = rp2.StateMachine(0, I2C_ReadSensor, out_base=SDA, sideset_base=SCL, freq=I2C_NXT_UTZ_FREQ*8)
            self.sm1 = rp2.StateMachine(1, I2C_ReadSensor2, in_base=SDA, out_base=SDA, sideset_base=SCL, freq=I2C_NXT_UTZ_FREQ*8)
            
            self.sm1.irq(self.handler2)
            self.sm1.active(1)
            self.sm0.irq(self.handler)
            self.sm0.active(1)
        else:    
            self.i2c = I2C(id=0,scl=Pin(UTZ_I2C_SCK_PIN), sda=Pin(UTZ_I2C_SDA_PIN), freq=(I2C_NXT_UTZ_FREQ))
        # Initialize return buffer    
        if only_first_target == True:
            self.read_buffer = [255]
        else:
            self.read_buffer = [255,255,255,255,255,255,255,255]
        
        # If periodic is on, create timer to call callback every 100ms
        if(periodic == True):
            self.timer = Timer()
            self.timer.init(period=100, mode=Timer.PERIODIC, callback=self.callback)
            self.read_data_from_sensor()
        
    def deinit(self):
        if(self.use_nonblocking == True):
            self.sm0.irq(handler=None)
            self.sm0.active(0)
            self.sm1.irq(handler=None)
            self.sm1.active(0)
        if self.periodic == True:
            self.timer.deinit()

    def read_data_from_sensor(self):
        try:
            # Write byte 0x42 to read data from sensor
            buff = bytearray([0x42])
            self.i2c.writeto(NXT_UTZ_I2C_ADDR, buff)
            # Because HW glitch in ultrasonic we need another clock pulse
            self.help_pin = Pin(17, mode=Pin.OPEN_DRAIN, value=1)
            self.help_pin.toggle()
            utime.sleep_us(self.clock_pulse)
            self.help_pin.toggle()
            
            # Reinitialize I2C
            self.i2c = I2C(id=0,scl=Pin(UTZ_I2C_SCK_PIN), sda=Pin(UTZ_I2C_SDA_PIN), freq=(I2C_NXT_UTZ_FREQ))
            
            # Read 8 targets
            if(self.oft == False):
                x = self.i2c.readfrom(NXT_UTZ_I2C_ADDR, 8)
                self.read_buffer = list(x)
            # Read first target
            else:
                x = self.i2c.readfrom(NXT_UTZ_I2C_ADDR, 1)
                self.read_buffer = list(x)
            
                
        # If I2c throws an error use except
        except Exception as e:
            print(e)
            print("Error during I2C communication with ultrasonic sensor")
            
    # Return measured distance
    def distance(self):
        # If periodic mode is set just return the read value
        # If not, read data from sensor and then return it
        if(not self.periodic):
            if(self.use_nonblocking == True):
                self.sm0.put(0x501)
            else:
                self.read_data_from_sensor()
        if self.oft:
            return self.read_buffer[0]
        else:
            return self.read_buffer
    
    # Callback for periodic reading
    def callback(self, timer):
        if(self.use_nonblocking == True):
            # Sends address in format bitwise: [ack (always 1), R/W, addr0, aadr1 ... addr6, pull sda (always 0), pin dirs (always 1)]
            # Please note address is in reversed bit order !
            self.sm0.put(0x501)   
        else:
            self.read_data_from_sensor()

    def handler(self,sm):        
        # Sends or reads data in following format: [ack (always 1), d0,d1 ... d8, pin dirs (0 for in, 1 for out)]
        self.sm1.put(0x285)
    
    def handler2(self,sm):
        self.sm0.irq(self.handler3)
        # Sends address in format bitwise: [ack (always 1), R/W, addr0, aadr1 ... addr6, pull sda (always 0), pinirs (always 0)]
        # Please note address is in reversed bit order !
        self.sm0.put(0x701)       
        
    def handler3(self,sm):
        self.sm0.irq(self.handler)
        self.sm1.irq(self.handler4)
        # Sends or reads data in following format: [ack (always 1), d0,d1 ... d8, pin dirs (0 for in, 1 for out)]
        self.sm1.put(0x3fe)      
    
    def handler4(self,sm):
        self.sm1.irq(self.handler2)
        sm.get()                    # Data not needed but we need to clear rx fifo
        result = sm.get()           # Read proper data
        self.read_buffer = [result] # Save proper data