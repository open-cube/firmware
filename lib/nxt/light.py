import analog_sensor


# Helper python object for the NXT light sensor
class LightSensor(analog_sensor.AnalogSensor):
    def __init__(self, port: int):
        super().__init__(port)

    # Returns the intensity of light 0-32768 
    # with 0 is being the brightest and 32768 is being the darkest
    # pin_on determines if the returned value is for LED on (true) or LED off (false)
    def intensity(self, pin_on=True) -> int:
        return self.get_value(pin_on)