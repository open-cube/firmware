const char webpageCode[] PROGMEM =
R"=====(
<!DOCTYPE HTML>
<html>
<head>
  <title>Open-Cube Web Server</title>
</head>
<!-------------------------------C S S------------------------------>
<style>
  .grid-container{
    width: 100%;
    display: grid;
    grid-template-columns: auto auto auto;
    grid-auto-rows: 200px;
    padding: 20px;
  }
  .grid-container-ind{
    width: 100%;
    display: grid;
    grid-template-columns: repeat(5,1fr);
    padding-top: 20px;
  }
  .grid-container-num{
    width: 100%;
    display: grid;
    grid-template-columns: repeat(4,1fr);
    padding: 20px;
  }
  .grid-item{
    display: grid;
    place-items: center;
    padding: 20px;
  }
    .grid-item-num{
    display: grid;
    place-items: center start;
    padding: 15px;
    font: 40px arial, sans-serif;
  }
    .grid-item-str{
    display: grid;
    place-items: center start;
    padding: 15px;
    font: 40px arial, sans-serif;
  }
  .button {
    box-shadow:inset 0px 1px 3px 0px #91b8b3;
    background-color:#768d87;
    width: 100%;
    height: 100%;
    border-radius:11px;
    border:1px solid #566963;
    display:inline-block;
    cursor:pointer;
    color:#ffffff;
    font: 60px arial, sans-serif;
    font-weight:bold;
    text-decoration:none;
    text-shadow:0px -1px 0px #2b665e;
    text-align: center;
    vertical-align: middle;
    touch-action: none;
    user-select: none;
  }
  .button:hover {
    background-color:#6c7c7c;
  }
  .button:active {
    position:relative;
    background-color:#6cfa7c;
    top:1px;
  }

  .switch
  {
    display: inline-block;
    text-decoration: none;
    background: #8FCE00;
    color: rgba(255,255,255, 0.80);
    font-weight: bold;
    font: 60px arial, sans-serif;
    width: 150px;
    height: 150px;
    line-height: 150px;
    border-radius:11px;
    border:5px solid #444444;
    text-align: center;
    vertical-align: middle;
    overflow: hidden;
    cursor:pointer;
    transition: 0.1s;
    touch-action: none;
    user-select: none;
  }

  .indicator
  {
    display: inline-block;
    text-decoration: none;
    background: #8FCE00;
    color: rgba(255,255,255, 0.80);
    font-weight: bold;
    font: 60px arial, sans-serif;
    width: 150px;
    height: 150px;
    line-height: 150px;
    border-radius: 50%;
    text-align: center;
    vertical-align: middle;
    overflow: hidden;
    transition: 0.1s;
    touch-action: none;
    user-select: none;
  }
  body {text-align:center; font-family:"Calibri"; background-color:rgba(0, 3, 8, 0.1)}
  h1   {color: rgba(10, 10, 10, 1); font-size: 50px;}
  h2   {color: rgba(10, 10, 10, 1); font-size: 30px;}
</style>
<!------------------------------H T M L----------------------------->
<body>
   <h1>Open-Cube Web Server</h1>
   <h2 id="btn_title">Buttons</h2>
   <div class="grid-container">
    <div class="grid-item">
      <button class="button" id="btn01">1</button>
    </div>
    <div class="grid-item">
      <button class="button" id="btn02">2</button>
    </div>
    <div class="grid-item">
      <button class="button" id="btn03">3</button>
    </div>

    <div class="grid-item">
      <button class="button" id="btn04">4</button>
    </div>
    <div class="grid-item">  
      <button class="button" id="btn05">5</button>
    </div>
    <div class="grid-item">  
      <button class="button" id="btn06">6</button>
    </div>

    <div class="grid-item">
      <button class="button" id="btn07">7</button>
    </div>
    <div class="grid-item">  
      <button class="button" id="btn08">8</button>
    </div>
    <div class="grid-item">  
      <button class="button" id="btn09">9</button>
    </div>
  </div>
  
  <h2 id="swt_title">Switches</h2>
  <div class="grid-container-ind">
    <div class="grid-item">
      <button class="switch" id="swt01">1</button>
    </div>
    <div class="grid-item">
      <button class="switch" id="swt02">2</button>
    </div>
    <div class="grid-item">
      <button class="switch" id="swt03">3</button>
    </div>
    <div class="grid-item">
      <button class="switch" id="swt04">4</button>
    </div>
    <div class="grid-item">
      <button class="switch" id="swt05">5</button>
    </div>
  </div>

  <h2 id="ind_title">Indicators</h2>
  <div class="grid-container-ind">
    <div class="grid-item">  
      <div  class="indicator" id="ind01">1</div>
    </div>
    <div class="grid-item">  
      <div  class="indicator" id="ind02">2</div>
    </div>
    <div class="grid-item">  
      <div  class="indicator" id="ind03">3</div>
    </div>
    <div class="grid-item">
      <div  class="indicator" id="ind04">4</div>
    </div>
    <div class="grid-item">    
      <div  class="indicator" id="ind05">5</div>
    </div>
  </div>
  <h2 id="num_title">Numbers</h2>
    <div class="grid-container-num">
        <div class="grid-item-str" id="num11">1: </div>
        <div class="grid-item-num" id="num01">0</div>
        <div class="grid-item-str" id="num12">2: </div>
        <div class="grid-item-num" id="num02">0</div>
        <div class="grid-item-str" id="num13">3: </div>
        <div class="grid-item-num" id="num03">0</div>
        <div class="grid-item-str" id="num14">4: </div>
        <div class="grid-item-num" id="num04">0</div>
        <div class="grid-item-str" id="num15">5: </div>
        <div class="grid-item-num" id="num05">0</div>
        <div class="grid-item-str" id="num16">6: </div>
        <div class="grid-item-num" id="num06">0</div>
    </div>
  
<!-----------------------------JavaScript--------------------------->
  <script>
    
    
    var switches = new Array(5).fill(false);
    for (let i = 0; i < 5; i++) {
      document.getElementById('swt0'+(i+1)).style.background = '#FF0000';
    }
    
    InitWebSocket()
    function InitWebSocket()
    {
      websock = new WebSocket('ws://'+window.location.hostname+':81/'); 
      websock.onmessage = function(evt)
      {
        JSONobj = JSON.parse(evt.data);
        if(JSONobj["ind"]) {
            for (let i = 0; i < 5; i++) {
              if(JSONobj["ind"][i] == 0)
              {
                document.getElementById("ind0"+(i+1)).style.background='#FF0000';
              }
              else if(JSONobj["ind"][i] == 1)
              {
                document.getElementById("ind0"+(i+1)).style.background='#8FCE00';
              }
            }
        }
        if(JSONobj["swt"]) {
            for (let i = 0; i < 5; i++) {
              if(JSONobj["swt"][i] == 0)
              {
                document.getElementById("swt0"+(i+1)).style.background='#FF0000';
              }
              else if(JSONobj["swt"][i] == 1)
              {
                document.getElementById("swt0"+(i+1)).style.background='#8FCE00';
              }
            }
        }
        if(JSONobj["num"]) {
            for (let i = 0; i < 6; i++) {
              document.getElementById("num0"+(i+1)).innerHTML= JSONobj["num"][i].toFixed(4);
            }
        }
        if(JSONobj["swt_l"]) {
            let swt_show = 'none';
            for (let i = 0; i < 5; i++) {
                if (JSONobj["swt_l"][i]) {
                    swt_show = '';
                    document.getElementById("swt0"+(i+1)).style.display = '';
                    document.getElementById("swt0"+(i+1)).innerHTML= JSONobj["swt_l"][i];
                } else {
                    document.getElementById("swt0"+(i+1)).style.display = 'none';
                }
            }   
            document.getElementById("swt_title").style.display = swt_show;
        }
        if(JSONobj["ind_l"]) {
            let ind_show = 'none';
            for (let i = 0; i < 5; i++) {
                if (JSONobj["ind_l"][i]) {
                    document.getElementById("ind0"+(i+1)).style.display = '';
                    document.getElementById("ind0"+(i+1)).innerHTML= JSONobj["ind_l"][i];
                } else {
                    document.getElementById("ind0"+(i+1)).style.display = 'none';
                }   
            }   
            document.getElementById("ind_title").style.display = ind_show;
        }
        if(JSONobj["btn_l"]) {
            let btn_show = 'none';
            for (let i = 0; i < 9; i++) {
                if (JSONobj["btn_l"][i]) {
                    document.getElementById("btn0"+(i+1)).style.display = '';
                    document.getElementById("btn0"+(i+1)).innerHTML= JSONobj["btn_l"][i];
                } else {
                    document.getElementById("btn0"+(i+1)).style.display = 'none';
                }   
            }   
            document.getElementById("btn_title").style.display = btn_show;
        }
        if(JSONobj["num_l"]) {
            let num_show = 'none';
            for (let i = 0; i < 6; i++) {
                if (JSONobj["num_l"][i]) {
                    document.getElementById("num1"+(i+1)).style.display = '';
                    document.getElementById("num0"+(i+1)).style.display = '';
                    document.getElementById("num1"+(i+1)).innerHTML= JSONobj["num_l"][i];
                } else {
                    document.getElementById("num1"+(i+1)).style.display = 'none';
                    document.getElementById("num0"+(i+1)).style.display = 'none';
                }   
            }   
            document.getElementById("num_title").style.display = num_show;
        }
      }
    }
    //-------------------------------------------------------------
    const btns = document.querySelectorAll('.button');
    for (const btn of btns) {
        btn.addEventListener('pointerdown', function(e) {
            e.preventDefault();
            console.log(e.target.id + ' touchstart');
            console.log(e);
            websock.send(e.target.id + "=DOWN");
        });
        btn.addEventListener('pointerup', function(e) {
            console.log(e.target.id + ' touchend');
            websock.send(e.target.id + "=CLICK");
        });
        btn.addEventListener('pointercancel', function(e) {
            console.log(e.target.id +' touchcancel');
            websock.send(e.target.id + "=CLICK");
        });
    }
    const swts = document.querySelectorAll('.switch');
    for (const swt of swts) {
        swt.addEventListener('pointerup', function(e) {
            e.preventDefault();
            console.log(e);
            console.log(e.target.id[4]);
            if(switches[e.target.id[4]-1] == true)
            {
                switches[e.target.id[4]-1] = false;
            }
            else
            {
                switches[e.target.id[4]-1] = true;
            }
            websock.send(e.target.id + "=" + switches[e.target.id[4]-1]);
        });
    }
    function button(name, action)
    {
      websock.send(name + "=" + action);
    }

    function switch_fun(name, action)
    {
      
      if(switches[name[3]-1] == true)
      {
        switches[name[3]-1] = false;
      }
      else
      {
        switches[name[3]-1] = true;
      }
      websock.send(name + "=" + switches[name[3]-1]);
    }

  </script>
</body>
</html>
)=====";