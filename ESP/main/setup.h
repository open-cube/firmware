void init_labels() {
	for (int i = 0; i < NUM_IND; i++) {
		memset(indicator_labels[i], '\0', sizeof(indicator_labels[i]));
		indicator_labels[i][0] = 49+i;
		j_ind.add(0);
		j_ind_labels.add(indicator_labels[i]);
	}
	for (int i = 0; i < BTN_COL * BTN_ROW; i++) {
		memset(button_labels[i], '\0', sizeof(button_labels[i]));
		button_labels[i][0] = 49+i;
		j_btn_labels.add(button_labels[i]);
	}
	for (int i = 0; i < NUM_SWT; i++) {
		memset(switch_labels[i], '\0', sizeof(switch_labels[i]));
		switch_labels[i][0] = 49+i;
		j_swt.add(0);
		j_swt_labels.add(switch_labels[i]);
	}	
	for (int i = 0; i < NUM_NUM; i++) {
		memset(number_labels[i], '\0', sizeof(number_labels[i]));
		number_labels[i][0] = 49+i;
		j_num.add(0);
		j_num_labels.add(number_labels[i]);
	}
}

void setup() {
	Serial.begin(115200);
	pinMode(CMD_PIN, INPUT);

	pinMode(LED_PIN, OUTPUT);
	cmd_pin = digitalRead(CMD_PIN);
	cmd_pin_prev = cmd_pin;
	init_labels();
	//SerialBT.setPin(pin);
	memset(esp_name, '\0', sizeof(esp_name));
	memcpy(esp_name, &default_name, 9);
	memset(esp_pass, '\0', sizeof(esp_pass));
	memcpy(esp_pass, &default_pass, 8);
	memset(JSONchr, '\0', sizeof(JSONchr));

	SerialBT.enableSSP();
	SerialBT.onConfirmRequest(BTConfirmRequestCallback);
	SerialBT.onAuthComplete(BTAuthCompleteCallback);
	SerialBT.begin(esp_name); //Bluetooth device name

	attachInterrupt(digitalPinToInterrupt(CMD_PIN), esp_cmd_change, CHANGE);
}