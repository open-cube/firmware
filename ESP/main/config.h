void BTConfirmRequestCallback(uint32_t numVal)
{
    confirmRequestPending = true;
    bt_pin = numVal;
}

void BTAuthCompleteCallback(boolean success)
{
    bt_success = success;
    confirmRequestPending = false;
}

void esp_cmd_change() {
    cmd_pin = digitalRead(CMD_PIN);
}

void send_bt_pin() {
    message_tx[0] = COM_BT_PIN;
    message_tx[1] = 4;
    memcpy(&message_tx[2], &bt_pin, 4);
    message_tx[6] = calculate_message_checksum(message_tx, 6);
    Serial.write(message_tx, 7);
}

void send_short_cmd(uint8_t command) {
    message_tx[0] = command;
    message_tx[1] = 0;
    message_tx[2] = calculate_message_checksum(message_tx, 2);
    Serial.write(message_tx, 3);
}

bool check_commands(uint8_t command) {
    switch (command) {
    case COM_NAME:
    case COM_PASSWORD:
    case COM_BT_PIN:
    case COM_RESET:
    case COM_ACK:
    case COM_NACK:
    case COM_BT_PAIR:
    case COM_BT_CANCEL:
    case COM_WIFI:
    case COM_BT_SUCCESS:
    case COM_BT:
        return true;
        break;
    default:
        return false;
    }

}

void config_read_msg() {
    ch = Serial.read();
    if (cmd_pin) {
        message_rx[message_idx] = ch;
        if (message_idx == 0) {
            if (check_commands(message_rx[0])) {
                message_idx++;
            }
        }
        else if (message_idx == 1) {
            message_idx++;
        }
        else if (message_idx == message_rx[1] + 2) {
            message_idx = 0;
            if (calculate_message_checksum(message_rx, message_rx[1] + 3) == 0) {
                switch (message_rx[0]) {
                case COM_NAME:
                    send_short_cmd(COM_ACK);
                    memset(esp_name, '\0', sizeof(esp_name));
                    memcpy(esp_name, &message_rx[2], message_rx[1]);
                    if (bt_on) {
                        SerialBT.begin(esp_name); //Bluetooth device name
                    }
                    else {
                        WiFi.softAP(esp_name, esp_pass);
                    }

                    break;
                case COM_PASSWORD:
                    send_short_cmd(COM_ACK);
                    memset(esp_pass, '\0', sizeof(esp_pass));
                    memcpy(esp_pass, &message_rx[2], message_rx[1]);
                    if (!bt_on) {
                        WiFi.softAP(esp_name, esp_pass);
                    }
                    break;
                case COM_RESET:
                case COM_BT:
                    send_short_cmd(COM_ACK);
                    if (bt_on) {
                        //SerialBT.end();
                    }
                    else {
                        timerAlarmDisable(My_timer);
                        timerAlarmDisable(My_timer2);
                        webSocket.close();
                        server.stop();
                        WiFi.softAPdisconnect(true);
                        bt_on = true;
                    }
                    SerialBT.begin(esp_name); 
                    break;
                case COM_ACK:
                    break;
                case COM_NACK:
                    break;
                case COM_BT_PIN:
                    if (confirmRequestPending) {
                        send_bt_pin();
                    }
                    else {
                        send_short_cmd(COM_NACK);
                    }
                    break;
                case COM_BT_PAIR:
                    if (confirmRequestPending) {
                        send_short_cmd(COM_ACK);
                        SerialBT.confirmReply(true);
                    }
                    break;
                case COM_BT_CANCEL:
                    if (confirmRequestPending) {
                        send_short_cmd(COM_ACK);
                        SerialBT.confirmReply(false);
                    }
                    break;
                case COM_BT_SUCCESS:
                    if (bt_success) {
                        send_short_cmd(COM_ACK);
                    }
                    else {
                        send_short_cmd(COM_NACK);
                    }
                    break;
                case COM_WIFI:
                    if (bt_on) {
                        send_short_cmd(COM_ACK);
                        bt_on = false;
                        SerialBT.end();
                        WiFi.softAP(esp_name, esp_pass);
                        WiFi.softAPConfig(local_ip, gateway, subnet);
                        IPAddress myIP = WiFi.softAPIP();
                        server.on("/", webpage);
                        server.begin();
                        webSocket.begin();
                        webSocket.onEvent(webSocketEvent);

                        My_timer = timerBegin(0, 80, true);
                        timerAttachInterrupt(My_timer, &onTimer, true);
                        timerAlarmWrite(My_timer, 100000, true);
                        timerAlarmEnable(My_timer);
                        /*
                        My_timer2 = timerBegin(3, 80, true);
                        timerAttachInterrupt(My_timer2, &wifi_server_broadcast, true);
                        timerAlarmWrite(My_timer2, 10000, true);
                        timerAlarmEnable(My_timer2);*/
                    }
                    else {
                        send_short_cmd(COM_NACK);
                    }
                    break;
                }
            }
            else {
                send_short_cmd(COM_NACK);
            }
        }
        else { //payload
            message_idx++;
        }
    }
}