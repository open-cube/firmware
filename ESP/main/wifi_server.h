void wifi_server_broadcast() {
    for (int i = 0; i < NUM_IND; i++) {
        if (indicators[i]) {
            j_ind[i] = 1;
        }
        else {
            j_ind[i] = 0;
        }
    }

    for (int i = 0; i < NUM_SWT; i++) {
        if (switches[i]) {
            j_swt[i] = 1;
        }
        else {
            j_swt[i] = 0;
        }
    }
    for (int i = 0; i < NUM_NUM; i++) {
            j_num[i] = numbers[i];
    }
    memset(JSONchr, '\0', sizeof(JSONchr));
    serializeJson(m_json, JSONchr, sizeof(JSONchr));
    webSocket.broadcastTXT(JSONchr);
}

void wifi_broadcast_labels() {
    for (int i = 0; i < NUM_IND; i++) {
        j_ind_labels[i] = indicator_labels[i];
    }
    for (int i = 0; i < BTN_COL * BTN_ROW; i++) {
        j_btn_labels[i] = button_labels[i];
    }
    for (int i = 0; i < NUM_SWT; i++) {
        j_swt_labels[i] = switch_labels[i];
    }
    for (int i = 0; i < NUM_NUM; i++) {
        j_num_labels[i] = number_labels[i];
    }
    memset(JSONchr, '\0', sizeof(JSONchr));
    serializeJson(m_json_labels, JSONchr, sizeof(JSONchr));
    webSocket.broadcastTXT(JSONchr);
}
void wifi_server() {
    webSocket.loop(); 
    server.handleClient();
    broadcast_counter++;
    if (broadcast_counter % 100 == 0) {
        wifi_server_broadcast();
    }
    if (broadcast_counter > 500) {
        broadcast_counter = 0;
        wifi_broadcast_labels();
    }
}

void wifi_send_buttons() {
    message_tx[0] = COM_WIFI_BUTTONS;
    message_tx[1] = 2;
    message_tx[2] = 0;
    message_tx[3] = 0;
    for (int i = 0; i < 8; i++) {
        if (buttons[i]) {
            message_tx[2] |= 1 << i;
        }
    }
    if (buttons[8]) {
        message_tx[3] |= 1;
    }
    message_tx[4] = calculate_message_checksum(message_tx, 4);
    Serial.write(message_tx, 5);
}

void wifi_send_switches() {
    message_tx[0] = COM_WIFI_SWITCHES;
    message_tx[1] = 1;
    message_tx[2] = 0;
    for (int i = 0; i < NUM_SWT; i++) {
        if (switches[i]) {
            message_tx[2] |= 1 << i;
        }
    }
    message_tx[3] = calculate_message_checksum(message_tx, 3);
    Serial.write(message_tx, 4);
}

void wifi_get_indicators() {
    for (int i = 0; i < NUM_IND; i++) {
        indicators[i] = ((message_rx[2] & 1 << i) != 0);
    }
}

void wifi_get_numbers() {
	for (int i = 0; i < NUM_NUM; i++) {
        memcpy(&numbers[i], &message_rx[2+4*i], 4);
	}
}
void wifi_label_buttons() {
    memset(button_labels[message_rx[2]], '\0', sizeof(button_labels[message_rx[2]]));
    if (message_rx[1] > 1) {
        memcpy(&button_labels[message_rx[2]][0], &message_rx[3], message_rx[1] - 1);
	}
    wifi_broadcast_labels();
}

void wifi_label_switches() {
    memset(switch_labels[message_rx[2]], '\0', sizeof(switch_labels[message_rx[2]]));
    if (message_rx[1] > 1) {
		memcpy(&switch_labels[message_rx[2]][0], &message_rx[3], message_rx[1] - 1);
    }
    wifi_broadcast_labels();
}

void wifi_label_indicators() {
    memset(indicator_labels[message_rx[2]], '\0', sizeof(indicator_labels[message_rx[2]]));
    if (message_rx[1] > 1) {
        memcpy(&indicator_labels[message_rx[2]][0], &message_rx[3], message_rx[1] - 1);
    }
    wifi_broadcast_labels();
}
void wifi_label_numbers() {
    memset(number_labels[message_rx[2]], '\0', sizeof(number_labels[message_rx[2]]));
    if (message_rx[1] > 1) {
        memcpy(&number_labels[message_rx[2]][0], &message_rx[3], message_rx[1] - 1);
    }
    wifi_broadcast_labels();
}
void IRAM_ATTR onTimer() {
    if (!cmd_pin && !bt_on) {
       wifi_send_buttons();
       wifi_send_switches();
    }
}

void wifi_read_msg() {
    ch = Serial.read();
    message_rx[message_idx] = ch;
    if (message_idx == 0) {
        message_idx++;
    }
    else if (message_idx == 1) {
        message_idx++;
    }
    else if (message_idx == message_rx[1] + 2) {
        message_idx = 0;
        if (calculate_message_checksum(message_rx, message_rx[1] + 3) == 0) {
            switch (message_rx[0]) {
                case COM_WIFI_BUTTONS:
                    break;
                case COM_WIFI_SWITCHES:
                    break;
                case COM_WIFI_INDICATORS:
                    wifi_get_indicators();
                    break;
                case COM_WIFI_NUMBERS:
                    digitalWrite(LED_PIN, HIGH);
                    wifi_get_numbers();
					break;
                case COM_WIFI_BUTTONS_STR:
                    wifi_label_buttons();
                    break;
                case COM_WIFI_SWITCHES_STR:
                    wifi_label_switches();
                    break;
                case COM_WIFI_INDICATORS_STR:
                    wifi_label_indicators();
                    break;
                case COM_WIFI_NUMBERS_STR:
                    wifi_label_numbers();
					break;
                default:
                    break;
            }
        }
        else {
        }
    }
    else { //payload
        message_idx++;
    }
}

//=======================================
//handle function: send webpage to client
//=======================================
void webpage()
{
    server.send(200, "text/html", webpageCode);
}
//=====================================================
//function process event: new data received from client
//=====================================================
void webSocketEvent(uint8_t num, WStype_t type, uint8_t* payload, size_t welength)
{
    /*
  String payloadString = (const char *)payload;
  Serial.print("payloadString= ");
  Serial.println(payloadString);
  */
    if (type == WStype_TEXT) //receive text from client
    {
        /*
        byte separator=payloadString.indexOf('=');
        String var = payloadString.substring(0,separator);
        String val = payloadString.substring(separator+1);
        */
        char* var = (char*)payload;
        char* val = (char*)&payload[6];
        //Serial.print("var= ");
        //Serial.println(var);
        //Serial.print("val= ");
        //Serial.println(val);
        //Serial.println(" ");


        if (strncmp(var, VAR_BTN, VAR_LEN) == 0) {

            int idx = atoi(&var[3]) - 1;
            if (strncmp(val, STR_DOWN, 4) == 0)
            {
                buttons[idx] = true;
            }
            else if (strncmp(val, STR_CLICK, 5) == 0)
            {
                buttons[idx] = false;
            }
        }
        else if (strncmp(var, VAR_SWT, VAR_LEN) == 0) {

            int idx = atoi(&var[3]) - 1;
            //Serial.printf("Swt %d", idx);
            if (strncmp(val, STR_TRUE, 4) == 0)
            {
                //Serial.printf("Swt %d", idx);
                switches[idx] = true;
            }
            else
            {
                switches[idx] = false;
                //Serial.println("Swt off");
            }
        }
    }
}