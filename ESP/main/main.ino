/* Main ESP32 firmware for Open-Cube
 * Allows the ESP to be used in Bluetooth mode or Wi-Fi WebServer mode configured from RPi user program
 * 
 * Default Wi-Fi ip: 192.168.4.1
 * 
 * To compile with Arduino IDE:
 * 1. In Tools>Boards>Boards Manager install esp32 by Espessif systems and choose board "ESP32 Wrover Module"
 * 2. Change Tools>Partition Scheme to "Huge APP"
*/
#include <BluetoothSerial.h>
#include <WiFi.h>
#include <WebServer.h>
#include <WiFiAP.h>

#include "src/WebSocketsServer.h"
#include "ArduinoJson/ArduinoJson.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

#include "const.h"

// config
bool startup = true;
bool cmd_mode = true;
bool bt_on = true;
int cmd_pin = 0;
int cmd_pin_prev = 0;

char default_name[] = "OPEN-CUBE";
char default_pass[] = "12345678";
uint8_t message_rx[MAX_PAYLOAD + 3];
uint8_t message_tx[MAX_PAYLOAD + 3];
char esp_name[MAX_PAYLOAD];
char esp_pass[MAX_PAYLOAD];
int message_idx = 0;
uint8_t ch;
bool led_state = false;

// bt
BluetoothSerial SerialBT;


uint32_t bt_pin = 0;
bool bt_success = false;
boolean confirmRequestPending = false;

// wifi server
IPAddress local_ip(192, 168, 4, 1);
IPAddress gateway(192, 168, 4, 1);
IPAddress subnet(255, 255, 255, 0);
WebServer server(80);
WebSocketsServer webSocket = WebSocketsServer(81);

StaticJsonDocument<1024> m_json;
JsonArray j_ind = m_json.createNestedArray("ind");
JsonArray j_swt = m_json.createNestedArray("swt");
JsonArray j_num = m_json.createNestedArray("num");

StaticJsonDocument<1024> m_json_labels;
JsonArray j_ind_labels = m_json_labels.createNestedArray("ind_l");
JsonArray j_swt_labels = m_json_labels.createNestedArray("swt_l");
JsonArray j_btn_labels = m_json_labels.createNestedArray("btn_l");
JsonArray j_num_labels = m_json_labels.createNestedArray("num_l");

String JSONtxt;
char JSONchr[1024];
bool indicators[NUM_IND];
bool buttons[BTN_COL*BTN_ROW];
bool switches[NUM_SWT];
float numbers[NUM_NUM] = { 0.0 };
char indicator_labels[NUM_IND][MAX_LABEL_LEN];
char button_labels[BTN_COL*BTN_ROW][MAX_LABEL_LEN];
char switch_labels[NUM_SWT][MAX_LABEL_LEN];
char number_labels[NUM_NUM][MAX_LABEL_LEN];
hw_timer_t* My_timer = NULL;
hw_timer_t* My_timer2 = NULL;
int broadcast_counter = 0;

uint8_t calculate_message_checksum(const uint8_t* buffer, size_t length) {
    uint8_t csum = 0xFF;
    for (size_t i = 0; i < length; i++) {
        csum ^= buffer[i];
    }
    return csum;
}

#include "html_page.h"
#include "wifi_server.h"
#include "config.h"
#include "setup.h"

void loop() {
  if (cmd_pin && cmd_pin_prev == 0) {
    message_idx = 0;
  }
  if (cmd_pin == 0 && cmd_pin_prev) {
    message_idx = 0;
  }
  cmd_pin_prev = cmd_pin;
  // CONFIG
  if (cmd_pin) { 
    if (Serial.available()) {
        config_read_msg();
    }
  } else {
      // BT RESEND MODE
    if (bt_on) {
      if (SerialBT.available()) {
        ch = SerialBT.read();
        SerialBT.write(ch);
        Serial.write(ch);
      }
      if (Serial.available()) {
        ch = Serial.read();
        SerialBT.write(ch);
      }
    // WIFI SERVER MODE
    } else {
        if (Serial.available()) {
            wifi_read_msg();
        }
        wifi_server();
        delay(1);

    }
  }

}
