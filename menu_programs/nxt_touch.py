'''
Example program for the NXT touch sensors on chosen port
The program measures touch sensor state
'''
from time import sleep

from lib.robot_consts import Button, Port, Sensor


def nxt_touch_run(robot, chosen_sensor_port):
    # Initialize sound sensors on chosen port
    robot.init_sensor(Sensor.NXT_TOUCH, chosen_sensor_port)
    while True:
        touch_pressed = robot.sensors.touch[chosen_sensor_port].pressed()
        
        robot.display.fill(0)
        robot.display.centered_text(f"NXT Touch S{chosen_sensor_port+1}", 0, 1)
        robot.display.text(f"Pressed: {touch_pressed}", 0, 16, 1)
        robot.display.text('< exit', 0, 54, 1)
        robot.display.show()
        print(f"Presed: {"True" if touch_pressed else "False"}")

        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed_since()
        if buttons[Button.LEFT]:
            break
        sleep(0.1)

    robot.deinit_all()