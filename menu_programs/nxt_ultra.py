'''
Example program for the NXT ultrasonic sensor
'''
from time import sleep

from lib.robot_consts import Button, Sensor


def nxt_ultra_run(robot):
    # Initialize the ultrasonic sensor
    robot.init_sensor(sensor_type=Sensor.NXT_ULTRASONIC)
    distance = 0

    while True:
        # Get distance measurement and print it
        distance = robot.sensors.ultra_nxt.distance()

        robot.display.fill(0)
        robot.display.centered_text("NXT Ultrasonic", 0, 1)
        robot.display.text(f'Distance: {distance}', 0, 16, 1)
        robot.display.text('< exit', 0, 54, 1)
        robot.display.show()
        
        print("Distance: ", distance)

        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed_since()
        if buttons[Button.LEFT]:
            robot.deinit_all()
            break
        sleep(0.1)