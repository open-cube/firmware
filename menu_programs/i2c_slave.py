import utime
from machine import mem32,mem8,Pin
from lib.robot_consts import Button
from lib.hw_defs.pins import UTZ_I2C_SDA_PIN, UTZ_I2C_SCK_PIN, I2C_STRONG_PULL_RPIN

ASCII_a = 97
ADDRESS = 0x44

def i2c_slave_run(robot):
    
    robot.init_i2c_slave(ADDRESS)
    
    data_received = None
    data_show = 0x5F
    data_sending = 0
    debounce = False

    robot.display.fill(0)
    robot.display.centered_text(f"I2C slave", 0, 1)
    robot.display.text('< exit', 0, 54, 1)
    robot.display.show()
    while True:
        robot.display.fill(0)
        robot.display.centered_text(f"I2C slave", 0, 1)
        robot.display.text(f"Sending: {chr(data_sending+ASCII_a)}", 0, 16, 1)
        robot.display.text(f"Received: {chr(data_show)}", 0, 26, 1)
        robot.display.text('< exit      char', 0, 54, 1)
        robot.display.draw_arrow(74, 56, robot.display.UP, 1)
        robot.display.draw_arrow(84, 61, robot.display.DOWN, 1)
        robot.display.show()
        data_received = robot.i2c_slave.read()

        if data_received:
            print(data_received)
            data_show = data_received
            robot.i2c_slave.write(data_sending + ASCII_a)
            
        
        buttons = robot.buttons.pressed_since()
        if buttons[Button.LEFT]:
            break
        if not debounce:
            if buttons[Button.UP]:
                data_sending -= 1
                data_sending = data_sending % 26
                debounce = True
            if buttons[Button.DOWN]:
                data_sending += 1
                data_sending = data_sending % 26
                debounce = True
        
        if not buttons[Button.UP] and not buttons[Button.DOWN]:
            debounce = False
        utime.sleep(0.1)