'''
Example program for the Open-Cube Laser sensor o port 1.
Measure and show distance on display.
Pressing down button changes sensor mode. (Normal, Large FOV, Short distance)
'''
import utime

from lib.robot_consts import Port, Button, Sensor
from ev3_sensor_ll import SensorNotReadyError, SensorMismatchError
from .display_exception import display_incorrect_sensor, display_sensor_not_connected

mode_str = (("Normal"), ("Large FOV"), ("Short dist"))
N_MODES = len(mode_str)
LED_DIST = 200

def oc_laser_run(robot, chosen_sensor_port):
    robot.init_sensor(sensor_type=Sensor.OC_LASER, port=chosen_sensor_port)

    robot.display.fill(0)
    robot.display.centered_text(f"OC Laser S{chosen_sensor_port+1}", 0, 1)
    robot.display.centered_text("Connecting...", 16, 1)
    robot.display.show()

    try:
        robot.sensors.laser[chosen_sensor_port].distance()
    except SensorNotReadyError as e:
        print(f"OC Laser S{chosen_sensor_port+1} sensor not connected")
        display_sensor_not_connected(robot)
        return
    except SensorMismatchError as e:
        print(f"OC Laser S{chosen_sensor_port+1} incorrect sensor connected")
        display_incorrect_sensor(robot)
        
    
    # At this distance (and closer) sensor green LED lights up (200 mm)
    robot.sensors.laser[chosen_sensor_port].set_led_distance(LED_DIST)
    mode = 0
    
    debounce = False
    while True:
        try:
            if mode == 0:
                dist = robot.sensors.laser[chosen_sensor_port].distance()
            if mode == 1:
                dist = robot.sensors.laser[chosen_sensor_port].distance_fov()
            if mode == 2:
                dist = robot.sensors.laser[chosen_sensor_port].distance_short()
        except:
            print(f"OC Laser S{chosen_sensor_port+1} sensor not connected")
            display_sensor_not_connected(robot)
            return
       
        robot.display.fill(0)
        robot.display.centered_text(f"OC Laser S{chosen_sensor_port+1}", 0, 1)
        robot.display.text(mode_str[mode], 0, 16, 1)
        robot.display.text(f"LED: {LED_DIST} mm", 0, 26, 1)
        robot.display.text(f"Dist: {dist:4d} mm", 0, 36, 1)
        robot.display.text('< exit      mode', 0, 54, 1)
        robot.display.draw_arrow(74, 56, robot.display.UP, 1)
        robot.display.draw_arrow(84, 61, robot.display.DOWN, 1)
        robot.display.show()
        
        pressed = robot.buttons.pressed_since()
        # Exit program if left cube button is pressed
        if pressed[Button.LEFT]:
            break
        if not debounce:
            if pressed[Button.DOWN]:
                mode += 1
                mode %= N_MODES
                debounce = True
                print("Mode:", mode)
            if pressed[Button.UP]:
                mode -= 1
                mode %= N_MODES
                debounce = True
                print("Mode:", mode)
            
        if not pressed[Button.DOWN] and not pressed[Button.UP]:
            debounce = False
        utime.sleep(0.1)