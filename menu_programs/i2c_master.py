import utime
from machine import I2C, Pin

from lib.robot_consts import Button
from lib.hw_defs.pins import UTZ_I2C_SDA_PIN, UTZ_I2C_SCK_PIN, I2C_STRONG_PULL_RPIN_BIT

ASCII_a = 97
ADDRESS = 0x44

def i2c_master_run(robot):
    robot.init_i2c_master()
    data_received_parsed = 0x5F
    data_received = None
    data_sending = 0
    debounce = False
    connected = False
    robot.display.fill(0)
    robot.display.centered_text(f"I2C master", 0, 1)
    robot.display.text('< exit', 0, 54, 1)
    robot.display.show()

    utime.sleep(0.2)
    while ADDRESS not in robot.i2c_master.i2c.scan():
        robot.display.centered_text(f"I2C master", 0, 1)
        robot.display.centered_text(f"Slave", 18, 1)
        robot.display.centered_text(f"not found", 28, 1)
        robot.display.text('< exit', 0, 54, 1)
        robot.display.show()
        buttons = robot.buttons.pressed_since()
        if buttons[Button.LEFT]:
            return

    while True:
        robot.display.fill(0)
        robot.display.centered_text(f"I2C master", 0, 1)
        robot.display.text(f"Sending: {chr(data_sending+ASCII_a)}", 0, 16, 1)
        robot.display.text(f"Received: {chr(data_received_parsed)}", 0, 26, 1)
        robot.display.text('< exit      char', 0, 54, 1)
        robot.display.draw_arrow(74, 56, robot.display.UP, 1)
        robot.display.draw_arrow(84, 61, robot.display.DOWN, 1)
        robot.display.show()

        robot.i2c_master.write(ADDRESS, (data_sending + ASCII_a).to_bytes(1, 'big'))
        data_received = robot.i2c_master.read(ADDRESS, 1)
        if data_received and data_received != b'\x00':
           data_received_parsed = int.from_bytes(data_received, 'big')

        buttons = robot.buttons.pressed_since()
        if buttons[Button.LEFT]:
            break
        if not debounce:
            if buttons[Button.UP]:
                robot.buttons.set_pin(I2C_STRONG_PULL_RPIN_BIT, True)
                data_sending -= 1
                data_sending = data_sending % 26
                debounce = True
            if buttons[Button.DOWN]:
                robot.buttons.set_pin(I2C_STRONG_PULL_RPIN_BIT, False)
                data_sending += 1
                data_sending = data_sending % 26
                debounce = True
        
        if not buttons[Button.UP] and not buttons[Button.DOWN]:
            debounce = False
        utime.sleep(0.1)