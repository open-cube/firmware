'''
Example program for the Open-Cube Gyro sensor on chosen port.
Pressing down button changes sensor mode. (Euler angles, Quaternions, Raw values, Calibration)
'''
import sys
import utime

from lib.robot_consts import Button, Sensor
from ev3_sensor_ll import SensorNotReadyError, SensorMismatchError
from .display_exception import display_incorrect_sensor, display_sensor_not_connected

mode_str = (("Euler angles"), ("Quaternions"), ("Raw values"), ("Gyro Calib"), ("Mag Calib"))
N_MODES = len(mode_str)

def oc_gyro_run(robot, chosen_sensor_port):
    robot.init_sensor(Sensor.OC_GYRO, chosen_sensor_port)

    robot.display.fill(0)
    robot.display.centered_text(f"OC AHRS S{chosen_sensor_port+1}", 0, 1)
    robot.display.centered_text("Connecting...", 16, 1)
    robot.display.show()

    try:
        robot.sensors.gyro[chosen_sensor_port].euler_angles()
    except SensorNotReadyError as e:
        #print("Error:", e)
        #print(e.args)
        #print(type(e))
        print(f"OC AHRS S{chosen_sensor_port+1} sensor not connected")
        display_sensor_not_connected(robot)
        return
    except SensorMismatchError as e:
        print(f"OC AHRS S{chosen_sensor_port+1} incorrect sensor connected")
        display_incorrect_sensor(robot)
        return
        

    mode = 0
    debounce = False
    while True:
        robot.display.fill(0)
        robot.display.centered_text(f"OC AHRS S{chosen_sensor_port+1}", 0, 1)
        robot.display.text(mode_str[mode], 0, 16, 1)

        try:
            if mode == 0:
                euler = robot.sensors.gyro[chosen_sensor_port].euler_angles()
                robot.display.text(f"yaw:   {euler[0]:.2f}", 0, 26, 1)
                robot.display.text(f"pitch: {euler[1]:.2f}", 0, 36, 1)
                robot.display.text(f"roll:  {euler[2]:.2f}", 0, 46, 1)
            if mode == 1:
                quat = robot.sensors.gyro[chosen_sensor_port].quaternions()
                robot.display.text(f"{quat[0]:.3f},{quat[1]:.3f}", 0, 26, 1)
                robot.display.text(f"{quat[2]:.3f},{quat[3]:.3f}", 0, 36, 1)        
            if mode == 2:
                raw = robot.sensors.gyro[chosen_sensor_port].raw()
                robot.display.text(f"g{raw[0][0]:.2f},{raw[0][1]:.2f},{raw[0][2]:.2f}", 0, 26, 1)
                robot.display.text(f"a{raw[1][0]:.2f},{raw[1][1]:.2f},{raw[1][2]:.2f}", 0, 36, 1)
                robot.display.text(f"m{raw[2][0]:.2f},{raw[2][1]:.2f},{raw[2][2]:.2f}", 0, 46, 1)
            if mode == 3:
                cal = robot.sensors.gyro[chosen_sensor_port].calibrate_gyro()
                robot.display.text("Calibrated: {}".format(cal), 0, 26, 1)
            if mode == 4:
                cal = robot.sensors.gyro[chosen_sensor_port].calibrate_mag()
                robot.display.text("Calibrated: {}".format(cal), 0, 26, 1)
        except:
            print(f"OC AHRS S{chosen_sensor_port+1} sensor not connected")
            display_sensor_not_connected(robot)
            return
            
        robot.display.text('< exit      mode', 0, 54, 1)
        robot.display.draw_arrow(74, 56, robot.display.UP, 1)
        robot.display.draw_arrow(84, 61, robot.display.DOWN, 1)
        robot.display.show()

        pressed = robot.buttons.pressed_since()
        # Exit program if left cube button is pressed
        if pressed[Button.LEFT]:
            break
        if not debounce:
            if pressed[Button.DOWN]:
                mode += 1
                mode %= N_MODES
                debounce = True
                print("Mode:", mode_str[mode])
            elif pressed[Button.UP]:
                mode -= 1
                mode %= N_MODES
                debounce = True
                print("Mode:", mode_str[mode])
            
        if not pressed[Button.DOWN] and not pressed[Button.UP]:
            debounce = False
        utime.sleep(0.1)
