'''
Example program for the Open-Cube Color sensor.
Measure and show light intensity on display.
Pressing down button changes sensor mode. (Reflection Red, Ambient, Color, 
Red reflection raw, RGB raw, Green reflection raw, Blue Reflection raw)
'''
import utime

from lib.robot_consts import Button, Sensor
from ev3_sensor_ll import SensorNotReadyError, SensorMismatchError
from .display_exception import display_incorrect_sensor, display_sensor_not_connected

mode_str = (("Reflection"), ("Ambient"), ("Color"), ("RGB raw"), 
            ("R Reflection raw"), ("G Reflection raw"), ("B Reflection raw"))
N_MODES = len(mode_str)

def oc_color_run(robot, chosen_sensor_port):
    robot.init_sensor(sensor_type=Sensor.OC_COLOR, port=chosen_sensor_port)

    robot.display.fill(0)
    robot.display.centered_text(f"OC Color S{chosen_sensor_port+1}", 0, 1)
    robot.display.centered_text("Connecting...", 16, 1)
    robot.display.show()
    
    try:
        robot.sensors.light[chosen_sensor_port].reflection()
    except SensorNotReadyError as e:
        print(f"OC Color S{chosen_sensor_port+1} sensor not connected")
        display_sensor_not_connected(robot)
        return
    except SensorMismatchError as e:
        print(f"OC Color S{chosen_sensor_port+1} incorrect sensor connected")
        display_incorrect_sensor(robot)
        return
    
    mode = 0
    debounce = False
    while True:
        robot.display.fill(0)
        robot.display.centered_text(f"OC Color S{chosen_sensor_port+1}", 0, 1)
        robot.display.text(mode_str[mode], 0, 8, 1)
        print("h")
        try:
            if mode == 0:
                ref = robot.sensors.light[chosen_sensor_port].reflection()
                robot.display.text("R: {:.2f}".format(ref), 0, 16, 1)
            if mode == 1:
                ambient = robot.sensors.light[chosen_sensor_port].ambient()
                robot.display.text("A: {:.2f}".format(ambient), 0, 16, 1)
            if mode == 2:
                color = robot.sensors.light[chosen_sensor_port].color()
                robot.display.text("C: {}".format(color), 0, 16, 1)
            if mode == 3:
                r, g, b, off = robot.sensors.light[chosen_sensor_port].rgb_raw()
                robot.display.text("R: {}".format(r), 0, 16, 1)
                robot.display.text("G: {}".format(g), 0, 26, 1)
                robot.display.text("B: {}".format(b), 0, 36, 1)
                robot.display.text("Off: {}".format(off), 0, 46, 1)
            if mode == 4:
                ref, off = robot.sensors.light[chosen_sensor_port].reflection_raw()
                robot.display.text("R: {:}".format(ref), 0, 16, 1)
                robot.display.text("Off: {}".format(off), 0, 26, 1)
            if mode == 5:
                ref, off = robot.sensors.light[chosen_sensor_port].reflection_raw_green()
                robot.display.text("G: {}".format(ref), 0, 16, 1)
                robot.display.text("Off: {}".format(off), 0, 26, 1)
            if mode == 6:
                ref, off = robot.sensors.light[chosen_sensor_port].reflection_raw_blue()
                robot.display.text("B: {}".format(ref), 0, 16, 1)
                robot.display.text("Off: {}".format(off), 0, 26, 1)
        except Exception as e:
            print(e)
            print(f"OC Color S{chosen_sensor_port+1} sensor not connected")
            display_sensor_not_connected(robot)
            return
        robot.display.text('< exit      mode', 0, 54, 1)
        robot.display.draw_arrow(74, 56, robot.display.UP, 1)
        robot.display.draw_arrow(84, 61, robot.display.DOWN, 1)
        robot.display.show()

        pressed = robot.buttons.pressed_since()
        # Exit program if left cube button is pressed
        if pressed[Button.LEFT]:
            break
        if not debounce:
            if pressed[Button.DOWN]:
                mode += 1
                mode %= N_MODES
                debounce = True
                print("Mode:", mode_str[mode])
            elif pressed[Button.UP]:
                mode -= 1
                mode %= N_MODES
                debounce = True
                print("Mode:", mode_str[mode])
            
        if not pressed[Button.DOWN] and not pressed[Button.UP]:
            debounce = False
        utime.sleep(0.1)
