'''
Example program for NXT or EV3 motors on a chosen port
'''
from time import sleep
import struct
import utime

from lib.robot_consts import Button, Port

MAX_MOTOR_POWER = 100
MOTOR_POWER_INC = 20

def motor_run(robot, chosen_motor_port):
    # Initialize motors with encoders
    robot.init_motor(chosen_motor_port)
    robot.motors[chosen_motor_port].init_encoder()
    pwr = 20
    debounce = False

    counter = 0
    while(True):
        counter+=1
        robot.motors[chosen_motor_port].set_power(pwr)
        # Get encoder positions and speeds and print them
        pos1 = robot.motors[chosen_motor_port].position()
        speed1 = robot.motors[chosen_motor_port].speed()
        print("Enc pos:", pos1,"Speed:", speed1)
        robot.display.fill(0)
        robot.display.centered_text(f"Motor M{chosen_motor_port+1}", 0, 1)
        robot.display.text(f"Power: {pwr}", 0, 16, 1)
        robot.display.text(f"Pos: {pos1}", 0, 26, 1)
        robot.display.text(f"Speed: {speed1}", 0, 36, 1)
        robot.display.text('< exit     power', 0, 54, 1)
        robot.display.draw_arrow(74-8, 56, robot.display.UP, 1)
        robot.display.draw_arrow(84-8, 61, robot.display.DOWN, 1)
        robot.display.show()

        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed_since()
        if buttons[Button.LEFT]:
            robot.deinit_all()
            break
        if buttons[Button.UP]:
            pwr += MOTOR_POWER_INC
            if pwr > MAX_MOTOR_POWER:
                pwr = MAX_MOTOR_POWER
        elif buttons[Button.DOWN]:
            pwr -= MOTOR_POWER_INC
            if pwr < -MAX_MOTOR_POWER:
                pwr = -MAX_MOTOR_POWER
        sleep(0.2)