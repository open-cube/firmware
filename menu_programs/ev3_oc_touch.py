'''
Example program for the EV3 and OC Touch sensor
Determine button state.
'''
import utime

from lib.robot_consts import Button, Sensor

def ev3_oc_touch_run(robot, chosen_sensor_port):
    robot.init_sensor(sensor_type=Sensor.EV3_TOUCH, port=chosen_sensor_port)
    
    robot.display.fill(0)
    robot.display.centered_text(f"EV3/OC Touch S{chosen_sensor_port+1}", 0, 1)
    robot.display.centered_text("Connecting...", 16, 1)
    robot.display.show()

    while True:
        robot.display.fill(0)
        robot.display.centered_text(f"EV3/OC Touch S{chosen_sensor_port+1}", 0, 1)
        touch_pressed = robot.sensors.touch[chosen_sensor_port].pressed()
        robot.display.text(f"Pressed: {touch_pressed}", 0, 16, 1)
        robot.display.text('< exit', 0, 54, 1)
        robot.display.show()
        
        pressed = robot.buttons.pressed_since()
        # Exit program if left cube button is pressed
        if pressed[Button.LEFT]:
            break
        utime.sleep(0.1)