'''
Example program for the NXT light sensors on chosen port
The program measures the light intensity with the LED on and off
'''
from time import sleep

from lib.robot_consts import Button, Port, Sensor


def nxt_light_run(robot, chosen_sensor_port):
    robot.init_sensor(Sensor.NXT_LIGHT, chosen_sensor_port)

    value_on, value_off = 0, 0
    counter = 0
    while True:
        counter += 1 

        # Change light sensor LED state
        if counter % 2:
            state = 'on'
            robot.sensors.light[chosen_sensor_port].on()
            value_on = robot.sensors.light[chosen_sensor_port].get_value(0)
        else:
            state = 'off'
            robot.sensors.light[chosen_sensor_port].off()
            value_off = robot.sensors.light[chosen_sensor_port].get_value(0)

        robot.display.fill(0)
        robot.display.centered_text(f"NXT Light S{chosen_sensor_port+1}", 0, 1)
        robot.display.text(f"LED state: {state}", 0, 16, 1)
        robot.display.text(f"Int on:  {value_on}", 0, 26, 1)
        robot.display.text(f"Int off: {value_off}", 0, 36, 1)
        robot.display.text('< exit', 0, 54, 1)
        robot.display.show()

        print("LED state", state, "Light intensity on:",value_on, "off:", value_off)
        
        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed_since()
        if buttons[Button.LEFT]:
            robot.deinit_all()
            break
        sleep(0.1)