# Helper file for importing menu programs
from .cube_gyro_acc import cube_gyro_acc_run
from .cube_utility import cube_utility_run
from .esp_wifi import esp_wifi_run
from .i2c_master import i2c_master_run
from .i2c_slave import i2c_slave_run
from .motor import motor_run
from .nxt_light import nxt_light_run
from .nxt_sound import nxt_sound_run
from .nxt_touch import nxt_touch_run
from .nxt_ultra import nxt_ultra_run
from .ev3_color import ev3_color_run
from .ev3_gyro import ev3_gyro_run
from .ev3_ultra import ev3_ultra_run
from .ev3_infra import ev3_infra_run
from .ev3_oc_touch import ev3_oc_touch_run
from .oc_color import oc_color_run
from .oc_gyro import oc_gyro_run
from .oc_laser import oc_laser_run
from .oc_ultra import oc_ultra_run

