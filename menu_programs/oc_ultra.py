'''
Example program for the Open-Cube Ultrasonic sensor
Measure and show distance on display.
'''
import utime

from lib.robot_consts import Button, Sensor
from ev3_sensor_ll import SensorNotReadyError, SensorMismatchError
from .display_exception import display_incorrect_sensor, display_sensor_not_connected

MAX_DIST = 2000

def oc_ultra_run(robot, chosen_sensor_port):
    robot.init_sensor(sensor_type=Sensor.OC_ULTRASONIC, port=chosen_sensor_port)
    
    led_dist = 200
    debounce = True
    
    robot.display.fill(0)
    robot.display.centered_text(f"OC Ultrasonic S{chosen_sensor_port+1}", 0, 1)
    robot.display.centered_text("Connecting...", 16, 1)
    robot.display.show()

    try:
        robot.sensors.ultrasonic[chosen_sensor_port].distance()
    except SensorNotReadyError as e:
        print(f"OC Ultrasonic S{chosen_sensor_port+1} sensor not connected")
        display_sensor_not_connected(robot)
        return
    except SensorMismatchError as e:
        print(f"OC Ultrasonic S{chosen_sensor_port+1} incorrect sensor connected")
        display_incorrect_sensor(robot)
        return
    
    # At this distance (and closer) sensor green LED lights up (200 mm)
    robot.sensors.ultrasonic[chosen_sensor_port].set_led_distance(led_dist)

    while True:
        try:
            dist = robot.sensors.ultrasonic[chosen_sensor_port].distance()
        except:
            print(f"OC Ultrasonic S{chosen_sensor_port+1} sensor not connected")
            display_sensor_not_connected(robot)
            return

        robot.display.fill(0)
        robot.display.centered_text(f"OC Ultrasonic S{chosen_sensor_port+1}", 0, 1)
        robot.display.text(f"LED: {led_dist} mm", 0, 16, 1)
        robot.display.text(f"Dist: {dist:4d} mm", 0, 26, 1)
        robot.display.text('< exit      dist', 0, 54, 1)
        robot.display.draw_arrow(74, 56, robot.display.UP, 1)
        robot.display.draw_arrow(84, 61, robot.display.DOWN, 1)
        robot.display.show()
        
        pressed = robot.buttons.pressed_since()
        if not debounce:
            if pressed[Button.DOWN]:
                led_dist += 10
                led_dist %= MAX_DIST
                robot.sensors.ultrasonic[chosen_sensor_port].set_led_distance(led_dist)
                debounce = True
            elif pressed[Button.UP]:
                led_dist -= 10
                led_dist %= MAX_DIST
                robot.sensors.ultrasonic[chosen_sensor_port].set_led_distance(led_dist)
                debounce = True
        if not pressed[Button.DOWN] and not pressed[Button.UP]:
            debounce = False
        # Exit program if left cube button is pressed
        if pressed[Button.LEFT]:
            break
        utime.sleep(0.1)