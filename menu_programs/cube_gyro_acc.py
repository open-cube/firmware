'''
Example program for showing gyroscope and accelerometer values from the ICM20608-G sensor
'''
from time import sleep

from lib.robot_consts import Button, Sensor


def cube_gyro_acc_run(robot):
    robot.init_sensor(sensor_type=Sensor.GYRO_ACC)

    while True:
        # Read and print gyroscope and accelerometer values
        gyro_acc_meas = robot.sensors.gyro_acc.read_value()
        robot.display.fill(0)
        robot.display.centered_text("Gyro Acc", 0, 1)
        robot.display.text('Gyro', 0, 16, 1)
        robot.display.text(f"{gyro_acc_meas[3]:6.2f}", 0, 24, 1)
        robot.display.text(f"{gyro_acc_meas[4]:6.2f}", 0, 32, 1)
        robot.display.text(f"{gyro_acc_meas[5]:6.2f}", 0, 40, 1)
        robot.display.text('Acc', 78, 16, 1)
        robot.display.text(f"{gyro_acc_meas[0]:5.2f}", 80, 24, 1)
        robot.display.text(f"{gyro_acc_meas[1]:5.2f}", 80, 32, 1)
        robot.display.text(f"{gyro_acc_meas[2]:5.2f}", 80, 40, 1)
        robot.display.centered_text("x", 24, 1)
        robot.display.centered_text("y", 32, 1)
        robot.display.centered_text("z", 40, 1)
        robot.display.text('< exit', 0, 54, 1)
        robot.display.show()
        print(f"Acc: {gyro_acc_meas[0]:.2f}, {gyro_acc_meas[1]:.2f}, {gyro_acc_meas[2]:.2f}, Gyro: {gyro_acc_meas[3]:.2f}, {gyro_acc_meas[4]:.2f}, {gyro_acc_meas[5]:.2f}")
        
        # Exit program if left cube button is pressed_since
        buttons = robot.buttons.pressed_since()
        if buttons[Button.LEFT]:
            robot.deinit_all()
            break
        sleep(0.1)