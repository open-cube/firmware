import utime

def display_sensor_not_connected(robot):
    robot.display.fill(0)
    robot.display.centered_text("Sensor", 16, 1)
    robot.display.centered_text("not connected", 24, 1)
    robot.display.show()
    utime.sleep(1)

def display_incorrect_sensor(robot):
    robot.display.fill(0)
    robot.display.centered_text("Incorrect", 16, 1)
    robot.display.centered_text("sensor", 24, 1)
    robot.display.show()
    utime.sleep(1)