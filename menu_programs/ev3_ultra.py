'''
Example program for the EV3 Ultrasonic sensor
Measure and show distance on display.
'''
import utime

from lib.robot_consts import Button, Sensor
from ev3_sensor_ll import SensorNotReadyError, SensorMismatchError
from .display_exception import display_incorrect_sensor, display_sensor_not_connected

mode_str = (("Distance"), ("Presence"))
N_MODES = len(mode_str)

def ev3_ultra_run(robot, chosen_sensor_port):
    robot.init_sensor(sensor_type=Sensor.EV3_ULTRASONIC, port=chosen_sensor_port)

    robot.display.fill(0)
    robot.display.centered_text(f"EV3 UTZ S{chosen_sensor_port+1}", 0, 1)
    robot.display.centered_text("Connecting...", 16, 1)
    robot.display.show()

    try:
        robot.sensors.ultrasonic[chosen_sensor_port].distance()
    except SensorNotReadyError as e:
        print(f"EV3 Ultrasonic S{chosen_sensor_port+1} sensor not connected")
        display_sensor_not_connected(robot)
        return
    except SensorMismatchError as e:
        print(f"EV3 Ultrasonic S{chosen_sensor_port+1} incorrect sensor connected")
        display_incorrect_sensor(robot)
        return

    mode = 0
    debounce = False
    while True:
        robot.display.fill(0)
        robot.display.centered_text(f"EV3 UTZ S{chosen_sensor_port+1}", 0, 1)
        robot.display.text(mode_str[mode], 0, 16, 1)
        try:
            if mode == 0:
                dist = robot.sensors.ultrasonic[chosen_sensor_port].distance()
                robot.display.text(f"Dist: {dist:4d} mm", 0, 26, 1)
            else:
                presence = robot.sensors.ultrasonic[chosen_sensor_port].presence()
                robot.display.text(f"Pres: {presence}", 0, 26, 1)
        except:
            print(f"EV3 Ultrasonic S{chosen_sensor_port+1} sensor not connected")
            display_sensor_not_connected(robot)
            return
        robot.display.text('< exit      mode', 0, 54, 1)
        robot.display.draw_arrow(74, 56, robot.display.UP, 1)
        robot.display.draw_arrow(84, 61, robot.display.DOWN, 1)
        robot.display.show()
        
        pressed = robot.buttons.pressed_since()
        # Exit program if left cube button is pressed
        if pressed[Button.LEFT]:
            break
        if not debounce:
            if pressed[Button.DOWN]:
                mode += 1
                mode %= N_MODES
                debounce = True
                print("Mode:", mode_str[mode])
            elif pressed[Button.UP]:
                mode -= 1
                mode %= N_MODES
                debounce = True
                print("Mode:", mode_str[mode])
            
        if not pressed[Button.DOWN] and not pressed[Button.UP]:
            debounce = False
        utime.sleep(0.1)