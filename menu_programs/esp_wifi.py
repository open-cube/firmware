'''
Example program for the Open-Cube ESP32 Wi-Fi mode with web server.
Start Wi-Fi acces point on the ESP32 with name 'Open-Cube-Wifi' and password '01234567'.
'''
import utime
import random

from lib.robot_consts import Button

def esp_wifi_run(robot):
    wifi_password = "12345678"
    wifi_name = robot.esp.get_name()
    counter = 0
    indicators = [True, False, True, False, False]
    labels = ["1", "test1", "test2", "", "123"]
    btn_labels = ["", "up", "", "left", "ok", "right", "", "down", "" ]
    run = True

    robot.display.fill(0)
    robot.display.centered_text(f"Wifi webserver", 0, 1)
    robot.display.text(f"Name:{wifi_name}", 0, 16, 1)
    robot.display.text(f"Pass:{wifi_password}", 0, 26, 1)
    robot.display.text('< exit', 0, 54, 1)
    robot.display.show()

    time = utime.ticks_us()
    wifi = robot.esp.wifi()
    if wifi:
        print("Wifi webserver start failed, restart cube")
        robot.display.fill(0)
        robot.display.centered_text(f"Wifi webserver", 16, 1)
        robot.display.centered_text(f"start failed", 26, 1)
        robot.display.centered_text(f"restart cube", 36, 1)
        robot.display.show()
        utime.sleep(2)
        return 
    print("Name: ", wifi_name)
    print("Pass: ", robot.esp.set_password(wifi_password))
    
    robot.esp.wifi_set_indicators_labels(("1", "", "test2", "test3", "123"))
    robot.esp.wifi_set_buttons_labels(btn_labels)
    robot.esp.wifi_set_switches_labels(labels)
    robot.esp.wifi_set_numbers_labels(("t:", "s:", "v:", "test1:", "", "test2:"))
    while run:
        utime.sleep(0.1)
        if counter % 10 == 0:
            robot.esp.wifi_set_indicators(indicators)
            robot.esp.wifi_set_numbers((123456789, 0.123456, random.random(), 0.123456, random.random(), 5.2))
            indicators = indicators[-1:]+indicators[:-1]

        wifi_buttons = robot.esp.wifi_get_buttons()
        wifi_switches = robot.esp.wifi_get_switches()
        wifi_buttons_string = ''.join(['1' if value else '0' for value in wifi_buttons])
        wifi_switches_string = ''.join(['1' if value else '0' for value in wifi_switches])

        robot.display.fill(0)
        robot.display.centered_text(f"Wifi webserver", 0, 1)
        robot.display.text(f"Name:{wifi_name}", 0, 16, 1)
        robot.display.text(f"Pass:{wifi_password}", 0, 26, 1)
        robot.display.text(f"Button:{wifi_buttons_string}", 0, 36, 1)
        robot.display.text(f"Switch:{wifi_switches_string}", 0, 46, 1)
        robot.display.text('< exit', 0, 54, 1)
        robot.display.show()
        
        print("Buttons: ", wifi_buttons)
        print("Switches: ", wifi_switches)
        
        counter +=1
        buttons = robot.buttons.pressed_since()
        if buttons[Button.LEFT]:
            run = False
            robot.esp.esp_running = False