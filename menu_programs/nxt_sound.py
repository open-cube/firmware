'''
Example program for the NXT sound sensors on chosen port
The program measures the sound intensity
'''
from time import sleep

from lib.robot_consts import Button, Port, Sensor


def nxt_sound_run(robot, chosen_sensor_port):
    # Initialize sound sensors on chosen port
    robot.init_sensor(Sensor.NXT_SOUND, chosen_sensor_port)
    while True:
        # Get sound intensity and print it
        intensity = robot.sensors.sound[chosen_sensor_port].intensity()
        
        robot.display.fill(0)
        robot.display.centered_text(f"NXT Sound S{chosen_sensor_port+1}", 0, 1)
        robot.display.text(f"Intensity: {intensity}", 0, 16, 1)
        robot.display.text('< exit', 0, 54, 1)
        robot.display.show()
        print(f"Sound level: {intensity}")

        # Exit program if left cube button is pressed
        buttons = robot.buttons.pressed_since()
        if buttons[Button.LEFT]:
            break
        sleep(0.1)

    robot.deinit_all()